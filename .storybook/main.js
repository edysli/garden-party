module.exports = {
  stories: [
    '../app/javascript/vue/common/**/*.stories.mdx',
    '../app/javascript/vue/common/**/*.stories.@(js|jsx|ts|tsx)',
  ],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    'storybook-addon-themes',
  ],
  framework: '@storybook/vue3',
  core: {
    builder: 'storybook-builder-vite',
    disableTelemetry: true,
  },
}
