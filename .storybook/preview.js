import { app } from '@storybook/vue3'
import { createI18n } from 'vue-i18n'
import '../app/javascript/entrypoints/locales/en'

const datetimeFormats = { en: window.I18n.translations.en.js_dates }

app.use(createI18n({
  locale: 'en',
  messages: window.I18n.translations,
  datetimeFormats,
  // Have $t, $n, etc... available in components
  globalInjection: true,
  legacy: false,
}))

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  backgrounds: { disable: true },
  grid: {
    disable: true,
  },
  themes: {
    default: 'browser',
    clearable: false,
    list: [
      { name: 'browser', class: ['gp-theme'] },
      { name: 'dark', class: ['gp-theme', 'gp-theme--dark'], color: '#2a2a2a' },
      { name: 'light', class: ['gp-theme', 'gp-theme--light'], color: 'white' },
    ],
  },
}
