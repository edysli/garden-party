module GardenParty
  module Activities
    module Element
      # @param element [::Element]
      def self.create_create_activity(element)
        create_implant_activity element if element.implanted_at.present?
        create_remove_activity element if element.implanted_at.present?
      end

      # @param element [::Element]
      def self.create_update_activity(element)
        return if element.discarded?

        if element.implanted_at_previously_was.blank? && element.implanted_at.present?
          create_implant_activity element
        elsif element.removed_at_previously_was.blank? && element.removed_at.present?
          create_remove_activity element
        end

        create_change_path_activity element if element.patch_id_previously_changed?
      end

      # @param element [::Element]
      def self.create_destroy_activity(element)
        Activity.create! action: :destroy, subject: element
      end

      # @param element [::Element]
      private_class_method def self.create_implant_activity(element)
        Activity.create! action: :implant, happened_at: element.implanted_at, subject: element
      end

      # @param element [::Element]
      private_class_method def self.create_remove_activity(element)
        Activity.create! action: :remove, happened_at: element.implanted_at, subject: element
      end

      # @param element [::Element]
      private_class_method def self.create_change_path_activity(element)
        Activity.create! action:      :change_path,
                         happened_at: element.updated_at,
                         subject:     element,
                         data:        {
                           from_id:   element.patch_id_previously_was,
                           from_name: ::Patch.find(element.patch_id_previously_was)&.name || I18n.t('generic.patch.name'),
                           to_id:     element.patch_id,
                           to_name:   ::Patch.find(element.patch_id)&.name || I18n.t('generic.patch.name'),
                         }
      end
    end
  end
end
