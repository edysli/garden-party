module GardenParty
  module Activities
    module Harvest
      ATTRIBUTE_CHANGES = {
        'quantity'     => :quantity_fix,
        'unit'         => :unit_fix,
        'harvested_at' => :date_fix,
      }.freeze

      # @param harvest [::Harvest]
      def self.create_create_activity(harvest)
        Activity.create! action: :create, subject: harvest
      end

      # @param harvest [::Harvest]
      def self.create_update_activity(harvest)
        return if harvest.discarded?

        harvest.saved_changes.each_key do |attribute|
          action = ATTRIBUTE_CHANGES[attribute]

          Activity.create! action: action, subject: harvest, data: { change: harvest.saved_changes[attribute] } if action
        end
      end

      # @param harvest [::Harvest]
      def self.create_destroy_activity(harvest)
        Activity.create! action: :destroy, subject: harvest
      end
    end
  end
end
