module GardenParty
  module Activities
    module Patch
      ATTRIBUTE_CHANGES = {
        'name'     => :rename,
        'geometry' => :move_or_resize,
        'layer'    => :layer_change,
      }.freeze

      # @param patch [::Patch]
      def self.create_create_activity(patch)
        Activity.create! action: :create, subject: patch unless patch.point?
      end

      # @param patch [::Patch]
      def self.create_update_activity(patch)
        return if patch.discarded?

        patch.saved_changes.each_key do |attribute|
          action = ATTRIBUTE_CHANGES[attribute]

          Activity.create! action: action, subject: patch, data: { change: patch.saved_changes[attribute] } if action
        end
      end

      # @param patch [::Patch]
      def self.create_destroy_activity(patch)
        Activity.create! action: :destroy, subject: patch
      end
    end
  end
end
