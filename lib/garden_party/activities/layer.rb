module GardenParty
  module Activities
    module Layer
      ATTRIBUTE_CHANGES = {
        'name' => :rename,
      }.freeze

      # @param layer [::Layer]
      def self.create_create_activity(layer)
        Activity.create! action: :create, subject: layer
      end

      # @param layer [::Layer]
      def self.create_update_activity(layer)
        return if layer.discarded?

        layer.saved_changes.each_key do |attribute|
          action = ATTRIBUTE_CHANGES[attribute]

          Activity.create! action: action, subject: layer, data: { change: layer.saved_changes[attribute] } if action
        end
      end

      # @param layer [::Layer]
      def self.create_destroy_activity(layer)
        Activity.create! action: :destroy, subject: layer
      end
    end
  end
end
