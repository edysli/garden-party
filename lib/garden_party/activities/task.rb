require 'garden_party/activity_helper'

module GardenParty
  module Activities
    module Task
      ATTRIBUTE_CHANGES = {
        'name'        => :rename,
        'notes'       => :complete,
        'planned_for' => :date_change,
        'assignee_id' => :reassign,
      }.freeze

      # @param task [::Task]
      def self.create_create_activity(task)
        if task.done_at.present?
          create_finish_activity task
        else
          Activity.create! action: :create, subject: task, data: {
            subject:     GardenParty::ActivityHelper.subject_name(task),
            planned_for: task.planned_for || task.done_at,
            assignee:    task.assignee.username,
          }
        end
      end

      # @param task [::Task]
      def self.create_update_activity(task)
        create_finish_activity task if task.just_done?

        task.saved_changes.each_key do |attribute|
          action = ATTRIBUTE_CHANGES[attribute]

          data = case action
                 when :reassign
                   { subject: GardenParty::ActivityHelper.subject_name(task), assignee: task.assignee.username }
                 else
                   { subject: GardenParty::ActivityHelper.subject_name(task), change: task.saved_changes[attribute] }
                 end

          Activity.create! action: action, subject: task, data: data if action
        end
      end

      # @param task [::Task]
      def self.create_destroy_activity(task)
        Activity.create! action: :destroy, subject: task, data: { subject: GardenParty::ActivityHelper.subject_name(task) }
      end

      private_class_method def self.create_finish_activity(task)
        Activity.create! action: :finish, happened_at: task.done_at, subject: task, data: { subject: GardenParty::ActivityHelper.subject_name(task) }
      end
    end
  end
end
