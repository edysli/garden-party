module GardenParty
  module Tasks
    class Element
      class << self
        TASKS_CALLBACKS = {
          update:  {
            implant: ->(task) { { implantation_planned_for: task.planned_for } },
            remove:  ->(task) { { removal_planned_for: task.planned_for } },
          },
          destroy: {
            implant: ->(_task) { { implantation_planned_for: nil } },
            remove:  ->(_task) { { removal_planned_for: nil } },
          },
          finish:  {
            implant: ->(task) { { implanted_at: task.done_at } },
            remove:  ->(task) { { removed_at: task.done_at } },
          },
        }.freeze

        # Create/update or destroy a task based on an Element changes.
        #
        # @param element [::Element]
        def update_tasks(element) # rubocop:disable Metrics/MethodLength, Metrics/CyclomaticComplexity
          action = action_type element

          case action
          when :implantation_planned
            create_implantation_task element
          when :implantation_updated
            update_implantation_task element
          when :implantation_canceled
            destroy_implantation_task element
          when :implantation_finished
            finish_implantation_task element
          when :removal_planned
            create_removal_task element
          when :removal_updated
            update_removal_task element
          when :removal_canceled
            destroy_removal_task element
          when :removal_finished
            finish_removal_task element
          end
        end

        ##
        # @param task [::Task] Task
        #
        # @return [Hash] Attributes to update
        def implied_task_changes(task)
          callback = TASKS_CALLBACKS.dig task.last_significant_change, task.action.to_sym

          callback&.call(task) || {}
        end

        private

        # Determines action type from Element state
        #
        # @param element [::Element]
        def action_type(element) # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
          return :implantation_canceled if element.implantation_planned_for_previously_was.present? && element.implantation_planned_for.blank?
          return :implantation_planned  if element.implantation_planned_for_previously_was.blank?   && element.implantation_planned_for.present?
          return :implantation_updated  if element.implantation_planned_for_previously_changed?     && element.implantation_planned_for_previously_was.present?
          return :implantation_finished if element.implanted_at_previously_changed?                 && element.implanted_at_previously_was.blank?
          return :removal_canceled      if element.removal_planned_for_previously_was.present?      && element.removal_planned_for.blank?
          return :removal_planned       if element.removal_planned_for_previously_was.blank?        && element.removal_planned_for.present?
          return :removal_updated       if element.removal_planned_for_previously_changed?          && element.removal_planned_for_previously_was.present?
          return :removal_finished      if element.removed_at_previously_changed?                   && element.removed_at_previously_was.blank?

          nil
        end

        # Creates an "implant" task for the element
        #
        # @param element [::Element]
        def create_implantation_task(element)
          Task.create! name:        I18n.t('js.elements.action_buttons.implant'),
                       subject:     element,
                       action:      'implant',
                       planned_for: element.implantation_planned_for
        end

        # Updates the first "implant" task for the element
        #
        # @param element [::Element]
        def update_implantation_task(element)
          Task.find_by(subject: element, action: 'implant')&.update_without_subject_hooks(planned_for: element.implantation_planned_for)
        end

        # Destroys the first "implant" task found for this element
        #
        # @param element [::Element]
        def destroy_implantation_task(element)
          Task.find_by(subject: element, action: 'implant')&.destroy_without_subject_hooks
        end

        # Finish an implantation task
        #
        # @param element [::Element]
        def finish_implantation_task(element)
          Task.find_by(subject: element, action: 'implant')&.update_without_subject_hooks done_at: element.implanted_at
        end

        # Creates a "remove" task for the element
        #
        # @param element [::Element]
        def create_removal_task(element)
          Task.create! name:        I18n.t('js.elements.action_buttons.remove'),
                       subject:     element,
                       action:      'remove',
                       planned_for: element.removal_planned_for
        end

        # Updates the first "remove" task for the element
        #
        # @param element [::Element]
        def update_removal_task(element)
          Task.find_by(subject: element, action: 'remove')&.update_without_subject_hooks(planned_for: element.removal_planned_for)
        end

        # Destroys the first "remove" task found for this element
        #
        # @param element [::Element]
        def destroy_removal_task(element)
          Task.find_by(subject: element, action: 'remove')&.destroy_without_subject_hooks
        end

        # Finish a removal task
        #
        # @param element [::Element]
        def finish_removal_task(element)
          Task.find_by(subject: element, action: 'remove')&.update_without_subject_hooks done_at: element.removed_at
        end
      end
    end
  end
end
