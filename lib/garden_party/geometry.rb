module GardenParty
  class Geometry
    class << self
      def rescale(geometry, scale)
        geometry['geometry']['coordinates'] = scale_coordinates geometry['geometry']['coordinates'], scale
        geometry['properties']['radius']    = geometry['properties']['radius'] * scale if geometry.dig 'properties', 'radius'

        geometry
      end

      private

      def scale_coordinates(geometry, scale)
        if geometry.is_a? Array
          geometry.map { |entry| scale_coordinates entry, scale }
        else
          geometry * scale
        end
      end
    end
  end
end
