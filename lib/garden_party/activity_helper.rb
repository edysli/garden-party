module GardenParty
  module ActivityHelper
    # Returns a name representing the subject of an activity
    # @param activity [::Activity]
    #
    # @return [String] A string to use as name
    def self.subject_name(activity) # rubocop:disable Metrics/MethodLength
      case activity.subject_type
      when 'Element'
        activity.subject.display_name
      when 'Harvest'
        activity.subject.element.display_name
      when 'Layer', 'Map', 'Path', 'Patch', 'Task'
        activity.subject.name
      when 'Observation'
        activity.subject.title
      when 'TeamMate'
        activity.subject.user.username
      else
        raise "Unsupported type #{activity.subject_type}"
      end
    end
  end
end
