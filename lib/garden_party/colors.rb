require 'digest'

module GardenParty
  class Colors
    def self.from_string(string)
      return nil unless string

      hash = Digest::MD5.hexdigest(string)[0..6]
      [hash[0..1].to_i(16), hash[2..3].to_i(16), hash[4..5].to_i(16)].join(',')
    end
  end
end
