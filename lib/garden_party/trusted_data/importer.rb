require 'garden_party/trusted_data'

module GardenParty
  module TrustedData
    class Importer
      def initialize
        data       = GardenParty::TrustedData.fetch
        @families  = data[:families]
        @genera    = data[:genera]
        @resources = data[:resources]
        @resource_interactions_groups = data[:resource_interactions_groups]
        @resource_interactions        = data[:resource_interactions]

        # Move resources with no parent at the beginning to be able to import
        # all resources in one go
        @resources.sort_by! { |resource| resource[:parent] ? 1 : 0 }
      end

      def import_all_new!
        import_new_families
        import_new_genera
        import_new_resource_interactions_groups
        import_new_resource_interactions
        import_new_resources
      end

      # Import new families, checking on the existence of `sync_id` to have a
      # quick filter.
      def import_new_families
        family_ids          = @families.pluck(:id).map(&:to_s)
        existing_family_ids = Family.where(sync_id: family_ids).pluck :sync_id
        new_family_ids      = family_ids - existing_family_ids
        @families.each do |f|
          next unless new_family_ids.include? f[:id].to_s

          self.class.import_family f
        end
      end

      # Import new genera, checking on the existence of `sync_id` to have a
      # quick filter.
      def import_new_genera
        genus_ids          = @genera.pluck(:id).map(&:to_s)
        existing_genus_ids = Genus.where(sync_id: genus_ids).pluck :sync_id
        new_genus_ids      = genus_ids - existing_genus_ids
        @genera.each do |g|
          next unless new_genus_ids.include? g[:id].to_s

          self.class.import_genus g
        end
      end

      # Import new resource interactions groups, checking on the existence of `sync_id` to have a
      # quick filter.
      def import_new_resource_interactions_groups
        group_ids          = @resource_interactions_groups.pluck(:id).map(&:to_s)
        existing_group_ids = ResourceInteractionsGroup.where(sync_id: group_ids).pluck :sync_id
        new_group_ids      = group_ids - existing_group_ids
        @resource_interactions_groups.each do |g|
          next unless new_group_ids.include? g[:id].to_s

          self.class.import_resource_interaction_group g
        end
      end

      # Import new resource interactions groups, checking on the existence of `sync_id` to have a
      # quick filter.
      def import_new_resource_interactions
        interaction_ids          = @resource_interactions.pluck(:id).map(&:to_s)
        existing_interaction_ids = ResourceInteraction.where(sync_id: interaction_ids).pluck :sync_id
        new_interaction_ids      = interaction_ids - existing_interaction_ids
        @resource_interactions.each do |g|
          next unless new_interaction_ids.include? g[:id].to_s

          self.class.import_resource_interaction g
        end
      end

      # Import new resources checking on the existence of `sync_id` to have a
      # quick filter.
      def import_new_resources
        resource_ids          = @resources.pluck(:id).map(&:to_s)
        existing_resource_ids = Resource.where(sync_id: resource_ids).pluck :sync_id
        new_resource_ids      = resource_ids - existing_resource_ids

        @resources.each do |r|
          next unless new_resource_ids.include? r[:id].to_s

          self.class.import_resource r
        end
      end

      class << self
        # Creates a family unless one with the same name/sync_id already exists
        #
        # @return [Family, nil]
        def import_family(data)
          return if Family.syncable_with(data).count.positive?

          attributes = Converter.td_to_family(data)

          Family.create! attributes
        end

        # Creates a genus unless one with the same name/sync_id already exists
        #
        # @return [Genus, nil]
        def import_genus(data)
          return if Genus.syncable_with(data).count.positive?

          attributes = Converter.td_to_genus(data)
          return unless attributes[:family]

          Genus.create! attributes
        end

        # Creates a resource unless one with the same name/sync_id already exists
        #
        # @return [Resource, nil]
        def import_resource(data)
          return if Resource.syncable_with(data).count.positive?

          attributes = Converter.td_to_resource(data)
          # Skip resources with missing data
          return unless attributes[:genus]
          return if data[:parent] && !attributes[:parent]

          Resource.create! attributes
        end

        # Creates a resource interactions group unless one with the same name/sync_id already exists
        #
        # @return [ResourceInteractionsGroup, nil]
        def import_resource_interaction_group(data)
          return if ResourceInteractionsGroup.syncable_with(data).count.positive?

          attributes = Converter.td_to_resource_interactions_group(data)

          ResourceInteractionsGroup.create! attributes
        end

        # Creates a resource interaction unless one with the same characteristics/sync_id already exists
        #
        # @return [ResourceInteraction, nil]
        def import_resource_interaction(data)
          return if ResourceInteraction.syncable_with(data).count.positive?

          attributes = Converter.td_to_resource_interaction(data)

          ResourceInteraction.create! attributes
        end
      end
    end
  end
end
