require 'garden_party/trusted_data'
require 'garden_party/trusted_data/converter'

module GardenParty
  module TrustedData
    class Differences
      def initialize(data = nil, url: nil)
        @data = data
        @data ||= TrustedData.fetch url unless data
      end

      # Lists all the trusted entries which can either be created or updated
      # @return [Hash]
      def list(force: false)
        return @list if @list && !force

        @list = {
          families:                     entities_to_sync(@data[:families], Family, :family_to_td),
          genera:                       entities_to_sync(@data[:genera], Genus, :genus_to_td),
          resources:                    entities_to_sync(@data[:resources], Resource, :resource_to_td),
          resource_interactions_groups: entities_to_sync(@data[:resource_interactions_groups], ResourceInteractionsGroup, :resource_interactions_group_to_td),
          resource_interactions:        entities_to_sync(@data[:resource_interactions], ResourceInteraction, :resource_interaction_to_td),
        }
      end

      # Lists all the entities that can be either created or updated
      #
      # @param entries [Array] List of entries to check
      # @param model_class [<ApplicationRecord>] Model used to search
      # @param converter_method [Symbol] Name of the Converter method to use for comparison
      #
      # @return [Array<Array[Hash]>] List of entries to sync or update
      #
      # The returned list is like:
      #    [
      #       [<trusted_family>, <nil>],                  # For new entries
      #       [<trusted_family>, <library_family_hash>],  # For entries with changes
      #       ...
      #    ]
      def entities_to_sync(entries, model_class, converter_method)
        list = []
        entries.each do |trusted_entry|
          library_entry = model_class.syncable_with(trusted_entry).first

          unless library_entry
            list.push trusted: trusted_entry, current: nil
            next
          end

          hash = Converter.send converter_method, library_entry
          diff = Hashdiff.diff trusted_entry, hash, strip: true

          list.push trusted: trusted_entry, current: hash.merge(library_id: library_entry.id) if diff.length.positive?
        end

        list
      end
    end
  end
end
