module GardenParty
  module SubjectHelper
    # Returns the map for a given entity's subject
    #
    # @param entity [::Activity, ::Notification, ::Observation, ::Task] Any entity with a polymorphic "subject" relation
    #
    # @return [::Map]
    def self.find_map(entity)
      case entity.subject_type
      when 'Element', 'Layer', 'Patch', 'Path', 'TeamMate'
        entity.subject.map
      when 'Map'
        entity.subject
      when 'Observation', 'Task'
        find_map entity.subject
      when 'Harvest'
        entity.subject.element.map
      else
        raise "Unsupported subject #{entity.subject_type}"
      end
    end
  end
end
