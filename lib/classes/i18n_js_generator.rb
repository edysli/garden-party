require 'i18n-js'

class I18nJsGenerator
  def self.generate_files
    output_dir = Rails.root.join('app', 'javascript', 'entrypoints', 'locales')
    # Generate translations
    I18nJS.call(config_file: Rails.root.join('config', 'i18n-js.yml'))
    # Generate JS files
    I18n.available_locales.each do |locale|
      File.write File.join(output_dir, "#{locale}.js"), content_for(locale)
    end
  end

  def self.content_for(locale)
    <<~JS
      // This file was generated by "rake i18n:generate-js". Do not edit.

      import translations from './#{locale}.json'
      import { I18n } from "i18n-js";

      // We don't allow the user to switch locale, so we only load one.
      const i18n = new I18n(translations)
      i18n.locale = '#{locale}'
      i18n.defaultLocale = '#{locale}'

      // And we set I18n to be globally available in `window` in order to be used
      // by both vanilla JS and VueJS apps.
      window.I18n = i18n
    JS
  end
end
