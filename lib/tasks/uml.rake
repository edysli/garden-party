require 'classes/puml_relations_generator'

namespace :uml do
  desc 'Outputs PlantUML diagram of tables and relations'
  task models: :environment do
    puts PumlRelationsGenerator.generate
  end
end
