require 'garden_party/trusted_data/converter'
require 'garden_party/trusted_data/importer'

def export_families
  Family.order(:name).all.map do |family|
    GardenParty::TrustedData::Converter.family_to_td family
  end
end

def export_genera
  Genus.order(:name).all.map do |genus|
    GardenParty::TrustedData::Converter.genus_to_td genus
  end
end

def export_resources
  Resource.order(:name).all.map do |resource|
    GardenParty::TrustedData::Converter.resource_to_td resource
  end
end

def export_resource_interactions_groups
  ResourceInteractionsGroup.all.map do |group|
    GardenParty::TrustedData::Converter.resource_interactions_group_to_td group
  end
end

def export_interactions
  ResourceInteraction.all.map do |interaction|
    GardenParty::TrustedData::Converter.resource_interaction_to_td interaction
  end
end

namespace :data do
  desc 'Exports database in YAML (families, genera, resources and interactions) to help complete data sources'
  task :export, [:output] => :environment do |_task, args|
    output = ENV.fetch('OUTPUT', nil) || args[:output].presence || Rails.root.join('tmp', 'instance_library.yml')
    seeds  = {
      families:                     export_families,
      genera:                       export_genera,
      resources:                    export_resources,
      resource_interactions_groups: export_resource_interactions_groups,
      resource_interactions:        export_interactions,
    }
    File.write output, seeds.to_yaml

    puts "File saved in #{output}"
  end
end

namespace :data do
  desc 'Imports new entities from trusted data'
  task import_new: :environment do
    importer = GardenParty::TrustedData::Importer.new
    importer.import_all_new!
  end
end

namespace :data do
  desc 'Normalizes families, genera and resources entities'
  task normalize_library: :environment do
    Family.normalize_fields
    Genus.normalize_fields
    Resource.normalize_fields
  end
end
