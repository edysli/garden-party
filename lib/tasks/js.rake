require 'classes/front_generator'

namespace :js do
  desc 'Generate javascript models and VueX modules'
  task generate: [:generate_models, :generate_modules, :generate_factories] do
    generator = FrontGenerator.new
    generator.generate(models, :models, :modules)
  end

  desc 'Generate javascript models'
  task generate_models: :environment do
    generator = FrontGenerator.new
    generator.generate(models, :models)
  end

  desc 'Generate javascript VueX modules'
  task generate_modules: :environment do
    generator = FrontGenerator.new
    generator.generate(models, :modules)
  end

  desc 'Generate JS factories test file'
  task generate_factories_test: :environment do
    generator = FrontGenerator.new
    generator.generate_factories_tests
  end

  desc 'Generate javascript Fishery factories'
  task generate_factories: :environment do
    generator = FrontGenerator.new
    generator.generate(models, :factories)
  end

  def models
    return @models if @models

    # Remove the task name.
    ARGV.shift

    # By default, rake considers each 'argument' to be the name of an actual task.
    # It will try to invoke each one as a task.  By dynamically defining a dummy
    # task for every argument, we can prevent an exception from being thrown
    # when rake inevitably doesn't find a defined task with that name.
    ARGV.each do |arg|
      task(arg.to_sym) {} # rubocop:disable Lint/EmptyBlock, Rails/RakeEnvironment
    end

    @models = ARGV
  end
end
