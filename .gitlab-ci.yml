---
image: registry.gitlab.com/experimentslabs/garden-party/ci-runner/garden-party-runner:latest

services:
  - postgres:latest

variables:
  POSTGRES_DB: ci_db
  POSTGRES_USER: runner
  POSTGRES_PASSWORD: password

stages:
  - prepare
  - test

cache: &cache
  untracked: true
  key:
    files:
      - Gemfile.lock
      - yarn.lock
  policy: pull
  paths:
    - 'vendor/'
    - 'node_modules/'
    - '.bundle/'

before_script:
  # Debug
  - node --version
  - ruby --version
  - bundle --version
  # Copy files
  - cp config/database.ci.yml config/database.yml
  # Use this line if there are issues with "yarn install" and the node engine not
  # being in range, while actually being in it...
  - yarn install --ignore-engines --silent
  # Install Ruby dependencies in vendors
  - bundle config set --local deployment 'true'
  - bundle config set --local jobs $(nproc)
  - bundle install --quiet

# Bundle stage
# ======================================

bundle:
  stage: prepare
  script:
    - echo "This job only fills the cache"
  cache:
    <<: *cache
    policy: push

# Code linting
# ======================================

# Rubocop and other ruby checks on files
rubocop:
  stage: test
  script:
    - bundle exec rubocop
    # Search for "@doing" tag in features
    - "! grep --quiet '@doing' features/**/*.feature"
  dependencies:
    - bundle
  except:
    - main
    - develop

sass_lint:
  stage: test
  script: yarn run lint:sass
  dependencies:
    - bundle
  except:
    - main
    - develop

es_lint:
  stage: test
  script: yarn run lint:js
  dependencies:
    - bundle
  except:
    - main
    - develop

haml_lint:
  stage: test
  script: bundle exec haml-lint app/views
  dependencies:
    - bundle
  except:
    - main
    - develop

generated_files:
  stage: test
  script:
    - bundle exec rails db:test:prepare
    - bundle exec rake i18n:add-models-attributes
    - git diff
    - git diff-index --quiet HEAD
    - bundle exec rake icons:generate
    - git diff
    - git diff-index --quiet HEAD
  dependencies:
    - bundle
  except:
    - main
    - develop

factory_bot_lint:
  stage: test
  script:
    - bundle exec rails db:test:prepare
    - bundle exec rake factory_bot:lint
  dependencies:
    - bundle
  except:
    - main
    - develop

# Tests
# ======================================

rspec:
  stage: test
  script:
    # Install dependencies
    - bundle exec rails db:test:prepare
    - bundle exec rake i18n:js:export
    - RAILS_ENV=test bundle exec rails assets:precompile
    - bundle exec rspec --fail-fast
    - bundle exec rake swagger:changed
  dependencies:
    - bundle
  artifacts:
    when: on_failure
    paths:
      - tmp/rspec_screenshots
    expire_in: 1 day
  coverage: '/\(\d+.\d+%\) covered/'
  except:
    - main
    - develop

cucumber:
  stage: test
  script:
    # Install dependencies
    - bundle exec rails db:test:prepare
    - bundle exec rake i18n:js:export
    - RAILS_ENV=test bundle exec rails assets:precompile
    - bundle exec cucumber --fail-fast
  dependencies:
    - bundle
  artifacts:
    when: on_failure
    paths:
      - tmp/cucumber_screenshots
    expire_in: 1 day
  coverage: '/\(\d+.\d+%\) covered/'
  except:
    - main
    - develop

brakeman:
  stage: test
  script:
    - bundle exec brakeman
  dependencies:
    - bundle
  except:
    - main
    - develop

jest:
  stage: test
  script:
    - bundle exec rake js:generate_factories_test
    - yarn run test --coverage
  dependencies:
    - bundle
  coverage: /All\sfiles.*?\s+(\d+.\d+)/
  except:
    - main
    - develop

# Deployment
# ======================================

# deploy:
#   stage: deployment
#   cache: {}
#   script:
#     - bundle install --deployment --without test development
#     - yarn install --production true
#     - bundle exec rails assets:precompile
#     # - YOUR DEPLOY STRATEGY
#   artifacts:
#     untracked: true
#     expire_in: 1 hour
#     paths:
#       - vendor/
#       - node_modules/
