require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module GardenParty
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0
    config.autoload_paths << Rails.root.join('app', 'models', 'notifications')

    VERSION = '0.12.0'.freeze

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    config.eager_load_paths << Rails.root.join('app', 'models', 'notifications')
    config.garden_party = config_for(:garden_party_defaults)

    # Instance configuration may not have overrides for current environment
    # so we load it first and only merge if it contains something
    instance_config = config_for(:garden_party_instance) if Rails.root.join('config', 'garden_party_instance.yml').exist?
    config.garden_party.deep_merge! instance_config if instance_config

    config.action_mailer.default_url_options = config.garden_party.default_url_options
    config.action_mailer.default_options = config.garden_party.default_email_options
  end
end
