# Garden Party migration guide

## General steps

- Pull the last version, using tags.
- Check if there are new configuration options and override them to your
  needs
- Install dependencies, as for deployment

  ```shell
  bundle install
  yarn install
  ```
- Run the migrations:

  ```shell
  RAILS_ENV=production bundle exec rails db:migrate`
  ```

In production, you have these extra steps to do:

- Export new javascript translations:

  ```shell
  rake i18n:js:export
  ```

- Precompile assets:

  ```shell
  bundle exec rails assets:precompile
  ```

- Restart the server:

  ```shell
  touch tmp/restart
  ```

Don't forget to run this with the appropriate user and the correct Rails
environment.

We use this small script to update our demo instance, feel free to
inspire yourself from it:

```shell
#!/bin/sh
# Updates the Garden Party instance.
#
# Usage:
#    gp-update <branch>
# Example:
#    gp-update origin/master
#    gp-update 0.0.14

set -xe

cd /path/to/instance

sudo -u deploy -H RAILS_ENV=production git fetch --prune
sudo -u deploy -H RAILS_ENV=production git reset --hard $1
sudo -u deploy -H yarn install --ignore-engines --check-files
sudo -u deploy -H RAILS_ENV=production bundle install
sudo -u deploy -H RAILS_ENV=production bundle exec rails db:migrate
sudo -u deploy -H RAILS_ENV=production rake i18n:js:export
sudo -u deploy -H RAILS_ENV=production bundle exec rails assets:precompile

sudo -u deploy -H touch tmp/restart.txt
```

## 0.11.x to 0.12.0

- We now use [Vips](https://github.com/libvips/ruby-vips) to process images:
  it's the default processor for Rails 7.
  Ensure you have the corresponding package installed on the server
  (`libvips42` on debian-based distributions).
- Default configuration changed: Default values are `production` values, and
  other environments override them. Update the instance configuration
  accordingly before restarting the server

## 0.10.0 to 0.11.0

- The configuration file changed (`config/garden_party_default.yml`). Update
  the instance configuration accordingly before restarting the server
  (`config/garden_party_instance.yml`).
- Follow the general migration steps

## 0.9.x to 0.10.0

- Follow the general migration steps

## 0.8.x to 0.9.0

- Update Node to Node 16.x
- Follow the general migration steps

**When deploying, don't run `bundle exec rails assets:precompile` anymore** as we
switched to Vite. Use this instead:

```sh
RAILS_ENV=production bin/vite build
```

Update your scripts accordingly.

## 0.7.0 to 0.8.0

- Follow the general migration steps
- You _may_ want to add a trusted data source and maybe contribute to the [data source
  project](https://gitlab.com/experimentslabs/garden-party/data).
  To define the datasource, fill `trusted_datasource_url` in `config/garden_party_instance.yml`

## 0.6.1 to 0.7.0

- Follow the general migration steps
- You can remove `public/pictures`

## 0.6.0 to 0.6.1

- Follow the general migration steps

## 0.5.0 to 0.6.0

- Follow the general migration steps

## 0.4.0 to 0.5.0

- Genera _must_ have family assigned to them now. If your database is not
  complete, migration will fail. You can check for missing families with
  ```rb
  Genus.where family_id: nil
  ```
- Families and genera now _must_ have unique names. Migration will fail
  unless you fixed duplicated names. Find them with
  ```rb
  # Returns duplicated names only
  Family.find_by_sql('SELECT name FROM families GROUP BY name HAVING COUNT(*) > 1').pluck :name
  Genus.find_by_sql('SELECT name FROM genera GROUP BY name HAVING COUNT(*) > 1').pluck :name
  ```
- Then, follow the general migration steps

## 0.3.0 to 0.4.0

- Follow the general migration steps

## 0.2.0 to 0.3.0

- Follow the general migration steps

## 0.1.0 to 0.2.0

- Follow the general migration steps
- At the end, run the rake task to re-analyze SVG pictures used as maps
  backgrounds:
  ```shell
  bundle exec rake maps:analyze_svg
  ```
  This will fix picture sizes and allow proper map scaling.

## 0.0.16 to 0.0.17

- Follow the general migration steps
- The layers system changed; resources now have colors. If you prefer
  resources to have distinct colors instead of the one from their old
  layer:
  ```shell
  bundle exec rake resources:reset_colors
  ```
  Or you can only re-generate pictures:
  ```shell
  bundle exec rake resources:generate_pictures
  ```
- You can safely delete `plants` and all numerical directories in
  `public/pictures/resources`

## 0.0.15 to 0.0.16

- Follow the general migration steps, everything should be fine.

## 0.0.14 to 0.0.15

- Configuration files changed; you need to rename
  `config/garden_party.yml` to `config/garden_party_instance.yml`. This
  file is now merged with `config/garden_party_defaults.yml`.
