class AddHideBackgroundToMap < ActiveRecord::Migration[6.1]
  def change
    add_column :maps, :hide_background, :boolean, default: false
  end
end
