# This migration adds a "discarded_at" field to map-related tables that needs to
# handle soft-delete
class AddDiscardedAtOnMapTables < ActiveRecord::Migration[6.1]
  def change
    add_column :elements, :discarded_at, :datetime
    add_column :layers, :discarded_at, :datetime
    add_column :observations, :discarded_at, :datetime
    add_column :patches, :discarded_at, :datetime
    add_column :paths, :discarded_at, :datetime

    add_index :elements, :discarded_at
    add_index :layers, :discarded_at
    add_index :observations, :discarded_at
    add_index :patches, :discarded_at
    add_index :paths, :discarded_at
  end
end
