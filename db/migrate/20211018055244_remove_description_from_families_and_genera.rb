class RemoveDescriptionFromFamiliesAndGenera < ActiveRecord::Migration[6.1]
  def change
    remove_column :families, :description, :text
    remove_column :genera, :description, :text
  end
end
