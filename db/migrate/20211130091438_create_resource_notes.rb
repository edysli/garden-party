class CreateResourceNotes < ActiveRecord::Migration[6.1]
  def change
    create_table :resource_notes do |t|
      t.text :content, null: false, default: nil
      t.boolean :visible, null: false, default: true, comment: 'Whether or not this content is available for the community'
      t.references :user, null: false, foreign_key: true
      t.references :resource, null: false, foreign_key: true

      t.timestamps
    end
  end
end
