# This migration makes the "assignee_id" field non-null and completes missing
# assignees with map owner
class MakeTasksAssigneesMandatory < ActiveRecord::Migration[6.1]
  # Finds a map from an element
  def element_map(id)
    select_one <<~SQL.squish, 'SELECT_MAP', [id.to_i]
      SELECT maps.* FROM maps
        INNER JOIN patches p on maps.id = p.map_id
        INNER JOIN elements e on p.id = e.patch_id
      WHERE e.id = $1
    SQL
  end

  # Finds a map for a given polymorphic subject type and id
  def subject_map_owner(type, id)
    map = case type
          when 'Map'
            select_one('SELECT * FROM maps WHERE id=$1', 'SELECT_SUBJECT', [id.to_i])
          when 'Element'
            element_map id
          when 'Patch', 'Path'
            select_one <<~SQL.squish, 'SELECT_SUBJECT', [id.to_i]
              SELECT * FROM maps m
                WHERE m.id IN (SELECT map_id FROM #{type.tableize} s WHERE s.id=$1)
            SQL
          else
            raise "Unsupported task subject '#{task['subject_type']}'"
          end

    select_one 'SELECT * FROM users WHERE id=$1', 'SELECT_USER', [map['user_id']]
  end

  def change
    reversible do |dir|
      dir.up do
        tasks = select_all 'SELECT * FROM tasks where assignee_id IS NULL'
        tasks.each do |task|
          user = subject_map_owner task['subject_type'], task['subject_id']

          update 'UPDATE tasks SET assignee_id=$1 WHERE id=$2', 'UPDATE_TASK', [user['id'].to_i, task['id'].to_i]
        end

        change_column_null :tasks, :assignee_id, false
      end

      dir.down do
        change_column_null :tasks, :assignee_id, true
      end
    end
  end
end
