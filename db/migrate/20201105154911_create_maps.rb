class CreateMaps < ActiveRecord::Migration[6.0]
  def change
    create_table :maps do |t|
      t.string :name,  null: false, default: nil
      t.point  :center, null: false, default: nil

      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
