# This migration renames table "activities" to "tasks".
#
# This is in the process of splitting the old "activities" table to
# tasks, observations and new activity system.
#
# Tasks names will be changed to a human readable version when it contains the
# an old reference to activities pre-filled actions
class RenameActivitiesToTasks < ActiveRecord::Migration[6.1]
  OLD_ACTIONS = {
    fr: {
      'fertilize' => 'Amender',
      'observe'   => 'Observer',
      'trim'      => 'Tailler',
      'water'     => 'Arroser',
    },
    en: {
      'fertilize' => 'Fertilize',
      'observe'   => 'Observe',
      'trim'      => 'Trim',
      'water'     => 'Water',
    },
  }.freeze

  def change
    reversible do |dir|
      dir.up do
        rename_table :activities, :tasks

        # Change name of the tasks using the old activities names
        tasks = select_all "SELECT * FROM tasks WHERE name IN ('fertilize', 'observe', 'trim', 'water')"
        tasks.each { |task| update 'UPDATE tasks SET name=$1 WHERE id=$2', 'UPDATE_TASK', [OLD_ACTIONS[I18n.locale][task['name']], task['id'].to_i] }
      end

      dir.down do
        reverse_actions = {}
        OLD_ACTIONS[I18n.locale].each_pair { |k, v| reverse_actions[v] = k }

        # Give old name back
        tasks = select_all "SELECT * FROM tasks WHERE name IN ('#{reverse_actions.keys.join("','")}')"
        tasks.each { |task| update 'UPDATE tasks SET name=$1 WHERE id=$2', 'UPDATE_TASK', [reverse_actions[task['name']], task['id'].to_i] }

        rename_table :tasks, :activities
      end
    end
  end
end
