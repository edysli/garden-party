class AddBackgroundColorToMaps < ActiveRecord::Migration[6.1]
  def change
    add_column :maps, :background_color, :string, null: true, default: nil
  end
end
