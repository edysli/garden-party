class AddPreferencesToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :preferences, :jsonb, default: { theme: nil, map: nil }
  end
end
