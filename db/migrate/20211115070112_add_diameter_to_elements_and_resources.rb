class AddDiameterToElementsAndResources < ActiveRecord::Migration[6.1]
  def change
    add_column :resources, :diameter, :float, default: nil
    add_column :elements, :diameter, :float, default: nil
  end
end
