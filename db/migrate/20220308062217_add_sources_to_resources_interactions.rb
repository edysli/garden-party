class AddSourcesToResourcesInteractions < ActiveRecord::Migration[6.1]
  def change
    add_column :resource_interactions, :sources, :text, null: true, default: nil
  end
end
