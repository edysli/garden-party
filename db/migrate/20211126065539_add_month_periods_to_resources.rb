class AddMonthPeriodsToResources < ActiveRecord::Migration[6.1]
  def change
    change_table :resources, bulk: true do |t|
      t.integer :sheltered_sowing_months, null: false, default: 0, comment: 'Bit field for sheltered sowing months'
      t.integer :soil_sowing_months, null: false, default: 0, comment: 'Bit field for sowing in soil months'
      t.integer :harvesting_months, null: false, default: 0, comment: 'Bit field for harvest months'
    end
  end
end
