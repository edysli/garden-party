class AddChildCountToResource < ActiveRecord::Migration[6.1]
  def change
    reversible do |dir|
      dir.up do
        add_column :resources, :child_count, :integer, null: false, default: 0

        resources = select_all 'SELECT * FROM resources WHERE parent_id IS NULL'
        resources.each do |resource|
          update 'UPDATE resources r SET child_count = (SELECT COUNT(id) FROM resources WHERE parent_id = r.id) WHERE id=$1', 'UPDATE_COUNT', [resource['id'].to_i]
        end
      end

      dir.down do
        remove_column :resources, :child_count
      end
    end
  end
end
