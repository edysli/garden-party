class MoveKingdomToFamilies < ActiveRecord::Migration[6.1]
  def change
    reversible do |dir|
      dir.up do
        add_column :families, :kingdom, :integer, default: nil

        genera = select_all 'SELECT * FROM genera'

        genera.each do |genus|
          family = select_one 'SELECT * FROM families WHERE kingdom IS NULL AND id = $1', 'FIND_FAMILY', [genus['family_id'].to_i]
          next unless family

          update 'UPDATE families SET kingdom = $1 WHERE id = $2', 'UPDATE_FAMILY', [genus['kingdom'], family['id'].to_i]
        end

        change_column_null :families, :kingdom, false
        remove_column :genera, :kingdom
      end

      dir.down do
        add_column :genera, :kingdom, :integer, default: nil

        genera = select_all 'SELECT * FROM genera'

        genera.each do |genus|
          family = select_one 'SELECT * FROM families WHERE id = $1', 'FIND_FAMILY', [genus['family_id'].to_i]
          update 'UPDATE genera SET kingdom = $1 WHERE id = $2', 'UPDATE_RESOURCE', [family['kingdom'], genus['id'].to_i]
        end

        change_column_null :genera, :kingdom, false
        remove_column :families, :kingdom
      end
    end
  end
end
