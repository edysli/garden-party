class AddRemovedAtToCrops < ActiveRecord::Migration[6.0]
  def change
    add_column :crops, :removed_at, :datetime
  end
end
