class CreateTeamMates < ActiveRecord::Migration[6.1]
  def change
    reversible do |dir|
      dir.up do
        create_table :team_mates do |t|
          t.references :user, null: false, foreign_key: true
          t.references :map, null: false, foreign_key: true

          t.datetime :accepted_at, default: nil
          t.timestamps

          t.index [:map_id, :user_id], unique: true
        end

        maps = select_all 'SELECT * FROM maps'

        maps.each do |map|
          bindings = [
            map['id'].to_i,
            map['user_id'].to_i,
            map['created_at'],
            map['created_at'],
            map['created_at'],
          ]
          exec_insert 'INSERT INTO team_mates (map_id, user_id, created_at, updated_at, accepted_at) values($1, $2, $3, $4, $5)', 'CREATE_TEAM_MATE', bindings
        end
      end

      dir.down do
        drop_table :team_mates
      end
    end
  end
end
