class RemoveEdibleFromResources < ActiveRecord::Migration[6.1]
  class TempResource < Resource
    def child_count
      0
    end
  end

  def change
    tag_name = I18n.t('migrations.remove_edible_from_resource.tag_value')

    reversible do |dir|
      dir.up do
        TempResource.where(edible: true).each do |r|
          r.tag_list << tag_name
          r.save!
        end

        remove_column :resources, :edible
      end

      dir.down do
        add_column :resources, :edible, :boolean, null: false, default: false

        TempResource.tagged_with(tag_name).each do |r|
          r.edible = true
          r.tag_list.remove tag_name
          r.save!
        end
      end
    end
  end
end
