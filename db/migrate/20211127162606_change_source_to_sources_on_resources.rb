# As we need to have more sources on the resources, this "converts" the current
# "source" field to a jsonb array
class ChangeSourceToSourcesOnResources < ActiveRecord::Migration[6.1]
  def change
    reversible do |dir|
      dir.up do
        add_column :resources, :sources, :jsonb, null: false, default: []

        resources = select_all 'SELECT id, source FROM resources'
        resources.each do |resource|
          bindings = [
            resource['id'].to_i,
            [resource['source']].to_json,
          ]
          update 'UPDATE resources set sources=$2 WHERE id=$1', 'UPDATE_RESOURCE', bindings
        end

        remove_column :resources, :source
      end

      dir.down do
        add_column :resources, :source, :string

        resources = select_all 'SELECT id, sources FROM resources'
        resources.each do |resource|
          source = JSON.parse(resource['sources']).first
          bindings = [
            resource['id'].to_i,
            source,
          ]
          update 'UPDATE resources set source=$2 WHERE id=$1', 'UPDATE_RESOURCE', bindings
        end

        remove_column :resources, :sources
      end
    end
  end
end
