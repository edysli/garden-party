require 'simplecov'
require 'capybara-screenshot/cucumber'
require_relative '../../spec/support/capybara'

SimpleCov.start 'rails'

Capybara.save_path = Rails.root.join('tmp', 'cucumber_screenshots')
