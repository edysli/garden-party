module Helpers
  def accept_browser_alert
    page.driver.browser.switch_to.alert.accept
  end

  def open_dropdown(trigger: nil)
    find('.gp-dropdown__trigger', text: trigger).hover
  end

  def click_on_in_menu(string)
    within '.gp-navbar' do
      click_on string
    end
  end

  def fill_in_task_with(name:, date: nil, notes: nil, assignee: nil)
    date ||= 1.day.from_now
    notes ||= Faker::Lorem.paragraph

    fill_in I18n.t('activerecord.attributes.task.name'), with: name
    select assignee, from: I18n.t('activerecord.attributes.task.assignee') if assignee
    fill_in I18n.t('js.tasks.form.date'), with: date
    fill_in I18n.t('activerecord.attributes.task.notes'), with: notes
  end

  def fill_in_observation_with(title:, date: nil, content: nil)
    date ||= Time.current
    content ||= Faker::Lorem.paragraph

    fill_in I18n.t('activerecord.attributes.observation.title'), with: title
    fill_in I18n.t('js.tasks.form.date'), with: date
    fill_in I18n.t('activerecord.attributes.observation.content'), with: content
  end

  def select_in_multiselect(id, value)
    find(".multiselect[aria-owns=\"listbox-#{id}\"]").click
    find("##{id}").fill_in(with: value).send_keys :enter
  end
end

World(Helpers)
