Given('I access the site') do
  visit '/'
end

Given('I access the app') do
  visit '/app'
end

Given('I access the inventory page') do
  click_on I18n.t('js.layouts.garden.toolbar.resource_overview')
end

Given('I access the {string} overview') do |map_name|
  click_on I18n.t('layouts.application.header.gardens')
  click_on map_name
  click_on I18n.t('js.layouts.garden.toolbar.resource_overview')
end
