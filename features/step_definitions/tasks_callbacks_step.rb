Given(/^there is a "(.*)" element planned for (implantation|removal)$/) do |element_name, action_name|
  now = Date.current

  element = FactoryBot.create :element, :point, name: element_name, map: @current_map, layer: @current_map.layers.first
  if action_name == 'implantation'
    element.update! implantation_planned_for: now + 3.days
  else
    # Done in two steps to have the removal task instead of a "finish implantation" status
    element.update! implanted_at: now.yesterday
    element.update! removal_planned_for: now + 3.days
  end
end

Given('there is an implanted {string}') do |element_name|
  element = FactoryBot.create :element, :point, name: element_name, map: @current_map, layer: @current_map.layers.first
  element.update! implanted_at: Date.current.yesterday
end

Given(/^I plan to (implant|remove) the "(.*)" tomorrow$/) do |action, element_name|
  within '.gpa-element', text: element_name do
    click_on I18n.t("js.elements.action_buttons.#{action}")
  end

  new_date = action == 'implant' ? Date.current + 2.days : Date.current + 4.days
  i18n_nibble = action == 'implant' ? 'plantation_date' : 'removal_date'
  fill_in I18n.t("js.elements.action_buttons.#{i18n_nibble}"), with: new_date

  click_on I18n.t('generic.save')
end

Then(/^the task to (implant|remove) the "(.*)" is visible in the tasks list$/) do |action, element_name|
  click_on I18n.t('js.layouts.garden.toolbar.todo')
  action_string = action == 'implant' ? I18n.t('js.elements.action_buttons.implant') : I18n.t('js.elements.action_buttons.remove')

  expect(find('.gpa-task', text: element_name)).to have_content action_string
end

Given(/^I validate the (implantation|removal) of the "(.*)" from the element$/) do |action_name, element_name|
  action = action_name == 'implantation' ? 'implant' : 'remove'
  status = action_name == 'implantation' ? 'implanted' : 'removed'

  within '.gpa-element', text: element_name do
    click_on I18n.t("js.elements.item_content.#{action}_now")

    expect(current_scope).to have_content I18n.t("activerecord.attributes.element.#{status}_at")
  end
end

Given(/^I finish the (implantation|removal) task of "(.*)"$/) do |_action_name, element_name|
  click_on I18n.t('js.layouts.garden.toolbar.todo')

  within '.gpa-task', text: element_name do
    click_on I18n.t('js.elements.last_action.finish')
  end

  expect(page).not_to have_content element_name
end

Then(/^the "(.*)" is (implanted|removed) in the overview$/) do |element_name, state|
  click_on I18n.t('js.layouts.garden.toolbar.resource_overview')

  within '.gpa-element', text: element_name do
    expect(current_scope).to have_content I18n.t("activerecord.attributes.element.#{state}_at")
  end
end

Given(/^I change the (implantation|removal) date of the "(.*)" from the element to tomorrow$/) do |action, element_name|
  within '.gpa-element', text: element_name do
    click_on I18n.t('generic.edit')
  end

  i18n_nibble = action == 'implantation' ? 'plantation_date' : 'removal_date'
  fill_in I18n.t("js.elements.action_buttons.#{i18n_nibble}"), with: Date.current.tomorrow

  click_on I18n.t('generic.save')
end

Then(/^the task to (implant|remove) the "(.*)" is planned tomorrow in the tasks list$/) do |action, element_name|
  click_on I18n.t('js.layouts.garden.toolbar.todo')

  action_string = action == 'implant' ? I18n.t('js.elements.action_buttons.implant') : I18n.t('js.elements.action_buttons.remove')
  within '.gpa-tasks__day', text: I18n.l(Date.current.tomorrow, format: :date_js) do
    within '.gpa-task', text: element_name do
      expect(current_scope).to have_content action_string
    end
  end
end

Given(/^I change the (implantation|removal) date of the "(.*)" from the task to tomorrow$/) do |_action, element_name|
  click_on I18n.t('js.layouts.garden.toolbar.todo')

  within '.gpa-task', text: element_name do
    click_on I18n.t('generic.edit')
  end

  fill_in I18n.t('js.tasks.form.date'), with: Date.tomorrow
  click_on I18n.t('generic.save')
end

Then(/^the "(.*)" element is planned for (implantation|removal) tomorrow in the overview$/) do |element_name, action|
  click_on I18n.t('js.layouts.garden.toolbar.resource_overview')

  within '.gpa-element', text: element_name do
    within '.-grid--2 > div', text: I18n.t("js.elements.item_content.#{action}_planned_for") do
      expect(current_scope).to have_content I18n.l(Date.tomorrow, format: :date_js)
    end
  end
end

Given(/^I cancel the (implantation|removal) date of the "(.*)" from the element$/) do |_action, element_name|
  within '.gpa-element', text: element_name do
    click_on I18n.t('generic.cancel')
  end
end

Then(/^there is no (implantation|removal) task for the "(.*)" in the tasks list$/) do |_action, element_name|
  click_on I18n.t('js.layouts.garden.toolbar.todo')
  expect(page).not_to have_content element_name
end

Given(/^I delete the (implantation|removal) task of "(.*)"$/) do |_action, element_name|
  click_on I18n.t('js.layouts.garden.toolbar.todo')

  within '.gpa-task', text: element_name do
    click_on I18n.t('generic.destroy')
  end

  click_on I18n.t('generic.ok')
end

Then(/^the "(.*)" is no longer planned for (implantation|removal) in the overview$/) do |element_name, action|
  click_on I18n.t('js.layouts.garden.toolbar.resource_overview')
  within '.gpa-element', text: element_name do
    expect(current_scope).not_to have_content I18n.t("activerecord.attributes.element.#{action}_planned_for")
  end
end

Given('I delete the {string} element') do |element_name|
  within '.gpa-element', text: element_name do
    open_dropdown
    click_on I18n.t('js.elements.action_buttons.destroy_element')
  end

  click_on I18n.t('generic.ok')
end
