Given('{string} is in {string} team') do |user_name, map_name|
  map = Map.find_by name: map_name
  user = User.find_by username: user_name
  FactoryBot.create :team_mate, user: user, map: map
end

Given('I\'m in the {string}\'s map team') do |username|
  user = User.find_by username: username
  FactoryBot.create :team_mate, :accepted, user: @signed_in_user, map: user.maps.first
end

Given('there is a {string} task on a {string} patch in {string}') do |task_name, patch_name, map_name|
  map = Map.find_by name: map_name
  patch = FactoryBot.create :patch, :with_element, map: map, name: patch_name
  FactoryBot.create :task, :planned, subject: patch, name: task_name, assignee: map.user
end

Given('there is a {string} task on a {string} patch in {string}, assigned to me') do |task_name, patch_name, map_name|
  map = Map.find_by name: map_name
  patch = FactoryBot.create :patch, :with_element, map: map, name: patch_name
  FactoryBot.create :task, :planned, subject: patch, name: task_name, assignee: @signed_in_user
end

Given('there are other pending tasks on the {string} map') do |map_name|
  map = Map.find_by name: map_name
  patch = FactoryBot.create :patch, :with_element, map: map

  FactoryBot.create_list :task, 3, :planned, subject: patch
end

Given('I visit the team section') do
  click_on I18n.t('js.layouts.garden.toolbar.team')
end

When('I invite {string}') do |email|
  within('.gpa-search-user', text: I18n.t('activerecord.attributes.user.email')) do
    fill_in I18n.t('activerecord.attributes.user.email'), with: email
    click_on I18n.t('generic.search')
    click_on I18n.t('js.users.search.invite')
  end
end

When('I invite {string} as {string}') do |email, username|
  within('.gpa-search-user', text: I18n.t('activerecord.attributes.user.email')) do
    fill_in I18n.t('activerecord.attributes.user.email'), with: email
    click_on I18n.t('generic.search')

    within '.gp-modal' do
      fill_in I18n.t('activerecord.attributes.user.username'), with: username
      click_on I18n.t('js.user.invite_on_instance')
    end
  end
end

Then('I see {string} as a pending member') do |username|
  within '.gp-list-item', text: username do
    expect(current_scope).to have_css('.gp-badge', text: I18n.t('js.user.pending_activation'))
  end
end

Then('I see {string} as a pending teammate') do |username|
  within '.gp-list-item', text: username do
    expect(current_scope).to have_css('.gp-badge', text: I18n.t('js.team_mates.pending_invitation'))
  end
end

When('I search for {string} user') do |username|
  within('.gpa-search-user', text: I18n.t('activerecord.attributes.user.username')) do
    fill_in I18n.t('activerecord.attributes.user.username'), with: username
  end
end

When('I add {string} to the team') do |username|
  within '.gp-list-item', text: username do
    click_on I18n.t('js.users.search.invite')
  end
end

Given('{string} invited me in her garden') do |user_name|
  user = User.find_by username: user_name
  map = user.maps.first

  FactoryBot.create :team_mate, map: map, user: @signed_in_user
end

Then(/I see a pending invitation from "(.*)" on (?:her|his) map$/) do |username|
  user = User.find_by username: username
  map = user.maps.first
  expect(page).to have_css('.gpa-notification', text: I18n.t('js.notifications.messages.team_mate_invitation', username: username, map: map.name))
end

Given('I access the notifications list') do
  step 'I click on the notifications button'
end

When('I accept the team invitation on {string}') do |map_name|
  within('.gpa-notification', text: map_name) do
    click_on I18n.t('js.notifications.action.accept')
  end
end

Then('I see a message stating I accepted to join the team') do
  expect(page).to have_content(I18n.t('js.team_mates.successfully_accepted'))
end

Given('I access the maps list') do
  click_on I18n.t('js.layouts.garden.main_menu.home')
end

Then('I see {string} map as a non-owned map') do |map_name|
  expect(page).not_to have_css '.map-entry--owned', text: map_name
end

When('I leave the team') do
  click_on I18n.t('js.team_mates.leave')
  # Countdown button
  sleep 2.5
end

Then('I see a message stating I left the team') do
  expect(page).to have_content I18n.t('js.team_mates.successfully_left')
end

Then('I\'m redirected to the maps list') do
  expect(page).to have_content(I18n.t('js.maps.index.title'))
end

Then('I don\'t see the {string} map in the list') do |map_name|
  expect(page).not_to have_content map_name
end

When('I remove {string} from the team') do |username|
  within '.gp-list-item', text: username do
    click_on I18n.t('js.team_mates.remove')
    # Countdown button and refetch
    sleep 3
  end
end

Then('I don\'t see {string} in the teammates') do |username|
  team_mates = find_all '.gp-list-item', text: username
  expect(team_mates.count).to eq 0
end

Then('there is no teammates but me') do
  expect(find_all('.gp-list-item').count).to eq 1
end

Then('the {string} task is assigned to {string}') do |task_action, user_name|
  within '.gpa-task', text: task_action do
    expect(current_scope).to have_content user_name
  end
end

Given('I assign the {string} task to {string}') do |task_name, user_name|
  within '.gpa-task', text: task_name do
    click_on I18n.t('generic.edit')
  end

  select user_name, from: I18n.t('activerecord.attributes.task.assignee')
  click_on I18n.t('generic.save')
end

When('I delete the {string} patch') do |patch_name|
  within '.gp-item', text: patch_name do
    click_on I18n.t('generic.destroy')
  end
  within '.gp-modal' do
    click_on I18n.t('generic.ok')
  end
end

Then('the {string} patch does not exist anymore') do |patch_name|
  expect(page).not_to have_css('.gp-item', text: patch_name)
end

Given('I create a {string} task assigned to {string}') do |task_action, user_name|
  patches = find_all '.gp-item'

  within patches.first do
    click_on I18n.t('js.layouts.garden.info.add_patch_task')
  end

  within '.gp-modal' do
    fill_in I18n.t('activerecord.attributes.task.name'), with: task_action
    select user_name, from: I18n.t('activerecord.attributes.task.assignee')
    date_trigger = find('label', text: I18n.t('js.tasks.form.date'))
    date_trigger.click

    date = 2.days.from_now
    # Zero-padded values
    month    = "0#{date.month}"[-2, 2]
    day      = "0#{date.day}"[-2, 2]
    fill_in I18n.t('js.tasks.form.date'), with: "#{date.year}-#{month}-#{day}"

    date_trigger.click
    click_on I18n.t('generic.save')
  end
end
