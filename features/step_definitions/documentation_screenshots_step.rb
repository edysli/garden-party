require 'garden_party/tools/seeder'

Given('I take screenshots for the sign up and login page') do
  visit '/'
  inject_dots [
    find('a', text: I18n.t('layouts.application.home')),
    find('a', text: I18n.t('layouts.application.tools')),
    find('a', text: I18n.t('layouts.application.changelog')),
    find('a', text: I18n.t('layouts.application.links')),
    find('a', text: I18n.t('layouts.application.sign_in')),
  ], position: :bottom
  take_and_crop_screenshot 'home_page_signed_out', height: 400

  visit '/users/sign_in'
  inject_dots [
    # Sign in button
    find('input[type="submit"]'),
    find('a', text: I18n.t('devise.shared.links.sign_up')),
  ]
  take_and_crop_screenshot 'sign_in_page', height: 400
end

Given('I have an uploaded map') do
  @current_map = FactoryBot.create :map, :user_submitted_png, user: @signed_in_user
end

Given('I take screenshots for the map management page') do
  visit '/'

  inject_dots [
    find('a', text: I18n.t('layouts.application.header.gardens')),
    # Sign out button
    find('input[type="submit"]'),
  ], position: :bottom
  take_and_crop_screenshot 'home_page_signed_in', height: 200

  visit '/app'
  # Wait for page to be ready
  find_all 'h1', text: I18n.t('js.maps.index.title')
  take_and_crop_screenshot 'map_management_empty', height: 600

  click_on I18n.t('js.maps.index.new_map')
  dots = []
  find_all('input[type="checkbox"]').each { |checkbox| dots.push checkbox }
  dots.push find('input[type="file"]')

  inject_dots dots
  take_and_crop_screenshot 'map_management_new_map', height: 600
  remove_injected_elements

  attach_file I18n.t('js.maps.form.select_picture'), Rails.root.join('spec', 'fixtures', 'files', 'map.svg')

  fill_in I18n.t('activerecord.attributes.map.name'), with: 'Super carte'
  inject_dots [
    find('input[type="number"]'),
    find('button', text: I18n.t('generic.continue')),
  ]
  take_and_crop_screenshot 'map_management_new_map_scale', height: 600

  click_on text: I18n.t('generic.continue')
  remove_injected_elements

  # Wait for map to be ready
  find_all 'button', text: I18n.t('js.maps.form.save_this_map')
  inject_circles [find('canvas')]
  inject_dots [find('button[type="submit"]')]
  take_and_crop_screenshot 'map_management_new_map_center', height: 900
end

Given('there is a nice dataset for screenshots') do
  @current_map.update name: 'Lórien', publicly_available: true
  @signed_in_user.update username: 'Camille'
  layer = @current_map.layers.first

  GardenParty::Tools::Seeder.apply Rails.root.join('db', "seeds_#{I18n.locale}.yml")

  # Patch with multiple elements
  FactoryBot.create(:patch, :with_element, :polygon, map: @current_map).tap do |patch|
    # Implanted
    FactoryBot.create(:element, resource: Resource.order('RANDOM()').first, patch: patch, implanted_at: 1.year.ago).tap do |element|
      # Overdue activity
      FactoryBot.create :task, :planned, planned_for: 10.days.ago, name: 'trim', subject: element
    end
    # Removed
    FactoryBot.create(:element, resource: Resource.order('RANDOM()').first, patch: patch, implanted_at: 1.year.ago, removed_at: 2.hours.ago)
    # Removal planned
    FactoryBot.create(:element, resource: Resource.order('RANDOM()').first, patch: patch, implanted_at: 2.weeks.ago, removal_planned_for: 2.days.from_now)
  end

  # Unique, implanted element
  FactoryBot.create(:element, :point, resource: Resource.order('RANDOM()').first, map: @current_map, layer: layer, implanted_at: 4.days.ago).tap do |element|
    # Planned activity
    FactoryBot.create :task, :planned, planned_for: 2.days.from_now, name: 'water', subject: element
    FactoryBot.create :task, :planned, planned_for: Time.zone.now, name: 'fertilize', subject: element
  end

  # Unique, non implanted element
  FactoryBot.create :element, :point, resource: Resource.order('RANDOM()').first, map: @current_map, layer: layer, implantation_planned_for: 2.hours.from_now

  # Unique, removed element
  FactoryBot.build :element, :point, resource: Resource.order('RANDOM()').first, map: @current_map, layer: layer, implanted_at: 4.years.ago, removed_at: 1.year.ago

  # Other user with a map in which signed in user is invited
  FactoryBot.create :user, username: 'Barbara', email: 'barbara@garden-party.io'

  other_user = FactoryBot.create :user, username: 'Daryl', email: 'daryl@garden-party.io'
  other_map  = FactoryBot.create :map, user: other_user, name: 'Beautiful patch'
  FactoryBot.create :team_mate, user: @signed_in_user, map: other_map

  # Invite other users on current map
  FactoryBot.create :team_mate, user: other_user, map: @current_map
  FactoryBot.create :team_mate, :accepted, user: FactoryBot.create(:user, username: 'Ann'), map: @current_map
end

Given('I take screenshots for garden management: overview') do
  step "I access the \"#{@current_map.name}\" map"

  inject_dots [
    find('.gp-button', text: I18n.t('js.layouts.garden.toolbar.map')),
    find('.gp-button', text: I18n.t('js.layouts.garden.toolbar.resource_overview')),
    find('.gp-button', text: I18n.t('js.layouts.garden.toolbar.todo')),
    find('.gp-button', text: I18n.t('js.layouts.garden.toolbar.team')),
    find('.gp-button', text: I18n.t('js.layouts.garden.toolbar.activities')),
  ]
  take_and_crop_screenshot 'garden_management__overview', height: 150
end

Given('I take screenshots for garden management: map: overview') do
  # We're still on the right page
  remove_injected_elements

  inject_outlined_dots [
    # Toolbar
    find('.gp-toolbar', text: I18n.t('js.layouts.garden.toolbar.review')),
    find('.gp-toolbar', text: I18n.t('js.layouts.garden.toolbar.add')),
    find('.gp-toolbar', text: I18n.t('js.layouts.garden.toolbar.draw')),
    # Map
    find('#map'),
    # Layers
    find('.drawer__content aside'),
  ], style: :shape

  # Map actions
  inject_dots [
    find(".gp-button[title=\"#{I18n.t('activerecord.attributes.map.notes')}\"]"),
    find(".gp-button[title=\"#{I18n.t('js.layouts.garden.info.add_map_observation')}\"]"),
    find(".gp-button[title=\"#{I18n.t('js.layouts.garden.info.add_map_task')}\"]"),
    find(".gp-button[title=\"#{I18n.t('js.elements.action_buttons.show_observations')}\"]"),
    find(".gp-button[title=\"#{I18n.t('generic.edit')}\"]"),
    find(".gp-button[title=\"#{I18n.t('js.layouts.garden.main_menu.display_shared_version')}\"]"),
  ], counter: 6, position: :bottom
  take_and_crop_screenshot 'garden_management__map__overview', height: 600
end

Given('I take screenshots for garden management: map: add resources') do
  # We're still on the right page
  remove_injected_elements

  within('.gp-toolbar', text: I18n.t('js.layouts.garden.toolbar.add')) do
    inject_dots [
      find("button[title=\"#{I18n.t('js.layouts.garden.side_panel.button_draw_point')}\""),
      find("button[title=\"#{I18n.t('js.layouts.garden.side_panel.button_draw_polygon')}\""),
      find("button[title=\"#{I18n.t('js.layouts.garden.side_panel.button_draw_circle')}\""),
    ]
  end
  take_and_crop_screenshot 'garden_management__map__add_resources', top: 60, left: 110, width: 260, height: 100
end

Given('I take screenshots for garden management: map: draw shapes') do
  # We're still on the right page
  remove_injected_elements

  within('.gp-toolbar', text: I18n.t('js.layouts.garden.toolbar.draw')) do
    inject_dots [
      find("button[title=\"#{I18n.t('js.layouts.garden.side_panel.button_draw_line')}\""),
      find("button[title=\"#{I18n.t('js.layouts.garden.side_panel.button_draw_polygon')}\""),
      find("button[title=\"#{I18n.t('js.layouts.garden.side_panel.button_draw_circle')}\""),
    ]
  end
  take_and_crop_screenshot 'garden_management__map__draw_shapes', top: 60, left: 205, width: 280, height: 100

  remove_injected_elements
  within('.gp-toolbar', text: I18n.t('js.layouts.garden.toolbar.draw')) do
    click_on I18n.t('js.layouts.garden.side_panel.button_draw_polygon')
  end

  # Path selector
  inject_dots [
    find_all('.paths_selector .dropdown').first,
    find_all('.paths_selector .dropdown').last,
    # Preview
    find('.paths_selector .centered'),
  ]
  take_and_crop_screenshot 'garden_management__map__draw_shapes__config', top: 90, left: 0, width: 300, height: 600
end

Given('I take screenshots for garden management: map: layers') do
  # We're still on the right page
  remove_injected_elements

  inject_dots [
    # Add layer
    find('button.gp-button--block', text: I18n.t('js.garden.layers.new_layer')),
    # Reorder
    find('.layers_selector .item:first-child .item__actions:first-child button:first-child'),
    # Toggle
    find('.layers_selector .item:first-child .item__actions:first-child button:nth-child(2)'),
    # Edit
    find('.layers_selector .item:first-child .item__actions:first-child button:nth-child(3)'),
    # Destroy
    find('.layers_selector .item:first-child .item__actions:last-child button'),
    # Background image
    find('.layers_selector .item', text: I18n.t('js.layers.background_layer')),
  ]
  take_and_crop_screenshot 'garden_management__map__layers', height: 400, left: 350
end

Given('I take screenshots for garden management: inventory') do
  remove_injected_elements
  resize_window width: 1360
  # Wait for content
  find_all 'a.gp-button', text: I18n.t('js.layouts.garden.toolbar.resource_overview')
  click_on I18n.t('js.layouts.garden.toolbar.resource_overview')

  outline_elements [
    '.gp-item .gp-item.gp-item--removed',
  ]

  patches = find_all('.gp-item')

  # Standalone resources actions
  within(patches.first) do
    inject_dots [
      find(".gp-item__header__actions .gp-button[title=\"#{I18n.t('js.elements.action_buttons.edit_element')}\"]"),
      find(".gp-item__header__actions .gp-button[title=\"#{I18n.t('js.elements.action_buttons.destroy_element')}\"]"),
    ]
  end

  # Element actions
  within(patches[1]) do
    inject_dots [
      find_all(".element-item__actions .gp-button[title=\"#{I18n.t('js.elements.action_buttons.add_observation')}\"]").first,
      find_all(".element-item__actions .gp-button[title=\"#{I18n.t('js.elements.action_buttons.add_task')}\"]").first,
      find_all(".element-item__actions .gp-button[title=\"#{I18n.t('js.elements.action_buttons.show_observations')}\"]").first,
      find_all(".element-item__actions .gp-button[title=\"#{I18n.t('js.elements.action_buttons.edit_element')}\"]").first,
    ], counter: 3
  end

  # Patch actions
  within(patches.last) do
    inject_dots [
      # Patch
      find('.gp-item__header button', text: I18n.t('js.layouts.garden.info.add_resource')),
      find(".gp-item__header button[title=\"#{I18n.t('js.layouts.garden.info.add_patch_observation')}\"]"),
      find(".gp-item__header button[title=\"#{I18n.t('js.layouts.garden.info.add_patch_task')}\"]"),
      find(".gp-item__header button[title=\"#{I18n.t('js.elements.action_buttons.show_observations')}\"]"),
      find(".gp-item__header button[title=\"#{I18n.t('generic.edit')}\"]"),
      find(".gp-item__header button[title=\"#{I18n.t('generic.destroy')}\"]"),
    ], counter: 7
  end

  # Other actions
  inject_dots [
    find("button[title=\"#{I18n.t('js.elements.item_content.implant_now')}\"]"),
  ], counter: 13

  inject_dots [
    find("button[title=\"#{I18n.t('js.elements.item_content.remove_now')}\"]"),
    find('.element-item.removed'),
  ], counter: 14

  take_and_crop_screenshot 'garden_management__inventory', height: 700
end

Given('I take screenshots for garden management: tasks') do
  # We're OK with the state of previous step
  remove_injected_elements
  click_on I18n.t('js.layouts.garden.toolbar.todo')

  inject_outlined_dots [
    find('.todo-day--overdue'),
    find('.todo-day--today'),
    find('.todo-day:last-child'),
  ], style: :shape

  inject_dots [
    find('.todo-day--today .todo:nth-child(2) .todo__header__user'),
    find('.todo-day--today .todo:nth-child(2) .todo__header__action'),
  ], counter: 4

  inject_outlined_dots [
    find('.todo-day--today .todo:nth-child(2) .todo__header__title'),
    find('.todo-day--today .todo:nth-child(2) .todo__header__actions'),
  ], style: :shape, counter: 7

  take_and_crop_screenshot 'garden_management__tasks', height: 500
end

Given('I take screenshots for garden management: team management') do
  # We're OK with the state of previous step
  remove_injected_elements

  #
  # Access team mates and take a screenshot of the interface
  #
  click_on I18n.t('js.layouts.garden.toolbar.team')

  # Search for users
  fill_in I18n.t('activerecord.attributes.user.username'), with: 'Barbara'
  fill_in I18n.t('activerecord.attributes.user.email'), with: 'barbara@garden-party.io'
  find_all('button', text: I18n.t('generic.search')).first.click

  elements = []

  find_all('.team_mate').each { |element| elements.push element }
  find_all('.search-form').each { |element| elements.push element }

  inject_outlined_dots elements, style: :rectangle

  take_and_crop_screenshot 'garden_management__team_mates', height: 600

  #
  # Take screenshots of the notifications
  #
  remove_injected_elements

  click_on I18n.t('js.layouts.garden.main_menu.notifications')

  inject_dots [
    find(".notification button[title='#{I18n.t('js.notifications.action.accept')}']"),
    find(".notification button[title='#{I18n.t('js.notifications.action.archive')}']"),
    find(".notification button[title='#{I18n.t('js.notifications.action.refuse')}']"),
  ]

  take_and_crop_screenshot 'garden_management__team_mates_notification', height: 400
end

Given('I take screenshots for community resources: families') do
  step "I access the \"#{@current_map.name}\" map"
  click_on I18n.t('js.layouts.garden.main_menu.library')

  inject_dots [
    find('.gp-navbar a', text: I18n.t('js.layouts.garden.main_menu.library')),
  ], position: :bottom
  inject_dots [
    find('button', text: I18n.t('js.genera.index.new_family')),
  ], counter: 2
  take_and_crop_screenshot 'community_resources__families__access_create_form', height: 200

  remove_injected_elements
  click_on I18n.t('js.genera.index.family')
  # Select first resource available
  find('div:first-child > a.item:nth-child(2)').click

  inject_dots [
    find('.gp-navbar a', text: I18n.t('js.layouts.garden.main_menu.library')),
  ], position: :bottom
  inject_dots [
    find('button', text: I18n.t('js.genera.index.family')),
    find('div:first-child > .library__family-title .actions button'),
  ], counter: 2

  take_and_crop_screenshot 'community_resources__families__access_edit_form', height: 400
end

Given('I take screenshots for community resources: genera') do
  step "I access the \"#{@current_map.name}\" map"
  click_on I18n.t('js.layouts.garden.main_menu.library')

  inject_dots [
    find('.gp-navbar a', text: I18n.t('js.layouts.garden.main_menu.library')),
  ], position: :bottom
  inject_dots [
    find('button', text: I18n.t('js.genera.index.new_genus')),
  ], counter: 2
  take_and_crop_screenshot 'community_resources__genera__access_create_form', height: 200

  remove_injected_elements
  click_on I18n.t('js.genera.index.genus')
  # Select first resource available
  find('div:first-child > a.item:nth-child(2)').click

  inject_dots [
    find('.gp-navbar a', text: I18n.t('js.layouts.garden.main_menu.library')),
  ], position: :left
  inject_dots [
    find('button', text: I18n.t('js.genera.index.genus')),
    find('div:first-child > .library__genus-title .actions button'),
  ], counter: 2

  take_and_crop_screenshot 'community_resources__genera__access_edit_form', height: 400
end

Given('I take screenshots for community resources: resources') do
  remove_injected_elements
  click_on I18n.t('js.genera.index.genus')

  inject_dots [
    find('.gp-navbar a', text: I18n.t('js.layouts.garden.main_menu.library')),
  ], position: :bottom
  inject_dots [
    find('button', text: I18n.t('js.genera.index.new_resource')),
  ], counter: 2
  take_and_crop_screenshot 'community_resources__resources__access_create_form', height: 200

  remove_injected_elements
  # Select first resource available
  find('div:first-child > a.item:nth-child(2)').click
  inject_dots [find('button', text: I18n.t('generic.edit'))]
  take_and_crop_screenshot 'community_resources__resources__access_edit_form', height: 400
end
