Given('I have {int} system notifications') do |amount|
  FactoryBot.create_list :notification, amount, recipient: @signed_in_user
end

Then(/^there is an indicator for the (\d) notifications?$/) do |amount|
  expect(page).to have_css(".gp-navbar .gp-menu-item[title=\"#{I18n.t('js.layouts.garden.main_menu.notifications')}\"]", text: amount)
end

When('I click on the notifications button') do
  element = find ".gp-navbar .gp-menu-item[title=\"#{I18n.t('js.layouts.garden.main_menu.notifications')}\"]"
  element.click
end

Then('I see {int} notifications in the list') do |amount|
  expect(find_all('.gpa-notification').count).to eq(amount)
end

Given('I display all the notifications') do
  step 'I click on the notifications button'
end

When('I archive the first notification') do
  within('.gpa-notification:first-child') do
    click_on I18n.t('js.notifications.action.archive')
  end
end

When('I delete the first notification') do
  within('.gpa-notification:first-child') do
    click_on I18n.t('js.notifications.action.delete')
  end
end

Then(/^there (?:are|is) (\d+) archived notifications?$/) do |amount|
  expect(find_all('.gpa-notification.gpa-notification--archived').count).to eq(amount)
end

Then(/^there (?:are|is) (\d+) non archived notifications?$/) do |amount|
  expect(find_all('.gpa-notification:not(.gpa-notification--archived)').count).to eq(amount)
end

Given('I archive all the notifications') do
  click_on I18n.t('js.notifications.archive_all')
end

Given('I delete all the archived notifications') do
  click_on I18n.t('js.notifications.delete_all_archived')
end

Then('I see {int} notification in the list') do |amount|
  expect(find_all('.gpa-notification').count).to eq(amount)
end
