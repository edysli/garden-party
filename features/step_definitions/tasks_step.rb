When('I access the tasks list') do
  click_on I18n.t('js.layouts.garden.toolbar.todo')
end

When(/^I planned to "(.*)" "(.*)" (yesterday|today|tomorrow)$/) do |task_name, element_name, day|
  # Disabling Rails/Timezone as we need the beginning of day for the timezone of
  # "the client"
  # rubocop:disable Rails/TimeZone
  date = case day
         when 'yesterday'
           Time.now.beginning_of_day - 1.day
         when 'today'
           Time.now.beginning_of_day
         when 'tomorrow'
           Time.now.beginning_of_day + 1.day
         end
  # rubocop:enable Rails/TimeZone
  element = Element.find_by name: element_name
  Task.create name: task_name, subject: element, planned_for: date
end

Given('I create a task {string} for the map') do |task_name|
  click_on I18n.t('js.layouts.garden.info.add_map_task')
  fill_in_task_with name: task_name
  click_on I18n.t('generic.save')
end

Then('I see the {string} task') do |task_name|
  expect(page).to have_css '.gpa-task', text: task_name
end

Given('I create a task {string} for the {string} patch') do |task_name, patch_name|
  within '.gp-item', text: patch_name do
    click_on I18n.t('js.layouts.garden.info.add_patch_task')
  end

  fill_in_task_with name: task_name
  click_on I18n.t('generic.save')
end

Given('I create a task {string} for the {string} element') do |task_name, element_name|
  within '.gpa-element', text: element_name do
    open_dropdown
    click_on I18n.t('js.elements.action_buttons.add_task')
  end

  fill_in_task_with name: task_name
  click_on I18n.t('generic.save')
end

Then('I see that {string} task is overdue') do |task_name|
  expect(page).to have_css('.gpa-tasks__day--overdue', text: task_name)
end

When('I finish the {string} task') do |task_name|
  within '.gpa-tasks__day--overdue', text: task_name do
    click_on I18n.t('js.elements.last_action.finish')
  end
end

Then('I don\'t see the {string} task anymore') do |task_name|
  expect(page).not_to have_content task_name
end
