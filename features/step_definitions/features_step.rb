Given('I have a map named {string}') do |name|
  @current_map = FactoryBot.create :map, name: name, user: @signed_in_user
end

Given('there is a resource named {string}') do |name|
  FactoryBot.create :resource, name: name
end

Given('there is a genus named {string}') do |name|
  FactoryBot.create :genus, name: name
end

Given('I access the {string} map') do |name|
  visit '/'
  click_on I18n.t('layouts.application.header.gardens')
  click_on I18n.t('js.layouts.garden.main_menu.home')

  click_on name
end

When('I click on the inventory link') do
  click_on I18n.t('js.layouts.garden.toolbar.resource_overview')
end

Then('I see the inventory page') do
  expect(page).to have_content I18n.t('js.patches.index.alone_resources')
end

Given('I have a tree named {string}') do |name|
  FactoryBot.create :element, :point, name: name, map: @current_map, layer: @current_map.layers.first
end

Given('I access the inventory page for {string}') do |string|
  step "I access the \"#{string}\" map"
  step 'I click on the inventory link'
end

Then('I see {string} in the elements list') do |name|
  within('section', text: I18n.t('js.patches.index.alone_resources')) do
    expect(current_scope).to have_content name
  end
end

Given('I have a patch named {string}') do |name|
  FactoryBot.create :patch, :with_element, :polygon, name: name, map: @current_map
end

Given('I have a patch named {string} with some {string} in it') do |patch_name, resource_name|
  resource = FactoryBot.create :resource, name: resource_name
  element  = FactoryBot.build :element, resource: resource, patch: nil
  FactoryBot.create :patch, :polygon, name: patch_name, map: @current_map, elements: [element]
end

Then('I see {string} in the patches list') do |name|
  within('section', text: I18n.t('js.patches.index.patches')) do
    expect(current_scope).to have_content name
  end
end

Then('I see that {string} is not yet implanted in the elements list') do |name|
  within 'section', text: I18n.t('js.patches.index.alone_resources') do
    within '.gpa-element', text: name do
      expect(current_scope).to have_button I18n.t('js.elements.action_buttons.implant')
    end
  end
end

Then('I see that {string} is not yet implanted in the {string} elements list') do |resource_name, patch_name|
  within '.gp-item', text: patch_name do
    within '.gpa-element', text: resource_name do
      expect(current_scope).to have_button I18n.t('js.elements.action_buttons.implant')
    end
  end
end

Then('I see that {string} is present in the {string} elements list') do |resource_name, patch_name|
  within '.gp-item', text: patch_name do
    expect(current_scope).to have_content(resource_name)
  end
end

When('I add some {string} in the {string}') do |resource_name, patch_name|
  within '.gp-item', text: patch_name do
    click_on I18n.t('js.layouts.garden.info.add_resource')
  end

  within '.gp-modal' do
    find('a', text: resource_name).click
  end
end

When('I implant {string} now') do |name|
  within '.gpa-element', text: name do
    click_on I18n.t('js.elements.action_buttons.implant')
  end

  click_on I18n.t('generic.save')
end

Then('I see that {string} has been implanted today') do |name|
  within '.gpa-element', text: name do
    expected_content = "#{I18n.t('activerecord.attributes.element.implanted_at')}\n#{I18n.l(Date.current, format: :date_js)}"
    expect(current_scope).to have_content expected_content
  end
end

When('I plan to implant {string} tomorrow') do |name|
  within '.gpa-element', text: name do
    click_on I18n.t('js.elements.action_buttons.implant')
  end

  fill_in I18n.t('js.elements.action_buttons.plantation_date'), with: Date.tomorrow

  click_on I18n.t('generic.save')
end

Then('I see that {string} should be planted tomorrow') do |name|
  within '.gpa-element', text: name do
    # Disabling Rails/Date as we need the date in the timezone of "the client"
    expect(current_scope).to have_content I18n.t('js.elements.item_content.implantation_planned_for', date: I18n.l(Date.today + 1.day, format: :date_js)) # rubocop:disable Rails/Date
  end
end

Given('I have a tree named {string} which I should implant tomorrow') do |name|
  element = FactoryBot.create :element, :point, name: name, map: @current_map, layer: @current_map.layers.first
  element.update implantation_planned_for: Time.current.beginning_of_day + 1.day
end

When('I validate the implantation of {string}') do |name|
  within '.gpa-element', text: name do
    click_on I18n.t('js.elements.item_content.implant_now')
  end
end

Given('I have a planted tree named {string}') do |name|
  element = FactoryBot.create :element, :point, name: name, map: @current_map, layer: @current_map.layers.first
  element.update(implanted_at: Time.current.beginning_of_day - 1.day)
end

When('I remove the {string} now') do |name|
  within '.gpa-element', text: name do
    click_on I18n.t('js.elements.action_buttons.remove')
  end
  click_on I18n.t('generic.save')
end

Given('I have a tree named {string} which I should remove tomorrow') do |name|
  element = FactoryBot.create :element, :point, name: name, map: @current_map, layer: @current_map.layers.first
  element.update implanted_at: Time.current.beginning_of_day - 1.day, removal_planned_for: Time.current.beginning_of_day + 1.day
end

When('I validate the removal of {string}') do |name|
  within '.gpa-element', text: name do
    click_on I18n.t('js.elements.item_content.remove_now')
  end
end

Then('the {string} has been removed') do |name|
  within '.gpa-element', text: name do
    # Disabling Rails/Date as we need the date in the timezone of "the client"
    expect(current_scope).to have_content I18n.t('activerecord.attributes.element.removed_at', date: I18n.l(Date.today, format: :date_js)) # rubocop:disable Rails/Date
  end
end

Given('I change the {string} to {string} in {string}') do |old_name, new_name, _patch_name|
  within '.gp-item .gpa-element', text: old_name do
    open_dropdown
    click_on I18n.t('generic.edit')
  end

  select_in_multiselect 'element_resource', new_name

  click_on I18n.t('generic.save')
end

Then('the element changed to {string}') do |string|
  expect(page).to have_content string
end

When('I duplicate {string}') do |name|
  within '.gpa-element', text: name do
    click_on I18n.t('js.elements.action_buttons.duplicate')
  end
end

When('I rename {string} to {string}') do |old_name, new_name|
  within '.gpa-element', text: old_name do
    open_dropdown
    click_on I18n.t('js.elements.action_buttons.edit_element')
  end

  fill_in I18n.t('activerecord.attributes.element.name'), with: new_name

  click_on I18n.t('generic.save')
end

Then('I see {string} duplicate in the elements list') do |name|
  within('section', text: I18n.t('js.patches.index.alone_resources')) do
    expect(current_scope).to have_content I18n.t('models.element.duplicate_name_suffix', name: name)
  end
end
