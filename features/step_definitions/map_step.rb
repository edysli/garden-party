Given('I access the map') do
  click_on I18n.t('js.layouts.garden.toolbar.map')
end

When('I display the map note') do
  click_on I18n.t('activerecord.attributes.map.notes')
end

When('I update the note') do
  within('.gp-modal') do
    click_on I18n.t('generic.edit')
    fill_in 'map_notes', with: '# My new note'
    click_on I18n.t('generic.save')
  end
end

Then('I see the rendered new note') do
  expect(find('.gp-modal .gp-modal__content h1')).to have_content 'My new note'
end
