Given('I start to write an observation for {string}') do |name|
  within '.gpa-element', text: name do
    open_dropdown
    click_on I18n.t('js.elements.action_buttons.add_observation')
  end

  fill_in I18n.t('activerecord.attributes.observation.title'), with: 'It speaks!'
  fill_in I18n.t('activerecord.attributes.observation.content'), with: 'It even said "Hello world"'
end

When('I attach two pictures to the activity') do
  attach_file I18n.t('activerecord.attributes.observation.pictures'), [
    Rails.root.join('spec', 'fixtures', 'files', 'observation_upload.png'),
    Rails.root.join('spec', 'fixtures', 'files', 'map.jpg'),
  ]
end

Then(/^I see (\d+) pictures? thumbnails? in the form$/) do |amount|
  # Well...
  sleep 1
  expect(find_all('._thumbnail').count).to eq amount
end

When('I remove the first picture') do
  within '.gp-thumbnails' do
    find('._thumbnail:first-child button.gp-button--destructive').click
  end
end

Given('I take some notes with two attached pictures') do
  element = Element.last

  FactoryBot.create :observation, title: 'Something beautiful', subject: element, pictures: [
    Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', 'observation_upload.png'), 'image/png'),
    Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', 'map.jpg'), 'image/jpeg'),
  ]
end

When('I display the observations history for {string}') do |name|
  within '.gpa-element', text: name do
    open_dropdown
    click_on I18n.t('js.elements.action_buttons.show_observations')
  end
end

Then('the first entry have {int} pictures') do |amount|
  find('.gp-observation:first-child .gp-observation__header').click
  thumbnails = find_all '.gp-observation:first-child ._thumbnail'

  expect(thumbnails.count).to eq amount
end

When('I display the first picture') do
  find('.gp-observation:first-child .gp-observation__header').click
  find('.gp-modal ._thumbnail:first-child').click
end

Then('I see the first picture in full screen.') do
  expect(page).to have_css '.gp-image-viewer__image'
end

Given('I do the {string} observation of the garden') do |observation_title|
  click_on I18n.t('js.layouts.garden.info.add_map_observation')
  fill_in_observation_with title: observation_title
  click_on I18n.t('generic.save')
end

When('I access the map\'s observations list') do
  click_on I18n.t('js.elements.action_buttons.show_observations')
end

When('I display the observations history for the {string} patch') do |patch_name|
  within '.gp-item', text: patch_name do
    # Use the first button found; the others belong to the patch elements
    within find_all('.gp-item__aside').first do
      click_on I18n.t('js.elements.action_buttons.show_observations')
    end
  end
end

When('I display the observations history for the {string} element') do |element_name|
  # Hover somewhere else; the dropdown won't open twice if it's already hovered...
  page.find('body').hover

  within '.gpa-element', text: element_name do
    open_dropdown

    click_on I18n.t('js.elements.action_buttons.show_observations')
  end
end

Then('I see the {string} observation') do |observation_title|
  expect(page).to have_css('.gp-observation__header__title', text: observation_title)
end

Given('I observe that {string} in the {string} patch') do |observation_title, patch_name|
  within '.gp-item', text: patch_name do
    click_on I18n.t('js.layouts.garden.info.add_patch_observation')
  end

  fill_in_observation_with title: observation_title
  click_on I18n.t('generic.save')
end

Given('I observe that {string} for the {string} element') do |observation_title, element_name|
  within '.gpa-element', text: element_name do
    open_dropdown
    click_on I18n.t('js.elements.action_buttons.add_observation')
  end

  fill_in_observation_with title: observation_title
  click_on I18n.t('generic.save')
end
