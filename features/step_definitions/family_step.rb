Given('there is a family named {string}') do |name|
  FactoryBot.create :family, name: name
end

When('I create the {string} family') do |name|
  find('.gp-toolbar > button', text: I18n.t('js.genera.index.new_family')).click

  within '.gp-modal form' do
    fill_in I18n.t('activerecord.attributes.family.name'), with: name
    select I18n.t('js.families.form.kingdom_options.plant'), from: I18n.t('activerecord.attributes.family.kingdom')

    click_on I18n.t('generic.save')
  end
end

Then("I see {string} in the library's family list") do |name|
  step 'I order the library by family'

  within '._virtual-scroll-area' do
    expect(current_scope).to have_content name
  end
end

When('I rename the {string} family to {string}') do |old_name, new_name|
  within '.gp-list-item', text: old_name do
    click_on I18n.t('generic.edit')
  end

  within '.gp-modal form' do
    fill_in I18n.t('activerecord.attributes.family.name'), with: new_name

    click_on I18n.t('generic.save')
  end
end
