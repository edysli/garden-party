Feature: Observations
  As an user
  In order to have a nice history
  I want to be able to take notes and pictures on things in the garden

  Background:
    Given I have an account and i'm logged in
    And I have a map named "My garden"
    And I have a patch named "Nice one" with some "Pipe-weed" in it
    And I have a tree named "Tally McShadow"
    And I access the inventory page for "My garden"

  Scenario: See thumbnails when attaching pictures
    Given I start to write an observation for "Tally McShadow"
    When I attach two pictures to the activity
    Then I see 2 pictures thumbnails in the form

  Scenario: Remove pictures from attachments
    Given I start to write an observation for "Tally McShadow"
    When I attach two pictures to the activity
    And I remove the first picture
    Then I see 1 picture thumbnail in the form

  Scenario: Display uploaded pictures' thumbnails
    Given I take some notes with two attached pictures
    When I display the observations history for "Tally McShadow"
    Then the first entry have 2 pictures

  Scenario: Display uploaded pictures
    Given I take some notes with two attached pictures
    When I display the observations history for "Tally McShadow"
    And I display the first picture
    Then I see the first picture in full screen.

  Scenario: Write an observation on the map
    Given I access the map
    And I do the "Beautiful day" observation of the garden
    When I access the map's observations list
    Then I see the "Beautiful day" observation

  Scenario: Write an observation on a patch
    Given I observe that "Things are growing" in the "Nice one" patch
    When I display the observations history for the "Nice one" patch
    Then I see the "Things are growing" observation

  Scenario: Write an observation on a path
    Given I observe that "Struck by lightning" for the "Tally McShadow" element
    When I display the observations history for the "Tally McShadow" element
    Then I see the "Struck by lightning" observation
