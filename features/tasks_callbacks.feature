Feature: Tasks callbacks
  As an user of the application
  In order to do the minimum of clicks when managing tasks
  I want tasks to change their related data on some actions

  Background:
    Given I have an account as "isa@garden-party.io"
    And I have a map named "Green field"
    And there is a "Beautiful bush" element planned for implantation
    And there is a "Funny flower" element planned for removal
    And there is an implanted "Giant carrot"
    And I have a tree named "Cone tree"
    And I sign in as "isa@garden-party.io"
    And I access the "Green field" overview

  Scenario Outline: Plan an element <action_name>
    Given I plan to <action> the "<element>" tomorrow
    Then the task to <action> the "<element>" is visible in the tasks list
    Examples:
      | action_name  | action  | element      |
      | implantation | implant | Cone tree    |
      | removal      | remove  | Giant carrot |

  Scenario Outline: Finish <action_name> from element
    Given I validate the <action_name> of the "<element>" from the element
    Then there is no <action_name> task for the "<element>" in the tasks list
    Examples:
      | action_name  | element        |
      | implantation | Beautiful bush |
      | removal      | Funny flower   |

  Scenario Outline: Finish <action_name> from task
    Given I finish the <action_name> task of "<element>"
    Then the "<element>" is <status> in the overview
    Examples:
      | action_name  | status    | element        |
      | implantation | implanted | Beautiful bush |
      | removal      | removed   | Funny flower   |

  Scenario Outline: Change the <action_name> date from element
    Given I change the <action_name> date of the "<element>" from the element to tomorrow
    Then the task to <action> the "<element>" is planned tomorrow in the tasks list
    Examples:
      | action_name  | action  | element        |
      | implantation | implant | Beautiful bush |
      | removal      | remove  | Funny flower   |

  Scenario Outline: Change <action_name> date from the task
    Given I change the <action_name> date of the "<element>" from the task to tomorrow
    Then the "<element>" element is planned for <action_name> tomorrow in the overview
    Examples:
      | action_name  | element        |
      | implantation | Beautiful bush |
      | removal      | Funny flower   |

  Scenario Outline: Cancel <action_name> from element
    Given I cancel the <action_name> date of the "<element>" from the element
    Then there is no <action_name> task for the "<element>" in the tasks list
    Examples:
      | action_name  | element        |
      | implantation | Beautiful bush |
      | removal      | Funny flower   |

  Scenario Outline: Delete the <action_name> task
    Given I delete the <action_name> task of "<element>"
    Then the "<element>" is no longer planned for <action_name> in the overview
    Examples:
      | action_name  | element        |
      | implantation | Beautiful bush |
      | removal      | Funny flower   |

  Scenario Outline: Delete the element
    Given I delete the "<element>" element
    Then there is no <action_name> task for the "<element>" in the tasks list
    Examples:
      | action_name  | element        |
      | implantation | Beautiful bush |
      | removal      | Funny flower   |
