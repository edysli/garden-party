Feature: Inventory
  As an user
  In order to have a plain list of the elements in my garden
  I want to have a kind of inventory page

  Background:
    Given there is a resource named "Pipe-weed"
    And there is a resource named "Black pepper"
    And I have an account and i'm logged in
    And I have a map named "My garden"
    And I have a tree named "The tall one"
    And I have a tree named "The medium one" which I should remove tomorrow
    And I have a tree named "The small one" which I should implant tomorrow
    And I have a planted tree named "Apple tree"
    And I have a patch named "Field of North Lindon"
    And I have a patch named "Aromatics" with some "Pepper" in it
    And I access the "My garden" map
    And I access the inventory page

  Scenario: Access the inventory page
    When I click on the inventory link
    Then I see the inventory page

  Scenario: Display elements
    Then I see "The tall one" in the elements list

  Scenario: Display patches
    Then I see "Field of North Lindon" in the patches list

  Scenario: Check non-implanted elements
    Then I see that "The tall one" is not yet implanted in the elements list
    And I see that "Pepper" is not yet implanted in the "Aromatics" elements list

  Scenario: Add elements to a patch
    When I add some "Pipe-weed" in the "Field of North Lindon"
    And I see that "Pipe-weed" is present in the "Field of North Lindon" elements list

  Scenario: Implant an element now
    When I implant "The tall one" now
    Then I see that "The tall one" has been implanted today

  Scenario: Implant an element later
    When I plan to implant "The tall one" tomorrow
    Then I see that "The tall one" should be planted tomorrow

  Scenario: Place a planned element
    When I validate the implantation of "The small one"
    Then I see that "The small one" has been implanted today

  Scenario: Remove a planted element now
    When I remove the "Apple tree" now
    Then the "Apple tree" has been removed

  Scenario: Remove a planned element
    When I validate the removal of "The medium one"
    Then the "The medium one" has been removed

  Scenario: Edit an element in a patch
    When I change the "Pepper" to "Black pepper" in "Aromatics"
    Then the element changed to "Black pepper"

  Scenario: Rename an element
    When I rename "The tall one" to "Cpt Longtree"
    Then I see "Cpt Longtree" in the elements list

  Scenario: Duplicate an element
    When I duplicate "The tall one"
    Then I see "The tall one" duplicate in the elements list
