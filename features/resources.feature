Feature: Resources management
  As an user
  In order to improve resources for the community
  I want to be able to create some ne ones
  And I want to be able to edit existing ones

  Background:
    Given I have an account and i'm logged in
    And there is a resource named "Tomato"
    And there is a resource named "Kiwi"

  Scenario: Create new resource
    And I access the library page in app
    When I create the "Oak" resource
    Then I see "Oak" in the library

  Scenario: Edit existing resource
    And I access the library page in app
    When I rename resource "Tomato" to "Red thing"
    Then I see "Red thing" in the library
    And I don't see "Tomato" in the library

  Scenario: Display notes on a resource
    Given there is a visible note on the "Tomato" resource
    And there is an invisible note on the "Tomato" resource
    When I access the "Tomato" page in the library
    Then I see 1 unmodifiable note

  Scenario: Add a note on a resource
    Given I access the "Tomato" page in the library
    When I add a note on the resource
    Then I see 1 modifiable note

  Scenario: Edit a note on a resource
    Given I wrote a note on the "Tomato" resource
    And I access the "Tomato" page in the library
    When I change the note to "Grows well in sunny places"
    Then I see a note stating "Grows well in sunny places"

  Scenario: Destroy a note on a resource
    Given I wrote a note on the "Tomato" resource
    And I access the "Tomato" page in the library
    When I destroy the note
    Then there is no note

  Scenario: Search resource
    Given I access the library page in app
    When I search for "kiw" resource
    Then I see "Kiwi" in the library
    And I don't see "Tomato" in the library
