Feature: Tasks
  As an user
  In order to plan my actions
  I want to create tasks on things in the garden

  Background:
    Given I have an account and i'm logged in
    And I have a map named "My garden"
    And I have a patch named "Nice one" with some "Pipe-weed" in it
    And I have a tree named "Tally McShadow"
    And I planned to "Water" "Tally McShadow" yesterday
    And I access the "My garden" map
    And I access the inventory page

  Scenario: Add a task on the map
    Given I access the map
    And I create a task "Install fences" for the map
    When I access the tasks list
    Then I see the "Install fences" task

  Scenario: Add a task on a patch
    Given I create a task "Maw the lawn" for the "Nice one" patch
    When I access the tasks list
    Then I see the "Maw the lawn" task

  Scenario: Add a task on a path
    Given I create a task "Harvest" for the "Tally McShadow" element
    When I access the tasks list
    Then I see the "Harvest" task

  Scenario: See overdue tasks
    Given I access the tasks list
    Then I see that "Water" task is overdue

  Scenario: Finish a task
    Given I access the tasks list
    When I finish the "Water" task
    Then I don't see the "Water" task anymore
