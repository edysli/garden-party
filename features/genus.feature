Feature: Genus management
  As an user
  In order to improve resources for the community
  I want to be able to create some new genera
  And I want to be able to edit existing ones

  Background:
    Given I have an account and i'm logged in
    And there is a genus named "Abies"
    And there is a genus named "Solanum"

  Scenario: Create new genus
    Given I access the library page in app
    When I create the "Allium" genus
    Then I see "Allium" in the library's genus list

  Scenario: Edit existing genus
    Given I access the library page in app
    And I order the library by genus
    When I rename the "Solanum" genus to "Something else"
    Then I see "Something else" in the library's genus list
    And I don't see the "Solanum" genus anymore

  Scenario: Search genus
    Given I access the library page in app
    And I order the library by genus
    When I search for "abi" genus
    Then I see "Abies" in the library's genus list
    And I don't see the "Solanum" genus anymore
