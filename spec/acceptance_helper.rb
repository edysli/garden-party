require 'rails_helper'
require 'rspec_rails_api'

RSpec.configure do |config|
  config.include RSpec::Rails::Api::DSL::Example
end

renderer = RSpec::Rails::Api::OpenApiRenderer.new
# Options here should be customized
renderer.api_title       = 'garden_party'
renderer.api_version     = '1'
renderer.api_description = 'A wonderful Rails application'
# Options below are optional
renderer.api_servers = [{ url: 'http://localhost:3000' }]

RSpec.configuration.after(:context, type: :acceptance) do |context|
  renderer.merge_context context.class.metadata[:rra].to_h, dump_metadata: true
end

RSpec.configuration.after(:suite) do
  # Default path is 'tmp/rspec_rails_api_output.json/yaml'
  renderer.write_files Rails.public_path.join('swagger'), only: [:json]
end

module RSpec::Rails::Api::DSL::ExampleGroup
  ##
  # Helper method to quickly create a form error entity
  #
  # @param entity [Symbol]        The entity name
  # @param fields [Array<Symbol>] Fields that may appear in error response
  #
  # @return [void]
  def form_errors_entity(entity, fields)
    attributes = {}
    fields.each { |f| attributes[f] = { type: :array, required: false, description: 'Errors for this field' } }

    entity entity, attributes
  end
end
