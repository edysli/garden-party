require 'rails_helper'

require Rails.root.join 'db', 'migrate', '20211118053742_add_geometry_and_name_to_elements.rb'

RSpec.describe AddGeometryAndNameToElements, type: :migration do
  let(:now) { Time.current }

  let(:genera) { table(:genera) }
  let(:families) { table(:families) }
  let(:resources) { table(:resources) }

  let(:users) { table(:users) }

  let(:maps) { table(:maps) }
  let(:layers) { table(:layers) }
  let(:patches) { table(:patches) }
  let(:paths) { table(:paths) }
  let(:elements) { table(:elements) }

  let(:activities) { table(:activities) }
  let(:observations) { table(:observations) }
  let(:tasks) { table(:tasks) }

  def create_activity(type, id, user, map)
    activities.create! happened_at: Time.current, subject_type: type, subject_id: id, action: 'Something', username: user.username, user_id: user.id, map_id: map.id
  end

  def create_observation(type, id, user)
    observations.create! title: 'Some observation', user_id: user.id, subject_type: type, subject_id: id, made_at: Time.current
  end

  def create_task(type, id, user)
    tasks.create! name: 'Some task', subject_type: type, subject_id: id, planned_for: Time.current, assignee_id: user.id
  end

  def fill_database
    family   = families.create! name: 'some family', kingdom: 0
    genus    = genera.create! name: 'some genus', family_id: family.id
    resource = resources.create! name: 'some resource', latin_name: 'latin_name', genus_id: genus.id, color: '1,2,3'

    user  = users.create! username: 'JohnDoe', email: 'j.doe@example.com'
    map   = maps.create! name: 'My map', user_id: user.id, center: [0, 0]
    layer = layers.create! name: 'Content', map_id: map.id, position: 1

    paths.create! name: 'Nice line', geometry: {}, layer_id: layer.id, map_id: map.id

    # Create a patch with multiple elements
    patches.create!(name: 'Point patch', geometry: { geometry: { type: 'Circle' } }, layer_id: layer.id, map_id: map.id).tap do |patch|
      # 1 patch activity, observation and task, all kept after migration
      create_activity 'Patch', patch.id, user, map
      create_observation 'Patch', patch.id, user
      create_task 'Patch', patch.id, user

      # 2 element activity, observation and task, all kept after migration
      2.times do
        element = elements.create! resource_id: resource.id, patch_id: patch.id
        create_activity 'Element', element.id, user, map
        create_observation 'Element', element.id, user
        create_task 'Element', element.id, user
      end
    end

    # Create an invalid point patch
    patches.create!(name: 'Invalid point patch', geometry: { geometry: { type: 'Point' } }, layer_id: layer.id, map_id: map.id).tap do |point_patch|
      # 1 element activity, observation and task, after migration
      create_activity 'Patch', point_patch.id, user, map
      create_observation 'Patch', point_patch.id, user
      create_task 'Patch', point_patch.id, user
      # only 1 element activity, observation and task kept after migration
      2.times do
        element = elements.create! resource_id: resource.id, patch_id: point_patch.id
        create_activity 'Element', element.id, user, map
        create_observation 'Element', element.id, user
        create_task 'Element', element.id, user
      end
    end

    # Create a point patch
    patches.create!(name: 'Invalid point patch', geometry: { geometry: { type: 'Point' } }, layer_id: layer.id, map_id: map.id).tap do |point_patch|
      # 1 element activity, observation and task, after migration
      create_activity 'Patch', point_patch.id, user, map
      create_observation 'Patch', point_patch.id, user
      create_task 'Patch', point_patch.id, user

      # 1 element activity, observation and task, all kept after migration
      element = elements.create! resource_id: resource.id, patch_id: point_patch.id
      create_activity 'Element', element.id, user, map
      create_observation 'Element', element.id, user
      create_task 'Element', element.id, user
    end
  end

  it 'is reversible' do
    expect do
      fill_database

      migrate!
      reset_column_in_all_models

      schema_migrate_down!
    end.not_to raise_error
  end

  it 'creates migrates data' do
    fill_database

    migrate!
    reset_column_in_all_models
    # on patch:       1
    expect(activities.where(subject_type: 'Patch').count).to eq 1
    expect(observations.where(subject_type: 'Patch').count).to eq 1
    expect(tasks.where(subject_type: 'Patch').count).to eq 1
    # on elements:
    #   - patch       2
    #   - point-patch 2 (invalid point patch should be fixed)
    expect(activities.where(subject_type: 'Element').count).to eq 6
    expect(observations.where(subject_type: 'Element').count).to eq 6
    expect(tasks.where(subject_type: 'Element').count).to eq 6

    # Extraneous elements are removed
    expect(elements.count).to eq 4
    # No more point-patch
    expect(patches.count).to eq 1

    schema_migrate_down!
    reset_column_in_all_models
    expect(patches.count).to eq 3
  end
end
