require 'rails_helper'

require Rails.root.join 'db', 'migrate', '20220409062028_create_resource_interactions_groups.rb'
CreateResourceInteractionsGroups::BACKUP_FILE = Rails.root.join('tmp', 'rspec_backup_create_resource_interactions_groups.yml')

RSpec.describe CreateResourceInteractionsGroups, type: :migration do
  def delete_backup_file
    FileUtils.rm_f CreateResourceInteractionsGroups::BACKUP_FILE
    # Not optimal but sometimes the file is not yet totally destroyed by the OS
    # leading to re-reads and failing tests.
    # Sleeping less while File.exists? don't work either
    sleep 0.5
  end

  let(:families) { table(:families) }
  let(:genera) { table(:genera) }
  let(:resources) { table(:resources) }
  let(:resource_interactions) { table(:resource_interactions) }
  let(:resource_interactions_groups) { table(:resource_interactions_groups) }

  before do
    delete_backup_file
  end

  after do
    delete_backup_file
  end

  def fill_database
    family    = families.create! name: 'A family', kingdom: 1
    genus     = genera.create! name: 'A genera', family_id: family.id
    resource1 = resources.create! name: 'A resource', latin_name: 'Something', color: '0,0,0', genus_id: genus.id
    resource2 = resources.create! name: 'Another resource', latin_name: 'Something', color: '0,0,0', genus_id: genus.id
    resource3 = resources.create! name: 'The last resource', latin_name: 'Something', color: '0,0,0', genus_id: genus.id
    resource_interactions.create! nature: 1, subject_id: resource1.id, target_id: resource2.id
    resource_interactions.create! nature: 1, subject_id: resource1.id, target_id: resource3.id
  end

  it 'is reversible' do
    expect do
      fill_database

      migrate!
      reset_column_in_all_models

      schema_migrate_down!
    end.not_to raise_error
  end

  it 'backups and restores destroyed data' do
    fill_database
    expect(resource_interactions.count).to eq 2

    migrate!
    reset_column_in_all_models
    expect(resource_interactions.count).to eq 0

    # Delete a resource so its interaction is not restorable
    resources.last.destroy

    schema_migrate_down!

    expect(resource_interactions.count).to eq 1
  end
end
