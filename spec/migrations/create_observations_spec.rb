require 'rails_helper'

require Rails.root.join 'db', 'migrate', '20211022230027_create_observations.rb'
CreateObservations::BACKUP_FILE = Rails.root.join('tmp', 'rspec_backup_create_observations.yml')

RSpec.describe CreateObservations, type: :migration do
  def delete_backup_file
    FileUtils.rm_f CreateObservations::BACKUP_FILE
    # Not optimal but sometimes the file is not yet totally destroyed by the OS
    # leading to re-reads and failing tests.
    # Sleeping less while File.exists? don't work either
    sleep 0.5
  end

  let(:users) { table(:users) }
  let(:maps) { table(:maps) }
  let(:layers) { table(:layers) }
  let(:paths) { table(:paths) }

  let(:activities) { table(:activities) }
  let(:observations) { table(:observations) }

  before do
    delete_backup_file
  end

  after do
    delete_backup_file
  end

  def fill_database
    activities.destroy_all

    user  = users.create! role: 'user', email: 'john.doe@example.com', encrypted_password: 'abc123', username: 'John'
    map   = maps.create! name: 'A map', center: '0.0,0.0', user_id: user.id
    layer = layers.create! name: 'A layer', map_id: map.id, position: 1
    path  = paths.create! name: 'A path', map_id: map.id, layer_id: layer.id, geometry: '{}'

    now = Time.current
    activities.create! name: 'observe', subject_type: 'Map', subject_id: map.id, done_at: (now - 1.day), created_at: now, updated_at: now
    activities.create! name: 'observe', subject_type: 'Path', subject_id: path.id, done_at: (now - 1.day), created_at: now, updated_at: now
    activities.create! name: 'something_else', subject_type: 'Map', subject_id: map.id, done_at: (now - 1.day), created_at: now, updated_at: now
  end

  context 'when no new observation was created' do
    it 'is reversible' do
      fill_database

      migrate!
      reset_column_in_all_models
      expect(observations.count).to eq 2

      schema_migrate_down!

      # Nothing changed
      expect(activities.count).to eq 3
    end
  end

  context 'when new observation were created' do
    it 'is reversible' do
      fill_database

      migrate!
      reset_column_in_all_models

      now = Time.current
      map = maps.first
      user = users.first
      path = paths.first
      observations.create! title: 'A sunny day', content: 'Hello', subject_type: 'Map', user_id: user.id, subject_id: map.id, made_at: (now - 1.day), created_at: now, updated_at: now
      observations.create! title: 'A rainy day', content: 'Hello', subject_type: 'Path', user_id: user.id, subject_id: path.id, made_at: (now - 1.day), created_at: now, updated_at: now

      schema_migrate_down!

      # 3 activities + 2 new observations
      expect(activities.count).to eq 5
    end
  end

  context 'when activities were destroyed' do
    it 'is reversible' do
      fill_database

      migrate!
      reset_column_in_all_models
      activities.first.destroy

      schema_migrate_down!

      # 2 activities + 1 observation for the deleted activity
      expect(activities.count).to eq 3
    end
  end

  context 'when resources were unchanged' do
    it 'is reversible' do
      fill_database

      migrate!
      reset_column_in_all_models
      expect(observations.count).to eq 2

      delete_backup_file
      schema_migrate_down!

      # 3 old activities + 2 observations from UP
      expect(activities.count).to eq 5
    end
  end

  context 'when resources changed' do
    it 'is reversible' do
      fill_database

      migrate!
      reset_column_in_all_models

      now = Time.current
      map = maps.first
      user = users.first
      path = paths.first
      activities.first.destroy
      observations.create! title: 'Another day in the garden', content: 'Hello', subject_type: 'Map', user_id: user.id, subject_id: map.id, made_at: (now - 1.day), created_at: now, updated_at: now
      observations.create! title: 'Another day in the garden', content: 'Hello', subject_type: 'Path', user_id: user.id, subject_id: path.id, made_at: (now - 1.day), created_at: now, updated_at: now

      delete_backup_file
      schema_migrate_down!

      # 2 old activities + 4 observations (2 from UP + 2 new)
      expect(activities.count).to eq 6
    end
  end
end
