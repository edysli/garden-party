# Preview all emails at http://localhost:3000/rails/mailers/link
class LinkPreview < ActionMailer::Preview
  def resource_creation_email
    values = data

    LinkMailer.with(recipient: values[:admin], action: :creation, link: values[:link]).notify_email
  end

  def resource_update_email
    values = data

    LinkMailer.with(recipient: values[:admin], action: :update, link: values[:link]).notify_email
  end

  def resource_destruction_email
    values = data

    LinkMailer.with(recipient: values[:admin], action: :destruction, link: values[:link]).notify_email
  end

  private

  def data
    admin = User.admins.first
    link  = Link.first

    raise 'No admin in database' unless admin
    raise 'No link in database' unless link

    { admin: admin, link: link }
  end
end
