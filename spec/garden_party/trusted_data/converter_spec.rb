require 'rails_helper'
require 'garden_party/trusted_data/converter'

RSpec.describe GardenParty::TrustedData::Converter do
  let(:trusted_data) do
    data = YAML.load_file file_fixture('trusted_data.yml')
    GardenParty::TrustedData.send :complete_entries, data
  end

  let(:trusted_family) { trusted_data[:families].first }
  let(:trusted_genus) { trusted_data[:genera].first }
  let(:trusted_resource) { trusted_data[:resources].first }

  describe '.td_to_family' do
    it 'returns attributes for a family' do
      expected = { sync_id: 57, name: 'Rosaceae', kingdom: 'plant', source: 'https://fr.wikipedia.org/wiki/Rosaceae' }
      expect(described_class.td_to_family(trusted_family)).to eq expected
    end

    it 'returns a subset of attributes' do
      expected = { sync_id: 57, name: 'Rosaceae' }
      expect(described_class.td_to_family(trusted_family, only: [:id, :name])).to eq expected
    end
  end

  describe '.td_to_genus' do
    let(:family) { Family.create! sync_id: trusted_family[:id], name: trusted_family[:name], kingdom: trusted_family[:kingdom], source: trusted_family[:source] }

    it 'returns attributes for a genus' do # rubocop:disable RSpec/MultipleExpectations
      family
      expected = [:sync_id, :name, :family, :source]
      current = described_class.td_to_genus(trusted_genus)
      expect(current.keys.sort).to eq expected.sort
      expect(current[:family].id).to eq family.id
    end

    it 'returns a subset of attributes' do
      expected = { sync_id: 118, name: 'Prunus' }
      expect(described_class.td_to_genus(trusted_genus, only: [:id, :name])).to eq expected
    end
  end

  describe '.td_to_resource' do # rubocop:disable RSpec/MultipleMemoizedHelpers
    let(:family) { Family.create! sync_id: trusted_family[:id], name: trusted_family[:name], kingdom: trusted_family[:kingdom], source: trusted_family[:source] }
    let(:genus) { Genus.create! sync_id: trusted_genus[:id], name: trusted_genus[:name], family: family, source: trusted_genus[:source] }
    let(:resource_interactions_group) do
      group = trusted_data[:resource_interactions_groups].first
      ResourceInteractionsGroup.create! sync_id: group[:id], name: group[:name]
    end

    it 'returns attributes for a resource' do # rubocop:disable RSpec/MultipleExpectations, RSpec/ExampleLength
      genus
      resource_interactions_group
      expected = [
        :common_names,
        :description,
        :diameter,
        :genus,
        :name,
        :parent,
        :resource_interactions_group,
        :sources,
        :sow_in_soil_in_april, :sow_in_soil_in_august, :sow_in_soil_in_december, :sow_in_soil_in_february, :sow_in_soil_in_january, :sow_in_soil_in_july, :sow_in_soil_in_june, :sow_in_soil_in_march, :sow_in_soil_in_may, :sow_in_soil_in_november, :sow_in_soil_in_october, :sow_in_soil_in_september, :sow_sheltered_in_april,
        :sow_sheltered_in_august, :sow_sheltered_in_december, :sow_sheltered_in_february, :sow_sheltered_in_january, :sow_sheltered_in_july, :sow_sheltered_in_june, :sow_sheltered_in_march, :sow_sheltered_in_may, :sow_sheltered_in_november, :sow_sheltered_in_october, :sow_sheltered_in_september,
        :harvest_in_april, :harvest_in_august, :harvest_in_december, :harvest_in_february, :harvest_in_january, :harvest_in_july, :harvest_in_june, :harvest_in_march, :harvest_in_may, :harvest_in_november, :harvest_in_october, :harvest_in_september,
        :sync_id,
        :latin_name,
        :tag_list
      ]
      current = described_class.td_to_resource(trusted_resource)
      expect(current.keys.sort).to eq expected.sort
      expect(current[:genus].id).to eq genus.id
    end

    it 'returns a subset of attributes' do
      expected = { sync_id: 1, name: 'Abricotier', genus: genus }
      expect(described_class.td_to_resource(trusted_resource, only: [:id, :name, :genus])).to eq expected
    end
  end

  describe '.td_to_resource_interactions_group' do
    it 'returns attributes for a resource interactions group' do
      expected = [:name, :description, :sync_id]
      group = trusted_data[:resource_interactions_groups].first
      current = described_class.td_to_resource_interactions_group(group)
      expect(current.keys.sort).to eq expected.sort
    end

    it 'returns a subset of attributes' do
      expected = { sync_id: 1 }
      group = trusted_data[:resource_interactions_groups].first
      expect(described_class.td_to_resource_interactions_group(group, only: [:id])).to eq expected
    end
  end

  describe '.td_to_resource_interaction' do
    let(:resource_interactions_groups) do
      trusted_data[:resource_interactions_groups].each { |t| ResourceInteractionsGroup.create! sync_id: t[:sync_id], name: t[:name] }
    end

    it 'returns attributes for a resource interaction' do
      resource_interactions_groups
      expected = [:subject, :target, :nature, :notes, :sources, :sync_id]
      interaction = trusted_data[:resource_interactions].first
      current = described_class.td_to_resource_interaction(interaction)

      expect(current.keys.sort).to eq expected.sort
    end

    it 'returns a subset of attributes' do
      resource_interactions_groups
      expected = { sync_id: 1 }
      interaction = trusted_data[:resource_interactions].first
      expect(described_class.td_to_resource_interaction(interaction, only: [:id])).to eq expected
    end
  end

  describe '.copy_values' do
    it 'copies values' do
      hash     = { name: 'the name', description: 'the description', other: 'value' }
      expected = { name: 'the name', description: 'the description' }

      expect(described_class.copy_values(hash, [:name, :description])).to eq expected
    end

    it 'ignore fields' do
      hash     = { name: 'the name', description: 'the description', other: 'value' }
      expected = { name: 'the name' }

      expect(described_class.copy_values(hash, [:name, :description], only: [:name])).to eq expected
    end
  end
end
