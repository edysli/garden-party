FactoryBot.define do
  factory :team_mate do
    user
    map

    trait :accepted do
      accepted_at { Time.current }
    end
  end
end
