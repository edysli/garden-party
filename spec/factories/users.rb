FactoryBot.define do
  factory :user do
    email { Faker::Internet.unique.email }
    password { 'password' }
    username { Faker::Internet.unique.username separators: %w[- _], specifier: 3 }

    factory :user_known do
      email { 'user@example.com' }
    end

    factory :user_admin do
      role { 'admin' }

      factory :user_admin_known do
        email { 'admin@example.com' }
      end
    end
  end
end
