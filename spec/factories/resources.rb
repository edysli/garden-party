FactoryBot.define do
  factory :resource do
    name { Faker::Lorem.sentence(word_count: rand(1...3)).tr('.', '') }
    latin_name { Faker::Lorem.words.join ' ' }
    common_names { [] }
    genus

    trait :with_parent do
      association :parent, factory: :resource
    end
  end
end
