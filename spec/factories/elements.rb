FactoryBot.define do
  factory :element do
    resource

    trait :in_patch do
      patch
      map_id { nil }
      layer_id { nil }
      geometry { nil }
    end

    trait :implanted do
      implanted_at { Date.current }
    end

    trait :point do
      patch_id { nil }
      map
      layer_id { map.layers.first.id }
      geometry { { type: 'Feature', properties: nil, geometry: { type: 'Point', coordinates: [rand(100), rand(100)] } } }
    end
  end
end
