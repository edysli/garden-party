FactoryBot.define do
  factory :resource_interactions_group do
    name { Faker::Lorem.unique.word }
  end
end
