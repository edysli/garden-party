FactoryBot.define do
  factory :genus do
    name { Faker::Lorem.unique.word.titleize }
    family
  end
end
