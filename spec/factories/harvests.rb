FactoryBot.define do
  factory :harvest do
    harvested_at { Time.current }
    quantity { Faker::Number.decimal l_digits: 2, r_digits: 2 }
    unit { Harvest.units.keys.sample }

    association :element, factory: [:element, :point]
    user
  end
end
