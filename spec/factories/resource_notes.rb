FactoryBot.define do
  factory :resource_note do
    content { Faker::Lorem.paragraph }
    user
    resource
  end
end
