FactoryBot.define do
  factory :patch do
    map
    layer_id { map.layers.first.id }
    # Default trait
    circle

    trait :polygon do
      geometry do
        coordinates = []
        coordinates << [rand(100), rand(100)]
        coordinates << [coordinates[0][0] + rand(100), coordinates[0][1] + rand(10)]
        coordinates << [coordinates[0][1] - rand(50), coordinates[0][0] - rand(50)]
        # Last point MUST be at the same coordinates as the first to close the ring
        coordinates << coordinates[0]

        { type: 'Feature', properties: nil, geometry: { type: 'Polygon', coordinates: [coordinates] } }
      end
    end

    trait :circle do
      geometry { { type: 'Feature', properties: { radius: rand(50) }, geometry: { type: 'Point', coordinates: [rand(100), rand(100)] } } }
    end

    trait :with_element do
      after :create do |record|
        record.elements = [create(:element, patch_id: record.id)]
      end
    end
  end
end
