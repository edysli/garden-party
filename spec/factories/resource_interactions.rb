FactoryBot.define do
  factory :resource_interaction do
    association :subject, factory: [:resource_interactions_group]
    association :target, factory: [:resource_interactions_group]
    nature { 0 }
  end
end
