require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::ElementsController, type: :acceptance do
  resource 'Elements', 'Manage elements'

  entity :element,
         id:                       { type: :integer, description: 'Entity identifier' },
         # Point
         name:                     { type: :string, required: false, description: 'Element name' },
         diameter:                 { type: :number, required: false, description: 'Current diameter in meters, when element is a point' },
         geometry:                 { type: :number, required: false, description: 'GeoJSON geometry when element is a point' },
         layer_id:                 { type: :number, required: false, description: 'Layer identifier when element is a point' },
         # Patch
         patch_id:                 { type: :integer, description: 'Patch identifier when not a point' },
         # Other fields
         resource_id:              { type: :integer, description: 'Resource identifier' },
         implantation_mode:        { type: :number, required: false, description: 'How the element was implanted' },
         implanted_at:             { type: :datetime, required: false, description: 'Implantation date' },
         implantation_planned_for: { type: :datetime, required: false, description: 'Planned implantation date' },
         removed_at:               { type: :datetime, required: false, description: 'Removal date' },
         removal_planned_for:      { type: :datetime, required: false, description: 'Planned removal date' },
         status:                   { type: :string, description: 'Element status' },
         created_at:               { type: :datetime, description: 'Creation date' },
         updated_at:               { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  form_errors_entity :element_form_errors, [:name, :implanted_at, :implantation_planned_for, :removed_at, :removal_planned_for, :resource, :patch_id, :geometry, :layer_id, :map_id]

  parameters :create_update_payload,
             name:                     { type: :string, required: false, description: 'Custom element name' },
             diameter:                 { type: :string, required: false, description: 'Actual diameter in meters' },
             implantation_mode:        { type: :number, required: false, description: 'How the element was implanted' },
             implantation_planned_for: { type: :datetime, required: false, description: 'Planned implantation date' },
             removed_at:               { type: :datetime, required: false, description: 'Removal date' },
             removal_planned_for:      { type: :datetime, required: false, description: 'Planned removal date' },
             resource_id:              { type: :string, description: 'Resource identifier' },
             # Point
             implanted_at:             { type: :datetime, required: false, description: 'Implantation date' },
             geometry:                 { type: :number, required: false, description: 'GeoJSON geometry when element is a point' },
             layer_id:                 { type: :number, required: false, description: 'Layer identifier when element is a point' },
             # In a patch
             patch_id:                 { type: :string, description: 'Patch identifier' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    let(:owned_map) { FactoryBot.create :map, user: signed_in_user }
    let(:owned_patch) { FactoryBot.create :patch, map: owned_map }
    let(:owned_elements) { FactoryBot.create_list :element, 2, patch: owned_patch }
    let(:owned_element) { FactoryBot.create :element, patch: owned_patch }

    on_get('/api/maps/:map_id/elements', 'List elements on a map') do
      path_params fields: { map_id: { type: :integer, description: 'Target map identifier' } }

      for_code 200, expect_many:  :element do |url|
        owned_elements
        test_response_of url, path_params: { map_id: owned_map.id }
      end
    end

    on_get('/api/patches/:patch_id/elements', 'List elements in a patch') do
      path_params fields: { patch_id: { type: :integer, description: 'Target patch identifier' } }

      for_code 200, expect_many: :element do |url|
        owned_elements
        test_response_of url, path_params: { patch_id: owned_patch.id }
      end
    end

    on_get('/api/elements/:id', 'Show one element') do
      path_params defined: :path_params

      for_code 200, expect_one: :element do |url|
        test_response_of url, path_params: { id: owned_element.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post('/api/elements', 'Create new element') do
      request_params defined: :create_update_payload
      let(:element_attributes) do
        resource = FactoryBot.create :resource
        FactoryBot.build(:element, patch: owned_patch, resource: resource).attributes
      end

      for_code 201, expect_one: :element do |url|
        test_response_of url, payload: element_attributes
      end

      for_code 422, expect_one: :element_form_errors do |url|
        test_response_of url, payload: { patch_id: owned_patch.id, geometry: {} }
      end
    end

    on_put('/api/elements/:id', 'Update an element') do
      path_params defined: :path_params
      request_params defined: :create_update_payload

      for_code 200, expect_one: :element do |url|
        test_response_of url, path_params: { id: owned_element.id }, payload: { implanted_at: Time.current }
      end

      for_code 422, expect_one: :element_form_errors do |url|
        now = Time.current
        test_response_of url, path_params: { id: owned_element.id }, payload: { removal_planned_for: (now - 2.days), implantation_planned_for: now }
      end
    end

    on_post('/api/elements/:id/duplicate', 'Duplicates an element and re-create implantation/removal tasks if needed') do
      path_params defined: :path_params

      for_code 201, expect_one: :element do |url|
        test_response_of url, path_params: { id: owned_element.id }
      end
    end

    on_delete('/api/elements/:id', 'Destroy an element') do
      path_params defined: :path_params

      for_code 204 do |url|
        test_response_of url, path_params: { id: owned_element.id }
      end
    end
  end
end
