require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::ObservationsController, type: :acceptance do
  resource 'Observations', 'Manage observations'

  entity :observation,
         id:           { type: :integer, description: 'Entity identifier' },
         title:        { type: :string, description: 'Observation title' },
         content:      { type: :string, required: false, description: 'Additional notes' },
         made_at:      { type: :datetime, required: false, description: 'Date when the observation was performed' },
         user_id:      { type: :integer, description: 'Identifier of the observer' },
         subject_id:   { type: :integer, description: 'Subject identifier' },
         subject_type: { type: :string, description: 'Subject type' },
         created_at:   { type: :datetime, description: 'Creation date' },
         updated_at:   { type: :datetime, description: 'Update date' },
         pictures:     { type: :array, description: 'List of URLS to attached pictures', of: {
           source:    { type: :string, description: 'Full picture URL' },
           thumbnail: { type: :string, description: 'Thumbnail URL' },
         } }

  entity :error,
         error: { type: :string, description: 'The error' }

  form_errors_entity :observation_form_errors, [:title, :content, :subject, :pictures]

  parameters :create_payload,
             title:        { type: :string, description: 'Observation title' },
             content:      { type: :string, required: false, description: 'Additional notes' },
             made_at:      { type: :datetime, required: false, description: 'Date when the observation was performed' },
             user_id:      { type: :integer, description: 'Identifier of the observer' },
             subject_id:   { type: :integer, description: 'Subject identifier' },
             subject_type: { type: :string, description: 'Subject type' },
             pictures:     { type: :array, description: 'List of URLS to attached pictures' }
  parameters :update_payload,
             title:   { type: :string, description: 'Observation title' },
             content: { type: :string, required: false, description: 'Additional notes. Required when no picture is attached' },
             made_at: { type: :datetime, required: false, description: 'Date when the observation was performed' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    let(:owned_map) { FactoryBot.create :map, user: signed_in_user }
    let(:owned_patch) { FactoryBot.create :patch, :with_element, map: owned_map }
    let(:owned_path) { FactoryBot.create :path, map: owned_map }
    let(:owned_observation) { FactoryBot.create :observation, subject: owned_patch.elements.first }

    describe 'lists' do
      before do
        FactoryBot.create_list :observation, 2, subject: owned_patch.elements.first
        FactoryBot.create_list :observation, 2, subject: owned_map
        FactoryBot.create_list :observation, 2, subject: owned_patch
        FactoryBot.create_list :observation, 2, subject: owned_path
      end

      on_get('/api/elements/:element_id/observations', 'List observations for an element') do
        path_params fields: { element_id: { type: :integer, description: 'Target element identifier' } }

        for_code 200, expect_many: :observation do |url|
          test_response_of url, path_params: { element_id: owned_patch.elements.first.id }
        end
      end

      on_get('/api/maps/:map_id/observations', 'List observations for a given map') do
        path_params fields: { map_id: { type: :integer, description: 'Target map identifier' } }

        for_code 200, expect_many:  :observation do |url|
          test_response_of url, path_params: { map_id: owned_map.id }
        end
      end

      on_get('/api/patches/:patch_id/observations', 'List observations for a given patch') do
        path_params fields: { patch_id: { type: :integer, description: 'Target patch identifier' } }

        for_code 200, expect_many: :observation do |url|
          test_response_of url, path_params: { patch_id: owned_patch.id }
        end
      end

      on_get('/api/paths/:path_id/observations', 'List observations for a given path') do
        path_params fields: { path_id: { type: :integer, description: 'Target path identifier' } }

        for_code 200, expect_many: :observation do |url|
          test_response_of url, path_params: { path_id: owned_path.id }
        end
      end

      on_get('/api/maps/:map_id/all_observations', 'List observations for a map and its elements, patches and paths') do
        path_params fields: { map_id: { type: :integer, description: 'Target map identifier' } }

        for_code 200, expect_many:  :observation do |url|
          test_response_of url, path_params: { map_id: owned_map.id }
        end
      end
    end

    on_get('/api/observations/:id', 'Show one observation') do
      path_params defined: :path_params

      for_code 200, expect_one: :observation do |url|
        test_response_of url, path_params: { id: owned_observation.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post('/api/observations', 'Create new observation') do
      request_params defined: :create_payload
      let(:observation_attributes) do
        FactoryBot.build(:observation, subject: owned_patch.elements.first).attributes
      end

      for_code 201, expect_one: :observation do |url|
        test_response_of url, payload: observation_attributes
      end

      for_code 422, expect_one: :observation_form_errors do |url|
        bad_attributes = observation_attributes.merge 'title' => ''
        test_response_of url, payload: bad_attributes
      end
    end

    on_put('/api/observations/:id', 'Update an observation') do
      path_params defined: :path_params
      request_params defined: :update_payload

      for_code 200, expect_one: :observation do |url|
        test_response_of url, path_params: { id: owned_observation.id }, payload: { title: 'New name' }
      end

      for_code 422, expect_one: :observation_form_errors do |url|
        test_response_of url, path_params: { id: owned_observation.id }, payload: { title: nil }
      end
    end

    on_delete('/api/observations/:id', 'Destroys an observation') do
      path_params defined: :path_params

      for_code 204 do |url|
        test_response_of url, path_params: { id: owned_observation.id }
      end
    end
  end
end
