require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::GeneraController, type: :acceptance do
  resource 'Genera', 'Manage genera'

  entity :genus,
         id:         { type: :integer, description: 'Entity identifier' },
         name:       { type: :string, description: 'Genus name' },
         source:     { type: :string, required: false, description: 'Link to description source' },
         family_id:  { type: :integer, description: 'Family identifier' },
         sync_id:    { type: :integer, required: false, description: 'Trusted data source identifier' },
         created_at: { type: :datetime, description: 'Creation date' },
         updated_at: { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  form_errors_entity :genus_form_errors, [:name, :family, :source]

  parameters :create_update_payload,
             name:      { type: :string, description: 'Genus name' },
             family_id: { type: :integer, description: 'Family identifier' },
             sync_id:   { type: :integer, required: false, description: 'Trusted data source identifier. Available to admins only' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/genera', 'List genera') do
      for_code 200, expect_many: :genus do |url|
        FactoryBot.create_list :genus, 2
        test_response_of url
      end
    end

    on_get('/api/genera/:id', 'Show one genus') do
      path_params defined: :path_params

      for_code 200, expect_one: :genus do |url|
        genus = FactoryBot.create :genus
        test_response_of url, path_params: { id: genus.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post('/api/genera', 'Create new genus') do
      request_params defined: :create_update_payload

      for_code 201, expect_one: :genus do |url|
        genus_attributes = FactoryBot.build(:genus).attributes
        test_response_of url, payload: genus_attributes
      end

      for_code 422, expect_one: :genus_form_errors do |url|
        test_response_of url, payload: { name: '' }
      end
    end

    on_put('/api/genera/:id', 'Update a genus') do
      path_params defined: :path_params
      request_params defined: :create_update_payload
      let(:genus) { FactoryBot.create :genus }

      for_code 200, expect_one: :genus do |url|
        test_response_of url, path_params: { id: genus.id }, payload: { name: 'New name' }
      end

      for_code 422, expect_one: :genus_form_errors do |url|
        test_response_of url, path_params: { id: genus.id }, payload: { name: '' }
      end
    end
  end

  context 'with authenticated admin' do
    include_context 'with authenticated admin'

    on_delete('/api/genera/:id', 'Destroys a genus') do
      path_params defined: :path_params
      let(:genus) { FactoryBot.create :genus }

      for_code 204 do |url|
        test_response_of url, path_params: { id: genus.id }
      end
    end
  end
end
