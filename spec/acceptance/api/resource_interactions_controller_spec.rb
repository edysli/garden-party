require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::ResourceInteractionsController, type: :acceptance do
  resource 'Resource interactions', 'Manage resource interactions'

  entity :resource_interaction,
         id:         { type: :integer, description: 'Entity identifier' },
         notes:      { type: :string, required: false, description: 'Notes on this interaction' },
         nature:     { type: :string, description: 'Interaction type' },
         sources:    { type: :string, required: false, description: 'Links, books,... one per line' },
         subject_id: { type: :integer, description: 'Resource interaction group identifier, from which the interaction is made' },
         target_id:  { type: :integer, description: 'Resource interaction group identifier, on which the interaction is made' },
         sync_id:    { type: :integer, required: false, description: 'Trusted data source identifier' },
         created_at: { type: :datetime, description: 'Creation date' },
         updated_at: { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  form_errors_entity :resource_interaction_form_errors, [:nature, :notes, :subject, :target]

  parameters :create_update_payload,
             nature:     { type: :string, required: false, description: 'Interaction of subject on target. Accepted values are "competition", "mutualism", "predation", "parasitism", "pathogens" or "herbivory"' },
             notes:      { type: :string, required: false, description: 'Notes on this interaction' },
             sources:    { type: :string, required: false, description: 'Links, books,... one per line' },
             subject_id: { type: :integer, description: 'Group identifier, from which the interaction is made' },
             target_id:  { type: :integer, description: 'Group identifier, on which the interaction is made' },
             sync_id:    { type: :integer, required: false, description: 'Trusted data source identifier. Available to admins only' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/resource_interactions', 'List resources interactions') do
      for_code 200, expect_many: :resource_interaction do |url|
        FactoryBot.create_list :resource_interaction, 2
        test_response_of url
      end
    end

    on_get('/api/resource_interactions/:id', 'Show one interaction') do
      path_params defined: :path_params

      for_code 200, expect_one: :resource_interaction do |url|
        resource_interaction = FactoryBot.create :resource_interaction
        test_response_of url, path_params: { id: resource_interaction.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post('/api/resource_interactions', 'Create new interaction') do
      request_params defined: :create_update_payload

      for_code 201, expect_one: :resource_interaction do |url|
        resource_interaction_attributes = FactoryBot.build(:resource_interaction).attributes
        test_response_of url, payload: resource_interaction_attributes
      end

      for_code 422, expect_one: :resource_interaction_form_errors do |url|
        test_response_of url, payload: { target_id: 0, subject_id: 0 }
      end
    end

    on_put('/api/resource_interactions/:id', 'Update an interaction') do
      path_params defined: :path_params
      request_params defined: :create_update_payload
      let(:resource_interaction) { FactoryBot.create :resource_interaction }

      for_code 200, expect_one:  :resource_interaction do |url|
        test_response_of url, path_params: { id: resource_interaction.id }, payload: { notes: 'Explanation:...' }
      end

      for_code 422, expect_one: :resource_interaction_form_errors do |url|
        test_response_of url, path_params: { id: resource_interaction.id }, payload: { subject_id: 'failure' }
      end
    end
  end

  context 'with authenticated admin' do
    include_context 'with authenticated admin'

    on_delete('/api/resource_interactions/:id', 'Destroys an interaction') do
      path_params defined: :path_params
      let(:resource_interaction) { FactoryBot.create :resource_interaction }

      for_code 204 do |url|
        test_response_of url, path_params: { id: resource_interaction.id }
      end
    end
  end
end
