require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::ResourcesController, type: :acceptance do
  resource 'Resources', 'Manage resources'

  entity :resource,
         id:                             { type: :integer, description: 'Entity identifier' },
         name:                           { type: :string, description: 'Resource name' },
         latin_name:                     { type: :string, description: 'Latin name' },
         common_names:                   { type: :array, required: false, description: 'Other common names' },
         color:                          { type: :string, description: 'Color to use as base to represent the resource' },
         generic:                        { type: :boolean, description: 'Whether or not this resource is generic' },
         parent_id:                      { type: :integer, required: false, description: 'Parent identifier, for plants with variants' },
         description:                    { type: :string, required: false, description: 'Resource description' },
         sources:                        { type: :array, required: false, description: 'Links to description sources' },
         diameter:                       { type: :float, required: false, description: 'Theoretical final diameter to represent clutter (in meters)' },
         tag_list:                       { type: :array, description: 'List of tags' },
         genus_id:                       { type: :integer, description: 'Resource family identifier' },
         resource_interactions_group_id: { type: :integer, required: false, description: 'Interactions group identifier' },
         sync_id:                        { type: :integer, required: false, description: 'Trusted data source identifier' },
         sheltered_sowing_months:        { type: :array, description: 'List of flags for which the resource can be sowed under shelter' },
         soil_sowing_months:             { type: :array, description: 'List of flags for which the resource can be sowed in soil' },
         harvesting_months:              { type: :array, description: 'List of flags for which the resource can be harvested' },
         created_at:                     { type: :datetime, description: 'Creation date' },
         updated_at:                     { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  form_errors_entity :resource_form_errors, [:name, :latin_name, :description, :common_names, :genus, :parent]

  parameters :create_update_payload,
             name:                           { type: :string, description: 'Resource name' },
             latin_name:                     { type: :string, description: 'Latin name' },
             description:                    { type: :string, required: false, description: 'Resource description' },
             sources:                        { type: :array, required: false, description: 'Links to description sources' },
             diameter:                       { type: :float, required: false, description: 'Theoretical final diameter to represent clutter (in meters)' },
             common_names:                   { type: :array, required: false, description: 'Other common names' },
             genus_id:                       { type: :integer, description: 'Genus identifier' },
             parent_id:                      { type: :integer, required: false, description: 'Parent resource identifier' },
             resource_interactions_group_id: { type: :integer, required: false, description: 'Interactions group identifier' },
             sheltered_sowing_months:        { type: :array, required: false, description: 'List of flags for which the resource can be sowed under shelter' },
             soil_sowing_months:             { type: :array, required: false, description: 'List of flags for which the resource can be sowed in soil' },
             harvesting_months:              { type: :array, required: false, description: 'List of flags for which the resource can be harvested' },
             sync_id:                        { type: :integer, required: false, description: 'Trusted data source identifier. Available to admins only' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/resources', 'List resources') do
      for_code 200, expect_many: :resource do |url|
        FactoryBot.create_list :resource, 2
        test_response_of url
      end
    end

    on_get('/api/resources/:id', 'Show one resource') do
      path_params defined: :path_params

      for_code 200, expect_one: :resource do |url|
        resource = FactoryBot.create :resource
        test_response_of url, path_params: { id: resource.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post('/api/resources', 'Create new resource') do
      request_params defined: :create_update_payload

      for_code 201, expect_one: :resource do |url|
        resource_attributes = FactoryBot.build(:resource).attributes
        test_response_of url, payload: resource_attributes
      end

      for_code 422, expect_one: :resource_form_errors do |url|
        test_response_of url, payload: { name: '' }
      end
    end

    on_put('/api/resources/:id', 'Update a resource') do
      path_params defined: :path_params
      request_params defined: :create_update_payload
      let(:resource) { FactoryBot.create :resource }

      for_code 200, expect_one: :resource do |url|
        test_response_of url, path_params: { id: resource.id }, payload: { name: 'New name' }
      end

      for_code 422, expect_one: :resource_form_errors do |url|
        test_response_of url, path_params: { id: resource.id }, payload: { name: '' }
      end
    end
  end

  context 'with authenticated admin' do
    include_context 'with authenticated admin'

    on_delete('/api/resources/:id', 'Destroys a resource') do
      path_params defined: :path_params
      let(:resource) { FactoryBot.create :resource }

      for_code 204 do |url|
        test_response_of url, path_params: { id: resource.id }
      end
    end
  end
end
