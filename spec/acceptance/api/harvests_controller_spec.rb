require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::HarvestsController, type: :acceptance do
  resource 'Harvests', 'Manage harvests'

  entity :harvest,
         id:           { type: :integer, description: 'Entity identifier' },
         quantity:     { type: :number, description: 'Quantity' },
         unit:         { type: :string, description: 'Unit' },
         user_id:      { type: :integer, description: 'Identifier of the harvester' },
         element_id:   { type: :integer, description: 'Associated element identifier' },
         harvested_at: { type: :datetime, description: 'Harvest date' },
         created_at:   { type: :datetime, description: 'Creation date' },
         updated_at:   { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  form_errors_entity :harvest_form_errors, [:quantity, :unit, :element, :user_id]

  parameters :create_payload,
             quantity:     { type: :string, description: 'Quantity' },
             unit:         { type: :integer, description: 'Unit' },
             element_id:   { type: :integer, description: 'Associated element identifier' },
             harvested_at: { type: :datetime, description: 'Harvest date' }

  parameters :update_payload,
             quantity:     { type: :string, description: 'Quantity' },
             unit:         { type: :integer, description: 'Unit' },
             harvested_at: { type: :datetime, description: 'Harvest date' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    let(:owned_map) { FactoryBot.create :map, user: signed_in_user }
    let(:owned_element) { FactoryBot.create :element, :point, map: owned_map }
    let(:owned_harvest) { FactoryBot.create :harvest, element: owned_element, user: signed_in_user }

    describe 'lists' do
      before do
        # Harvests on known element
        FactoryBot.create_list :harvest, 2, element: owned_element
        # Harvests on other map elements
        FactoryBot.create_list :harvest, 2, element: FactoryBot.create(:element, :point, map: owned_map)
      end

      on_get('/api/elements/:element_id/harvests', 'List harvests for an element') do
        path_params fields: { element_id: { type: :integer, description: 'Target element identifier' } }

        for_code 200, expect_many: :harvest do |url|
          test_response_of url, path_params: { element_id: owned_element.id }
        end
      end

      on_get('/api/maps/:map_id/all_harvests', 'List all harvests for elements in a given map') do
        path_params fields: { map_id: { type: :integer, description: 'Target map identifier' } }

        for_code 200, expect_many:  :harvest do |url|
          test_response_of url, path_params: { map_id: owned_map.id }
        end
      end
    end

    on_get('/api/harvests/:id', 'Show one harvest') do
      path_params defined: :path_params

      for_code 200, expect_one: :harvest do |url|
        test_response_of url, path_params: { id: owned_harvest.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post('/api/harvests', 'Create new harvest') do
      request_params defined: :create_payload
      let(:harvest_attributes) do
        FactoryBot.build(:harvest, element: owned_element).attributes
      end

      for_code 201, expect_one: :harvest do |url|
        test_response_of url, payload: harvest_attributes
      end

      for_code 422, expect_one: :harvest_form_errors do |url|
        bad_attributes = harvest_attributes.merge 'unit' => ''
        test_response_of url, payload: bad_attributes
      end
    end

    on_put('/api/harvests/:id', 'Update an harvest') do
      path_params defined: :path_params
      request_params defined: :update_payload

      for_code 200, expect_one: :harvest do |url|
        test_response_of url, path_params: { id: owned_harvest.id }, payload: { quantity: 10 }
      end

      for_code 422, expect_one: :harvest_form_errors do |url|
        test_response_of url, path_params: { id: owned_harvest.id }, payload: { quantity: nil }
      end
    end

    on_delete('/api/harvests/:id', 'Discards a harvest') do
      path_params defined: :path_params

      for_code 204 do |url|
        test_response_of url, path_params: { id: owned_harvest.id }
      end
    end
  end
end
