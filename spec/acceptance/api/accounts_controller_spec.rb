require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::AccountsController, type: :acceptance do
  resource 'Account', 'Manage account information'

  entity :preferences,
         map:   { type: :integer, required: false, description: 'Favorite map identifier' },
         theme: { type: :string, required: false, description: 'Application theme. Null is for browser/system theme' }

  entity :user_account,
         id:          { type: :integer, description: 'User identifier' },
         role:        { type: :string, description: 'User role (user or admin)' },
         email:       { type: :string, description: 'Email address' },
         username:    { type: :string, description: 'Username' },
         preferences: { type: :object, description: 'Preferences', attributes: :preferences },
         created_at:  { type: :datetime, description: 'Account creation date' },
         updated_at:  { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  # We only support preference edition for now
  form_errors_entity :user_account_form_errors, [:map, :theme]

  parameters :update_account_payload,
             preferences: { type: :object, required: true, description: 'New user preferences', attributes: {
               map:   { type: :integer, required: true, description: 'The new value, or unchanged one. Should be present anyway and `null` is supported.' },
               theme: { type: :string, required: true, description: 'The new value, or unchanged one. Should be present anyway and `null` is supported.' },
             } }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/account', 'Display account information') do
      for_code 200, expect_one: :user_account do |url|
        test_response_of url
      end
    end

    on_put('/api/account', 'Update account information') do
      request_params defined: :update_account_payload
      for_code 200, expect_one: :user_account do |url|
        test_response_of url, payload: { preferences: { theme: 'light', map: nil } }
      end
    end
  end
end
