require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::NotificationsController, type: :acceptance do
  resource 'Notifications', 'Manage notifications'

  entity :user,
         id:            { type: :integer, description: 'Entity identifier' },
         role:          { type: :string, description: 'Either "user" or "admin"' },
         username:      { type: :string, description: 'Username' },
         account_state: { type: :string, description: 'Either "valid" or "pending"' }

  entity :notification,
         id:           { type: :integer, description: 'Entity identifier' },
         type:         { type: :string, description: 'Type of notification' },
         content:      { type: :object, required: false, description: 'Custom properties' },
         subject_type: { type: :string, required: false, description: 'Subject class name' },
         subject_id:   { type: :integer, required: false, description: 'Subject identifier' },
         subject:      { type: :object, required: false, description: 'Subject, if any' },
         recipient_id: { type: :integer, description: 'Subject identifier' },
         sender_id:    { type: :integer, required: false, description: 'Sender identifier' },
         sender:       { type: :object, required: false, description: 'Sender object, if any', attributes: :user },
         archived_at:  { type: :datetime, required: false, description: 'Time when the notification has been considered as seen' },
         created_at:   { type: :datetime, description: 'Creation date' },
         updated_at:   { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/notifications', 'List notifications') do
      for_code 200, expect_many: :notification do |url|
        FactoryBot.create_list :notification, 2, recipient: signed_in_user
        test_response_of url
      end
    end

    on_get('/api/notifications/:id', 'Show one notification') do
      path_params defined: :path_params

      for_code 200, expect_one: :notification do |url|
        notification = FactoryBot.create :notification, recipient: signed_in_user
        test_response_of url, path_params: { id: notification.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_patch('/api/notifications/:id/archive', 'Mark a notification as seen') do
      path_params defined: :path_params

      for_code 200, expect_one: :notification do |url|
        notification = FactoryBot.create :notification, recipient: signed_in_user
        test_response_of url, path_params: { id: notification.id }
      end
    end

    on_delete('/api/notifications/:id', 'Destroys a notification') do
      path_params defined: :path_params

      for_code 204 do |url|
        notification = FactoryBot.create :notification, recipient: signed_in_user
        test_response_of url, path_params: { id: notification.id }
      end
    end

    on_patch('/api/notifications/archive_all', 'Marks unread notifications as archived') do
      before do
        FactoryBot.create_list :notification, 3, recipient: signed_in_user
        FactoryBot.create_list :notification, 2, archived_at: Time.current, recipient: signed_in_user
      end

      for_code 200, expect_many: :notification do |url|
        test_response_of url
      end

      # rubocop:disable RSpec/MultipleExpectations
      for_code 200, 'returns the correct amount', expect_many: :notification, test_only: true do |url|
        test_response_of url

        expect(JSON.parse(response.body).count).to eq(3)
      end
      # rubocop:enable RSpec/MultipleExpectations
    end

    on_delete('/api/notifications/destroy_all_archived', 'Deletes all archived notifications') do
      before do
        FactoryBot.create_list :notification, 3, recipient: signed_in_user
        FactoryBot.create_list :notification, 2, archived_at: Time.current, recipient: signed_in_user
      end

      for_code 200, expect_many: :notification do |url|
        test_response_of url
      end

      # rubocop:disable RSpec/MultipleExpectations
      for_code 200, 'returns the correct amount', expect_many: :notification, test_only: true do |url|
        test_response_of url

        expect(JSON.parse(response.body).count).to eq(3)
      end
      # rubocop:enable RSpec/MultipleExpectations
    end
  end
end
