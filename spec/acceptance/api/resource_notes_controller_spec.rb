require 'rails_helper'
require 'acceptance_helper'

RSpec.describe Api::ResourceNotesController, type: :acceptance do
  resource 'ResourcesNotes', 'Manage resource notes'

  entity :resource_note,
         id:          { type: :integer, description: 'Entity identifier' },
         content:     { type: :string, description: 'Note content' },
         user_id:     { type: :integer, required: false, description: 'User identifier, if current user is the author' },
         username:    { type: :string, description: 'Author username' },
         visible:     { type: :boolean, description: 'Whether or not the note is visible by the community' },
         resource_id: { type: :integer, description: 'Target resource identifier' },
         created_at:  { type: :datetime, description: 'Creation date' },
         updated_at:  { type: :datetime, description: 'Update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  form_errors_entity :resource_note_form_errors, [:content, :user, :resource]

  parameters :create_payload,
             content:     { type: :string, description: 'Note content' },
             visible:     { type: :boolean, description: 'Whether or not the note is visible by the community' },
             resource_id: { type: :integer, description: 'Target resource identifier' }

  parameters :update_payload,
             content: { type: :string, description: 'Note content' },
             visible: { type: :boolean, description: 'Whether or not the note is visible by the community' }

  parameters :path_params,
             id: { type: :integer, description: 'Target entity identifier' }

  context 'with authenticated member' do
    include_context 'with authenticated member'

    on_get('/api/resources/:resource_id/notes', 'List notes for a given resource') do
      path_params fields: { resource_id: { type: :integer, description: 'Target resource identifier' } }

      for_code 200, expect_many: :resource_note do |url|
        resource = FactoryBot.create :resource
        FactoryBot.create_list :resource_note, 2, resource: resource
        test_response_of url, path_params: { resource_id: resource.id }
      end
    end

    on_get('/api/resource_notes/:id', 'Show one note') do
      path_params defined: :path_params

      for_code 200, expect_one: :resource_note do |url|
        resource_note = FactoryBot.create :resource_note
        test_response_of url, path_params: { id: resource_note.id }
      end

      for_code 404, expect_one: :error do |url|
        test_response_of url, path_params: { id: 0 }
      end
    end

    on_post('/api/resource_notes', 'Create new note') do
      request_params defined: :create_payload

      for_code 201, expect_one:  :resource_note do |url|
        resource_note_attributes = FactoryBot.build(:resource_note).attributes
        test_response_of url, payload: resource_note_attributes
      end

      for_code 422, expect_one: :resource_note_form_errors do |url|
        test_response_of url, payload: { content: '' }
      end
    end

    on_put('/api/resource_notes/:id', 'Update a note') do
      path_params defined: :path_params
      request_params defined: :update_payload
      let(:resource_note) { FactoryBot.create :resource_note, user: signed_in_user }

      for_code 200, expect_one: :resource_note do |url|
        test_response_of url, path_params: { id: resource_note.id }, payload: { content: 'Something interesting' }
      end

      for_code 422, expect_one: :resource_note_form_errors do |url|
        test_response_of url, path_params: { id: resource_note.id }, payload: { content: '' }
      end
    end

    on_delete('/api/resource_notes/:id', 'Destroys a note') do
      path_params defined: :path_params
      let(:resource_note) { FactoryBot.create :resource_note, user: signed_in_user }

      for_code 204 do |url|
        test_response_of url, path_params: { id: resource_note.id }
      end
    end
  end
end
