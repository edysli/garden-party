require 'rails_helper'

RSpec.describe Path, type: :model do
  let(:map) { FactoryBot.create :map }

  describe 'scopes' do
    describe '.kept' do
      it 'ignores records of discarded layers' do
        layer           = map.layers.first
        discarded_layer = FactoryBot.create :layer, map: map, discarded_at: Time.current
        FactoryBot.create :path, layer: layer
        FactoryBot.create :path, layer: discarded_layer
        expect(described_class.kept.count).to eq 1
      end
    end

    describe '.kept_for_map' do
      context 'with paths on discarded and undiscarded layers' do
        before do
          layer           = map.layers.first
          discarded_layer = FactoryBot.create :layer, map: map, discarded_at: Time.current

          FactoryBot.create :path, layer: layer, map: map
          # Discarded paths
          FactoryBot.create :path, layer: layer, map: map, discarded_at: Time.current
          FactoryBot.create :path, layer: discarded_layer, map: map
          FactoryBot.create :path, layer: discarded_layer, map: map, discarded_at: Time.current

          # Other paths
          FactoryBot.create :path
          FactoryBot.create :path, discarded_at: Time.current
        end

        it('returns only patches of a map') do
          expect(described_class.kept_for_map(map.id).count).to eq 1
        end
      end
    end
  end
end
