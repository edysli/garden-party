require 'rails_helper'

RSpec.describe Observation, type: :model do
  let(:observation_with_800px_attachments) do
    Rails.configuration.garden_party.observations[:largest_pictures_size] = 800
    FactoryBot.create :observation, :with_pictures
  end

  it 'downsizes attached pictures' do
    picture = observation_with_800px_attachments.pictures.first
    picture.analyze

    expect(picture.metadata[:width]).to eq Rails.configuration.garden_party.observations[:largest_pictures_size]
  end

  it 'does not resize previously attached pictures' do
    observation_with_800px_attachments

    Rails.configuration.garden_party.observations[:largest_pictures_size] = 1024
    observation_with_800px_attachments.pictures.attach [Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', 'observation_upload.png'), 'image/png')]
    observation_with_800px_attachments.pictures.each(&:analyze)

    expect(observation_with_800px_attachments.pictures.first.metadata[:width]).to eq 800
  end
end
