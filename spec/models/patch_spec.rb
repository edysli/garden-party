require 'rails_helper'

RSpec.describe Patch, type: :model do
  let(:map) { FactoryBot.create :map }

  describe 'scopes' do
    describe '.kept' do
      it 'ignores records of discarded layers' do
        layer           = map.layers.first
        discarded_layer = FactoryBot.create :layer, map: map, discarded_at: Time.current
        FactoryBot.create :patch, layer: layer
        FactoryBot.create :patch, layer: discarded_layer
        expect(described_class.kept.count).to eq 1
      end
    end

    describe '.kept_for_map' do
      context 'with patches in discarded and undiscarded layers' do
        before do
          layer           = map.layers.first
          discarded_layer = FactoryBot.create :layer, map: map, discarded_at: Time.current

          FactoryBot.create :patch, layer: layer, map: map
          # Discarded patches
          FactoryBot.create :patch, layer: layer, map: map, discarded_at: Time.current
          FactoryBot.create :patch, layer: discarded_layer, map: map
          FactoryBot.create :patch, layer: discarded_layer, map: map, discarded_at: Time.current

          # Other patches
          FactoryBot.create :patch
          FactoryBot.create :patch, discarded_at: Time.current
        end

        it('returns only patches of a map') do
          expect(described_class.kept_for_map(map.id).count).to eq 1
        end
      end
    end
  end

  context 'when creating a patch' do
    let(:first_element) { FactoryBot.build :element }
    let(:second_element) { FactoryBot.build :element }

    context 'when it is a polygon with associated elements' do
      let(:geometry) { FactoryBot.build(:patch, :polygon).geometry }
      let(:patch) { described_class.new map: map, geometry: geometry, elements: [first_element, second_element], layer: map.layers.first }

      it 'is valid' do
        expect(patch).to be_valid
      end

      it 'creates the associated elements on creation' do
        expect do
          patch.save!
        end.to change(Element, :count).by(2)
      end
    end

    context 'when it is a circle with associated elements' do
      let(:geometry) { FactoryBot.build(:patch, :circle).geometry }
      let(:patch) { described_class.new map: map, geometry: geometry, elements: [first_element, second_element], layer: map.layers.first }

      it 'is valid' do
        expect(patch).to be_valid
      end

      it 'creates the associated elements on creation' do
        expect do
          patch.save!
        end.to change(Element, :count).by(2)
      end
    end
  end
end
