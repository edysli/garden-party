require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'scopes' do
    describe 'search' do
      before do
        FactoryBot.create :user, username: 'Bobby'
        FactoryBot.create :user, username: 'Alice'
        FactoryBot.create :user, username: 'Alicia'
      end

      it 'does not care of the search string case' do
        expect(described_class.search('bob').count).to eq 1
      end

      it 'returns all matches' do
        expect(described_class.search('lic').count).to eq 2
      end
    end
  end
end
