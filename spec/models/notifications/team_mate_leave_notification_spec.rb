require 'rails_helper'

RSpec.describe TeamMateLeaveNotification, type: :model do
  include ActiveJob::TestHelper

  describe 'validation' do
    # Valid values
    let(:notification_subject) { FactoryBot.build :map }
    let(:recipient) { notification_subject.user }
    let(:sender) { FactoryBot.create :user }
    let(:instance) { described_class.new subject: notification_subject, sender: sender, recipient: recipient }

    context 'when recipient is an user' do
      context 'when sender is the map owner' do
        it 'validates' do
          expect(instance).to be_valid
        end
      end

      context 'when sender is not the map owner' do
        it 'fails validation' do
          instance.sender_id = FactoryBot.create :user
          instance.validate
          expect(instance.errors[:sender][0]).to eq I18n.t('activerecord.errors.models.team_mate_leave_notification.sender.bad_sender')
        end
      end
    end

    context 'when recipient is unspecified' do
      it 'fails validation' do
        instance.recipient = nil
        instance.validate
        expect(instance.errors[:recipient][0]).to eq I18n.t('activerecord.errors.models.team_mate_leave_notification.recipient.bad_recipient')
      end
    end

    context 'when subject is not a Map' do
      it 'fails validation' do
        instance.subject = FactoryBot.create :user
        instance.validate
        expect(instance.errors[:subject][0]).to eq I18n.t('activerecord.errors.models.team_mate_leave_notification.subject.bad_subject')
      end
    end
  end

  describe 'hooks' do
    describe 'after_create' do
      it 'sends an email to the map owner' do
        map  = FactoryBot.create :map
        user = FactoryBot.create :user
        expect do
          perform_enqueued_jobs { described_class.create! recipient_id: map.user_id, subject: map, sender_id: user.id }
        end.to change(ActionMailer::Base.deliveries, :count).by(1)
      end
    end
  end

  describe '.create_for!' do
    it 'creates a new notification' do
      team_mate = FactoryBot.create :team_mate
      expect do
        described_class.create_for! team_mate
      end.to change(described_class, :count).by 1
    end
  end
end
