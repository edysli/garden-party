require 'rails_helper'

RSpec.describe Task, type: :model do
  let(:now) { Time.current }
  let(:tomorrow) { now + 1.day }
  let(:day_after_tomorrow) { now + 2.days }
  let(:task) { FactoryBot.create :task, :for_element, action: 'something', planned_for: tomorrow }

  describe '.update_without_subject_hooks' do
    it 'does not perform subject hooks' do
      allow(task).to receive :perform_action
      task.update_without_subject_hooks planned_for: day_after_tomorrow

      expect(task).not_to have_received(:perform_action)
    end
  end

  describe '.destroy_without_subject_hooks' do
    it 'does not perform subject hooks' do
      allow(task).to receive :perform_action
      task.destroy_without_subject_hooks

      expect(task).not_to have_received(:perform_action)
    end
  end

  describe '.last_significant_change' do
    let(:task) { FactoryBot.create :task, :planned, :for_element, action: 'something', planned_for: tomorrow }

    context 'when the task is already finished' do
      it 'returns nil' do
        task.update! done_at: Time.current

        task.update! planned_for: Time.current.yesterday
        expect(task.last_significant_change).to be_nil
      end
    end

    context 'when the task is pending' do
      context 'when the task date is changed' do
        it 'returns :update' do
          task.update_without_subject_hooks planned_for: Time.current
          expect(task.last_significant_change).to eq :update
        end
      end

      context 'when the task is finished' do
        it 'returns :finish' do
          task.update_without_subject_hooks done_at: Time.current
          expect(task.last_significant_change).to eq :finish
        end
      end

      context 'when the task is destroyed' do
        it 'returns :destroy' do
          task.destroy_without_subject_hooks
          expect(task.last_significant_change).to eq :destroy
        end
      end

      context 'when another field is changed' do
        it 'returns nil' do
          task.update_without_subject_hooks notes: 'Something new'
          expect(task.last_significant_change).to be_nil
        end
      end
    end
  end
end
