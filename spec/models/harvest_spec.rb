require 'rails_helper'

RSpec.describe Harvest, type: :model do
  let(:known_map) { FactoryBot.create :map }
  let(:known_element) { FactoryBot.create :element, :point, map: known_map }

  describe 'scopes' do
    describe 'all_for_map' do
      before do
        FactoryBot.create_list :harvest, 2, element: known_element
        FactoryBot.create_list :harvest, 2, element: FactoryBot.create(:element, :point, map: known_map)
        FactoryBot.create_list :harvest, 5
      end

      it('returns the right amount of elements') do
        expect(described_class.all_for_map(known_map.id).count).to eq 4
      end
    end

    describe 'for_element' do
      before do
        FactoryBot.create_list :harvest, 2, element: known_element
        FactoryBot.create_list :harvest, 2, element: FactoryBot.create(:element, :point, map: known_map)
        FactoryBot.create_list :harvest, 5
      end

      it('returns the right amount of elements') do
        expect(described_class.for_element(known_element.id).count).to eq 2
      end
    end
  end
end
