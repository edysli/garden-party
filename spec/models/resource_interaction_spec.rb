require 'rails_helper'

RSpec.describe ResourceInteraction, type: :model do
  describe '.validate_pair' do
    context 'when subject and target are the same' do
      let(:resource_interaction) do
        first           = FactoryBot.build :resource_interaction
        first.target_id = first.subject_id

        first
      end

      it 'is invalid' do
        resource_interaction.validate

        expect(resource_interaction.errors[:target].first).to eq I18n.t('activerecord.errors.models.resource_interaction.target.has_same_resource')
      end
    end

    context 'when a pair already exist' do
      let(:resource_interaction) do
        first = FactoryBot.create :resource_interaction
        described_class.new subject_id: first.target_id, target_id: first.subject_id, nature: 0
      end

      it 'is invalid' do
        resource_interaction.validate
        expect(resource_interaction.errors[:target].first).to eq I18n.t('activerecord.errors.models.resource_interaction.target.pair_already_exists')
      end
    end
  end
end
