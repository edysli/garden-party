require 'rails_helper'

RSpec.describe Admin::ResourceInteractionsGroupsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/resource_interactions_groups').to route_to('admin/resource_interactions_groups#index')
    end

    it 'routes to #new' do
      expect(get: '/admin/resource_interactions_groups/new').to route_to('admin/resource_interactions_groups#new')
    end

    it 'routes to #show' do
      expect(get: '/admin/resource_interactions_groups/1').to route_to('admin/resource_interactions_groups#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/admin/resource_interactions_groups/1/edit').to route_to('admin/resource_interactions_groups#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/admin/resource_interactions_groups').to route_to('admin/resource_interactions_groups#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/admin/resource_interactions_groups/1').to route_to('admin/resource_interactions_groups#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/admin/resource_interactions_groups/1').to route_to('admin/resource_interactions_groups#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/admin/resource_interactions_groups/1').to route_to('admin/resource_interactions_groups#destroy', id: '1')
    end
  end
end
