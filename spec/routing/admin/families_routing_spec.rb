require 'rails_helper'

RSpec.describe Admin::FamiliesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/families').to route_to('admin/families#index')
    end

    it 'routes to #new' do
      expect(get: '/admin/families/new').to route_to('admin/families#new')
    end

    it 'routes to #show' do
      expect(get: '/admin/families/1').to route_to('admin/families#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/admin/families/1/edit').to route_to('admin/families#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/admin/families').to route_to('admin/families#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/admin/families/1').to route_to('admin/families#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/admin/families/1').to route_to('admin/families#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/admin/families/1').to route_to('admin/families#destroy', id: '1')
    end
  end
end
