require 'rails_helper'

RSpec.describe Admin::ResourceNotesController, type: :routing do
  describe 'routing' do
    it 'routes to #destroy' do
      expect(delete: '/admin/resource_notes/1').to route_to('admin/resource_notes#destroy', id: '1')
    end
  end
end
