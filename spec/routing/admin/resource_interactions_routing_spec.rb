require 'rails_helper'

RSpec.describe Admin::ResourceInteractionsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/resource_interactions').to route_to('admin/resource_interactions#index')
    end

    it 'routes to #new' do
      expect(get: '/admin/resource_interactions/new').to route_to('admin/resource_interactions#new')
    end

    it 'routes to #show' do
      expect(get: '/admin/resource_interactions/1').to route_to('admin/resource_interactions#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/admin/resource_interactions/1/edit').to route_to('admin/resource_interactions#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/admin/resource_interactions').to route_to('admin/resource_interactions#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/admin/resource_interactions/1').to route_to('admin/resource_interactions#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/admin/resource_interactions/1').to route_to('admin/resource_interactions#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/admin/resource_interactions/1').to route_to('admin/resource_interactions#destroy', id: '1')
    end
  end
end
