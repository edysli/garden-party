require 'rails_helper'

RSpec.describe Admin::ImportController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/import').to route_to('admin/import#index')
    end

    it 'routes to #differences' do
      expect(get: '/admin/import/differences').to route_to('admin/import#differences')
    end

    it 'routes to #library_names' do
      expect(get: '/admin/import/library_names').to route_to('admin/import#library_names')
    end

    it 'routes to #family' do # rubocop:disable RSpec/MultipleExpectations
      expect(post: '/admin/import/family').to route_to('admin/import#family')
      expect(put: '/admin/import/family').to route_to('admin/import#family')
    end

    it 'routes to #genus' do # rubocop:disable RSpec/MultipleExpectations
      expect(post: '/admin/import/genus').to route_to('admin/import#genus')
      expect(put: '/admin/import/genus').to route_to('admin/import#genus')
    end

    it 'routes to #resource' do # rubocop:disable RSpec/MultipleExpectations
      expect(post: '/admin/import/resource').to route_to('admin/import#resource')
      expect(put: '/admin/import/resource').to route_to('admin/import#resource')
    end
  end
end
