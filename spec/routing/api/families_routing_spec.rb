require 'rails_helper'

RSpec.describe Api::FamiliesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/families').to route_to('api/families#index')
    end

    it 'routes to #show' do
      expect(get: '/api/families/1').to route_to('api/families#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/families').to route_to('api/families#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/families/1').to route_to('api/families#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/families/1').to route_to('api/families#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/families/1').to route_to('api/families#destroy', id: '1')
    end
  end
end
