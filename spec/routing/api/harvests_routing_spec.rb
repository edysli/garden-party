require 'rails_helper'

RSpec.describe Api::HarvestsController, type: :routing do
  describe 'routing' do
    it 'routes to #index via maps' do
      expect(get: '/api/maps/1/all_harvests').to route_to('api/harvests#all_for_map', map_id: '1')
    end

    it 'routes to #index via elements' do
      expect(get: '/api/elements/1/harvests').to route_to('api/harvests#index', element_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/harvests/1').to route_to('api/harvests#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/harvests').to route_to('api/harvests#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/harvests/1').to route_to('api/harvests#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/harvests/1').to route_to('api/harvests#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/harvests/1').to route_to('api/harvests#destroy', id: '1')
    end
  end
end
