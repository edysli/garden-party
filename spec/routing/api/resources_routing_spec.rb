require 'rails_helper'

RSpec.describe Api::ResourcesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/resources').to route_to('api/resources#index')
    end

    it 'routes to #show' do
      expect(get: '/api/resources/1').to route_to('api/resources#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/resources').to route_to('api/resources#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/resources/1').to route_to('api/resources#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/resources/1').to route_to('api/resources#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/resources/1').to route_to('api/resources#destroy', id: '1')
    end
  end
end
