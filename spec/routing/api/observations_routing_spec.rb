require 'rails_helper'

RSpec.describe Api::ObservationsController, type: :routing do
  describe 'routing' do
    it 'routes to #index via elements' do
      expect(get: '/api/elements/1/observations').to route_to('api/observations#index', element_id: '1')
    end

    it 'routes to #index via maps' do
      expect(get: '/api/maps/1/observations').to route_to('api/observations#index', map_id: '1')
    end

    it 'routes to #index via patches' do
      expect(get: '/api/patches/1/observations').to route_to('api/observations#index', patch_id: '1')
    end

    it 'routes to #index via paths' do
      expect(get: '/api/paths/1/observations').to route_to('api/observations#index', path_id: '1')
    end

    it 'routes to #all_observations via maps' do
      expect(get: '/api/maps/1/all_observations').to route_to('api/observations#all_for_map', map_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/observations/1').to route_to('api/observations#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/observations').to route_to('api/observations#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/observations/1').to route_to('api/observations#update', id: '1')
    end

    it 'routes to #picture' do
      expect(get: '/api/observations/10/pictures/1').to route_to('api/observations#picture', id: '10', picture_id: '1')
    end

    it 'routes to #picture_thumbnail' do
      expect(get: '/api/observations/10/pictures/1/thumbnail').to route_to('api/observations#picture_thumbnail', id: '10', picture_id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/observations/1').to route_to('api/observations#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/observations/1').to route_to('api/observations#destroy', id: '1')
    end
  end
end
