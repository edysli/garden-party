require 'rails_helper'

RSpec.describe Api::TeamMatesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/maps/1/team_mates').to route_to('api/team_mates#index', map_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/team_mates/1').to route_to('api/team_mates#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/team_mates').to route_to('api/team_mates#create')
    end

    it 'routes to #accept' do
      expect(patch: '/api/team_mates/1/accept').to route_to('api/team_mates#accept', id: '1')
    end

    it 'routes to #refuse' do
      expect(delete: '/api/team_mates/1/refuse').to route_to('api/team_mates#refuse', id: '1')
    end

    it 'routes to #leave' do
      expect(delete: '/api/team_mates/1/leave').to route_to('api/team_mates#leave', id: '1')
    end

    it 'routes to #remove' do
      expect(delete: '/api/team_mates/1/remove').to route_to('api/team_mates#remove', id: '1')
    end
  end
end
