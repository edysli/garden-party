require 'rails_helper'

RSpec.describe Api::ResourceInteractionsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/resource_interactions').to route_to('api/resource_interactions#index')
    end

    it 'routes to #show' do
      expect(get: '/api/resource_interactions/1').to route_to('api/resource_interactions#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/resource_interactions').to route_to('api/resource_interactions#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/resource_interactions/1').to route_to('api/resource_interactions#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/resource_interactions/1').to route_to('api/resource_interactions#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/resource_interactions/1').to route_to('api/resource_interactions#destroy', id: '1')
    end
  end
end
