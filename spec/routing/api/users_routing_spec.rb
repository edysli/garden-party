require 'rails_helper'

RSpec.describe Api::UsersController, type: :routing do
  describe 'routing' do
    it 'routes to #search' do
      expect(post: '/api/users/search').to route_to('api/users#search')
    end

    it 'routes to #show' do
      expect(get: '/api/users/1').to route_to('api/users#show', id: '1')
    end
  end
end
