require 'rails_helper'

RSpec.describe Api::ActivitiesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/maps/1/all_activities').to route_to('api/activities#index', map_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/activities/1').to route_to('api/activities#show', id: '1')
    end
  end
end
