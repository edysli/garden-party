require 'rails_helper'

RSpec.describe Api::TasksController, type: :routing do
  describe 'routing' do
    it 'routes to #index via elements' do
      expect(get: '/api/elements/1/tasks').to route_to('api/tasks#index', element_id: '1')
    end

    it 'routes to #index via maps' do
      expect(get: '/api/maps/1/tasks').to route_to('api/tasks#index', map_id: '1')
    end

    it 'routes to #index via patches' do
      expect(get: '/api/patches/1/tasks').to route_to('api/tasks#index', patch_id: '1')
    end

    it 'routes to #index via paths' do
      expect(get: '/api/paths/1/tasks').to route_to('api/tasks#index', path_id: '1')
    end

    it 'routes to #all_tasks via maps' do
      expect(get: '/api/maps/1/all_tasks').to route_to('api/tasks#all_for_map', map_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/tasks/1').to route_to('api/tasks#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/tasks').to route_to('api/tasks#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/tasks/1').to route_to('api/tasks#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/tasks/1').to route_to('api/tasks#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/tasks/1').to route_to('api/tasks#destroy', id: '1')
    end
  end
end
