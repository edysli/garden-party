require 'rails_helper'

RSpec.describe Api::MapsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/maps').to route_to('api/maps#index')
    end

    it 'routes to #show' do
      expect(get: '/api/maps/1').to route_to('api/maps#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/maps').to route_to('api/maps#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/maps/1').to route_to('api/maps#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/maps/1').to route_to('api/maps#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/maps/1').to route_to('api/maps#destroy', id: '1')
    end

    it 'routes to #shared' do
      expect(get: '/api/shared/maps/1').to route_to('api/maps#shared', id: '1')
    end
  end
end
