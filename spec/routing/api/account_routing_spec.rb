require 'rails_helper'

RSpec.describe Api::AccountsController, type: :routing do
  describe 'routing' do
    it 'routes to #show via elements' do
      expect(get: '/api/account').to route_to('api/accounts#show')
    end
  end
end
