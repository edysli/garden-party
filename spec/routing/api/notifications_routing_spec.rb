require 'rails_helper'

RSpec.describe Api::NotificationsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/notifications').to route_to('api/notifications#index')
    end

    it 'routes to #show' do
      expect(get: '/api/notifications/1').to route_to('api/notifications#show', id: '1')
    end

    it 'routes to #archive via PATCH' do
      expect(patch: '/api/notifications/1/archive').to route_to('api/notifications#archive', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/notifications/1').to route_to('api/notifications#destroy', id: '1')
    end

    it 'routes to #archive_all' do
      expect(patch: '/api/notifications/archive_all').to route_to('api/notifications#archive_all')
    end

    it 'routes to #destroy_all_archived' do
      expect(delete: '/api/notifications/destroy_all_archived').to route_to('api/notifications#destroy_all_archived')
    end
  end
end
