require 'rails_helper'

RSpec.describe Api::ResourceInteractionsGroupsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/resource_interactions_groups').to route_to('api/resource_interactions_groups#index')
    end

    it 'routes to #show' do
      expect(get: '/api/resource_interactions_groups/1').to route_to('api/resource_interactions_groups#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/resource_interactions_groups').to route_to('api/resource_interactions_groups#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/resource_interactions_groups/1').to route_to('api/resource_interactions_groups#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/resource_interactions_groups/1').to route_to('api/resource_interactions_groups#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/resource_interactions_groups/1').to route_to('api/resource_interactions_groups#destroy', id: '1')
    end
  end
end
