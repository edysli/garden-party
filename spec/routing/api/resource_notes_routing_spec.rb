require 'rails_helper'

RSpec.describe Api::ResourceNotesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/resources/1/notes').to route_to('api/resource_notes#index', resource_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/api/resource_notes/1').to route_to('api/resource_notes#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/resource_notes').to route_to('api/resource_notes#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/resource_notes/1').to route_to('api/resource_notes#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/resource_notes/1').to route_to('api/resource_notes#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/resource_notes/1').to route_to('api/resource_notes#destroy', id: '1')
    end
  end
end
