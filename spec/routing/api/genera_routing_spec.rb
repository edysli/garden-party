require 'rails_helper'

RSpec.describe Api::GeneraController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/genera').to route_to('api/genera#index')
    end

    it 'routes to #show' do
      expect(get: '/api/genera/1').to route_to('api/genera#show', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/api/genera').to route_to('api/genera#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/genera/1').to route_to('api/genera#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/api/genera/1').to route_to('api/genera#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/api/genera/1').to route_to('api/genera#destroy', id: '1')
    end
  end
end
