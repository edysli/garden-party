require 'rails_helper'

RSpec.describe '/api/resource_notes', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:resource_note, user: signed_in_user).attributes
  end

  let(:invalid_attributes) do
    { content: '' }
  end

  let(:valid_headers) do
    {}
  end

  context 'with authenticated member' do
    include_context 'with authenticated member'

    describe 'GET /api/index' do
      it 'renders a successful response' do
        resource_note = ResourceNote.create! valid_attributes
        get api_resource_resource_notes_url(resource_note.resource), headers: valid_headers, as: :json
        expect(response).to be_successful
      end
    end

    describe 'GET /api/resource_notes/1' do
      it 'renders a successful response' do
        resource_note = ResourceNote.create! valid_attributes
        get api_resource_note_url(resource_note), as: :json
        expect(response).to be_successful
      end
    end

    describe 'POST /api/create' do
      context 'with valid parameters' do
        it 'creates a new ResourceNote' do
          expect do
            post api_resource_notes_url, params: { resource_note: valid_attributes }, headers: valid_headers, as: :json
          end.to change(ResourceNote, :count).by(1)
        end

        it 'renders an successful response' do
          post api_resource_notes_url, params: { resource_note: valid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:created)
        end

        it 'renders the new ResourceNote as JSON' do
          post api_resource_notes_url, params: { resource_note: valid_attributes }, headers: valid_headers, as: :json
          expect(response.content_type).to match(a_string_including('application/json'))
        end
      end

      context 'with invalid parameters' do
        it 'does not create a new ResourceNote' do
          expect do
            post api_resource_notes_url, params: { resource_note: invalid_attributes }, as: :json
          end.not_to change(ResourceNote, :count)
        end

        it 'renders an error response' do
          post api_resource_notes_url, params: { resource_note: invalid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'renders the error as JSON' do
          post api_resource_notes_url, params: { resource_note: invalid_attributes }, headers: valid_headers, as: :json
          expect(response.content_type).to match(a_string_including('application/json'))
        end
      end
    end

    describe 'PUT /api/resource_notes/1' do
      context 'with valid parameters' do
        let(:new_attributes) do
          { content: 'New content' }
        end

        it 'updates the requested ResourceNote' do
          resource_note = ResourceNote.create! valid_attributes
          patch api_resource_note_url(resource_note), params: { resource_note: new_attributes }, headers: valid_headers, as: :json
          resource_note.reload
          expect(resource_note.content).to eq new_attributes[:content]
        end

        it 'renders a succes response' do
          resource_note = ResourceNote.create! valid_attributes
          patch api_resource_note_url(resource_note), params: { resource_note: new_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:ok)
        end

        it 'renders a JSON response with the resource_note' do
          resource_note = ResourceNote.create! valid_attributes
          patch api_resource_note_url(resource_note), params: { resource_note: new_attributes }, headers: valid_headers, as: :json
          expect(response.content_type).to match(a_string_including('application/json'))
        end
      end

      context 'with invalid parameters' do
        it 'renders an error response' do
          resource_note = ResourceNote.create! valid_attributes
          patch api_resource_note_url(resource_note), params: { resource_note: invalid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'renders the error as JSON' do
          resource_note = ResourceNote.create! valid_attributes
          patch api_resource_note_url(resource_note), params: { resource_note: invalid_attributes }, headers: valid_headers, as: :json
          expect(response.content_type).to match(a_string_including('application/json'))
        end
      end
    end

    describe 'DELETE /api/resource_notes/1' do
      it 'destroys the requested resource_note' do
        resource_note = ResourceNote.create! valid_attributes
        expect do
          delete api_resource_note_url(resource_note), headers: valid_headers, as: :json
        end.to change(ResourceNote, :count).by(-1)
      end
    end
  end
end
