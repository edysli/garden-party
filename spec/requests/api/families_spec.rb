require 'rails_helper'

RSpec.describe '/api/families', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:family).attributes
  end

  let(:invalid_attributes) do
    { name: nil }
  end

  let(:valid_headers) do
    {}
  end

  context 'with authenticated member' do
    include_context 'with authenticated member'

    describe 'GET /api/index' do
      it 'renders a successful response' do
        Family.create! valid_attributes
        get api_families_url, headers: valid_headers, as: :json
        expect(response).to be_successful
      end
    end

    describe 'GET /api/families/1' do
      it 'renders a successful response' do
        family = Family.create! valid_attributes
        get api_family_url(family), as: :json
        expect(response).to be_successful
      end
    end

    describe 'POST /api/create' do
      context 'with valid parameters' do
        it 'creates a new Family' do
          expect do
            post api_families_url, params: { family: valid_attributes }, headers: valid_headers, as: :json
          end.to change(Family, :count).by(1)
        end

        it 'renders an successful response' do
          post api_families_url, params: { family: valid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:created)
        end

        it 'renders the new Family as JSON' do
          post api_families_url, params: { family: valid_attributes }, headers: valid_headers, as: :json
          expect(response.content_type).to match(a_string_including('application/json'))
        end
      end

      context 'with invalid parameters' do
        it 'does not create a new Family' do
          expect do
            post api_families_url, params: { family: invalid_attributes }, as: :json
          end.not_to change(Family, :count)
        end

        it 'renders an error response' do
          post api_families_url, params: { family: invalid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'renders the error as JSON' do
          post api_families_url, params: { family: invalid_attributes }, headers: valid_headers, as: :json
          expect(response.content_type).to match(a_string_including('application/json'))
        end
      end
    end

    describe 'PUT /api/families/1' do
      context 'with valid parameters' do
        let(:new_attributes) do
          { name: 'New name' }
        end

        it 'updates the requested Family' do
          family = Family.create! valid_attributes
          patch api_family_url(family), params: { family: new_attributes }, headers: valid_headers, as: :json
          family.reload
          expect(family.name).to eq new_attributes[:name]
        end

        it 'renders a succes response' do
          family = Family.create! valid_attributes
          patch api_family_url(family), params: { family: new_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:ok)
        end

        it 'renders a JSON response with the family' do
          family = Family.create! valid_attributes
          patch api_family_url(family), params: { family: new_attributes }, headers: valid_headers, as: :json
          expect(response.content_type).to match(a_string_including('application/json'))
        end
      end

      context 'with invalid parameters' do
        it 'renders an error response' do
          family = Family.create! valid_attributes
          patch api_family_url(family), params: { family: invalid_attributes }, headers: valid_headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'renders the error as JSON' do
          family = Family.create! valid_attributes
          patch api_family_url(family), params: { family: invalid_attributes }, headers: valid_headers, as: :json
          expect(response.content_type).to match(a_string_including('application/json'))
        end
      end
    end
  end

  context 'with authenticated admin' do
    include_context 'with authenticated admin'
    describe 'DELETE /api/families/1' do
      it 'destroys the requested family' do
        family = Family.create! valid_attributes
        expect do
          delete api_family_url(family), headers: valid_headers, as: :json
        end.to change(Family, :count).by(-1)
      end
    end
  end
end
