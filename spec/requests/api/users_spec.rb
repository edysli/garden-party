require 'rails_helper'

RSpec.describe '/api/users', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:user).attributes.merge password: 'password'
  end

  let(:invalid_attributes) do
    { email: nil }
  end

  let(:valid_headers) do
    {}
  end

  let(:invitation_params) do
    {
      email:    Faker::Internet.unique.email,
      username: Faker::Internet.unique.username(separators: %w[- _], specifier: 3),
    }
  end

  include_context 'with authenticated member'

  describe 'POST /api/users/search' do
    it 'renders a successful response' do
      user = User.create! valid_attributes
      post search_api_users_url, params: { search: user.username }, headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'POST /api/users/invite' do
    describe 'with valid data' do
      it 'creates a new user' do
        expect do
          post invite_api_users_url, params: { user: invitation_params }, headers: valid_headers, as: :json
        end.to change(User, :count).by(1)
      end

      it 'renders a successful response' do
        post invite_api_users_url, params: { user: invitation_params }, headers: valid_headers, as: :json
        expect(response).to be_successful
      end

      it 'renders the new user as JSON' do
        post invite_api_users_url, params: { user: invitation_params }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    describe 'with missing username' do
      let(:invitation_params) { { email: Faker::Internet.unique.email, username: '' } }

      it 'does not create a new user' do
        expect do
          post invite_api_users_url, params: { user: invitation_params }, headers: valid_headers, as: :json
        end.not_to change(User, :count)
      end

      it 'renders an error response' do
        post invite_api_users_url, params: { user: invitation_params }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'renders the new user as JSON' do
        post invite_api_users_url, params: { user: invitation_params }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'POST /api/users/1/resend_invitation' do
    it 'renders a successful response' do
      user = User.invite! invitation_params
      post resend_invitation_api_user_url(user), headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'GET /api/users/1' do
    it 'renders a successful response' do
      user = User.create! valid_attributes
      get api_user_url(user), as: :json
      expect(response).to be_successful
    end
  end
end
