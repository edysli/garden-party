require 'rails_helper'

RSpec.describe '/api/activities', type: :request do
  let(:map) { FactoryBot.create :map, user: signed_in_user }
  let(:valid_attributes) do
    layer = map.layers.first

    FactoryBot.build(:activity, subject: layer, action: 'test', user: signed_in_user).attributes
  end

  let(:valid_headers) do
    {}
  end

  include_context 'with authenticated member'

  describe 'GET /api/index' do
    it 'renders a successful response' do
      Activity.create! valid_attributes
      get api_map_all_activities_url(map_id: map.id), headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'GET /api/activities/1' do
    it 'renders a successful response' do
      activity = Activity.create! valid_attributes
      get api_activity_url(activity), as: :json
      expect(response).to be_successful
    end
  end
end
