require 'rails_helper'

RSpec.describe '/api/tasks', type: :request do
  let(:owned_element) do
    map   = FactoryBot.create :map, user: signed_in_user
    patch = FactoryBot.create :patch, :with_element, map: map
    patch.elements.first
  end
  let(:valid_attributes) do
    FactoryBot.build(:task, subject: owned_element).attributes
  end

  let(:invalid_attributes) do
    valid_attributes.merge! 'planned_for' => nil, 'done_at' => nil
  end

  let(:valid_headers) do
    {}
  end

  include_context 'with authenticated member'

  describe 'GET /api/elements/1/tasks' do
    it 'renders a successful response' do
      task = Task.create! valid_attributes
      get api_element_tasks_url(element_id: task.subject_id), headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'GET /api/tasks/1' do
    it 'renders a successful response' do
      task = Task.create! valid_attributes
      get api_task_url(task), as: :json
      expect(response).to be_successful
    end
  end

  describe 'POST /api/tasks' do
    context 'with valid parameters' do
      it 'creates a new Task' do
        expect do
          post api_tasks_url, params: { task: valid_attributes }, headers: valid_headers, as: :json
        end.to change(Task, :count).by(1)
      end

      it 'renders an successful response' do
        post api_tasks_url, params: { task: valid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
      end

      it 'renders the new Task as JSON' do
        post api_tasks_url, params: { task: valid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Task' do
        expect do
          post api_tasks_url, params: { task: invalid_attributes }, as: :json
        end.not_to change(Task, :count)
      end

      it 'renders an error response' do
        post api_tasks_url, params: { task: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'renders the error as JSON' do
        post api_tasks_url, params: { task: invalid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'PUT /api/tasks/1' do
    context 'with valid parameters' do
      let(:new_attributes) do
        { notes: 'Something new' }
      end

      it 'updates the requested Task' do
        task = Task.create! valid_attributes
        patch api_task_url(task), params: { task: new_attributes }, headers: valid_headers, as: :json
        task.reload
        expect(task.notes).to eq new_attributes[:notes]
      end

      it 'renders a success response' do
        task = Task.create! valid_attributes
        patch api_task_url(task), params: { task: new_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:ok)
      end

      it 'renders a JSON response with the task' do
        task = Task.create! valid_attributes
        patch api_task_url(task), params: { task: new_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'renders an error response' do
        task = Task.create! valid_attributes
        patch api_task_url(task), params: { task: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'renders the error as JSON' do
        task = Task.create! valid_attributes
        patch api_task_url(task), params: { task: invalid_attributes }, headers: valid_headers, as: :json
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end
  end

  describe 'DELETE /api/tasks/1' do
    it 'destroys the requested task' do
      task = Task.create! valid_attributes
      expect do
        delete api_task_url(task), headers: valid_headers, as: :json
      end.to change(Task, :count).by(-1)
    end
  end
end
