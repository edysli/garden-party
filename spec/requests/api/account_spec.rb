require 'rails_helper'

RSpec.describe '/api/account', type: :request do
  let(:valid_headers) do
    {}
  end

  include_context 'with authenticated member'

  describe 'GET /api/account' do
    it 'renders a successful response' do
      get api_account_url, headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end
end
