require 'rails_helper'

RSpec.describe '/admin/families', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:family).attributes
  end
  let(:invalid_attributes) do
    { name: '' }
  end

  include_context 'with authenticated admin'

  describe 'GET /admin/families' do
    it 'returns a success response' do
      Family.create! valid_attributes
      get admin_families_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/families/1' do
    it 'returns a success response' do
      family = Family.create! valid_attributes
      get admin_family_url(family)
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get new_admin_family_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/families/1/edit' do
    it 'returns a success response' do
      family = Family.create! valid_attributes
      get edit_admin_family_url(family)
      expect(response).to be_successful
    end
  end

  describe 'POST /admin/families' do
    context 'with valid params' do
      it 'creates a new Family' do
        expect do
          post admin_families_url, params: { family: valid_attributes }
        end.to change(Family, :count).by(1)
      end

      it 'redirects to the created admin_family' do
        post admin_families_url, params: { family: valid_attributes }
        expect(response).to redirect_to(admin_family_path(Family.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post admin_families_url, params: { family: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT /admin/families/1' do
    let(:family) { Family.create! valid_attributes }

    context 'with valid params' do
      let(:new_attributes) do
        { name: 'New name' }
      end

      it 'updates the requested admin_family' do
        put admin_family_url(family), params: { family: new_attributes }
        family.reload
        expect(family.name).to eq new_attributes[:name]
      end

      it 'redirects to the admin_family' do
        put admin_family_url(family), params: { family: new_attributes }
        expect(response).to redirect_to(admin_family_path(Family.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put admin_family_url(family), params: { family: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /admin/families/1' do
    it 'destroys the requested admin_family' do
      family = Family.create! valid_attributes
      expect do
        delete admin_family_url(family)
      end.to change(Family, :count).by(-1)
    end

    it 'redirects to the admin_families list' do
      family = Family.create! valid_attributes
      delete admin_family_url(family)
      expect(response).to redirect_to(admin_families_url)
    end
  end
end
