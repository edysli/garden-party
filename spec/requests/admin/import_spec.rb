require 'rails_helper'

RSpec.describe '/admin/import', type: :request do
  include_context 'with authenticated admin'

  let(:family) { Family.create name: 'the family', kingdom: 'plant' }
  let(:genus) { Genus.create name: 'the genus', family: family }

  describe 'GET /admin/import' do
    it 'returns a success response' do
      get admin_import_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/import/differences' do
    before do
      fixture = YAML.load_file file_fixture('trusted_data.yml')
      allow(GardenParty::TrustedData).to receive(:fetch).and_return(fixture)
    end

    it 'returns a success response' do
      get admin_import_differences_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/import/library_names' do
    it 'returns a success response' do
      get admin_import_library_names_url
      expect(response).to be_successful
    end
  end

  describe 'POST/PUT /admin/import/family' do
    it 'creates a new family' do
      payload = { name: 'the family', kingdom: 'plant', id: 10 }
      expect do
        post admin_import_family_url, params: payload, as: :json
      end.to change(Family, :count).by 1
    end

    it 'updates a family' do
      payload = { name: 'new name', library_id: family.id }

      put admin_import_family_url, params: payload, as: :json
      family.reload

      expect(family.name).to eq 'new name'
    end
  end

  describe 'POST/PUT /admin/import/genus' do
    before { family }

    it 'creates a new genus' do
      payload = { name: 'the genus', family: 'the family', id: 10 }
      expect do
        post admin_import_genus_url, params: payload, as: :json
      end.to change(Genus, :count).by 1
    end

    it 'updates a genus' do
      payload = { name: 'new name', library_id: genus.id }

      put admin_import_genus_url, params: payload, as: :json
      genus.reload

      expect(genus.name).to eq 'new name'
    end
  end

  describe 'POST/PUT /admin/import/resource' do
    before { genus }

    it 'creates a new resource' do
      payload = { name: 'the resource', genus: 'the genus', latin_name: 'something', id: 10 }
      expect do
        post admin_import_resource_url, params: payload, as: :json
      end.to change(Resource, :count).by 1
    end

    it 'updates a resource' do
      resource = Resource.create! name: 'the resource', genus: genus, latin_name: 'something'
      payload = { name: 'new name', library_id: resource.id }

      put admin_import_resource_url, params: payload, as: :json
      resource.reload

      expect(resource.name).to eq 'new name'
    end
  end
end
