require 'rails_helper'

RSpec.describe '/admin/resource_notes', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:resource_note).attributes
  end

  include_context 'with authenticated admin'

  describe 'DELETE /admin/resource_notes/1' do
    it 'destroys the requested admin_resource_note' do
      resource_note = ResourceNote.create! valid_attributes
      expect do
        delete admin_resource_note_url(resource_note)
      end.to change(ResourceNote, :count).by(-1)
    end

    it 'redirects to the admin_resource_notes list' do
      resource_note = ResourceNote.create! valid_attributes
      delete admin_resource_note_url(resource_note)
      expect(response).to redirect_to(admin_resource_url(resource_note.resource_id))
    end
  end
end
