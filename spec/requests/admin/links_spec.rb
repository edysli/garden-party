require 'rails_helper'

RSpec.describe '/admin/links', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:link).attributes
  end
  let(:invalid_attributes) do
    { url: nil }
  end

  include_context 'with authenticated admin'

  describe 'GET /admin/links' do
    it 'returns a success response' do
      Link.create! valid_attributes
      get admin_links_url
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'returns a success response' do
      get new_admin_link_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/links/1/edit' do
    it 'returns a success response' do
      link = Link.create! valid_attributes
      get edit_admin_link_url(link)
      expect(response).to be_successful
    end
  end

  describe 'POST /admin/links' do
    context 'with valid params' do
      it 'creates a new Link' do
        expect do
          post admin_links_url, params: { link: valid_attributes }
        end.to change(Link, :count).by(1)
      end

      it 'redirects to the links list' do
        post admin_links_url, params: { link: valid_attributes }
        expect(response).to redirect_to(admin_links_path)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post admin_links_url, params: { link: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT /admin/links/1' do
    let(:link) { Link.create! valid_attributes }

    context 'with valid params' do
      let(:new_attributes) do
        { title: 'Fixed title' }
      end

      it 'updates the requested admin_link' do
        put admin_link_url(link), params: { link: new_attributes }
        link.reload
        expect(link.title).to eq new_attributes[:title]
      end

      it 'redirects to the admin_link' do
        put admin_link_url(link), params: { link: new_attributes }
        expect(response).to redirect_to(admin_links_path)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put admin_link_url(link), params: { link: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT /admin/links/1/approve' do
    let(:link) { FactoryBot.create :link, :pending }

    it 'redirects to the admin_links' do
      put approve_admin_link_url(link)
      expect(response).to redirect_to(admin_links_path)
    end

    it 'approves the link' do
      put approve_admin_link_url(link)
      link.reload

      expect(link).to be_approved
    end
  end

  describe 'DELETE /admin/links/1' do
    it 'destroys the requested admin_link' do
      link = Link.create! valid_attributes
      expect do
        delete admin_link_url(link)
      end.to change(Link, :count).by(-1)
    end

    it 'redirects to the admin_links list' do
      link = Link.create! valid_attributes
      delete admin_link_url(link)
      expect(response).to redirect_to(admin_links_url)
    end
  end
end
