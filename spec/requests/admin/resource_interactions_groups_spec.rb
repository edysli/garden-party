require 'rails_helper'

RSpec.describe '/admin/resource_interactions_groups', type: :request do
  let(:valid_attributes) do
    FactoryBot.build(:resource_interactions_group).attributes
  end
  let(:invalid_attributes) do
    { name: nil }
  end

  include_context 'with authenticated admin'

  describe 'GET /admin/resource_interactions_groups' do
    it 'returns a success response' do
      ResourceInteractionsGroup.create! valid_attributes
      get admin_resource_interactions_groups_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/resource_interactions_groups/1' do
    it 'returns a success response' do
      resource_interactions_group = ResourceInteractionsGroup.create! valid_attributes
      get admin_resource_interactions_group_url(resource_interactions_group)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'returns a success response' do
      get new_admin_resource_interactions_group_url
      expect(response).to be_successful
    end
  end

  describe 'GET /admin/resource_interactions_groups/1/edit' do
    it 'returns a success response' do
      resource_interactions_group = ResourceInteractionsGroup.create! valid_attributes
      get edit_admin_resource_interactions_group_url(resource_interactions_group)
      expect(response).to be_successful
    end
  end

  describe 'POST /admin/resource_interactions_groups' do
    context 'with valid params' do
      it 'creates a new ResourceInteractionsGroup' do
        expect do
          post admin_resource_interactions_groups_url, params: { resource_interactions_group: valid_attributes }
        end.to change(ResourceInteractionsGroup, :count).by(1)
      end

      it 'redirects to the created ResourceInteractionsGroup' do
        post admin_resource_interactions_groups_url, params: { resource_interactions_group: valid_attributes }
        expect(response).to redirect_to(admin_resource_interactions_group_path(ResourceInteractionsGroup.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post admin_resource_interactions_groups_url, params: { resource_interactions_group: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT /admin/resource_interactions_groups/1' do
    let(:resource_interactions_group) { ResourceInteractionsGroup.create! valid_attributes }

    context 'with valid params' do
      let(:new_attributes) do
        { name: 'New name' }
      end

      it 'updates the requested admin_resource_interactions_group' do
        put admin_resource_interactions_group_url(resource_interactions_group), params: { resource_interactions_group: new_attributes }
        resource_interactions_group.reload
        expect(resource_interactions_group.name).to eq new_attributes[:name]
      end

      it 'redirects to the admin_resource_interactions_group' do
        put admin_resource_interactions_group_url(resource_interactions_group), params: { resource_interactions_group: new_attributes }
        expect(response).to redirect_to(admin_resource_interactions_group_path(ResourceInteractionsGroup.last))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put admin_resource_interactions_group_url(resource_interactions_group), params: { resource_interactions_group: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /admin/resource_interactions_groups/1' do
    it 'destroys the requested admin_resource_interactions_group' do
      resource_interactions_group = ResourceInteractionsGroup.create! valid_attributes
      expect do
        delete admin_resource_interactions_group_url(resource_interactions_group)
      end.to change(ResourceInteractionsGroup, :count).by(-1)
    end

    it 'redirects to the admin_resource_interactions_groups list' do
      resource_interactions_group = ResourceInteractionsGroup.create! valid_attributes
      delete admin_resource_interactions_group_url(resource_interactions_group)
      expect(response).to redirect_to(admin_resource_interactions_groups_url)
    end
  end
end
