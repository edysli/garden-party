/* eslint-disable no-console, jsdoc/require-returns-description */
/**
 * Collection of helpers to manipulate the map during system tests
 *
 * @typedef {import('vuex').Store} VuexStore
 * @typedef {import('vue').App} VueApp
 * @typedef {VueApp<import('../../app/javascript/vue/apps/garden/components/Garden.vue')>} GpaGardenInstance
 * @typedef {VueApp<import('../../app/javascript/vue/classes/GardenMap')>} GardenMap
 */

const __SLEEP_TIME = 500
const __RETRIES = 10
const __GARDEN_SELECTORS = '#app .gpa-garden'
const __APP_SELECTORS = '#app'

// Helper methods

/**
 * Waits for a small amount of time.
 *
 * @param   {number}        milliseconds Time to wait
 * @returns {Promise<void>}              Fulfilled when time is up
 */
window.__sleep = function (milliseconds = __SLEEP_TIME) {
  return new Promise((resolve) => window.setTimeout(resolve, milliseconds))
}

/**
 * Executes a callback a few times until it returns a truthy value.
 *
 * @param   {Function}   callback    - Method to execute to check result
 * @param   {number}     sleepAmount - Time to sleep in millisecond
 * @param   {number}     maxRetries  - Number of retries before failing
 * @returns {Promise<*>}             Callback result
 */
window.__waitForResult = async function (callback, sleepAmount = __SLEEP_TIME, maxRetries = __RETRIES) {
  let times = 1
  let value = callback()

  console.log('__waitForResult > initial value: ', value)

  while (!value && times <= maxRetries) {
    console.log(`__waitForResult > #${times} > value:`, value)

    await window.__sleep(sleepAmount)
    times++

    value = callback()
  }

  if (value) return value

  throw new Error(`No truthy value returned from callback in ${sleepAmount * maxRetries / 1000} seconds`)
}

/**
 * Waits for a HTML element to be found
 *
 * @param   {string}           selectors - CSS selector
 * @returns {Promise<Element>}
 */
window.__waitForElement = async function (selectors) {
  console.log(`__waitForElement > waiting for "${selectors}..."`)

  const element = await window.__waitForResult(() => window.document.querySelector(selectors))
  if (element) return element

  throw new Error(`Element not found or not ready for selector "${selectors}"`)
}

/**
 * Waits and returns the GpaGarden context
 *
 * @see GpaGarden
 * @returns {Promise<GpaGardenInstance>}
 */
window.__waitForMapContext = async function () {
  console.log('__waitForMapContext > waiting for map...')
  const element = await window.__waitForElement(__GARDEN_SELECTORS)

  console.log('__waitForMapContext > element:', element)
  const context = await window.__waitForResult(() => element?.__vnode?.ctx?.ctx, __SLEEP_TIME, 10)

  console.log('__waitForMapContext > context:', context)
  if (!context) throw new Error('GpaGarden context not found')

  return context
}

/**
 * Waits for the garden to be in a "ready" state: everything is drawn on it
 *
 * @see GpaGarden
 * @see GardenMap.constructor
 * @returns {Promise<boolean>}
 */
window.__waitForMapToBeReady = async function () {
  console.log('__waitForMapToBeReady > waiting for context...')
  const context = await window.__waitForMapContext()

  return context.mapIsReady
}

// Instance-related methods

/**
 * Returns the store from the App
 *
 * @returns {Promise<VuexStore>}
 */
window.__store = async function () {
  await window.__waitForMapToBeReady()

  const element = await window.__waitForElement(__APP_SELECTORS)
  console.log('__store > element', element)

  const store = await window.__waitForResult(() => element?.__vue_app__?._context?.config?.globalProperties?.$store)

  if (!store) throw new Error('Store not found on mount point')

  return store
}

/**
 * Returns the element GardenMap instance
 *
 * @returns {Promise<GardenMap>}
 */
window.__gardenMap = async function () {
  const element = await window.__waitForElement('#app .gpa-garden')
  console.log('__gardenMap > element', element)

  const context = await window.__waitForMapContext()

  return context.gardenMap
}

// Selection methods

/**
 * Simulate a feature selection
 *
 * @param   {import('ol/Feature')} feature - Map feature to select
 * @returns {Promise<void>}
 */
window.__selectFeature = async function (feature) {
  const geom = feature.getGeometry().getFlatCoordinates()
  const gardenMap = await window.__gardenMap()
  gardenMap._select(feature, [geom[0], geom[1]])
}

/**
 * Simulates a patch selection
 *
 * @param   {number}        patchId - Patch ID
 * @returns {Promise<void>}
 */
window.__selectPatch = async function (patchId) {
  await window.__waitForMapToBeReady()
  console.log('__selectPatch > map is ready...')

  const store = await window.__store()
  const feature = await window.__waitForResult(() => store.getters.patch(patchId)?.feature)

  console.log(`__selectPatch ${patchId} >> feature`, feature)
  await window.__selectFeature(feature)
}

/**
 * Simulates an element selection
 *
 * @param   {number}        elementId - Element ID
 * @returns {Promise<void>}
 */
window.__selectElement = async (elementId) => {
  console.log('__selectElement > map is ready...')

  const store = await window.__store()
  const feature = await window.__waitForResult(() => store.getters.element(elementId)?.feature)

  console.log(`__selectElement ${elementId} >> feature`, feature)
  await window.__selectFeature(feature)
}

/* eslint-enable no-console, jsdoc/require-returns-description */
