require 'webdrivers/geckodriver'

Capybara.register_driver(:selenium) do |app|
  options = Selenium::WebDriver::Options.firefox

  # Custom Firefox profile to use light theme.
  firefox_profile                           = Selenium::WebDriver::Firefox::Profile.new
  firefox_profile['ui.systemUsesDarkTheme'] = 0
  options.add_option 'profile', firefox_profile

  args = []
  args << '--headless' unless ENV.fetch('HEADLESS', 'true') == 'false'
  options.add_option 'args', args

  # Custom Firefox executable for testing
  custom_firefox_path = Rails.configuration.garden_party.tests[:firefox_path].presence
  Selenium::WebDriver::Firefox.path = custom_firefox_path if custom_firefox_path

  Capybara::Selenium::Driver.new(app, browser: :firefox, capabilities: options)
end

Capybara.default_driver    = :selenium
Capybara.javascript_driver = :selenium

Capybara::Screenshot.prune_strategy = :keep_last_run
