module SystemHelpers
  MAP_SCRIPTS = File.read File.join(__dir__, 'map_scripts.js')

  def login_user(user)
    login user.email
  end

  def login(email)
    visit '/'

    fill_in I18n.t('activerecord.attributes.user.email'), with: email
    fill_in I18n.t('activerecord.attributes.user.password'), with: 'password'

    click_on I18n.t('devise.sessions.new.sign_in')
  end

  # Injects JS methods to manipulate the garden view
  def inject_garden_manipulation_scripts
    sleep 1
    execute_script MAP_SCRIPTS
  end

  def select_patch(patch_id)
    puts evaluate_script "__selectPatch(#{patch_id})"
  end

  def select_element(element_id)
    puts evaluate_script "__selectElement(#{element_id})"
  end
end

RSpec.configure do |config|
  config.include SystemHelpers, type: :system
end
