require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::FamilyPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:user_admin) { FactoryBot.create :user_admin }
  let(:family) { FactoryBot.create :family }

  permissions '.scope' do
    before { FactoryBot.create_list :family, 2 }

    context 'with no authenticated user' do
      it 'returns no families' do
        expect(Pundit.policy_scope!(nil, [:api, Family]).all.count).to eq 0
      end
    end

    context 'with authenticated user' do
      it 'returns all the families' do
        expect(Pundit.policy_scope!(user, [:api, Family]).all.count).to eq 2
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Family)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Family)
      end
    end
  end

  permissions :show?, :update? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, family)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, family)
      end
    end
  end

  permissions :create? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Family)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Family)
      end
    end
  end

  permissions :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, family)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(user, family)
      end
    end

    context 'with authenticated admin' do
      it 'grants access' do
        expect(described_class).to permit(user_admin, family)
      end
    end
  end
end
