require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::MapPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:map) { FactoryBot.create :map, user: user }
  let(:other_user) { FactoryBot.create :user }
  let(:team_mate) do
    relation = FactoryBot.create :team_mate, :accepted, map: map
    relation.user
  end
  let(:pending_team_mate) do
    relation = FactoryBot.create :team_mate, map: map
    relation.user
  end

  permissions '.scope' do
    before do
      map
      FactoryBot.create :map, user: user, publicly_available: true
      # Unrelated maps
      FactoryBot.create :map
      FactoryBot.create :map, :publicly_available
    end

    context 'with no authenticated user' do
      it 'returns no maps' do
        expect(Pundit.policy_scope!(nil, [:api, Map]).all.count).to eq 0
      end
    end

    context 'with another user' do
      it 'returns all the accessible maps' do
        expect(Pundit.policy_scope!(other_user, [:api, Map]).all.count).to eq 0
      end
    end

    context 'with authenticated owner' do
      it 'returns all the accessible maps' do
        expect(Pundit.policy_scope!(user, [:api, Map]).all.count).to eq 2
      end
    end

    context 'with authenticated pending team mate' do
      it 'returns all the accessible maps' do
        expect(Pundit.policy_scope!(pending_team_mate, [:api, Map]).all.count).to eq 0
      end
    end

    context 'with authenticated accepted team mate' do
      it 'returns all the accessible maps' do
        expect(Pundit.policy_scope!(team_mate, [:api, Map]).all.count).to eq 1
      end
    end
  end

  describe 'model actions' do
    permissions :index?, :create? do
      context 'with no authenticated user' do
        it 'denies access' do
          expect(described_class).not_to permit(nil, Map)
        end
      end

      context 'with authenticated user' do
        it 'grants access' do
          expect(described_class).to permit(user, Map)
        end
      end
    end
  end

  context 'when the map is shared' do
    let(:map) { FactoryBot.create :map, user: user, publicly_available: true }

    permissions :show? do
      context 'with a visitor' do
        it 'denies access' do
          expect(described_class).not_to permit(nil, map)
        end
      end

      context 'with the owner' do
        it 'grants access' do
          expect(described_class).to permit(user, map)
        end
      end

      context 'with another user' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, map)
        end
      end

      context 'with an accepted team mate' do
        it 'grants access' do
          expect(described_class).to permit(team_mate, map)
        end
      end

      context 'with a pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, map)
        end
      end
    end

    permissions :picture?, :shared? do
      context 'with a visitor' do
        it 'grants access' do
          expect(described_class).to permit(nil, map)
        end
      end

      context 'with the owner' do
        it 'grants access' do
          expect(described_class).to permit(user, map)
        end
      end

      context 'with another user' do
        it 'grants access' do
          expect(described_class).to permit(other_user, map)
        end
      end

      context 'with an accepted team mate' do
        it 'grants access' do
          expect(described_class).to permit(team_mate, map)
        end
      end

      context 'with a pending team mate' do
        it 'grants access' do
          expect(described_class).to permit(pending_team_mate, map)
        end
      end
    end

    permissions :update?, :destroy? do
      context 'with a visitor' do
        it 'denies access' do
          expect(described_class).not_to permit(nil, map)
        end
      end

      context 'with the owner' do
        it 'grants access' do
          expect(described_class).to permit(user, map)
        end
      end

      context 'with another user' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, map)
        end
      end

      context 'with an accepted team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(team_mate, map)
        end
      end

      context 'with a pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, map)
        end
      end
    end
  end

  context 'when the map is private' do
    let(:map) { FactoryBot.create :map, user: user }

    permissions :show?, :picture? do
      context 'with a visitor' do
        it 'denies access' do
          expect(described_class).not_to permit(nil, map)
        end
      end

      context 'with the owner' do
        it 'grants access' do
          expect(described_class).to permit(user, map)
        end
      end

      context 'with another user' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, map)
        end
      end

      context 'with an accepted team mate' do
        it 'grants access' do
          expect(described_class).to permit(team_mate, map)
        end
      end

      context 'with a pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, map)
        end
      end
    end

    permissions :shared? do
      context 'with a visitor' do
        it 'denies access' do
          expect(described_class).not_to permit(nil, map)
        end
      end

      context 'with the owner' do
        it 'denies access' do
          expect(described_class).not_to permit(user, map)
        end
      end

      context 'with another user' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, map)
        end
      end

      context 'with an accepted team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(team_mate, map)
        end
      end

      context 'with a pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, map)
        end
      end
    end

    permissions :update?, :destroy? do
      context 'with a visitor' do
        it 'denies access' do
          expect(described_class).not_to permit(nil, map)
        end
      end

      context 'with the owner' do
        it 'grants access' do
          expect(described_class).to permit(user, map)
        end
      end

      context 'with another user' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, map)
        end
      end

      context 'with an accepted team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(team_mate, map)
        end
      end

      context 'with a pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, map)
        end
      end
    end
  end
end
