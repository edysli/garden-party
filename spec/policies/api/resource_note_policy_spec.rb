require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::ResourceNotePolicy, type: :policy do
  subject { described_class }

  let(:user) { FactoryBot.create :user }
  let(:other_user) { FactoryBot.create :user }
  let(:resource_note) { FactoryBot.create :resource_note, user: user }
  let(:private_resource_note) { FactoryBot.create :resource_note, user: user, visible: false }

  permissions '.scope' do
    before do
      FactoryBot.create :resource_note
      resource_note
      private_resource_note
    end

    context 'with no authenticated user' do
      it 'returns no note' do
        expect(Pundit.policy_scope!(nil, [:api, ResourceNote]).all.count).to eq 0
      end
    end

    context 'with another user' do
      it 'returns all visible notes' do
        expect(Pundit.policy_scope!(other_user, [:api, ResourceNote]).all.count).to eq 2
      end
    end

    context 'with authenticated owner' do
      it 'returns all visible and owned notes' do
        expect(Pundit.policy_scope!(user, [:api, ResourceNote]).all.count).to eq 3
      end
    end
  end

  permissions :index?, :create? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, ResourceNote)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, ResourceNote)
      end
    end
  end

  permissions :show? do
    context 'with a visible note' do
      context 'with no authenticated user' do
        it 'denies access' do
          expect(described_class).not_to permit(nil, resource_note)
        end
      end

      context 'with authenticated user' do
        it 'grants access' do
          expect(described_class).to permit(other_user, resource_note)
        end
      end

      context 'with authenticated owner' do
        it 'grants access' do
          expect(described_class).to permit(user, resource_note)
        end
      end
    end

    context 'with a private note' do
      context 'with no authenticated user' do
        it 'denies access' do
          expect(described_class).not_to permit(nil, private_resource_note)
        end
      end

      context 'with authenticated user' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, private_resource_note)
        end
      end

      context 'with authenticated owner' do
        it 'grants access' do
          expect(described_class).to permit(user, private_resource_note)
        end
      end
    end
  end

  permissions :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, private_resource_note)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(other_user, private_resource_note)
      end
    end

    context 'with authenticated owner' do
      it 'grants access' do
        expect(described_class).to permit(user, private_resource_note)
      end
    end
  end
end
