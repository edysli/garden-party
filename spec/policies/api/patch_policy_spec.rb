require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::PatchPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:patch) do
    map = FactoryBot.create :map, user: user
    FactoryBot.create :patch, map: map
  end
  let(:team_mate) do
    relation = FactoryBot.create :team_mate, :accepted, map: patch.map
    relation.user
  end
  let(:pending_team_mate) do
    relation = FactoryBot.create :team_mate, map: patch.map
    relation.user
  end
  let(:other_user) { FactoryBot.create :user }

  permissions '.scope' do
    before do
      patch
      FactoryBot.create_list :patch, 2
    end

    context 'with no authenticated user' do
      it 'returns no patches' do
        expect(Pundit.policy_scope!(nil, [:api, Patch]).all.count).to eq 0
      end
    end

    context 'with authenticated map owner' do
      it 'returns all the owned patches' do
        expect(Pundit.policy_scope!(user, [:api, Patch]).all.count).to eq 1
      end
    end

    context 'with authenticated team mate' do
      it 'returns all the available patches' do
        expect(Pundit.policy_scope!(team_mate, [:api, Patch]).all.count).to eq 1
      end
    end

    context 'with authenticated pending team mate' do
      it 'returns all the available patches' do
        expect(Pundit.policy_scope!(pending_team_mate, [:api, Patch]).all.count).to eq 0
      end
    end
  end

  permissions :create?, :show?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, patch)
      end
    end

    context 'with authenticated user' do
      context 'when user is not owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, patch)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, patch)
        end
      end

      context 'when user is a team mate' do
        it 'grants access' do
          expect(described_class).to permit(team_mate, patch)
        end
      end

      context 'when user is a pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, patch)
        end
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Patch)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Patch)
      end
    end
  end
end
