require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Api::ObservationPolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:observation) do
    map = FactoryBot.create :map, user: user
    patch = FactoryBot.create :patch, :with_element, map: map
    element = patch.elements.first
    FactoryBot.create :observation, subject: element
  end
  let(:team_mate) do
    relation = FactoryBot.create :team_mate, :accepted, map: observation.subject.map
    relation.user
  end
  let(:pending_team_mate) do
    relation = FactoryBot.create :team_mate, map: observation.subject.map
    relation.user
  end
  let(:other_user) { FactoryBot.create :user }

  permissions '.scope' do
    before do
      observation
      FactoryBot.create_list :observation, 2
    end

    context 'with no authenticated user' do
      it 'returns all observations' do
        expect(Pundit.policy_scope!(nil, [:api, Observation]).all.count).to eq 3
      end
    end

    context 'with authenticated map owner' do
      it 'returns all observations' do
        expect(Pundit.policy_scope!(user, [:api, Observation]).all.count).to eq 3
      end
    end

    context 'with authenticated team mate' do
      it 'returns all the available observations' do
        expect(Pundit.policy_scope!(team_mate, [:api, Observation]).all.count).to eq 3
      end
    end

    context 'with authenticated pending team mate' do
      it 'returns all the available observations' do
        expect(Pundit.policy_scope!(pending_team_mate, [:api, Observation]).all.count).to eq 3
      end
    end
  end

  permissions :picture?, :picture_thumbnail? do
    context 'when map is not publicly available' do
      context 'with no authenticated user' do
        it 'denies access' do
          expect(described_class).not_to permit(nil, observation)
        end
      end

      context 'with authenticated user' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, observation)
        end
      end

      context 'with authenticated owner' do
        it 'grants access' do
          expect(described_class).to permit(user, observation)
        end
      end

      context 'with authenticated team mate' do
        it 'grants access' do
          expect(described_class).to permit(team_mate, observation)
        end
      end

      context 'with authenticated pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, observation)
        end
      end
    end

    context 'when map is publicly available' do
      before do
        observation.subject.map.update! publicly_available: true
      end

      context 'with no authenticated user' do
        it 'grants access' do
          expect(described_class).to permit(nil, observation)
        end
      end

      context 'with authenticated user' do
        it 'grants access' do
          expect(described_class).to permit(other_user, observation)
        end
      end

      context 'with authenticated owner' do
        it 'grants access' do
          expect(described_class).to permit(user, observation)
        end
      end

      context 'with authenticated team mate' do
        it 'grants access' do
          expect(described_class).to permit(team_mate, observation)
        end
      end

      context 'with authenticated pending team mate' do
        it 'grants access' do
          expect(described_class).to permit(pending_team_mate, observation)
        end
      end
    end
  end

  permissions :create?, :show?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, observation)
      end
    end

    context 'with authenticated user' do
      context 'when user is not observation owner' do
        it 'denies access' do
          expect(described_class).not_to permit(other_user, observation)
        end
      end

      context 'when user is owner' do
        it 'grants access' do
          expect(described_class).to permit(user, observation)
        end
      end

      context 'when user is a team mate' do
        it 'grants access' do
          expect(described_class).to permit(team_mate, observation)
        end
      end

      context 'when user is a pending team mate' do
        it 'denies access' do
          expect(described_class).not_to permit(pending_team_mate, observation)
        end
      end
    end
  end

  permissions :index? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Observation)
      end
    end

    context 'with authenticated user' do
      it 'grants access' do
        expect(described_class).to permit(user, Observation)
      end
    end
  end
end
