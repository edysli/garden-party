require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Admin::ResourceNotePolicy, type: :policy do
  let(:user) { FactoryBot.create :user }
  let(:user_admin) { FactoryBot.create :user_admin }
  let(:resource_note) { FactoryBot.create :resource_note }

  permissions '.scope' do
    before { FactoryBot.create_list :resource_note, 2 }

    context 'with authenticated admin' do
      it 'returns all the notes' do
        expect(Pundit.policy_scope!(user_admin, [:admin, ResourceNote]).all.count).to eq 2
      end
    end
  end

  permissions :index?, :show?, :new?, :create?, :edit?, :update?, :destroy? do
    context 'with no authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, resource_note)
      end
    end

    context 'with authenticated user' do
      it 'denies access' do
        expect(described_class).not_to permit(user, resource_note)
      end
    end

    context 'with authenticated admin' do
      it 'grants access' do
        expect(described_class).to permit(user_admin, resource_note)
      end
    end
  end
end
