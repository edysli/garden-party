require 'rails_helper'

RSpec.describe 'Harvests', type: :system do
  let(:user) { FactoryBot.create :user }
  let(:map) { FactoryBot.create :map, :user_submitted_png, user: user }
  let(:element) { FactoryBot.create :element, :in_patch, patch: FactoryBot.create(:patch, map: map) }

  before do
    element

    login user.email

    click_on I18n.t('layouts.application.header.gardens')
    click_on map.name
  end

  describe 'harvesting form modal' do
    # JS translations plurals are separated by a pipe
    let(:piece_strings) { I18n.t('js.unit.piece').split('|').map(&:strip) }

    before do
      FactoryBot.create_list :harvest, 3, quantity: 2, unit: Harvest.units[:piece], element: element
    end

    describe 'modal behavior' do
      describe 'in the inventory view' do
        before do
          click_on I18n.t('js.layouts.garden.toolbar.resource_overview')

          within('.gpa-element__actions') do
            click_on I18n.t('js.elements.action_buttons.harvest')
          end
        end

        # Use same selector to ease maintenance
        let(:modal_title_selector) { ['.gp-modal .gp-modal__header', { text: I18n.t('js.harvests.form.title') }] }

        it('opens using the elements actions menu') do
          expect(page).to have_css(*modal_title_selector)
        end

        it('closes on "close"') do
          click_on I18n.t('js.generic.close')

          expect(page).not_to have_css(*modal_title_selector)
        end
      end

      describe 'in the coordination view' do
        before do
          click_on I18n.t('js.layouts.garden.toolbar.coordination')

          within('.gp-toolbar.gp-toolbar--has-help') do
            click_on I18n.t('js.elements.action_buttons.harvest')
          end
        end

        # Use same selector to ease maintenance
        let(:modal_title_selector) { ['.gp-modal .gp-modal__header', { text: I18n.t('js.harvests.form.title') }] }

        it('opens using the elements actions menu') do
          expect(page).to have_css(*modal_title_selector)
        end

        it('closes on "close"') do
          click_on I18n.t('js.generic.close')

          expect(page).not_to have_css(*modal_title_selector)
        end
      end
    end

    describe 'form content' do
      before do
        click_on I18n.t('js.layouts.garden.toolbar.resource_overview')

        within('.gpa-element__actions') do
          click_on I18n.t('js.elements.action_buttons.harvest')
        end
      end

      it('displays past harvests near the form') do
        within('.gp-modal table tbody') do
          amount = current_scope.find_all('tr', text: "2 #{piece_strings[1]}").count
          expect(amount).to eq 3
        end
      end

      it('displays a summary of the quantities per unit') do
        within('.gp-modal') do
          expect(current_scope).to have_css 'li', text: "6 #{piece_strings[1]}"
        end
      end
    end

    describe 'form behavior' do
      before do
        click_on I18n.t('js.layouts.garden.toolbar.resource_overview')

        within('.gpa-element__actions') do
          click_on I18n.t('js.elements.action_buttons.harvest')
        end

        fill_in I18n.t('activerecord.attributes.harvest.quantity'), with: 10
        select piece_strings[0], from: I18n.t('activerecord.attributes.harvest.unit')
      end

      it('saves harvesting data') do
        expect do
          click_on I18n.t('generic.save')
        end.to change(Harvest, :count).by 1
      end

      it('updates the list') do
        click_on I18n.t('generic.save')
        within('.gp-modal table tbody') do
          expect(current_scope).to have_css 'tr', text: "10 #{piece_strings[1]}"
        end
      end

      it('updates the summary') do
        click_on I18n.t('generic.save')
        within('.gp-modal') do
          expect(current_scope).to have_css 'li', text: "16 #{piece_strings[1]}"
        end
      end
    end
  end

  describe 'harvests page' do
    context 'with no harvests' do
      it 'displays a message stating the lack of data' do
        click_on I18n.t('js.layouts.garden.toolbar.harvests')

        expect(page).to have_content I18n.t('js.harvests.no_harvests').tr("\n", ' ').strip
      end
    end

    context 'with a few harvests' do
      before do
        FactoryBot.create :harvest, quantity: 2, unit: Harvest.units[:piece], element: element
        FactoryBot.create :harvest, quantity: 1, unit: Harvest.units[:piece], element: element, harvested_at: 1.year.ago
        FactoryBot.create :harvest, quantity: 4, unit: Harvest.units[:piece], element: element, harvested_at: 2.years.ago

        FactoryBot.create :harvest, quantity: 100, unit: Harvest.units[:gram], element: element, harvested_at: 1.year.ago

        click_on I18n.t('js.layouts.garden.toolbar.harvests')
      end

      it 'shows the harvests by element' do
        expect(page).to have_css 'th', text: element.name
      end

      it 'shows a sum per unit for every year' do
        expect(page).to have_css 'tr', text: '2.00 pce +1.00 pce 1.00 pce -3.00 pce 4.00 pce +4.00 pce'
      end

      it 'shows the evolution from year to year' do
        expect(page).to have_css 'tr', text: '— — -100.00 g 100.00 g +100.00 g — — — —'
      end
    end
  end
end
