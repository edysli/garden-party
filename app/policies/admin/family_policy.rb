module Admin
  class FamilyPolicy < AdminApplicationPolicy
    class Scope < Scope
      def resolve
        scope.all
      end
    end
  end
end
