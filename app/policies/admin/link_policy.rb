module Admin
  class LinkPolicy < AdminApplicationPolicy
    class Scope < Scope
      def resolve
        scope.all
      end
    end

    def approve?
      admin?
    end
  end
end
