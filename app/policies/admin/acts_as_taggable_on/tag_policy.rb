module Admin
  module ActsAsTaggableOn
    class TagPolicy < AdminApplicationPolicy
      def batch_process?
        destroy?
      end

      class Scope < Scope
        def resolve
          scope.all
        end
      end
    end
  end
end
