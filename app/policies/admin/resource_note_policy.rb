module Admin
  class ResourceNotePolicy < AdminApplicationPolicy
    class Scope < Scope
      def resolve
        scope.all
      end
    end
  end
end
