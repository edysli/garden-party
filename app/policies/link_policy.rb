class LinkPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      return scope.approved_or_owned(@user.id) if @user

      scope.approved
    end
  end

  def edit?
    owner?
  end

  def update?
    edit?
  end

  def destroy?
    owner?
  end
end
