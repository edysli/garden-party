module Api
  class ApiApplicationPolicy < ApplicationPolicy
    def index?
      logged_in?
    end

    def show?
      logged_in?
    end

    def create?
      logged_in?
    end

    def update?
      logged_in?
    end

    def destroy?
      logged_in?
    end

    private

    def in_team? # rubocop:disable Metrics/MethodLength
      return false unless logged_in?

      map = case @record
            when Element, Layer, Patch, Path
              @record.map
            when Harvest
              @record.element&.map
            when Map
              @record
            else
              return subject_in_team?
            end

      return false unless map

      map.accepted_team_mate?(@user)
    end

    def subject_in_team?
      raise "Unsupported class #{@record.class}" unless @record.respond_to? :subject_type

      map = case @record.subject_type
            when 'Map'
              @record.subject
            else
              @record.subject.map
            end

      return false unless map

      map.accepted_team_mate?(@user)
    end

    def publicly_available?
      case @record
      when Map
        @record.publicly_available
      else
        subject_publicly_available?
      end
    end

    def subject_publicly_available?
      raise "Unsupported class #{@record.class}" unless @record.respond_to? :subject_type

      case @record.subject_type
      when 'Element', 'Patch', 'Path'
        @record.subject.map.publicly_available
      when 'Map'
        @record.subject.publicly_available
      else
        raise "Unsupported subject type #{@record.subject_type}"
      end
    end
  end
end
