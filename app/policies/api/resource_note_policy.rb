module Api
  class ResourceNotePolicy < ApiApplicationPolicy
    def update?
      owner?
    end

    def destroy?
      admin? || owner?
    end

    def show?
      super && (owner? || @record.visible?)
    end

    class Scope < Scope
      def resolve
        return ResourceNote.none if @user.blank?

        scope.visible.or(ResourceNote.where(user_id: @user.id)).all
      end
    end
  end
end
