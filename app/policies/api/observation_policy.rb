module Api
  class ObservationPolicy < ApiApplicationPolicy
    def create?
      in_team? && kept?
    end

    def show?
      in_team? && kept?
    end

    def update?
      in_team? && kept?
    end

    def destroy?
      in_team? && kept?
    end

    def picture_thumbnail?
      in_team? || publicly_available?
    end

    def picture?
      picture_thumbnail?
    end

    class Scope < Scope
      # Returning all observations by default; authorization must be made in
      # controller on an higher entity (as the map).
      def resolve
        scope.kept.all
      end
    end
  end
end
