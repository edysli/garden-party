module Api
  class ResourceInteractionsGroupPolicy < ApiApplicationPolicy
    def destroy?
      admin?
    end

    class Scope < Scope
      def resolve
        return ResourceInteractionsGroup.none if @user.blank?

        scope.all
      end
    end
  end
end
