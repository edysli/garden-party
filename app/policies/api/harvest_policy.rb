module Api
  class HarvestPolicy < ApiApplicationPolicy
    def create?
      in_team? && kept?
    end

    def show?
      in_team? && kept?
    end

    def update?
      in_team? && kept?
    end

    def destroy?
      in_team? && kept?
    end

    class Scope < Scope
      # Returning all harvesting records by default; authorization must be made in
      # controller on an higher entity (as the map).
      def resolve
        scope.kept.all
      end
    end
  end
end
