module Api
  class ElementPolicy < ApiApplicationPolicy
    def create?
      in_team? && kept?
    end

    def show?
      in_team? && kept?
    end

    def update?
      in_team? && kept?
    end

    def duplicate?
      in_team? && kept?
    end

    def destroy?
      in_team? && kept?
    end

    class Scope < Scope
      def resolve
        return Element.none if @user.blank?

        # All possible maps
        map_ids = @user.accessible_maps.select :id
        # All possible patches
        patch_ids = Patch.where(map_id: map_ids).select :id
        # All possible elements
        scope.kept.where(map_id: map_ids).or(Element.where(patch_id: patch_ids))
      end
    end
  end
end
