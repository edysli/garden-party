module Api
  class MapPolicy < ApiApplicationPolicy
    def show?
      in_team?
    end

    def shared?
      publicly_available?
    end

    def update?
      map_owner?
    end

    def picture?
      owned_or_in_team_or_shared?
    end

    def destroy?
      map_owner?
    end

    def owned_or_in_team_or_shared?
      in_team? || publicly_available?
    end

    class Scope < Scope
      def resolve
        return Map.none if @user.blank?

        map_ids = @user.accessible_maps.pluck :id
        scope.where(id: map_ids).with_attached_picture
      end
    end

    private

    def map_owner?
      return false unless logged_in?

      @record.user == @user
    end
  end
end
