module Api
  class PatchPolicy < ApiApplicationPolicy
    def create?
      in_team? && kept?
    end

    def show?
      in_team? && kept?
    end

    def update?
      in_team? && kept?
    end

    def destroy?
      in_team? && kept?
    end

    class Scope < Scope
      def resolve
        return Patch.none if @user.blank?

        map_ids = @user.accessible_maps.pluck :id
        scope.kept.where(map: map_ids)
      end
    end
  end
end
