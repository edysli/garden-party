module Api
  class UserPolicy < ApiApplicationPolicy
    def show?
      logged_in?
    end

    def search?
      logged_in?
    end

    def invite?
      logged_in?
    end

    def resend_invitation?
      logged_in?
    end

    class Scope < Scope
      def resolve
        return User.none if @user.blank?

        scope.confirmed
      end
    end
  end
end
