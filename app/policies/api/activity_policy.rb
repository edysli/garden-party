module Api
  class ActivityPolicy < ApiApplicationPolicy
    def show?
      in_team?
    end

    class Scope < Scope
      def resolve
        return Activity.none if @user.blank?

        map_ids = @user.accessible_maps.pluck :id
        scope.where(map_id: map_ids)
      end
    end
  end
end
