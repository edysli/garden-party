module Api
  class FamilyPolicy < ApiApplicationPolicy
    def destroy?
      admin?
    end

    class Scope < Scope
      def resolve
        return Family.none if @user.blank?

        scope.all
      end
    end
  end
end
