import { Factory } from 'fishery'
import faker from 'faker'
import { genusFactory } from './genera'

export const monthsPeriodFactory = Factory.define(() => ([
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
  false,
]))

export const resourceFactory = Factory.define(({ params, sequence }) => {
  const creationDate = new Date().toString()
  const genus = genusFactory.build()

  return {
    id: sequence,
    color: faker.internet.color(),
    common_names: [],
    description: null,
    diameter: null,
    generic: false,
    genus_id: genus.id,
    harvesting_months: monthsPeriodFactory.build(),
    latin_name: faker.lorem.words(3),
    name: faker.lorem.words(3),
    parent_id: params.parent ? params.parent.id : null,
    sheltered_sowing_months: monthsPeriodFactory.build(),
    soil_sowing_months: monthsPeriodFactory.build(),
    sources: [],
    sync_id: faker.datatype.number(),
    tag_list: [],

    created_at: creationDate,
    updated_at: creationDate,
  }
})
