import { Factory } from 'fishery'
import faker from 'faker'
import { elementFactory } from './elements'
import { userFactory } from './users'
import { randomEntry } from '../../helpers/Arrays'

export const harvestFactory = Factory.define(({ sequence }) => {
  const creationDate = new Date().toString()
  const element = elementFactory.build()
  const user = userFactory.build()

  return {
    id: sequence,
    element_id: element.id,
    harvested_at: faker.datatype.datetime(),
    quantity: faker.datatype.float(),
    unit: randomEntry(['gram', 'litre', 'piece']),
    user_id: user.id,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
