import { Factory } from 'fishery'
import { mapFactory } from './maps'
import { layerFactory } from './layers'

export const patchFactory = Factory.define(({ sequence }) => {
  const creationDate = new Date().toString()
  const map = mapFactory.build()
  const layer = layerFactory.build()

  return {
    id: sequence,
    geometry: {}, // FIXME
    geometry_type: 'Point',
    layer_id: layer.id,
    map_id: map.id,
    name: null,

    created_at: creationDate,
    updated_at: creationDate,
  }
})
