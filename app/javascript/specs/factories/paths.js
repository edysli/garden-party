import { Factory } from 'fishery'
import { mapFactory } from './maps'
import { layerFactory } from './layers'

/**
 * Returns a random integer
 *
 * @param   {number} max - Maximum value
 * @returns {number}     The random value
 */
function randomInt (max = 100) {
  return Math.round(Math.random() * max)
}

export const circleGeometryFactory = Factory.define(() => ({
  type: 'Feature',
  properties: { radius: randomInt(50) },
  geometry: { type: 'Point', coordinates: [randomInt(100), randomInt(100)] },
}))

export const lineStringGeometryFactory = Factory.define(() => ({
  type: 'Feature',
  properties: null,
  geometry: { type: 'LineString', coordinates: [[randomInt(100), randomInt(100)], [randomInt(100), randomInt(100)]] },
}))

export const polyLineGeometryFactory = Factory.define(() => {
  const startingPoint = [randomInt(100), randomInt(100)]

  return {
    type: 'Feature',
    properties: null,
    geometry: {
      type: 'Polygon',
      coordinates: [startingPoint, [randomInt(100), randomInt(100)], startingPoint],
    },
  }
})

export const pathFactory = Factory.define(({ sequence }) => {
  const creationDate = new Date().toString()
  const map = mapFactory.build()
  const layer = layerFactory.build({ map_id: map.id })

  return {
    id: sequence,
    name: null,
    stroke_width: 1,
    stroke_color: '0,0,0',
    background_color: '0,0,0,1',
    stroke_style_preset: null,
    layer_id: layer.id,
    map_id: map.id,
    geometry: circleGeometryFactory.build(),
    geometry_type: 'Circle',

    created_at: creationDate,
    updated_at: creationDate,
  }
})

export const pathLineFactory = pathFactory.params({
  geometry: lineStringGeometryFactory.build(),
  geometry_type: 'LineString',
})

export const pathPolygonFactory = pathFactory.params({
  geometry: lineStringGeometryFactory.build(),
  geometry_type: 'Polygon',
})
