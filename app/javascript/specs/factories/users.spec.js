import * as factories from './users'

let attributes

describe('Users factories', () => {
  describe('userFactory', () => {
    beforeEach(() => {
      attributes = factories.userFactory.build()
    })

    it('builds an active user', () => {
      expect(attributes.account_state).toBe('active')
    })
  })

  describe('inactiveUserFactory', () => {
    beforeEach(() => {
      attributes = factories.inactiveUserFactory.build()
    })

    it('has a pending user', () => {
      expect(attributes.account_state).toBe('pending')
    })
  })
})
