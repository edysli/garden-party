import { Factory } from 'fishery'
import faker from 'faker'

export const mapFactory = Factory.define(({ sequence }) => {
  const creationDate = new Date().toString()

  return {
    id: sequence,
    background_color: faker.internet.color(),
    center: { x: faker.datatype.float(), y: faker.datatype.float() },
    extent_height: faker.datatype.float(),
    extent_width: faker.datatype.float(),
    hide_background: false,
    name: faker.lorem.word(),
    notes: null,
    picture: { url: 'http://example.com/picture.png', size: [200, 200] },
    publicly_available: false,
    user_id: faker.datatype.number(),

    created_at: creationDate,
    updated_at: creationDate,
  }
})

export const mapUserSubmitted = mapFactory.params(() => ({
  picture: null, // Have to check
}))
