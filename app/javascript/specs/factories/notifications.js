import { Factory } from 'fishery'
import faker from 'faker'
import { userFactory } from './users'

const notificationFactory = Factory.define(({ sequence, associations, params }) => {
  const creationDate = new Date().toString()
  const recipient = params.recipient || userFactory.build()
  const subjectType = params.subject_type || null
  let subject = null
  let subjectId = null
  let sender = null
  let senderId = null

  if (associations.subject) {
    subject = associations.subject
    subjectId = subject.id
  }

  if (associations.sender) {
    sender = associations.sender
    senderId = sender.id
  }

  return {
    id: sequence,
    archived_at: null,
    content: {},
    recipient_id: recipient.id,
    sender,
    sender_id: senderId,
    subject,
    subject_id: subjectId,
    subject_type: subjectType,
    type: '',

    created_at: creationDate,
    updated_at: creationDate,
  }
})

export const systemNotificationFactory = notificationFactory.params({
  type: 'SystemNotification',
  content: {
    title: faker.lorem.sentence(),
    message: faker.lorem.paragraph(),
  },
})
