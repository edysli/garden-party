import { Factory } from 'fishery'
import { inactiveUserFactory, userFactory } from './users'
import { mapFactory } from './maps'
import faker from 'faker'

/**
 * Data for a pending team mate
 */
export const teamMateFactory = Factory.define(({ sequence, params, transientParams }) => {
  const creationDate = params.created_at || new Date().toString()
  const userFactoryAttributes = transientParams.username ? { username: transientParams.username } : {}
  const user = params.user || userFactory.build(userFactoryAttributes)
  const map = transientParams.map || mapFactory.build()

  return {
    id: sequence,
    accepted_at: null,
    map,
    map_id: map.id,
    user_id: user.id,
    user,

    created_at: creationDate,
    updated_at: creationDate,
  }
})

/**
 * Returns team mate data for an user with a non-active account
 */
export const pendingUserTeamMateFactory = teamMateFactory.params({
  accepted_at: null,
  user: inactiveUserFactory.build(),
})

/**
 * Data for an accepted team mate
 */
export const acceptedTeamMateFactory = teamMateFactory.params({
  accepted_at: faker.datatype.datetime(),
})
