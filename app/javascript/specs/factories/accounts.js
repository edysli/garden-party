import { Factory } from 'fishery'
import faker from 'faker'

export const accountFactory = Factory.define(({ sequence, params }) => {
  const creationDate = params.created_at || new Date().toString()
  const updateDate = params.updated_at || creationDate

  return {
    id: sequence,
    role: 'user',
    username: faker.internet.userName(),
    email: faker.internet.email(),
    created_at: creationDate,
    updated_at: updateDate,
  }
})
