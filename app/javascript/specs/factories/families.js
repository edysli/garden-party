import { Factory } from 'fishery'
import faker from 'faker'

export const familyFactory = Factory.define(({ sequence }) => {
  const creationDate = new Date().toString()

  return {
    id: sequence,
    kingdom: 'plant',
    name: faker.lorem.word(),
    source: null,
    sync_id: faker.datatype.number(),

    created_at: creationDate,
    updated_at: creationDate,
  }
})
