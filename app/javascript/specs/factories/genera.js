import { Factory } from 'fishery'
import faker from 'faker'
import { familyFactory } from './families'

export const genusFactory = Factory.define(({ sequence }) => {
  const creationDate = new Date().toString()
  const family = familyFactory.build()

  return {
    id: sequence,
    family_id: family.id,
    name: faker.lorem.word(),
    source: null,
    sync_id: faker.datatype.number(),

    created_at: creationDate,
    updated_at: creationDate,
  }
})
