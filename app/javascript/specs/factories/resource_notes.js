import { Factory } from 'fishery'
import faker from 'faker'
import { userFactory } from './users'

export const resourceNoteFactory = Factory.define(({ sequence }) => {
  const creationDate = new Date().toString()
  const user = userFactory.build()

  return {
    id: sequence,
    content: faker.lorem.paragraphs(),
    resource_id: faker.datatype.number(),
    user_id: user.id,
    username: user.username,
    visible: faker.datatype.boolean(),

    created_at: creationDate,
    updated_at: creationDate,
  }
})
