import { Factory } from 'fishery'
import { resourceFactory } from './resources'
import { patchFactory } from './patches'
import { layerFactory } from './layers'
import { mapFactory } from './maps'

export const elementFactory = Factory.define(({ sequence }) => {
  const creationDate = new Date().toString()
  const resource = resourceFactory.build()
  const map = mapFactory.build()
  const patch = patchFactory.build({ map_id: map.id })
  const layer = layerFactory.build({ map_id: map.id })

  return {
    id: sequence,
    diameter: null,
    geometry: {},
    implantation_planned_for: null,
    implanted_at: null,
    layer_id: layer.id,
    map_id: map.id,
    name: resource.name,
    patch_id: patch.id,
    removal_planned_for: null,
    removed_at: null,
    resource_id: resource.id,
    status: 'planned',

    created_at: creationDate,
    updated_at: creationDate,
  }
})

export const implantedElementFactory = elementFactory.params(() => ({
  implanted_at: new Date().toString(),
}))
