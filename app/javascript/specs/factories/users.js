import { Factory } from 'fishery'
import faker from 'faker'

/**
 * Data for an user with active account
 */
export const userFactory = Factory.define(({ sequence }) => {
  const creationDate = new Date().toString()

  return {
    id: sequence,
    account_state: 'active',
    role: 'user',
    username: faker.internet.userName(),

    created_at: creationDate,
    updated_at: creationDate,
  }
})

/**
 * Data for an user with inactive account
 */
export const inactiveUserFactory = userFactory.params({
  account_state: 'pending',
})
