import { createApp } from 'vue'
import App from '../vue/apps/shared_garden/App.vue'
import store from '../vue/apps/shared_garden/store'
import i18n from '../vue/app_helpers/i18n'

import { $onBus, $emitBus } from '../vue/tools/EventBus'

window.sharedMapApp = function (mapId) {
  const app = createApp(App)
    .use(i18n)
    .use(store)
  app.config.globalProperties.$onBus = $onBus
  app.config.globalProperties.$emitBus = $emitBus
  app.config.globalProperties.$mapId = mapId
  app.mount('#app')
}
