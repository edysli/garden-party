import { createApp } from 'vue'
import App from '../../vue/apps/admin_library_sync/App.vue'
import router from '../../vue/apps/admin_library_sync/router'
import store from '../../vue/apps/admin_library_sync/store'
import i18n from '../../vue/app_helpers/i18n'

document.addEventListener('DOMContentLoaded', () => {
  createApp(App)
    .use(router)
    .use(i18n)
    .use(store)
    .mount('#app')
})
