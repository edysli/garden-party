import GpHelpButtonContent from './HelpButtonContent.vue'
import GpHelpButton from './HelpButton.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Help button content',
  component: GpHelpButtonContent,
  argTypes: {
  },
}

const Template = (args) => ({
  components: { GpHelpButtonContent, GpHelpButton },
  setup () {
    return { args }
  },
  template: '<div style="height: 100px">'
    + '  <gp-help-button>'
    + '    <gp-help-button-content v-bind="args">'
    + '      This thing does something'
    + '    </gp-help-button-content>'
    + '  </gp-help-button>'
    + '</div>',
})

export const Default = Template.bind({})
Default.args = {
  lines: [
    { icon: 'pencil', text: 'Edits the elements properties' },
    { icon: 'trash', text: 'Destroys the whole planet. Use with caution.', variant: 'destructive' },
  ],
  caption: 'Caption here',
}
