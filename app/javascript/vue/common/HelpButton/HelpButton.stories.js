import GpHelpButton from './HelpButton.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Help button',
  component: GpHelpButton,
  argTypes: {
  },
}

const Template = (args) => ({
  components: { GpHelpButton },
  setup () {
    return { args }
  },
  template: '<div style="height: 100px">'
    + '  <gp-help-button v-bind="args" />'
    + '</div>',
})

export const Default = Template.bind({})
Default.args = {}
