import GpForm from './Form.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Form/Form',
  component: GpForm,
  argTypes: {
  },
}

const Template = (args) => ({
  components: { GpForm },
  setup () {
    return { args }
  },
  template: '<gp-form v-bind="args" @submit.prevent="handleSubmit" :loading="loading" :errors="errors">'
    + '  Click on save to simulate a success or failure<br>'
    + '  Status: <code>{{ status }}</code>'
    + '</gp-form>',
  data () {
    return {
      // These should be present in the form definition
      loading: false,
      errors: null,
      // This is only to have feedback on this example
      status: 'unknown',
    }
  },
  methods: {
    handleSubmit () {
      this.status = 'unknown'
      // Reset the state before the call
      this.loading = true
      this.errors = null

      // Simulate an API call
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          if (Math.random() >= 0.5) {
            resolve()
          } else {
            reject({ button: ['not pressed enough'] })
          }
        }, 2000)
      })
        .then(() => { this.status = 'Success!' })
        .catch((errors) => {
          this.status = 'Failure!'
          // Set errors so they can be displayed
          this.errors = errors
        })
        .then(() => {
          // In any case, we are not loading anymore
          this.loading = false
        })
    },
  },
})

export const Default = Template.bind({})
Default.args = {}
