import GpFormErrors from './FormErrors.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Form/Errors',
  component: GpFormErrors,
  argTypes: {
  },
}

const Template = (args) => ({
  components: { GpFormErrors },
  setup () {
    return { args }
  },
  template: '<gp-form-errors v-bind="args" />',
})

export const Default = Template.bind({})
Default.args = {
  model: 'user',
  errors: {
    username: ['is empty'],
    password: ['is too short', 'is well known'],
  },
}
