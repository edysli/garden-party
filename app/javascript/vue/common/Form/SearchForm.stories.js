import GpSearchForm from './SearchForm.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Form/Search form',
  component: GpSearchForm,
  argTypes: {
  },
}

const Template = (args) => ({
  components: { GpSearchForm },
  setup () {
    return { args }
  },
  template: '<gp-search-form v-bind="args" v-model="searchTerm" />'
    + 'Search term: <code>{{ searchTerm }}</code>',
  data () {
    return {
      searchTerm: args.searchTerm,
    }
  },
})

export const Default = Template.bind({})
Default.args = {
  searchTerm: '',
}
