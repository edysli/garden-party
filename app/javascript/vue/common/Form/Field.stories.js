import GpField from './Field.vue'
import GpButton from '../Button/Button.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Form/Field',
  component: GpField,
  argTypes: {
  },
}

const Template = (args) => ({
  components: { GpField },
  setup () {
    return { args }
  },
  template: '<div>'
    + '  <gp-field v-bind="args" inputId="the_input">'
    + '    <input type="text" id="the_input" v-model="value"/>'
    + '  </gp-field>'
    + '  <code>{{value}}</code>'
    + '</div>',
  data () {
    return {
      value: args.value,
    }
  },
})

export const Default = Template.bind({})
Default.args = {
  value: '',
  inline: false,
}

const InlineTemplate = (args) => ({
  components: { GpField, GpButton },
  setup () {
    return { args }
  },
  template: '<div>'
    + '  <gp-field v-bind="args" inputId="the_input">'
    + '    <input type="text" id="the_input" v-model="value"/>'
    + '    <gp-button variant="creative">Search</gp-button>'
    + '  </gp-field>'
    + '  <code>{{value}}</code>'
    + '</div>',
  data () {
    return {
      value: args.value,
    }
  },
})

export const Inline = InlineTemplate.bind({})
Inline.args = {
  value: '',
  inline: true,
}
