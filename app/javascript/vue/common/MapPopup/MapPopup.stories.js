import GpMapPopup from './MapPopup.vue'
import GpButton from '../Button/Button.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Map popup',
  component: GpMapPopup,
  argTypes: {
  },
}

const DefaultTemplate = (args) => ({
  components: { GpMapPopup, GpButton },
  setup () {
    return { args }
  },
  template: '<div style="position: absolute">'
    + '  <gp-map-popup v-bind="args" @close="visible = false" v-if="visible">'
    + '    <gp-button icon="pencil"/>'
    + '    <gp-button icon="trash" variant="destructive"/>'
    + '  </gp-map-popup>'
    + '  <button @click="visible=true" v-show="!visible">Show popup</button>'
    + '</div>',
  data () {
    return {
      visible: false,
    }
  },
  computed: {
    selected () { return this[args.pathType] },
  },
})

export const Default = DefaultTemplate.bind({})
Default.args = {
  surface: '120m<sup>2</sup>',
  caption: 'Some popup',
  help: 'Some help text here',
}
