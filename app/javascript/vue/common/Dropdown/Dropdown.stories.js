import GpDropdown from './Dropdown.vue'
import GpMenuItem from '../Menu/MenuItem.vue'
import GpMenuItemSeparator from '../Menu/MenuItemSeparator.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Dropdown',
  component: GpDropdown,
  argTypes: {
    caption: { control: 'text' },
  },
}

const DefaultTemplate = (args) => ({
  components: { GpDropdown, GpMenuItem, GpMenuItemSeparator },
  setup () {
    return { args }
  },
  template: '<div style="height: 150px">'
    + '<gp-dropdown v-bind="args" caption="Hover me">'
    + '  <gp-menu-item caption="URL" variant="recorder" icon="user" href="https://garden-party.io" target="_blank" />'
    + '  <gp-menu-item caption="JS @click" variant="creative" icon="pencil" @click="doSomething" />'
    + '  <gp-menu-item-separator/>'
    + '  <gp-menu-item caption="Action" variant="destructive" icon="trash" href="#" />'
    + '  <gp-menu-item caption="Router link" icon="blank" to="/" />'
    + '</gp-dropdown>'
    + '</div>',
  methods: {
    /* eslint-disable no-console */
    doSomething () { console.log('clicked') },
    /* eslint-enable no-console */
  },
})

export const Default = DefaultTemplate.bind({})
