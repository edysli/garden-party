import GpDateTimePicker from './DateTimePicker.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Input/Datetime picker',
  component: GpDateTimePicker,
  argTypes: {
  },
}

const DefaultTemplate = (args) => ({
  components: { GpDateTimePicker },
  setup () {
    return { args }
  },
  template: '<div>'
    + '  <gp-date-time-picker v-bind="args" v-model="value" input-id="example" input-name="date" /><br>'
    + '  <code>{{ value }}</code>'
    + '</div>',
  data () {
    return {
      value: new Date(),
    }
  },
})

export const Default = DefaultTemplate.bind({})
Default.args = {
  disabled: false,
}
