import GpRgbaPicker from './RgbaPicker.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Input/RGBA Picker',
  component: GpRgbaPicker,
  argTypes: {
  },
}

const DefaultTemplate = (args) => ({
  components: { GpRgbaPicker },
  setup () {
    return { args }
  },
  template: '<div>'
    + '  <gp-rgba-picker v-bind="args" v-model="value" />'
    + '  <code>{{ value }}</code>'
    + '</div>',
  data () {
    return {
      value: '10,30,20,0.6',
    }
  },
})

export const Default = DefaultTemplate.bind({})
Default.args = {
}
