import GpTimeOutButton from './TimeOutButton.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Input/Timeout button',
  component: GpTimeOutButton,
  argTypes: {
  },
}

const DefaultTemplate = (args) => ({
  components: { GpTimeOutButton },
  setup () {
    return { args }
  },
  template: '<div>'
    + '  <gp-time-out-button v-bind="args" @timed-out="doSomething" @click="done=false">'
    + '    Destroy'
    + '  </gp-time-out-button><br>'
    + '  Done: <code>{{ done }}</code>'
    + '</div>',
  data () {
    return {
      done: false,
    }
  },
  methods: {
    doSomething () {
      this.done = true
    },
  },
})

export const Default = DefaultTemplate.bind({})
Default.args = {
}
