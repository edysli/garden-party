import GpSubmitButton from './SubmitButton.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Input/Submit button',
  component: GpSubmitButton,
  argTypes: {
  },
}

const DefaultTemplate = (args) => ({
  components: { GpSubmitButton },
  setup () {
    return { args }
  },
  template: '<form @submit.prevent="save">'
    + '  <gp-submit-button v-bind="args" :loading="loading" />'
    + '</form>',
  data () {
    return {
      loading: false,
    }
  },
  methods: {
    save () {
      this.loading = true
      setTimeout(() => { this.loading = false }, 2000)
    },
  },
})

export const Default = DefaultTemplate.bind({})
Default.args = {
}
