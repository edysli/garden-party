import GpMonthsFlagsInput from './MonthsFlags.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Input/Months flags',
  component: GpMonthsFlagsInput,
  argTypes: {
    format: {
      control: 'select',
      options: ['integers', 'booleans'],
    },
  },
}

const booleansValue = [true, true, true, false, false, false, false, false, false, false, true, true]
const DefaultTemplate = (args) => ({
  components: { GpMonthsFlagsInput },
  setup () {
    return { args }
  },
  template: '<div>'
    + '  <gp-months-flags-input v-bind="args" v-model="value" input-name="storybook_month" />'
    + '  <code>{{ value }}</code>'
    + '</div>',
  data () {
    return {
      value: booleansValue,
    }
  },
})

export const Default = DefaultTemplate.bind({})
Default.args = {
  format: 'booleans',
  value: booleansValue,
}

const integersValue = [1, 2, 3, 11, 12]
const IntegersTemplate = (args) => ({
  components: { GpMonthsFlagsInput },
  setup () {
    return { args }
  },
  template: '<div>'
    + '  <gp-months-flags-input v-bind="args" v-model="value" input-name="storybook_month" />'
    + '  <code>{{ value }}</code>'
    + '</div>',
  data () {
    return {
      value: integersValue,
    }
  },
})

export const IntegersList = IntegersTemplate.bind({})
IntegersList.args = {
  format: 'integers',
  value: integersValue,
}
