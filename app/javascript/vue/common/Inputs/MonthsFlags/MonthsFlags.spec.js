/**
 * @jest-environment  jsdom
 */
import { mount } from '@vue/test-utils'
import MonthsFlagsComponent from './MonthsFlags'

/**
 * @typedef {import('vue')}                          Vue
 * @typedef {import('@vue/test-utils').VueWrapper  } Wrapper
 */

let wrapper

/**
 * @returns {Wrapper<Vue>} Mounted wrapper for the test
 */
function makeParentWrapper () {
  return mount({
    template: '<template><div><months-flags-component v-model="input" input-id="test" input-name="month" /></div></template>',
    components: { MonthsFlagsComponent },
    data () {
      return { input: [true, true, true, true, true, true, true, true, true, true, true, true] }
    },
  }, {
    global: {
      mocks: {
        $tm: () => ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'nov', 'dec'],
      },
    },
  })
}

describe('bindings with parent', () => {
  beforeEach(() => {
    wrapper = makeParentWrapper()
  })

  it('mounts', () => {
    expect(wrapper.element).toMatchSnapshot()
  })

  describe('interactions', () => {
    describe('when changing a value', () => {
      beforeEach(async () => {
        await wrapper.find('input:first-child').setValue(false)
      })

      it('updates parents state', () => {
        expect(wrapper.vm.input[0]).toBe(false)
      })
    })

    describe('when changing parent state', () => {
      beforeEach(async () => {
        await wrapper.setData({ input: [false, false, false, false, false, false, false, false, false, false, false, false] })
      })

      it('updates the inputs data', () => {
        const input = wrapper.find('input:first-child')
        expect(input.element.checked).toBe(false)
      })
    })
  })
})
