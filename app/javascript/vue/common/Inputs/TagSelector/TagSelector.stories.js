import GpTagSelector from './TagSelector.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Input/Tag selector',
  component: GpTagSelector,
  argTypes: {
  },
}

const DefaultTemplate = (args) => ({
  components: { GpTagSelector },
  setup () {
    return { args }
  },
  template: '<div>'
    + '  <gp-tag-selector v-bind="args" v-model="value" :tags="tags" />'
    + '  <code>{{ value }}</code>'
    + '</div>',
  data () {
    return {
      loading: false,
      value: ['hello', 'world'],
      tags: ['hello', 'world', 'beautiful::true'],
    }
  },
})

export const Default = DefaultTemplate.bind({})
Default.args = {
}
