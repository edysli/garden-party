/**
 * @jest-environment  jsdom
 */
import { mount } from '@vue/test-utils'
import TagSelector from './TagSelector'

/**
 * @typedef {import('vue')}                          Vue
 * @typedef {import('@vue/test-utils').VueWrapper  } Wrapper
 */

let wrapper

/**
 * @returns {Wrapper<Vue>} Mounted wrapper for the test
 */
function makeParentWrapper () {
  return mount({
    template: '<template><div><tag-selector v-model="input"/></div></template>',
    components: { TagSelector },
    data () {
      return { input: ['hello', 'world'] }
    },
  })
}

describe('bindings with parent', () => {
  beforeEach(() => {
    wrapper = makeParentWrapper()
  })

  it('mounts', () => {
    expect(wrapper.element).toMatchSnapshot()
  })

  describe('interactions', () => {
    describe('when adding a tag', () => {
      beforeEach(async () => {
        const input = wrapper.find('.multiselect__input')

        await wrapper.find('.multiselect__tags').trigger('click')
        await input.setValue('test')
        await input.trigger('keypress.enter')
      })

      it('updates parents state', () => {
        expect(wrapper.vm.input).toStrictEqual(['hello', 'world', 'test'])
      })
    })

    describe('changing parent state', () => {
      beforeEach(async () => {
        await wrapper.setData({ input: ['test'] })
      })

      it('updates the Inputs data', () => {
        expect(wrapper.find('.multiselect__tags').text()).toBe('test')
      })
    })
  })
})
