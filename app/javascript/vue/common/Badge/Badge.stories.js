import GpBadge from './Badge.vue'
import '../../../stylesheets/style.scss'

const variants = [null, 'danger', 'success', 'warning', 'help', 'mute']
const sizes = [null, 'small']

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Badge',
  component: GpBadge,
  // More on argTypes: https://storybook.js.org/docs/vue/api/argtypes
  argTypes: {
    variant: {
      control: { type: 'select' },
      options: variants,
    },
    size: {
      control: { type: 'select' },
      options: sizes,
    },
  },
}

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args) => ({
  components: { GpBadge },
  setup () {
    return { args }
  },
  template: '<gp-badge v-bind="args" />',
})

export const Default = Template.bind({})
// More on args: https://storybook.js.org/docs/vue/writing-stories/args
Default.args = {
  caption: 'Notice',
  size: null,
  variant: null,
}
