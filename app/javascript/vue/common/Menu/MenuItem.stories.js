import GpMenuItem from './MenuItem.vue'
import '../../../stylesheets/style.scss'

const variants = [null, 'destructive', 'creative', 'recorder', 'canceling', 'helper']

export default {
  title: 'Menu item',
  component: GpMenuItem,
  argTypes: {
    variant: {
      control: { type: 'select' },
      options: variants,
    },
  },
}

const DefaultTemplate = (args) => ({
  components: { GpMenuItem },
  setup () {
    return { args }
  },
  template: '<div style="height: 150px; width: 200px">'
    + '<gp-menu-item v-bind="args"></gp-menu-item>'
    + '</div>',
})

export const Default = DefaultTemplate.bind({})
Default.args = { caption: 'Action' }
