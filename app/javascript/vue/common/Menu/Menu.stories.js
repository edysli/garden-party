import GpMenu from './Menu.vue'
import GpMenuItem from './MenuItem.vue'
import GpMenuItemSeparator from './MenuItemSeparator.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Menu',
  component: GpMenu,
  argTypes: {

  },
}

const DefaultTemplate = (args) => ({
  components: { GpMenu, GpMenuItem, GpMenuItemSeparator },
  setup () {
    return { args }
  },
  template: '<gp-menu v-bind="args">'
    + '  <gp-menu-item caption="Account" variant="primary" href="/" target="_blank" icon="user" />'
    + '  <gp-menu-item caption="Action" icon="blank" @click="doSomething" />'
    + '  <gp-menu-item-separator />'
    + '  <gp-menu-item caption="Action" @click="doSomething" />'
    + '</gp-menu>',
  methods: {
    /* eslint-disable no-console */
    doSomething () { console.log('clicked') },
    /* eslint-enable no-console */
  },
})

export const Default = DefaultTemplate.bind({})
