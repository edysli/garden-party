import GpButton from './Button.vue'
import '../../../stylesheets/style.scss'

const variants = [null, 'destructive', 'creative', 'recording', 'canceling', 'helping']
const sizes = [null, 'small', 'large']
const types = ['button', 'submit', 'reset']

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Button',
  component: GpButton,
  // More on argTypes: https://storybook.js.org/docs/vue/api/argtypes
  argTypes: {
    variant: {
      control: { type: 'select' },
      options: variants,
    },
    size: {
      control: { type: 'select' },
      options: sizes,
    },
    type: {
      control: { type: 'select' },
      options: types,
    },
  },
}

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args) => ({
  components: { GpButton },
  setup () {
    return { args }
  },
  template: '<gp-button v-bind="args">Click me</gp-button>',
})

export const Default = Template.bind({})
// More on args: https://storybook.js.org/docs/vue/writing-stories/args
Default.args = {}
