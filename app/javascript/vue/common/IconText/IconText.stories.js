import GpIconText from './IconText.vue'
import '../../../stylesheets/style.scss'

const variants = [null, 'info', 'success', 'danger', 'warning', 'mute']
const sizes = [null, 'small', 'large']

export default {
  title: 'Icon and text',
  component: GpIconText,
  argTypes: {
    variant: {
      control: { type: 'select' },
      options: variants,
    },
    size: {
      control: { type: 'select' },
      options: sizes,
    },
  },
}

const Template = (args) => ({
  components: { GpIconText },
  setup () {
    return { args }
  },
  template: '<gp-icon-text v-bind="args">Lorem ipsum dolor sit amet.</gp-icon-text>',
})

export const Default = Template.bind({})
// More on args: https://storybook.js.org/docs/vue/writing-stories/args
Default.args = {
  icon: 'map-pin',
}
