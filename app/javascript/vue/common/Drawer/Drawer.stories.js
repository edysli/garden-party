import GpDrawer from './Drawer.vue'
import GpButton from '../Button/Button.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Drawer',
  component: GpDrawer,
  argTypes: {
    title: {
      control: { type: 'text' },
    },
  },
}

const DefaultTemplate = (args) => ({
  components: { GpDrawer, GpButton },
  setup () {
    return { args }
  },
  template: '<div>'
    + '  <gp-drawer v-show="showDrawer" @close="showDrawer = false" title="A title">Some content here</gp-drawer>'
    + '  <div>'
    + '    <gp-button @click="showDrawer = !showDrawer">Toggle drawer</gp-button>'
    + '  </div>'
    + '</div>',
  data () {
    return {
      showDrawer: false,
    }
  },
})

export const Default = DefaultTemplate.bind({})
