import GpIcon from './Icon.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Icon',
  component: GpIcon,
  argTypes: {
    name: { control: 'text' },
    size: { control: 'select', options: [3, 4, 10] },
    title: { control: 'text' },
    spin: { control: 'boolean' },
  },
}

const Template = (args) => ({
  components: { GpIcon },
  setup () {
    return { args }
  },
  template: '<gp-icon v-bind="args" />',
})

export const Example = Template.bind({})
Example.args = {
  name: 'map-pin',
}
