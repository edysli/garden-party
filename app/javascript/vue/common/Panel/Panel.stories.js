import GpPanel from '../Panel/Panel.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Panel',
  component: GpPanel,
  argTypes: {
    resize: {
      control: { type: 'select' },
      options: [null, 'right', 'left'],
    },
  },
}

const DefaultTemplate = (args) => ({
  components: { GpPanel },
  setup () {
    return { args }
  },
  template: '<div style="display: flex">'
    + '  <gp-panel resize="right">Resizable with some content</gp-panel>'
    + '  <div style="flex-grow: 1">Page content</div>'
    + '  <gp-panel v-bind="args">Some content</gp-panel>'
    + '</div>',
})

export const Default = DefaultTemplate.bind({})
