import GpTabs from './Tabs.vue'
import GpButton from '../Button/Button.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Tabs',
  component: GpTabs,
  argTypes: {
  },
}

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const DefaultTemplate = (args) => ({
  components: { GpTabs, GpButton },
  setup () {
    return { args }
  },
  template: '<div>'
    + '  <gp-tabs v-bind="args">'
    + '    <gp-button @click="tab=\'tab1\'" :active="tab === \'tab1\'" icon="info">Tab1</gp-button>'
    + '    <gp-button @click="tab=\'tab2\'" :active="tab === \'tab2\'">Tab2</gp-button>'
    + '    <gp-button @click="tab=\'tab3\'" :active="tab === \'tab3\'" icon="image">Tab3</gp-button>'
    + '  </gp-tabs>'
    + '  <div v-show="tab === \'tab1\'">Tab 1 content</div>'
    + '  <div v-show="tab === \'tab2\'">Tab 2 content</div>'
    + '  <div v-show="tab === \'tab3\'">Tab 3 content</div>'
    + '</div>',
  data () {
    return {
      tab: 'tab2',
    }
  },
})

export const Default = DefaultTemplate.bind({})
