import GpToolbar from './Toolbar.vue'
import GpToolbarSeparator from './ToolbarSeparator.vue'
import GpButton from '../Button/Button.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Toolbar',
  component: GpToolbar,
  argTypes: {
    label: { control: 'text' },
    help: { control: 'text' },
  },
}

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const DefaultTemplate = (args) => ({
  components: { GpToolbar, GpButton, GpToolbarSeparator },
  setup () {
    return { args }
  },
  template: '<gp-toolbar v-bind="args">'
    + '<gp-button icon="polygon-plus"></gp-button>'
    + '<gp-button icon="circle-plus"></gp-button>'
    + '<gp-toolbar-separator />'
    + '<gp-button icon="pencil" variant="creative"></gp-button>'
    + '<gp-toolbar-separator />'
    + '<gp-button icon="trash" variant="destructive" title="destroy"/>'
    + '</gp-toolbar>',
})

export const Default = DefaultTemplate.bind({
  label: 'Actions',
})
