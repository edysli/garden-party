import GpToolbars from './Toolbars.vue'
import GpToolbarsSpring from './ToolbarsSpring.vue'
import GpToolbar from './Toolbar.vue'
import GpButton from '../Button/Button.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Toolbars',
  component: GpToolbars,
  argTypes: {
  },
}

const toolbarTemplate = ''
  + '<gp-toolbar title="Actions">'
    + '<gp-button icon="polygon-plus"></gp-button>'
    + '<gp-button icon="circle-plus"></gp-button>'
    + '<gp-button icon="pencil" variant="creative"></gp-button>'
    + '<gp-button icon="trash" variant="destructive" title="destroy"/>'
    + '<gp-button variant="recorder">Help</gp-button>'
  + '</gp-toolbar>'

const DefaultTemplate = (args) => ({
  components: { GpToolbars, GpToolbarsSpring, GpToolbar, GpButton },
  setup () {
    return { args }
  },
  template: '<gp-toolbars v-bind="args">'
    + toolbarTemplate
    + toolbarTemplate
    + '<gp-toolbars-spring />'
    + toolbarTemplate
    + '</gp-toolbars>',
})

export const Default = DefaultTemplate.bind({})
