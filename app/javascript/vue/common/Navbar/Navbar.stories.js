import GpNavbar from './Navbar.vue'
import GpNavbarMenu from './NavbarMenu.vue'
import GpMenuItem from '../Menu/MenuItem.vue'
import GpMenuItemSeparator from '../Menu/MenuItemSeparator.vue'
import GpButton from '../Button/Button.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Navbar',
  component: GpNavbar,
  argTypes: {
    caption: { control: 'text' },
  },
}

const DefaultTemplate = (args) => ({
  components: { GpNavbar, GpNavbarMenu, GpMenuItem, GpMenuItemSeparator },
  setup () {
    return { args }
  },
  template: '<gp-navbar v-bind="args" caption="Home">'
    + '<template #left>'
    + '  <gp-navbar-menu caption="main">'
    + '    <gp-menu-item href="/">Home</gp-menu-item>'
    + '    <gp-menu-item href="#">Posts</gp-menu-item>'
    + '  </gp-navbar-menu>'
    + '</template>'
    + '<template #right>'
    + '  <gp-navbar-menu right toolbar>'
    + '    <gp-menu-item icon="user" href="/">Account</gp-menu-item>'
    + '    <gp-menu-item-separator />'
    + '    <gp-menu-item icon="sign-out" href="#">Sign out</gp-menu-item>'
    + '  </gp-navbar-menu>'
    + '</template>'
    + '</gp-navbar>',
})

export const Default = DefaultTemplate.bind({})

const WithActionTemplate = (args) => ({
  components: { GpNavbar, GpNavbarMenu, GpMenuItem, GpMenuItemSeparator, GpButton },
  setup () {
    return { args }
  },
  template: '<gp-navbar v-bind="args" caption="Home" back-to="/" back-caption="Back to map">'
    + '<template #left>'
    + '  <gp-navbar-menu caption="main">'
    + '    <gp-menu-item href="/">Home</gp-menu-item>'
    + '    <gp-menu-item href="#">Posts</gp-menu-item>'
    + '  </gp-navbar-menu>'
    + '</template>'
    + '<template #right>'
    + '  <gp-navbar-menu right toolbar>'
    + '    <gp-menu-item icon="user" href="/">Account</gp-menu-item>'
    + '    <gp-menu-item-separator />'
    + '    <gp-menu-item icon="sign-out" href="#">Sign out</gp-menu-item>'
    + '  </gp-navbar-menu>'
    + '</template>'
    + '</gp-navbar>',
})

export const WithAction = WithActionTemplate.bind({})
