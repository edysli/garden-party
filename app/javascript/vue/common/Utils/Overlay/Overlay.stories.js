import GpOverlay from './Overlay.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Utils/Overlay',
  component: GpOverlay,
}

export const Example = (args) => ({
  components: { GpOverlay },
  setup () {
    return { args }
  },
  template: '<div style="min-height: 300px">'
    + '<gp-overlay />'
    + 'The overlay covers all the available space and is above the content'
    + '</div>',
})
