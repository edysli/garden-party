import GpObservation from './Observation.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Data display/Observation',
  component: GpObservation,
  argTypes: {
  },
}

const Template = (args) => ({
  components: { GpObservation },
  setup () {
    return { args }
  },
  template: '<gp-observation v-bind="args" />',
})
export const Example = Template.bind({})
Example.args = {
  caption: 'Beautiful day',
  date: new Date(),
  content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt excepturi hic quidem totam voluptatibus. Ab culpa id nulla. Aliquam aperiam error illum necessitatibus provident sint! Cumque error eum inventore vero!',
  pictures: [
    // Portrait thumbnail
    { source: 'https://picsum.photos/id/1/200/300', thumbnail: 'https://picsum.photos/id/1/600/800' },
    // Landscape thumbnail
    { source: 'https://picsum.photos/id/10/300/200', thumbnail: 'https://picsum.photos/id/10/800/600' },
  ],
}
