import GpImageViewer from './ImageViewer.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Data display/Image viewer',
  component: GpImageViewer,
  argTypes: {
    preselected: { control: 'text' },
  },
}

const list = [
  'https://picsum.photos/id/10/1920/1080',
  'https://picsum.photos/id/100/1920/1080',
  'https://picsum.photos/id/1000/800/600',
]

const DefaultTemplate = (args) => ({
  components: { GpImageViewer },
  setup () {
    return { args }
  },
  template: '<div style="min-height: 300px">'
    + '  <gp-image-viewer v-bind="args" :list="list" :preselected="list[0]" @close="handleClose"/>'
    + '</div>',
  data () {
    return {
      list,
      preselected: list[0],
    }
  },
  methods: {
    /* eslint-disable no-console */
    handleClose () { console.log('close') },
    /* eslint-enable no-console */
  },
})

export const Default = DefaultTemplate.bind({
  list,
  preselected: list[0],
})
