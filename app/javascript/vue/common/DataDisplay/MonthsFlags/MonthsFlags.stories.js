import GpMonthsFlags from './MonthsFlags.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Data display/Months flags',
  component: GpMonthsFlags,
  argTypes: {
  },
}

const flags = {
  'sowing months': [true, true, true, false, false, false, true, true, false, false, false, false],
}

const DefaultTemplate = (args) => ({
  components: { GpMonthsFlags },
  setup () {
    return { args }
  },
  template: '<gp-months-flags v-bind="args" :flags="flags" />',
  data () {
    return {
      flags,
    }
  },
})

export const Default = DefaultTemplate.bind({})
Default.args = {
  flags,
}
