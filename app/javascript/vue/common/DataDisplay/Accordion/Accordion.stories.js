import GpAccordion from './Accordion.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Data display/Accordion',
  component: GpAccordion,
  argTypes: {
    caption: { control: 'text' },
    icon: { control: 'text' },
  },
}

const DefaultTemplate = (args) => ({
  components: { GpAccordion },
  setup () {
    return { args }
  },
  template: '<div>'
    + '  <gp-accordion v-bind="args">'
    + '    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At delectus dolores, expedita, ipsam laudantium libero minima modi non numquam, odit officia possimus quibusdam quod repellat similique tempore vero voluptas? Dolor.'
    + '  </gp-accordion>'
    + '</div>',
})

export const Default = DefaultTemplate.bind({})
Default.args = {
  caption: 'Expand me',
  icon: 'user',
}
