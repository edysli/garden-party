import GpBreadcrumbs from './Breadcrumbs.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Data display/Breadcrumbs',
  component: GpBreadcrumbs,
}

const DefaultTemplate = (args) => ({
  components: { GpBreadcrumbs },
  setup () {
    return { args }
  },
  template: '<gp-breadcrumbs v-bind="args" />',
})

export const Default = DefaultTemplate.bind({})
Default.args = {
  chunks: [
    { name: 'Home', to: '/' },
    { name: 'Category 1', to: { name: 'category', params: { id: 1 } } },
    { name: 'Subcategory 1', to: { name: 'subcategory', params: { id: 1 } } },
    { name: 'Item' },
  ],
}
