import GpThumbnails from './Thumbnails.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Data display/Thumbnails',
  component: GpThumbnails,
  argTypes: {
  },
}

const Template = (args) => ({
  components: { GpThumbnails },
  setup () {
    return { args }
  },
  template: '<gp-thumbnails v-bind="args" />',
})

export const Example = Template.bind({})
Example.args = {
  list: [
    // Portrait thumbnail
    { source: 'https://picsum.photos/id/1/200/300', thumbnail: 'https://picsum.photos/id/1/600/800' },
    // Landscape thumbnail
    { source: 'https://picsum.photos/id/10/300/200', thumbnail: 'https://picsum.photos/id/10/800/600' },
  ],
}
