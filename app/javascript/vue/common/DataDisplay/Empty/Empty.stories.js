import GpEmpty from './Empty.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Data display/Empty',
  component: GpEmpty,
  argTypes: {
    iconSize: {
      control: 'select',
      options: [null, 3, 4, 10],
    },
    icon: { control: 'text' },
  },
}

const DefaultTemplate = (args) => ({
  components: { GpEmpty },
  setup () {
    return { args }
  },
  template: '<gp-empty v-bind="args">'
    + '    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At delectus dolores, expedita, ipsam laudantium libero minima modi non numquam, odit officia possimus quibusdam quod repellat similique tempore vero voluptas? Dolor.'
    + '</gp-empty>',
})

export const Default = DefaultTemplate.bind({})
Default.args = {
}
