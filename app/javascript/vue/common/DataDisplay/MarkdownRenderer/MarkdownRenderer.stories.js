import GpMarkdownRenderer from './MarkdownRenderer.vue'
import '../../../../stylesheets/style.scss'
import '../../../../helpers/markdown'

export default {
  title: 'Data display/Markdown renderer',
  component: GpMarkdownRenderer,
  argTypes: {
  },
}

const DefaultTemplate = (args) => ({
  components: { GpMarkdownRenderer },
  setup () {
    return { args }
  },
  template: '<gp-markdown-renderer v-bind="args" />',
})

export const Default = DefaultTemplate.bind({})
Default.args = {
  content: '# Title1\n\n_Hello_ **world**.',
}
