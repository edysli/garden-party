import GpSoilAnalyzer from './SoilAnalyzer.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Data display/Soil analyzer',
  component: GpSoilAnalyzer,
  argTypes: {
    clayPercent: { control: 'range', min: 0, max: 100, step: 1 },
    siltPercent: { control: 'range', min: 0, max: 100, step: 1 },
    sandPercent: { control: 'range', min: 0, max: 100, step: 1 },
  },
}

const DefaultTemplate = (args) => ({
  components: { GpSoilAnalyzer },
  setup () {
    return { args }
  },
  template: '<gp-soil-analyzer v-bind="args" style="width:400px" />',
})

export const Default = DefaultTemplate.bind({})
Default.args = {
  clayPercent: 10,
  siltPercent: 30,
  sandPercent: 60,
}
