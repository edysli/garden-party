import GpTags from './Tags.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Data display/Tags',
  component: GpTags,
  argTypes: {
  },
}

const DefaultTemplate = (args) => ({
  components: { GpTags },
  setup () {
    return { args }
  },
  template: '<gp-tags v-bind="args" />',
})

export const Default = DefaultTemplate.bind({})
Default.args = {
  tags: [
    'color::red',
    'color::blue',
    'size::large',
    'front::wheels::rubber',
  ],
}
