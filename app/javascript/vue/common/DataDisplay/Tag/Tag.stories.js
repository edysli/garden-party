import GpTag from './Tag.vue'
import '../../../../stylesheets/style.scss'

export default {
  title: 'Data display/Tag',
  component: GpTag,
  argTypes: {
  },
}

const DefaultTemplate = (args) => ({
  components: { GpTag },
  setup () {
    return { args }
  },
  template: '<gp-tag v-bind="args" />',
})

export const Default = DefaultTemplate.bind({})
Default.args = {
  tag: 'hello::world',
}
