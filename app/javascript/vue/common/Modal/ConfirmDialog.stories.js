import GpConfirmDialog from './ConfirmDialog.vue'
import GpButton from '../Button/Button.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Confirm dialog',
  component: GpConfirmDialog,
  argTypes: {},
}

const Template = (args) => ({
  components: { GpConfirmDialog, GpButton },
  setup () {
    return { args }
  },
  template: '<div style="height: 300px">'
    + '  <gp-button @click="showDialog = !showDialog">Show dialog</gp-button><br>'
    + '  Choice: <code>{{choice}}</code>'
    + '  <gp-confirm-dialog v-bind="args" '
    + '                     v-show="showDialog" '
    + '                     @close="handleClose"'
    + '                     @reject="handleRefuse"'
    + '                     @accept="handleAccept">'
    + '    Really?'
    + '  </gp-confirm-dialog>'
    + '</div>',
  data () {
    return {
      showDialog: false,
      choice: null,
    }
  },
  methods: {
    handleClose () {
      this.choice = 'Closed'
      this.showDialog = false
    },
    handleRefuse () {
      this.choice = 'Refused'
      this.showDialog = false
    },
    handleAccept () {
      this.choice = 'Accepted'
      this.showDialog = false
    },
  },
})

export const Default = Template.bind({})
Default.args = {}
