import GpModal from './Modal.vue'
import GpButton from '../Button/Button.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Modal',
  component: GpModal,
  argTypes: {},
}

const Template = (args) => ({
  components: { GpModal, GpButton },
  setup () {
    return { args }
  },
  template: '<div style="height: 800px">'
    + '  <gp-button @click="showModal = !showModal">Show modal</gp-button>'
    + '  <gp-modal v-bind="args" '
    + '            caption="Nice title" '
    + '            v-show="showModal" '
    + '            @close="showModal = false"'
    + '            @accept="showModal = false"'
    + '            @cancel="showModal = false">'
    + '    <gp-button @click="linesOfContent += 1">Add content</gp-button>'
    + '    <p v-for="i in linesOfContent">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias aperiam asperiores atque beatae delectus ea eligendi enim eveniet id, incidunt itaque maiores maxime nisi, sapiente sit voluptas! Autem, nesciunt?</p>'
    + '</gp-modal>'
    + '</div>',
  data () {
    return {
      showModal: false,
      linesOfContent: 1,
    }
  },
})

export const Default = Template.bind({})
Default.args = {
  title: 'Nice title',
}

const TemplateWithPageActions = (args) => ({
  components: { GpModal, GpButton },
  setup () {
    return { args }
  },
  template: '<div style="height: 800px">'
    + '  <gp-button @click="showModal = !showModal">Show modal</gp-button>'
    + '  <gp-modal v-bind="args" '
    + '            caption="Nice title" '
    + '            v-show="showModal" '
    + '            @close="showModal = false"'
    + '            @accept="showModal = false"'
    + '            @cancel="showModal = false">'
    + '    <div class="actions actions--top">Some custom actions (top)</div>'
    + '    <gp-button @click="linesOfContent += 1">Add content</gp-button>'
    + '    <p v-for="i in linesOfContent">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias aperiam asperiores atque beatae delectus ea eligendi enim eveniet id, incidunt itaque maiores maxime nisi, sapiente sit voluptas! Autem, nesciunt?</p>'
    + '    <div class="actions actions--bottom">Some custom actions (bottom)</div>'
    + '</gp-modal>'
    + '</div>',
  data () {
    return {
      showModal: false,
      linesOfContent: 1,
    }
  },
})

export const WithPageActions = TemplateWithPageActions.bind({})
WithPageActions.args = {
  title: 'Nice title',
}
