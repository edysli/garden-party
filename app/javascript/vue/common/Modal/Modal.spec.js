/**
 * @jest-environment  jsdom
 */
import { mount } from '@vue/test-utils'
import ModalComponent from './Modal'

/**
 * @typedef {import('vue')}                          Vue
 * @typedef {import('@vue/test-utils').VueWrapper  } Wrapper
 */

/**
 * @param   {object}       props - Custom props
 * @param   {object}       slots - Slot values
 * @returns {Wrapper<Vue>}       Mounted wrapper for the test
 */
function makeWrapper (props = {}, slots = {}) {
  return mount(ModalComponent, {
    props,
    slots,
    global: {
      mocks: {
        $t: s => s,
      },
    },
  })
}

let wrapper

describe('interface', () => {
  describe('with only title prop', () => {
    beforeEach(() => {
      wrapper = makeWrapper({ caption: 'Hello world' })
    })

    test('displays title', () => {
      expect(wrapper.text()).toContain('Hello world')
    })

    test('has only the validation button by default', () => {
      expect(wrapper.text()).toContain('generic.ok')
      expect(wrapper.findAll('.gp-modal__footer button')).toHaveLength(1)
    })
  })

  describe('with cancellable option', () => {
    beforeEach(() => {
      wrapper = makeWrapper({
        caption: 'Hello world',
        cancellable: true,
      })
    })

    test('has a cancel button', () => {
      expect(wrapper.text()).toContain('generic.cancel')
    })
  })

  describe('without footer', () => {
    beforeEach(() => {
      wrapper = makeWrapper({
        caption: 'Hello world',
        hasFooter: false,
      })
    })

    test('has no footer', () => {
      expect(wrapper.text()).not.toContain('generic.cancel')
      expect(wrapper.text()).not.toContain('generic.ok')
    })
  })

  describe('with a slot', () => {
    let slotContent

    beforeEach(() => {
      slotContent = 'This is the slot content'
      wrapper = makeWrapper({
        caption: 'Hello world',
        hasFooter: false,
      },
      { default: slotContent })
    })

    test('renders slot', () => {
      expect(wrapper.text()).toContain(slotContent)
    })
  })
})

// TODO: Test interactions
