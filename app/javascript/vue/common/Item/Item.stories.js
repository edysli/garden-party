import GpItem from './Item.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Item',
  component: GpItem,
  argTypes: {
  },
}

const Template = (args) => ({
  components: { GpItem },
  setup () {
    return { args }
  },
  template: '<gp-item v-bind="args">'
    + '  <template #aside>'
    + '    Actions, infos,...'
    + '  </template>'
    + '  Some content here'
    + '</gp-item>',
})

export const Example = Template.bind({})
Example.args = {
  caption: 'An item',
  icon: 'map-pin',
}
