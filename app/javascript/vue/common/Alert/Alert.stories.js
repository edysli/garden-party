import GpAlert from './Alert.vue'
import '../../../stylesheets/style.scss'

const variants = [null, 'danger', 'warning', 'info', 'success', 'mute']

export default {
  title: 'Alert',
  component: GpAlert,
  argTypes: {
    variant: {
      control: { type: 'select' },
      options: variants,
    },
  },
}

const Template = (args) => ({
  components: { GpAlert },
  setup () {
    return { args }
  },
  template: '<gp-alert v-bind="args">'
    + '  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam distinctio in inventore officiis! Aperiam at blanditiis consectetur deleniti doloremque dolorum eaque eveniet, ipsa nihil quaerat quam reiciendis repellendus suscipit vitae.'
    + '</gp-alert>',
})

export const Default = Template.bind({})
Default.args = {}
