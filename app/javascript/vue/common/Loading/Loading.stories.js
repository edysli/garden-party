import GpLoading from './Loading.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Loading',
  component: GpLoading,
  argTypes: {
  },
}

const DefaultTemplate = (args) => ({
  components: { GpLoading },
  setup () {
    return { args }
  },
  template: '<div style="height: 200px;">'
    + '<gp-loading v-bind="args" />'
    + '</div>',
})

export const Default = DefaultTemplate.bind({})
Default.args = { text: 'Loading something' }
