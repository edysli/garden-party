import GpActions from './Actions.vue'
import GpButton from '../Button/Button.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Actions',
  component: GpActions,
  argTypes: {
  },
}

const DefaultTemplate = (args) => ({
  components: { GpActions, GpButton },
  setup () {
    return { args }
  },
  template: '<div style="max-height: 100px; overflow-y: auto">'
    + '  Scroll to see the actions bar'
    + '  <p v-for="i in 15">line {{i}}</p>'
    + '  <gp-actions v-bind="args">'
    + '    <gp-button>Action 1</gp-button>'
    + '    <gp-button>Action 2</gp-button>'
    + '  </gp-actions>'
    + '  <p v-for="i in 15">line {{i}}</p>'
    + '</div>',
})

export const Default = DefaultTemplate.bind({})
Default.args = {
}
