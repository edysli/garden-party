import GpCollapsibleListItem from './CollapsibleListItem.vue'
import GpButton from '../Button/Button.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Lists / Collapsible list item',
  component: GpCollapsibleListItem,
  argTypes: {
  },
}

const Template = (args) => ({
  components: { GpCollapsibleListItem, GpButton },
  setup () {
    return { args }
  },
  template: '<gp-collapsible-list-item v-bind="args">'
    + '  <template #actions><gp-button icon="trash" variant="destructive" /></template>'
    + ''
    + '  <p>Here is some hidden content</p>'
    + '</gp-collapsible-list-item>',
})

export const Example = Template.bind({})
Example.args = {
  caption: 'Some content that may overflow',
  subCaption: 'Some precisions',
}
