import GpListItem from './ListItem.vue'
import GpButton from '../Button/Button.vue'
import '../../../stylesheets/style.scss'

export default {
  title: 'Lists / List item',
  component: GpListItem,
  argTypes: {
  },
}

const Template = (args) => ({
  components: { GpListItem, GpButton },
  setup () {
    return { args }
  },
  template: '<gp-list-item v-bind="args">'
    + '  <template #left><gp-button icon="pencil" /></template>'
    + '  <template #right><gp-button icon="trash" variant="destructive" /></template>'
    + '</gp-list-item>',
})

export const Example = Template.bind({})
Example.args = {
  caption: 'Some content that may overflow',
  subCaption: 'Some precisions',
  icon: 'map-pin',
}
