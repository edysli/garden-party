// Base map requirements
import OlMap from 'ol/Map'
import View from 'ol/View'
import Projection from 'ol/proj/Projection'
import { ScaleLine } from 'ol/control'
// For user-submitted background layer
import ImageLayer from 'ol/layer/Image'
import StaticSource from 'ol/source/ImageStatic'
// For OSM map
import TileLayer from 'ol/layer/Tile'
import OSMSource from 'ol/source/OSM'
// For vector layers (plantationLayer representations)
import VectorLayer from 'ol/layer/Vector'
import VectorSource from 'ol/source/Vector'
// For map interactions
import DrawInteraction from 'ol/interaction/Draw'
import ModifyInteraction from 'ol/interaction/Modify'
import RotateInteraction from 'ol-rotate-feature'
import TranslateInteraction from 'ol/interaction/Translate'
import SelectInteraction from 'ol/interaction/Select'
// For drawing shapes
import LineStringGeom from 'ol/geom/LineString'
import { getArea, getLength } from 'ol/sphere'
// For features styling
import StyleLibrary from './StyleLibrary'
// For popups
import Overlay from 'ol/Overlay'
// VueX store
import Store from '../apps/garden/store'
// Models
import Patch from './models/Patch'
import Path from './models/Path'
// Other
import { featureFromGeometry, geoJSONFromFeature } from '../tools/Geometry'
import { formatLength, formatArea } from '../tools/MeasureFormatters'

import { unByKey } from 'ol/Observable'

/**
 * @typedef {import('ol/Feature')}             Feature
 * @typedef {import('ol/interaction/Pointer')} PointerInteraction
 * @typedef {import('ol/style/Style')}         Style
 * @typedef {import('./models/Resource')}      Resource
 * @typedef {import('./models/Layer')}         Layer
 * @typedef {import('./models/Placeable')}     Placeable
 */

const ALLOWED_SHAPES = {
  patch: ['Circle', 'Polygon'],
  path: ['LineString', 'Circle', 'Polygon'],
}

export default class GardenMap {
  /**
   * Prepares a new map to display.
   *
   * @param {Map}         mapConfig               - Map configuration from API
   * @param {string}      elementId               - Target DOM element ID
   * @param {string}      popupOverlayElementId   - DOM element ID of the popup box
   * @param {string|null} tooltipOverlayElementId - DOM element ID of the tooltip box
   */
  constructor (mapConfig, elementId, popupOverlayElementId, tooltipOverlayElementId = null) {
    /** @type {HTMLElement} Map element, to bind events */
    this.mapElement = document.getElementById(elementId)

    /**
     * @type {Map} GardenParty Map instance
     * @private
     */
    this._mapConfig = mapConfig

    /**
     * @type {string|null} Current state/step/whatever the map "is doing"
     * @private
     */
    this._mode = null

    /**
     * @type {Feature|null} The pointed feature
     * @private
     */
    this._pointedFeature = null

    /**
     * Drawing interaction, is reinitialized for every drawing
     *
     * @type {PointerInteraction|null}
     * @private
     */
    this._interaction = null

    /**
     * Select interaction, is active during the whole lifecycle, only deactivated sometimes
     *
     * @type {SelectInteraction|null}
     * @private
     */
    this._selectInteraction = null

    /**
     * @type {Style[]|null} Style backup when modifying features
     * @private
     */
    this._styleBackup = null

    /**
     * @type {Placeable|null} - Externally changed placeable to force redraw when selection is over
     * @private
     */
    this._externallyChangedPlaceable = null

    /**
     * @type {Overlay|null} The map overlay for popups
     * @private
     */
    this._popupOverlay = null

    /**
     * @type {HTMLElement} Tooltip element, to bind events
     * @private
     */
    this._tooltipElement = tooltipOverlayElementId ? document.getElementById(tooltipOverlayElementId) : null
    /**
     * @type {Overlay|null} The map overlay for popup
     * @private
     */
    this._tooltipOverlay = null

    /**
     * @type {VectorLayer|null} Active layer to work on
     * @private
     */
    this._activeLayer = null

    this._createMap(elementId)
    this._createOverlays(popupOverlayElementId)
    this._createLayers()
    this._createInteractions()
    this._preloadPatterns()
      .then(() => {
        for (const path of Store.getters.paths) this.addPlaceable(path)
        for (const patch of Store.getters.patches) this.addPlaceable(patch)
        for (const element of Store.getters.activePointElements) this.addPlaceable(element)

        this._notify('ready', true)
      })
  }

  /**
   * Sets the map in drawing mode for a given shape and parameters
   *
   * @param {string}   shape    - Shape of the thing to draw
   * @param {Resource} resource - Resource to represent
   */
  async drawPatch (shape, resource) {
    if (ALLOWED_SHAPES.patch.indexOf(shape) === -1) throw new Error(`Unsupported shape ${shape} for a Patch`)
    if (!this._activeLayer) throw new Error('No active layer to draw on')

    this._changeMode('draw')

    const styles = await StyleLibrary.stylesForShape({
      fillColor: resource.fillColor,
      borderColor: resource.borderColor,
      resource,
    })
    this._interaction = new DrawInteraction({
      source: this._activeLayer.getSource(),
      type: shape,
      style: styles,
    })

    this._addDrawInteractionListeners('patch', shape, styles, (feature) => {
      this._notify('add-patch', {
        resourceId: resource.id,
        geoJSON: geoJSONFromFeature(feature),
        feature,
      })
    },)

    // Start drawing
    this._gardenMap.addInteraction(this._interaction)
  }

  /**
   * Sets the map in drawing mode to add a resource element
   *
   * @param {Resource} resource - Resource to represent
   */
  drawElement (resource) {
    if (!this._activeLayer) throw new Error('No active layer to draw on')

    this._changeMode('draw')

    const styles = StyleLibrary.stylesForPoint(this._gardenMap.getView(), resource)
    this._interaction = new DrawInteraction({
      source: this._activeLayer.getSource(),
      type: 'Point',
      style: styles,
    })

    this._addDrawInteractionListeners('element', 'Point', styles, (feature) => {
      this._notify('add-element', {
        resourceId: resource.id,
        geoJSON: geoJSONFromFeature(feature),
        feature,
      })
    },)

    // Start drawing
    this._gardenMap.addInteraction(this._interaction)
  }

  /**
   * Sets map in a "drawing path mode", to add geometries
   *
   * @param {string} shape                   - Shape type. Check code for allowed values
   * @param {object} style                   - Path style
   * @param {string} style.backgroundColor   - rgba like '123,123,123,1'
   * @param {string} style.strokeColor       - rgba like '123,123,123,1'
   * @param {number} style.strokeWidth       - Thickness
   * @param {number} style.strokeStylePreset - Preset for stroke style
   */
  async drawPath (shape, { backgroundColor, strokeColor, strokeWidth, strokeStylePreset }) {
    if (ALLOWED_SHAPES.path.indexOf(shape) === -1) throw new Error(`Unsupported shape ${shape} for a Path`)
    if (!this._activeLayer) throw new Error('No active layer to draw on')

    this._changeMode('draw')

    const styles = await StyleLibrary.stylesForShape({
      fillColor: `rgba(${backgroundColor})`,
      borderColor: `rgba(${strokeColor})`,
      strokeWidth,
      strokeStylePreset,
    })
    this._interaction = new DrawInteraction({
      source: this._activeLayer.getSource(),
      type: shape,
      styles,
    })

    this._addDrawInteractionListeners('path', shape, styles, (feature) => {
      this._notify('add-path', {
        geoJSON: geoJSONFromFeature(feature),
        feature,
        backgroundColor,
        strokeColor,
        strokeWidth,
        strokeStylePreset,
      })
    },)

    // Start drawing
    this._gardenMap.addInteraction(this._interaction)
  }

  /**
   * Adds a geometry from an entity to the map
   *
   * @param {Placeable} placeable - Thing to add
   */
  async addPlaceable (placeable) {
    const feature = featureFromGeometry(placeable.geometry, placeable.geometryType)

    const type = placeable.className.toLowerCase()
    feature.setStyle(await StyleLibrary.stylesFromEntity(this._gardenMap.getView(), placeable))
    feature.setProperties({ type, id: placeable.id })

    placeable.layer.mapLayer.getSource().addFeature(feature)
    placeable.setFeature(feature)
  }

  deselect () { this._changeMode('review') }

  /**
   * Puts map in review mode: click somewhere and get info
   */
  review () {
    this._changeMode('review')
  }

  /**
   * Start rotating the pointed Feature
   */
  rotateElement () {
    this._interact(RotateInteraction, 'rotateend')
  }

  /**
   * Start transforming the pointed Feature
   */
  transformElement () {
    this._interact(ModifyInteraction, 'modifyend')
  }

  /**
   * Start translating the pointed Feature
   */
  translateElement () {
    this._interact(TranslateInteraction, 'translateend')
  }

  /**
   * Removes a given feature
   *
   * @param {Placeable} placeable - Thing to remove
   */
  removeEntityFeature (placeable) {
    placeable.layer.mapLayer.getSource().removeFeature(placeable.feature)
  }

  /**
   * Redraws a given feature from its entity
   *
   * @param {Placeable} placeable - Thing to redraw
   */
  async redrawEntity (placeable) {
    placeable.feature.setStyle(await StyleLibrary.stylesFromEntity(this._gardenMap.getView(), placeable))
  }

  forceRedrawSelection (placeable) {
    if (this._externallyChangedPlaceable
      && placeable.className !== this._externallyChangedPlaceable.className
      && placeable.id !== this._externallyChangedPlaceable.id) {
      throw new Error('A placeable is already waiting for a forced redraw')
    }

    this._externallyChangedPlaceable = placeable
  }

  /**
   * Moves a feature from its declared mapLayer to its current layer
   *
   * @param {Layer}     fromLayer - Original layer
   * @param {Placeable} placeable - The thing to move
   */
  moveEntityToOtherLayer (fromLayer, placeable) {
    fromLayer.mapLayer.getSource().removeFeature(placeable.feature)
    if (placeable instanceof Patch) this.addPlaceable(placeable)
    else if (placeable instanceof Path) this.addPlaceable(placeable)
  }

  /**
   * Toggle layers visibility
   *
   * @param {number[]} ids - List of enabled layer IDs
   */
  setVisibleLayers (ids) {
    for (const plantationLayer of Store.getters.layers) {
      plantationLayer.mapLayer.setVisible(ids.indexOf(plantationLayer.id) > -1)
    }
  }

  /**
   * Toggles visibility of the background layer
   */
  setBackgroundLayerVisibility () {
    this.backgroundLayer.setVisible(!this._mapConfig.hideBackground)
  }

  /**
   * Adds a layer to the map from a Layer instance
   *
   * @param {Layer} layer - The layer
   */
  addLayer (layer) {
    const source = new VectorSource({ wrapX: false })
    const vectorLayer = new VectorLayer({ source })
    vectorLayer.setZIndex(layer.position)

    this._gardenMap.addLayer(vectorLayer)

    // Assigns the map layer to layer for reference
    layer.setMapLayer(vectorLayer)
  }

  /**
   * Removes a layer from the map
   *
   * @param {Layer} patchLayer - The layer to remove
   */
  removeLayer (patchLayer) { this._gardenMap.removeLayer(patchLayer.mapLayer) }

  /**
   * Defines active layer
   *
   * @param {Layer} layer - New active layer
   */
  useLayer (layer) {
    this._activeLayer = layer.mapLayer
  }

  /**
   * Removes map from element, can be nulled after
   */
  destroy () {
    this._gardenMap.setTarget(null)
  }

  /**
   * Initializes the map and insert it in DOM
   *
   * @param {string} elementId - Target DOM element ID
   * @private
   */
  _createMap (elementId) {
    const viewOptions = {
      center: [this._mapConfig.center[0], this._mapConfig.center[1]],
    }

    const projection = new Projection({ code: window.MAPS_PROJECTION, units: 'm' })

    if (this._mapConfig.isOSM) {
      viewOptions.zoom = 20
    } else {
      viewOptions.zoom = 2
      viewOptions.projection = projection
      this.extent = [0, 0, this._mapConfig.extentWidth, this._mapConfig.extentHeight]
      projection.setExtent(this.extent)
    }

    this._gardenMap = new OlMap({
      target: elementId,
      view: new View(viewOptions),
    })

    this._gardenMap.addControl(new ScaleLine({ units: 'metric' }))
    this._gardenMap.updateSize()
  }

  /**
   * Creates the overlays and attach them to the map.
   *
   * @param {string|null} popupOverlayElementId - DOM element id
   * @private
   */
  _createOverlays (popupOverlayElementId = null) {
    // Create the overlays
    this._popupOverlay = new Overlay({
      element: document.getElementById(popupOverlayElementId),
      positioning: 'bottom-center',
      stopEvent: true,
      offset: [0, -10],
    })
    this._gardenMap.addOverlay(this._popupOverlay)

    if (this._tooltipElement) {
      this._tooltipOverlay = new Overlay({
        element: this._tooltipElement,
        offset: [15, 0],
        positioning: 'center-left',
      })
      this._gardenMap.addOverlay(this._tooltipOverlay)
    }
  }

  /**
   * Hides the popup overlay. This is useful when selection should be kept with
   * nothing in the way.
   *
   * @private
   */
  _hidePopupOverlay () {
    this._popupOverlay.setPosition(null)
  }

  /**
   * Creates the long term interactions, that are only added/removed from map
   * with no change to configuration
   *
   * @private
   */
  _createInteractions () {
    this._selectInteraction = new SelectInteraction({
      style: [...StyleLibrary.SELECT_STYLE],
    })
    this._selectInteraction.on('select', (event) => {
      let selected
      if (event.selected.length === 0) {
        selected = null
      } else {
        selected = event.selected[0]

        // Don't handle multiple selects by replacing selections
        if (this._selectInteraction.getFeatures().get('length') > 1) {
          this._selectInteraction.getFeatures().clear()
          this._selectInteraction.getFeatures().extend([selected])
        }
      }

      if (selected) {
        this._select(selected, event.mapBrowserEvent.coordinate)
      } else {
        this._deselect()
      }

      // Handle the case where a placeable have been changed externally and must
      // be redrawn when selection changes
      // @see forceRedrawSelection()
      if (this._externallyChangedPlaceable) {
        this.redrawEntity(this._externallyChangedPlaceable)
        this._externallyChangedPlaceable = null
      }
    })
    this._gardenMap.addInteraction(this._selectInteraction)
  }

  /**
   * Preload resources pattern for quick renders
   *
   * @returns {Promise<void>} Resolves when done
   * @private
   */
  _preloadPatterns () {
    return StyleLibrary.preloadPatterns()
      .then(() => { return Promise.resolve() })
  }

  _changeMode (mode) {
    switch (mode) {
      case 'review':
        this._selectInteraction.setActive(true)
        this._stopInteraction()
        this._deselect()
        break
      case 'selected':
        this._stopInteraction()
        break
      case 'draw':
        this._stopInteraction()
        this._deselect()
        this._selectInteraction.setActive(false)
        break
      case 'transform':
        this._stopInteraction()
        this._hidePopupOverlay()
        break
      default:
        throw new Error(`Unsupported mode ${mode}`)
    }
    this._mode = mode
  }

  _stopInteraction () {
    if (this._interaction) {
      this._styleBack()
      this._gardenMap.removeInteraction(this._interaction)
      this._interaction = null
    }
  }

  _select (feature, coordinate) {
    this._popupOverlay.setPosition(coordinate)
    if (this._isPointedFeature(feature)) return

    this._pointedFeature = feature

    const featureProperties = feature.getProperties()
    this._notify('point-entity', { type: featureProperties.type, id: featureProperties.id })

    this._changeMode('selected')
  }

  _deselect () {
    if (!this._pointedFeature) return

    this._selectInteraction.getFeatures().clear()
    this._pointedFeature = null
    this._hidePopupOverlay()
    this._notify('point-entity', null)
  }

  /**
   * Checks if the given feature is already pointed
   *
   * @param   {Feature} feature - Feature to compare
   * @returns {boolean}         True when its... true
   * @private
   */
  _isPointedFeature (feature) {
    if (!this._pointedFeature) return false

    const pointedProperties = this._pointedFeature.getProperties()
    const featureProperties = feature.getProperties()
    return pointedProperties.id === featureProperties.id && pointedProperties.type === featureProperties.type
  }

  /**
   * Backup feature styles to be reapplied when needed
   *
   * @param {Feature} feature - Feature to backup the styles from
   * @private
   */
  _backupStyle (feature) {
    if (!feature) throw new Error('No pointed feature from which to backup style')
    const oldStyle = feature.getStyle()
    this._styleBackup = [...oldStyle]
  }

  /**
   * Applies backed-up style to pointed feature
   *
   * @private
   */
  _styleBack () {
    if (this._styleBackup) {
      if (!this._pointedFeature) throw new Error('No feature to revert style')

      this._pointedFeature.setStyle([...this._styleBackup])
      this._styleBackup = null
    }
  }

  /**
   * Creates the layers from Vuex store
   *
   * @private
   */
  _createLayers () {
    // Background layers
    if (this._mapConfig.isOSM) {
      this.backgroundLayer = new TileLayer({ source: new OSMSource() })
    } else {
      this.backgroundLayer = new ImageLayer({
        source: new StaticSource({
          url: this._mapConfig.picture.url,
          imageExtent: this.extent,
        }),
      })
    }

    this._gardenMap.addLayer(this.backgroundLayer)
    this.setBackgroundLayerVisibility()

    for (const patchLayer of Store.getters.layers) {
      this.addLayer(patchLayer)
    }
  }

  /**
   * @callback           _addDrawInteractionListenersOnEndCallback
   * @param {Feature} feature - The newly drawn feature
   */
  /**
   * Attaches events listeners to current interaction
   * During drawing:
   * - display measure tooltip
   * When drawing ends
   * - Adds some metadata to the feature
   * - Style the feature
   * Applies new style when drawing ends and display measure tooltip during drawing
   *
   * @param {'patch'|'path'|'element'}                  type      - Type of entity
   * @param {string}                                    shape     - Current shape being drawn
   * @param {Style[]}                                   newStyles - Style to apply to the shape once drawing ends
   * @param {_addDrawInteractionListenersOnEndCallback} onEnd     - Additional callback method
   * @private
   */
  _addDrawInteractionListeners (type, shape, newStyles, onEnd = () => {}) {
    let listener
    // Apply new style
    this._interaction.on('drawend', (event) => {
      const feature = event.feature
      feature.setStyle(newStyles)
      feature.setProperties({ type })

      onEnd(feature)
      unByKey(listener)
      this._tooltipOverlay.setPosition(null)
    })

    // Ignore measure tooltips when adding a point
    if (shape === 'Point') return

    // Measurement tooltip
    this._interaction.on('drawstart', (event) => {
      let tooltipCoord = event.coordinate
      const sketch = event.feature
      listener = sketch.getGeometry().on('change', (event) => {
        const geom = event.target
        let measure
        let area
        const points = geom.getCoordinates()

        if (shape === 'LineString') {
          measure = formatLength(getLength(new LineStringGeom([geom.getLastCoordinate(), points[points.length - 2]])))
          tooltipCoord = geom.getLastCoordinate()
        } else if (shape === 'Polygon') {
          area = formatArea(getArea(geom))
          const segmentLength = formatLength(getLength(new LineStringGeom([points[0][points[0].length - 2], points[0][points[0].length - 3]])))
          measure = `${segmentLength} (${area})`
          tooltipCoord = points[0][points[0].length - 2]
        } else if (shape === 'Circle') {
          const radius = getLength(new LineStringGeom([[0, 0], [0, geom.getRadius()]]))
          area = formatArea(Math.PI * Math.pow(radius, 2))
          measure = `R: ${formatLength(radius)} (${area})`
          tooltipCoord = geom.getLastCoordinate()
        } else {
          throw new Error(`Can't measure a ${shape}`)
        }

        this._tooltipElement.innerHTML = measure
        this._tooltipOverlay.setPosition(tooltipCoord)
      })
    })
  }

  /**
   * Creates an interaction on the pointed Feature
   *
   * @param {PointerInteraction} InteractionClass - Target interaction class
   * @param {string}             endEvent         - Interaction event to listen for
   */
  _interact (InteractionClass, endEvent) {
    if (!this._pointedFeature) throw new Error('Missing _pointedFeature to transform')

    this._changeMode('transform')

    const properties = this._pointedFeature.getProperties()
    this._backupStyle(this._pointedFeature)
    this._pointedFeature.setStyle([...this._styleBackup, ...StyleLibrary.INTERACTION_STYLE])

    const interaction = new InteractionClass({ features: this._selectInteraction.getFeatures() })
    interaction.on(endEvent, () => {
      let eventName
      if (['element', 'patch', 'path'].indexOf(properties.type) > -1) eventName = `update-${properties.type}`
      else throw new Error(`Unsupported ${properties.type}`)

      this._notify(eventName, { id: properties.id, feature: this._pointedFeature, geoJSON: geoJSONFromFeature(this._pointedFeature) })
    })

    this._interaction = interaction
    this._gardenMap.addInteraction(interaction)
  }

  /**
   * @param {string} event - Event to dispatch
   * @param {*}      data  - Event additional payload
   * @private
   */
  _notify (event, data) {
    this.mapElement.dispatchEvent(new CustomEvent(event, { detail: data }))
  }
}
