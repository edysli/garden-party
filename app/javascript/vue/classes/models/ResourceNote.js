import Model from './Model'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['content', 'visible', 'userId', 'resourceId', 'createdAt', 'updatedAt', 'username']

class ResourceNote extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload             - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.content
   * @param {boolean}     payload.visible
   * @param {number}      payload.user_id
   * @param {number}      payload.resource_id
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {string}      payload.username
   */
  constructor ({ id = null, content, visible, user_id, resource_id, created_at, updated_at, username }) {
    super()
    this.id = id
    this.content = content
    this.visible = visible
    this.userId = user_id
    this.resourceId = resource_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.username = username
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'ResourceNote' }
}

ResourceNote.updatableAttributes = ATTRIBUTES
ResourceNote.urls = {
  index (resourceId) { return `/api/resources/${resourceId}/notes` },
  show (id) { return `/api/resource_notes/${id}` },
  create () { return '/api/resource_notes' },
  update (payload) { return `/api/resource_notes/${payload.id}` },
  destroy (id) { return `/api/resource_notes/${id}` },
}

export default ResourceNote
