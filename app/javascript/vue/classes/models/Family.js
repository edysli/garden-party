import Model from './Model'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'source', 'createdAt', 'updatedAt', 'kingdom', 'syncId']

class Family extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload            - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.name
   * @param {string}      payload.source
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {string}      payload.kingdom
   * @param {string}      payload.sync_id
   */
  constructor ({ id = null, name, source, created_at, updated_at, kingdom, sync_id }) {
    super()
    this.id = id
    this.name = name
    this.source = source
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.kingdom = kingdom
    this.syncId = sync_id
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  get resources () { return this.constructor.store.getters.resourcesByFamily(this.id) }

  get genera () { return this.constructor.store.getters.generaByFamily(this.id) }

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'Family' }
}

Family.updatableAttributes = ATTRIBUTES
Family.urls = {
  index () { return '/api/families' },
  show (id) { return `/api/families/${id}` },
  create () { return '/api/families' },
  update (payload) { return `/api/families/${payload.id}` },
  destroy (id) { return `/api/families/${id}` },
}

export default Family
