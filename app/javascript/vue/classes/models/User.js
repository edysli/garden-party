import Model from './Model'
import api from '../../tools/api'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['role', 'username', 'accountState']

class User extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload               - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.role
   * @param {string}      payload.username
   * @param {string}      payload.account_state
   */
  constructor ({ id = null, role, username, account_state }) {
    super()
    this.id = id
    this.role = role
    this.username = username
    this.accountState = account_state
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'User' }

  /**
   * Search for users from partial username or email.
   * Only use one or the other, the server will only process one of them.
   *
   * @param   {object}          payload            - Object with username or email
   * @param   {string}          [payload.email]    - Email address
   * @param   {string}          [payload.username] - Partial username
   * @returns {Promise<User[]>}                    List of User instances
   */
  static search (payload) {
    return api('post', '/api/users/search', payload)
      .then(entities => entities.map(e => new this(e)))
  }

  static sendInvitation (payload) {
    return api('post', '/api/users/invite', { user: payload })
      .then(entity => new this(entity))
  }
}

User.updatableAttributes = ATTRIBUTES
User.urls = {
  show (id) { return `/api/users/${id}` },
}

export default User
