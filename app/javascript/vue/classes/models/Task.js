import api from '../../tools/api'
import Model from './Model'

const TASKS_BASE_URLS = {
  Element: '/api/elements',
  Map: '/api/maps',
  Patch: '/api/patches',
  Path: '/api/paths',
}

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'notes', 'createdAt', 'updatedAt', 'doneAt', 'plannedFor', 'subjectType', 'subjectId', 'assigneeId', 'updatedSubject']

class Task extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload              - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.name
   * @param {string}      payload.notes
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {Date|string} payload.done_at
   * @param {Date|string} payload.planned_for
   * @param {string}      payload.subject_type
   * @param {number}      payload.subject_id
   * @param {number}      payload.assignee_id
   * @param {object|null} payload.subject
   */
  constructor ({ id = null, name, notes, created_at, updated_at, done_at, planned_for, subject_type, subject_id, assignee_id, subject }) {
    super()
    this.id = id
    this.name = name
    this.notes = notes
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.doneAt = done_at ? new Date(done_at) : null
    this.plannedFor = planned_for ? new Date(planned_for) : null
    this.subjectType = subject_type
    this.subjectId = subject_id
    this.assigneeId = assignee_id
    this.updatedSubject = subject
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  get subject () { return this.constructor.store.getters.taskSubject(this.id) }

  /**
   * Fetches last tasks filtered by action for given element
   *
   * @param   {number}            mapId - Target map identifier
   * @returns {Promise<string[]>}       List of used tasks names
   */
  static getNames (mapId) {
    return api('get', `/api/maps/${mapId}/tasks/names`)
  }

  /**
   * Fetches last tasks filtered by action for given element
   *
   * @param   {string}          subjectType - Subject type
   * @param   {number}          subjectId   - Subject identifier
   * @param   {string[]}        actions     - List of actions to filter
   * @returns {Promise<Task[]>}             Filtered list of Task instances
   */
  static getTasksForSubject (subjectType, subjectId, actions = []) {
    const baseUrl = TASKS_BASE_URLS[subjectType] || null
    if (!baseUrl) throw new Error(`Unsupported task type "${subjectType}"`)

    return api('get', `${baseUrl}/${subjectId}/tasks`, { last_actions: actions })
      .then(tasks => tasks.map(data => new Task(data)))
  }

  /**
   * Fetches pending tasks for given map
   *
   * @param   {number}          mapId - Target map identifier
   * @returns {Promise<Task[]>}       Filtered list of Task instances
   */
  static getPending (mapId) {
    return api('get', `/api/maps/${mapId}/all_tasks?pending=true`)
      .then(tasks => tasks.map(data => new Task(data)))
  }

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'Task' }
}

Task.updatableAttributes = ATTRIBUTES
Task.urls = {
  index (elementId) { return `/api/elements/${elementId}/tasks` },
  show (id) { return `/api/tasks/${id}` },
  create () { return '/api/tasks' },
  update (payload) { return `/api/tasks/${payload.id}` },
  destroy (id) { return `/api/tasks/${id}` },
}

export default Task
