import Model from './Model'
import api from '@/vue/tools/api'

/**
 * @typedef {import('./Element')} Element
 */

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['quantity', 'unit', 'elementId', 'userId', 'harvestedAt', 'discardedAt', 'createdAt', 'updatedAt']

/**
 * Available units.
 *
 * FIXME: Find a way to sync this with Rails' model enum
 */
export const UNITS = [
  'gram',
  'kilogram',
  'ton',
  'piece',
  'centilitre',
  'litre',
  'cubic_meter',
]

class Harvest extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload              - Data from API
   * @param {null|number} payload.id
   * @param {number}      payload.quantity
   * @param {string}      payload.unit
   * @param {number}      payload.element_id
   * @param {number}      payload.user_id
   * @param {Date|string} payload.harvested_at
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   */
  constructor ({ id = null, quantity, unit, element_id, user_id, harvested_at, created_at, updated_at }) {
    super()
    this.id = id
    this.quantity = quantity
    this.unit = unit
    this.elementId = element_id
    this.userId = user_id
    this.harvestedAt = harvested_at ? new Date(harvested_at) : null
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /** @returns {Element} Element from VueX store */
  get element () { return this.constructor.store.getters.element(this.elementId) }
  /** @returns {Element} Element from VueX store */
  get user () { return this.constructor.store.getters.user(this.userId) }

  /**
   * Fetches the list for all harvest associated to map elements
   *
   * @param   {number}             mapId - Map identifier
   * @returns {Promise<Harvest[]>}       Harvest entities
   */
  static getMapIndex (mapId) {
    return api('get', this.urlsList.indexForMap(mapId))
      .then(entities => entities.map(entity => new this(entity)))
  }

  /**
   * Fetches the list for all harvest associated to an element
   *
   * @param   {number}             elementId - Map identifier
   * @returns {Promise<Harvest[]>}           Harvest entities
   */
  static getElementIndex (elementId) {
    return api('get', this.urlsList.indexForElement(elementId))
      .then(entities => entities.map(entity => new this(entity)))
  }

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'Harvest' }
}

Harvest.updatableAttributes = ATTRIBUTES
Harvest.urls = {
  indexForMap (mapId) { return `/api/maps/${mapId}/all_harvests` },
  indexForElement (elementId) { return `/api/elements/${elementId}/harvests` },
  show (id) { return `/api/harvests/${id}` },
  create () { return '/api/harvests' },
  update (payload) { return `/api/harvests/${payload.id}` },
  destroy (id) { return `/api/harvests/${id}` },
}

export default Harvest
