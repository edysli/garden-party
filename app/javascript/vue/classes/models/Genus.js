import Model from './Model'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'createdAt', 'updatedAt', 'source', 'familyId', 'syncId']

class Genus extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload            - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.name
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {string}      payload.source
   * @param {number}      payload.family_id
   * @param {string}      payload.sync_id
   */
  constructor ({ id = null, name, created_at, updated_at, source, family_id, sync_id }) {
    super()
    this.id = id
    this.name = name
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.source = source
    this.familyId = family_id
    this.syncId = sync_id
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  get resources () { return this.constructor.store.getters.resourcesByGenus(this.id) }

  get family () { return this.constructor.store.getters.family(this.familyId) }

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'Genus' }
}

Genus.updatableAttributes = ATTRIBUTES
Genus.urls = {
  index () { return '/api/genera' },
  show (id) { return `/api/genera/${id}` },
  create () { return '/api/genera' },
  update (payload) { return `/api/genera/${payload.id}` },
  destroy (id) { return `/api/genera/${id}` },
}

export default Genus
