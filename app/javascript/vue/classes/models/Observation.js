import api from '../../tools/api'
import Model from './Model'

const OBSERVATION_BASE_URLS = {
  Element: '/api/elements',
  Map: '/api/maps',
  Patch: '/api/patches',
  Path: '/api/paths',
}

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['title', 'content', 'subjectType', 'subjectId', 'userId', 'madeAt', 'createdAt', 'updatedAt', 'pictures']

class Observation extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}                                payload              - Data from API
   * @param {null|number}                           payload.id
   * @param {string}                                payload.title
   * @param {string}                                payload.content
   * @param {string}                                payload.subject_type
   * @param {number}                                payload.subject_id
   * @param {number}                                payload.user_id
   * @param {Date|string}                           payload.made_at
   * @param {Date|string}                           payload.created_at
   * @param {Date|string}                           payload.updated_at
   * @param {{source: string, thumbnail: string}[]} payload.pictures
   */
  constructor ({ id = null, title, content, subject_type, subject_id, user_id, made_at, created_at, updated_at, pictures }) {
    super()
    this.id = id
    this.title = title
    this.content = content
    this.subjectType = subject_type
    this.subjectId = subject_id
    this.userId = user_id
    this.madeAt = made_at ? new Date(made_at) : null
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.pictures = pictures
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  get subject () { return this.constructor.store.getters.taskSubject(this.id) }

  /**
   * Fetches last observations for given element
   *
   * @param   {string}                 subjectType - Subject type
   * @param   {number}                 subjectId   - Subject identifier
   * @returns {Promise<Observation[]>}             Filtered list of Observation instances
   */
  static getObservationsForSubject (subjectType, subjectId) {
    const baseUrl = OBSERVATION_BASE_URLS[subjectType] || null
    if (!baseUrl) throw new Error(`Unsupported observation type "${subjectType}"`)

    return api('get', `${baseUrl}/${subjectId}/observations`)
      .then(observations => observations.map(data => new Observation(data)))
  }

  /**
   * Creates a new observation
   *
   * @param   {object}               payload - Object with underscored keys
   * @returns {Promise<Observation>}         Observation instance
   */
  static create (payload) {
    const formData = new FormData()
    for (const key of Object.keys(payload)) {
      if (key === 'pictures') {
        for (const file of payload[key]) formData.append(`observation[${key}][]`, file)
      } else { formData.append(`observation[${key}]`, payload[key]) }
    }

    return api('post', '/api/observations', formData)
      .then(observation => new Observation(observation))
  }

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'Observation' }
}

Observation.updatableAttributes = ATTRIBUTES
Observation.urls = {
  index () { return '/api/observations' },
  show (id) { return `/api/observations/${id}` },
  update (payload) { return `/api/observations/${payload.id}` },
  destroy (id) { return `/api/observations/${id}` },
}

export default Observation
