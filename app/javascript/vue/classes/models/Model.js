import api from '../../tools/api'

/**
 * @typedef {import('vuex').Store} VueXStore
 */

export default class Model {
  /**
   * Update from another instance
   *
   * @param {Model|object} newInstance - Instance or object to merge data from
   */
  updateAttributes (newInstance) {
    this.constructor.updatableAttributes.forEach((attribute) => {
      if (newInstance[attribute] !== undefined) this[attribute] = newInstance[attribute]
    })
  }

  get className () { return this.constructor.getClassName() }

  static getClassName () { throw new Error('Class name not defined in model') }

  static set updatableAttributes (attributes) { this.UPDATABLE_ATTRIBUTES = attributes }

  static get updatableAttributes () { return this.UPDATABLE_ATTRIBUTES }

  /* eslint-disable accessor-pairs */
  static set urls (urls) {
    /**
     * @type {object}
     */
    this.urlsList = urls
  }
  /* eslint-enable accessor-pairs */

  /**
   * Assigns VueX Store to Store for usage in methods
   *
   * @param {VueXStore} store VueX Store instance
   */
  static set store (store) { this.VueXStore = store }

  /**
   * Reach the store
   *
   * @returns {VueXStore} store VueX Store instance
   */
  static get store () { return this.VueXStore }

  /**
   * Fetches the list
   *
   * @param   {*}                params - Parameters for API call
   * @returns {Promise<Model[]>}        List of entities
   */
  static getIndex (params) {
    return api('get', this.urlsList.index(params))
      .then(entities => entities.map(entity => new this(entity)))
  }

  /**
   * Fetches one entity
   *
   * @param   {*}              params - Parameters for API call
   * @returns {Promise<Model>}        Model instance
   */
  static get (params) {
    return api('get', this.urlsList.show(params))
      .then(entity => new this(entity))
  }

  /**
   * Creates a new entity
   *
   * @param   {*}              params - Parameters for API call
   * @returns {Promise<Model>}        Model instance
   */
  static create (params) {
    return api('post', this.urlsList.create(params), params)
      .then(entity => new this(entity))
  }

  /**
   * Updates one entity
   *
   * @param   {*}              payload - Parameters for API call
   * @returns {Promise<Model>}         Model instance
   */
  static update (payload) {
    return api('put', this.urlsList.update(payload), payload)
      .then(entity => new this(entity))
  }

  /**
   * Destroys one entity
   *
   * @param   {*}             params - Call parameters
   * @returns {Promise<null>}        Nothing on success
   */
  static destroy (params) {
    return api('delete', this.urlsList.destroy(params))
  }
}
