import Model from './Model'
import api from '../../tools/api'
import User from './User'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['role', 'username', 'accountState', 'preferences']

class Account extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload             - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.role
   * @param {string}      payload.email
   * @param {string}      payload.username
   * @param {object}      payload.preferences
   * @param {string}      payload.created_at
   * @param {string}      payload.updated_at
   */
  constructor ({ id = null, role, email, username, preferences, created_at, updated_at }) {
    super()
    this.id = id
    this.role = role
    this.email = email
    this.username = username
    this.preferences = preferences
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  toUser () {
    return new User({
      id: this.id,
      role: this.role,
      username: this.username,
      account_state: 'active',
    })
  }

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'Account' }

  /**
   * @returns {Promise<Account>} Account instance
   */
  static getAccount () {
    return api('get', '/api/account')
      .then(entity => new this(entity))
  }

  /** Override as there is no route in the api anyway */
  static getIndex () { throw new Error('Not implemented') }
  /** Override as there is no route in the api anyway */
  static get () { throw new Error('Not implemented') }
  /** Override as there is no route in the api anyway */
  static create () { throw new Error('Not implemented') }
  /** Override as there is no route in the api anyway */
  static destroy () { throw new Error('Not implemented') }
}

Account.updatableAttributes = ATTRIBUTES
Account.urls = {
  update () { return '/api/account' },
}

export default Account
