import isEqual from 'lodash.isequal'

/**
 * @typedef {import('vuex').Store}  VueXStore
 * @typedef {import('../Family')}   Family
 * @typedef {import('../Genus')}    Genus
 * @typedef {import('../Resource')} Resource
 * @typedef {Family|Genus|Resource} ModelClass
 */

/**
 * Model used to represent a trusted data to sync.
 *
 * This is the base model and should not be used as-is.
 */
export default class TrustedModel {
  /**
   * Creates an instance of data from trusted entity
   *
   * @param {object} attributes    Trusted data
   * @param {string} attributes.id Unique identifier
   */
  constructor (attributes) {
    this.id = attributes.id
  }

  /**
   * Returns a payload to use to create/update library data with the instance
   *
   * This must be overridden in child class to perform relevant conversions
   *
   * @interface
   * @returns {object} Payload to use to synchronize the entity
   */
  payloadToSync () { throw new Error('payloadToSync is not overridden in model') }

  /**
   * Returns a payload to use to update an entity field in the library
   *
   * This must be overridden in child class to perform relevant conversions
   *
   * @interface
   * @param   {string} field Field to sync
   * @returns {object}       Minimal payload, without the ID
   */
  /* eslint-disable no-unused-vars */
  payloadToSyncField (field) { throw new Error('payloadToSyncField is not overridden in model') }
  /* eslint-enable no-unused-vars */

  /**
   * Updates or creates the entity of the library with trusted data
   *
   * @param   {object|null}         libraryEntity Library entity to update
   * @param   {string|null}         [field]       Optional field name. Will only update the field when provided
   * @returns {Promise<ModelClass>}               Promise resolving the new/updated instance
   */
  updateCurrent (libraryEntity, field = null) {
    let payload

    if (field) payload = this.payloadToSyncField(field)
    else payload = this.payloadToSync()

    if (libraryEntity) payload.library_id = libraryEntity.library_id

    return this.constructor.store.dispatch(this.constructor.vueXSaveAction, { trustedId: this.id, payload })
  }

  /**
   * Checks if the trusted data is the same as in the given object.
   * Comparison is made on the fields listed in `fieldsToCompare`
   *
   * @param   {object}  entry Object to compare
   * @returns {boolean}       True if the data has the same values in fieldsToCompare
   */
  isEqual (entry) {
    if (!entry) return false

    let equal = true
    this.constructor.fieldsToCompare.forEach((f) => { if (!isEqual(this[f], entry[f])) equal = false })

    return equal
  }

  /**
   * @interface
   * @returns {object} List of trusted fields
   */
  referencesValues () { throw new Error('referencesValues is not overridden in model') }

  /**
   * Assigns VueX Store to Store for usage in methods
   *
   * @param {VueXStore} store VueX Store instance
   */
  static set store (store) { this.VueXStore = store }

  /**
   * @returns {VueXStore} VueX Store instance
   */
  static get store () { return this.VueXStore }

  /**
   * @interface
   * @returns {string[]} List of required reference fields
   */
  static get requiredReferences () { throw new Error('requiredReferences is not overridden in model') }

  /**
   * @interface
   * @returns {string[]} List of fields to compare against in library data
   */
  static get fieldsToCompare () { throw new Error('fieldsToCompare is not overridden in model') }

  /**
   * @interface
   * @returns {string} Name of the VueX action to use to update/create a library entity
   */
  static get vueXSaveAction () { throw new Error('vueXSaveAction is not overridden in model') }
}
