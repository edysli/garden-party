import Model from './Model'
import { testModelFactory, TestModel } from '../../../specs/fakes/TestModel'

// Ignore all possible API calls
jest.mock('../../tools/api', () => {})

describe('Model', () => {
  describe('#getClassName()', () => {
    describe('when overridden in inherited model', () => {
      it('returns a string', () => {
        expect(TestModel.getClassName()).toBe('TestModel')
      })
    })
    describe('when not overridden in inherited model', () => {
      it('throws an error', () => {
        const executor = () => new Promise(() => { Model.getClassName() })
        return expect(executor()).rejects.toThrow('not defined')
      })
    })
  })

  describe('.updateAttributes', () => {
    let instance
    beforeEach(() => {
      instance = new TestModel(testModelFactory.build({ id: 1 }))
    })
    it('updates the instance', () => {
      instance.updateAttributes({ name: 'new name' })
      expect(instance.name).toBe('new name')
    })

    it('ignores non updatable attributes', () => {
      instance.updateAttributes({ id: 2 })
      expect(instance.id).toBe(1)
    })
  })
})
