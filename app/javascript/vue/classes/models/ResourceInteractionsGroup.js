import Model from './Model'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'description', 'createdAt', 'updatedAt', 'syncId']

class ResourceInteractionsGroup extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload             - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.name
   * @param {string}      payload.description
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {string}      payload.sync_id
   */
  constructor ({ id = null, name, description, created_at, updated_at, sync_id }) {
    super()
    this.id = id
    this.name = name
    this.description = description
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.syncId = sync_id
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  get resources () { return this.constructor.store.getters.resourcesByResourceInteractionsGroup(this.id) }
  get interactions () { return this.constructor.store.getters.resourceInteractionsByGroup(this.id) }

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'ResourceInteractionsGroup' }
}

ResourceInteractionsGroup.updatableAttributes = ATTRIBUTES
ResourceInteractionsGroup.urls = {
  index () { return '/api/resource_interactions_groups' },
  show (id) { return `/api/resource_interactions_groups/${id}` },
  create () { return '/api/resource_interactions_groups' },
  update (payload) { return `/api/resource_interactions_groups/${payload.id}` },
  destroy (id) { return `/api/resource_interactions_groups/${id}` },
}

export default ResourceInteractionsGroup
