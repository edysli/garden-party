import Model from './Model'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['action', 'data', 'happenedAt', 'subjectType', 'subjectId', 'userId', 'username', 'subjectName', 'mapId', 'createdAt', 'updatedAt']

class Activity extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload              - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.action
   * @param {object}      payload.data
   * @param {Date|string} payload.happened_at
   * @param {string}      payload.subject_type
   * @param {number}      payload.subject_id
   * @param {number}      payload.user_id
   * @param {string}      payload.username
   * @param {string}      payload.subject_name
   * @param {number}      payload.map_id
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   */
  constructor ({ id = null, action, data, happened_at, subject_type, subject_id, user_id, username, subject_name, map_id, created_at, updated_at }) {
    super()
    this.id = id
    this.action = action
    this.data = data
    this.happenedAt = happened_at ? new Date(happened_at) : null
    this.subjectType = subject_type
    this.subjectId = subject_id
    this.userId = user_id
    this.username = username
    this.subjectName = subject_name
    this.mapId = map_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'Activity' }
}

Activity.updatableAttributes = ATTRIBUTES
Activity.urls = {
  index (mapId) { return `/api/maps/${mapId}/all_activities` },
  show (id) { return `/api/activities/${id}` },
  create () { return '/api/activities' },
  update (payload) { return `/api/activities/${payload.id}` },
  destroy (id) { return `/api/activities/${id}` },
}

export default Activity
