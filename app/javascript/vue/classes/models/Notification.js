import Model from './Model'
import api from '../../tools/api'
import User from './User'

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['type', 'content', 'subjectType', 'subjectId', 'subject', 'recipientId', 'senderId', 'sender', 'archivedAt', 'createdAt', 'updatedAt']

class Notification extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload              - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.type
   * @param {object}      payload.content
   * @param {object|null} payload.subject
   * @param {string}      payload.subject_type
   * @param {number}      payload.subject_id
   * @param {number}      payload.recipient_id
   * @param {number}      payload.sender_id
   * @param {object|null} payload.sender
   * @param {Date|string} payload.archived_at
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   */
  constructor ({ id = null, type, content, subject, subject_type, subject_id, recipient_id, sender, sender_id, archived_at, created_at, updated_at }) {
    super()
    this.id = id
    this.type = type
    this.content = content
    this.subjectType = subject_type
    this.subjectId = subject_id
    this.recipientId = recipient_id
    this.senderId = sender_id
    this.archivedAt = archived_at ? new Date(archived_at) : null
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null

    this.sender = sender ? new User(sender) : null
    this.subject = subject
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'Notification' }

  /**
   * Sets a notification as archive
   *
   * @param   {number}                id - Identifier
   * @returns {Promise<Notification>}    The new Notification instance
   */
  static archive (id) {
    return api('patch', `/api/notifications/${id}/archive`, {}).then((entity) => new this(entity))
  }

  static archiveAll () {
    return api('patch', '/api/notifications/archive_all').then((entities) => entities.map(entity => new this(entity)))
  }

  static destroyAllArchived () {
    return api('delete', '/api/notifications/destroy_all_archived').then((entities) => entities.map(entity => new this(entity)))
  }

  /** Override as there is no route in the api anyway */
  static create () { throw new Error('Not implemented') }
  /** Override as there is no route in the api anyway */
  static update () { throw new Error('Not implemented') }
}

Notification.updatableAttributes = ATTRIBUTES
Notification.urls = {
  index () { return '/api/notifications' },
  show (id) { return `/api/notifications/${id}` },
  destroy (id) { return `/api/notifications/${id}` },
}

export default Notification
