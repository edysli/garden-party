import Model from './Model'

/**
 * @typedef {import('./Genus')}                    Genus
 * @typedef {import('./ResourceNote')}             ResourceNote
 * @typedef {import('./ResourceInteractionsGroup')} ResourceInteractionsGroup
 * @typedef {import('./ResourceInteraction')}       ResourceInteraction
 */

/**
 * List of attributes that can be updated in `updateAttributes()`
 *
 * @type {string[]}
 */
const ATTRIBUTES = ['name', 'description', 'parentId', 'createdAt', 'updatedAt', 'genusId', 'commonNames', 'color', 'latinName', 'diameter', 'shelteredSowingMonths', 'soilSowingMonths', 'harvestingMonths', 'sources', 'syncId', 'generic', 'resourceInteractionsGroupId', 'tagList']

class Resource extends Model {
  /* eslint-disable camelcase, jsdoc/require-param-description */
  /**
   * @param {object}      payload                                - Data from API
   * @param {null|number} payload.id
   * @param {string}      payload.name
   * @param {string}      payload.description
   * @param {number|null} payload.parent_id
   * @param {Date|string} payload.created_at
   * @param {Date|string} payload.updated_at
   * @param {number}      payload.genus_id
   * @param {object}      payload.common_names
   * @param {string}      payload.color
   * @param {string}      payload.latin_name
   * @param {number}      payload.diameter
   * @param {boolean[]}   payload.sheltered_sowing_months
   * @param {boolean[]}   payload.soil_sowing_months
   * @param {boolean[]}   payload.harvesting_months
   * @param {object}      payload.sources
   * @param {string}      payload.sync_id
   * @param {boolean}     payload.generic
   * @param {number}      payload.resource_interactions_group_id
   * @param {string[]}    payload.tag_list
   */
  constructor ({ id = null, name, description, parent_id, created_at, updated_at, genus_id, common_names, color, latin_name, diameter, sheltered_sowing_months, soil_sowing_months, harvesting_months, sources, sync_id, generic, resource_interactions_group_id, tag_list }) {
    super()
    this.id = id
    this.name = name
    this.description = description || null
    this.parentId = parent_id
    this.createdAt = created_at ? new Date(created_at) : null
    this.updatedAt = updated_at ? new Date(updated_at) : null
    this.genusId = genus_id || null
    this.commonNames = common_names
    this.latinName = latin_name
    this.diameter = diameter || null
    this.shelteredSowingMonths = sheltered_sowing_months || []
    this.soilSowingMonths = soil_sowing_months || []
    this.harvestingMonths = harvesting_months || []
    this.sources = sources || []
    this.syncId = sync_id || null
    this.generic = generic
    this.resourceInteractionsGroupId = resource_interactions_group_id
    this.tagList = tag_list || []

    this.fillColor = `rgba(${color},0.2)`
    this.borderColor = `rgb(${color})`

    this.generatePicturePath()
    this.generatePatternPicturePath()
  }
  /* eslint-enable camelcase, jsdoc/require-param-description */

  /**
   * Gets child resources from VueX store
   *
   * @returns {Resource[]} The resource's child resources
   */
  get children () { return this.constructor.store.getters.childResources(this.id) }

  /**
   * Gets parent resource from VueX store
   *
   * @returns {(Resource|null)} The resource's parent resource
   */
  get parent () { return this.constructor.store.getters.resource(this.parentId) }

  /**
   * Gets genus from VueX store
   *
   * @returns {(Genus|null)} The resource's genus
   */
  get genus () { return this.constructor.store.getters.genus(this.genusId) }

  /**
   * Gets resources notes from VueX store
   *
   * @returns {ResourceNote[]} The resource's notes
   */
  get notes () { return this.constructor.store.getters.resourceNotesForResource(this.id) }

  /**
   * Gets resources interactions group from VueX store
   *
   * @returns {ResourceInteractionsGroup|null} The resource's group
   */
  get resourceInteractionsGroup () {
    if (!this.resourceInteractionsGroupId) return null

    return this.constructor.store.getters.resourceInteractionsGroup(this.resourceInteractionsGroupId)
  }

  /**
   * Returns the interactions between this resource and a list of interaction groups
   *
   * @param   {ResourceInteractionsGroup[]} otherGroups - List of other groups
   * @returns {ResourceInteraction[]}                   List of interactions
   */
  resourceInteractionsWithGroups (otherGroups) {
    const group = this.resourceInteractionsGroup
    if (!group || group.interactions.length === 0) return []

    // Interactions in which current group is involved
    const finalInteractions = []

    // Find unique interactions with current resource
    otherGroups.forEach((otherGroup) => {
      otherGroup.interactions.forEach((interaction) => {
        if (interaction.targetId === group.id || interaction.subjectId === group.id) {
          finalInteractions.push(interaction)
        }
      })
    })

    return finalInteractions
  }

  /**
   * Builds an encoded source for picture
   */
  generatePicturePath () {
    const string = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 6.5 6.5">
      <path d="M6.35 3.175A3.175 3.175 0 013.175 6.35 3.175 3.175 0 010 3.175 3.175 3.175 0 013.175 0 3.175 3.175 0 016.35 3.175z" stroke-width="0.2"
            fill="${this.fillColor}"
            stroke="${this.borderColor}" />
      <text style="line-height:1.25" x="1.6" y="4.9" font-weight="400" font-size="5" font-family="monospace" stroke-width="0.1" fill="black" fill-opacity="0.5">
        <tspan>${this.name[0]}</tspan>
      </text>
    </svg>`
    const blob = new Blob([string], { type: 'image/svg+xml;charset=utf-8' })
    const domURL = self.URL || self.webkitURL || self

    this.picturePath = domURL.createObjectURL(blob)
  }

  /**
   * Builds an encoded source for pattern picture
   */
  generatePatternPicturePath () {
    const string = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 6.5 6.5">
      <path d="M0 0h6.5v6.5H0z" stroke="transparent"
            fill="${this.fillColor}" />
      <text style="line-height:1.25"
            x="1.6"
            y="4.9"
            stroke="transparent"
            font-weight="400"
            font-size="3"
            font-family="sans-serif"
            stroke-width="0.1"
            fill="black"
            fill-opacity="0.5">
        <tspan>${this.name[0]}</tspan>
      </text>
    </svg>`
    const blob = new Blob([string], { type: 'image/svg+xml;charset=utf-8' })
    const domURL = self.URL || self.webkitURL || self

    this.patternPicturePath = domURL.createObjectURL(blob)
  }

  /**
   * @returns {string} The hardcoded class name
   */
  static getClassName () { return 'Resource' }
}

Resource.updatableAttributes = ATTRIBUTES
Resource.urls = {
  index () { return '/api/resources' },
  show (id) { return `/api/resources/${id}` },
  create () { return '/api/resources' },
  update (payload) { return `/api/resources/${payload.id}` },
  destroy (id) { return `/api/resources/${id}` },
}

export default Resource
