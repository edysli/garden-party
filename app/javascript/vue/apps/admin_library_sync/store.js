import { createStore } from 'vuex'

import TrustedModel from '../../classes/models/trustedData/TrustedModel'

import TrustedFamily from '../../classes/models/trustedData/TrustedFamily'
import TrustedResource from '../../classes/models/trustedData/TrustedResource'
import TrustedGenus from '../../classes/models/trustedData/TrustedGenus'
import TrustedResourceInteraction from '../../classes/models/trustedData/TrustedResourceInteraction'
import TrustedResourceInteractionsGroup from '../../classes/models/trustedData/TrustedResourceInteractionsGroup'

import api from '../../tools/api'

const DATA_CLASSES = {
  families: TrustedFamily,
  genera: TrustedGenus,
  resources: TrustedResource,
  resource_interactions: TrustedResourceInteraction,
  resource_interactions_groups: TrustedResourceInteractionsGroup,
}

const DATA_KEYS = Object.keys(DATA_CLASSES)

const Store = createStore({
  state: {
    differences: {
      /** @type {TrustedFamily[]} */
      families: [],
      /** @type {TrustedGenus[]} */
      genera: [],
      /** @type {TrustedResource[]} */
      resources: [],
      /** @type {TrustedResourceInteraction[]} */
      resource_interactions: [],
      /** @type {TrustedResourceInteractionsGroup[]} */
      resource_interactions_groups: [],
    },
    names: {
      /** @type {{id: number, sync_id: string, name: string}[]} */
      families: [],
      /** @type {{id: number, sync_id: string, name: string}[]} */
      genera: [],
      /** @type {{id: number, sync_id: string, name: string}[]} */
      resources: [],
      /** @type {{id: number, sync_id: string, name: string}[]} */
      resource_interactions_groups: [],
    },
  },
  mutations: {
    setDifferences (state, data) {
      Object.keys(data).forEach((key) => { data[key].forEach((entry) => { state.differences[key].push(entry) }) })
    },
    updatePair (state, { type, trustedId, data }) {
      const index = state.differences[type].findIndex(p => p.trusted.id === trustedId)
      state.differences[type][index].current = data
    },
    setNames (state, data) {
      data.families.forEach((f) => { state.names.families.push(f) })
      data.genera.forEach((f) => { state.names.genera.push(f) })
      data.resources.forEach((f) => { state.names.resources.push(f) })
      data.resource_interactions_groups.forEach((f) => { state.names.resource_interactions_groups.push(f) })
    },
    setName (state, { type, value }) {
      if (!state.names[type]) return

      const index = state.names[type].findIndex((e) => e.id === value.id)
      if (index > -1) state.names[type][index] = value
      else state.names[type].push(value)
    },
  },
  actions: {
    loadDataDifferences ({ commit }) {
      return api('get', '/admin/import/differences')
        .then((data) => {
          DATA_KEYS.forEach((key) => {
            if (!data[key]) data[key] = []
            data[key] = data[key].map((entry) => {
              entry.trusted = new DATA_CLASSES[key](entry.trusted)
              return entry
            })
          })

          commit('setDifferences', data)
        })
    },
    loadNames ({ commit }) {
      return api('get', '/admin/import/library_names')
        .then((data) => {
          commit('setNames', data)
        })
    },
    syncFamily ({ dispatch }, { trustedId, payload }) {
      return dispatch('syncEntity', { type: 'families', endpoint: '/admin/import/family', trustedId, payload })
    },
    syncGenus ({ dispatch }, { trustedId, payload }) {
      return dispatch('syncEntity', { type: 'genera', endpoint: '/admin/import/genus', trustedId, payload })
    },
    syncResource ({ dispatch }, { trustedId, payload }) {
      return dispatch('syncEntity', { type: 'resources', endpoint: '/admin/import/resource', trustedId, payload })
    },
    syncResourceInteractionsGroup ({ dispatch }, { trustedId, payload }) {
      return dispatch('syncEntity', { type: 'resource_interactions_groups', endpoint: '/admin/import/resource_interactions_group', trustedId, payload })
    },
    syncResourceInteraction ({ dispatch }, { trustedId, payload }) {
      return dispatch('syncEntity', { type: 'resource_interactions', endpoint: '/admin/import/resource_interaction', trustedId, payload })
    },
    syncEntity ({ commit }, { type, endpoint, trustedId, payload }) {
      let action = 'post'
      if (payload.library_id) action = 'put'
      return api(action, endpoint, payload)
        .then((data) => {
          commit('updatePair', { type, trustedId, data })
          // Update name
          commit('setName', { type, value: { id: data.library_id, sync_id: data.id, name: data.name } })
          return data
        })
    },
  },
  getters: {
    // Find data by name in library
    familyByName: state => name => state.names.families.find(family => family.name === name),
    genusByName: state => name => state.names.genera.find(genus => genus.name === name),
    resourceByName: state => name => state.names.resources.find(resource => resource.name === name),
    resourceInteractionsGroupByName: state => name => state.names.resource_interactions_groups.find(group => group.name === name),

    trustedFamilyByName: state => name => state.differences.families.find(f => f.trusted.name === name),
    trustedGenusByName: state => name => state.differences.genera.find(f => f.trusted.name === name),
    trustedResourceByName: state => name => state.differences.resources.find(f => f.trusted.name === name),
    trustedResourceInteractionsGroupByName: state => name => state.differences.resource_interactions_groups.find(f => f.trusted.name === name),

    familiesToSync: state => state.differences.families.filter(pair => !pair.trusted.isEqual(pair.current)),
    generaToSync: state => state.differences.genera.filter(pair => !pair.trusted.isEqual(pair.current)),
    resourcesToSync: state => state.differences.resources.filter(pair => !pair.trusted.isEqual(pair.current)),
    resourceInteractionsGroupsToSync: state => state.differences.resource_interactions_groups.filter(pair => !pair.trusted.isEqual(pair.current)),
    resourceInteractionsToSync: state => state.differences.resource_interactions.filter(pair => !pair.trusted.isEqual(pair.current)),
  },
})

TrustedModel.store = Store

export default Store
