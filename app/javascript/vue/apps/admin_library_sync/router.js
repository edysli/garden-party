import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'root',
    component: () => import(/* webpackChunkName: "genera" */ './components/Families.vue'),
  },
  {
    path: '/genera',
    name: 'genera',
    component: () => import(/* webpackChunkName: "genera" */ './components/Genera.vue'),
  },
  {
    path: '/resources',
    name: 'resources',
    component: () => import(/* webpackChunkName: "genera" */ './components/Resources.vue'),
  },
  {
    path: '/resource_interactions_groups',
    name: 'resource_interactions_groups',
    component: () => import(/* webpackChunkName: "genera" */ './components/ResourceInteractionsGroup.vue'),
  },
  {
    path: '/resource_interactions',
    name: 'resource_interactions',
    component: () => import(/* webpackChunkName: "genera" */ './components/ResourceInteraction.vue'),
  },
]

const router = createRouter({
  routes,
  history: createWebHashHistory(),
})

export default router
