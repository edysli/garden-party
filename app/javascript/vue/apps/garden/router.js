import { createRouter, createWebHistory } from 'vue-router'
import Home from './components/Home.vue'

import Store from './store'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/soil_analysis',
    name: 'soilAnalysis',
    component: () => import('./components/SoilAnalysis.vue'),
  },
  {
    path: '/library',
    component: () => import('./components/Layouts/_LibraryLayout.vue'),
    children: [
      {
        path: '/library/:resourceId?',
        name: 'library',
        component: () => import('./components/Library.vue'),
      },
      {
        path: '/library/genera/:genusId?/:resourceId?',
        name: 'libraryByGenus',
        component: () => import('./components/LibraryByGenus.vue'),
      },
      {
        path: '/library/families/:familyId?/:resourceId?',
        name: 'libraryByFamily',
        component: () => import('./components/LibraryByFamily.vue'),
      },
      {
        path: '/library/resource_interactions_groups/:resourceInteractionsGroupId?',
        name: 'resourceInteractionsGroups',
        component: () => import('./components/ResourceInteractionsGroups.vue'),
      },
    ],
  },
  // Maps
  // --------------------------------------------------------------------------
  {
    // All child routes should have the "inMap" metadata set to true, as long as
    // they are related to current map.
    path: '/maps/new',
    component: () => import('./components/Layouts/NewMapLayout.vue'),
    children: [
      {
        path: '/maps/new/picture',
        name: 'newPictureMap',
        component: () => import('./components/NewPictureMap.vue'),
      },
      {
        path: '/maps/new/osm',
        name: 'newOSMMap',
        component: () => import('./components/NewOsmMap.vue'),
      },
    ],
  },
  {
    path: '/maps/:mapId(\\d+)/edit',
    name: 'editMap',
    component: () => import('./components/EditMap.vue'),
    meta: { mustBeOwner: true, inMap: true },
  },
  {
    path: '/maps/:mapId(\\d+)',
    component: () => import('./components/Layouts/_GardenLayout.vue'),
    children: [
      {
        path: '/maps/:mapId(\\d+)/map',
        name: 'map',
        component: () => import('./components/Garden.vue'),
        meta: { inMap: true },
      },
      {
        path: '/maps/:mapId(\\d+)/inventory',
        name: 'inventory',
        component: () => import('./components/Inventory.vue'),
        meta: { inMap: true },
      },
      {
        path: '/maps/:mapId(\\d+)/tasks',
        name: 'tasks',
        component: () => import('./components/Tasks.vue'),
        meta: { inMap: true },
      },
      {
        path: '/maps/:mapId(\\d+)/harvests',
        name: 'harvests',
        component: () => import('./components/Harvests.vue'),
        meta: { inMap: true },
      },
      {
        path: '/maps/:mapId(\\d+)/activities',
        name: 'activities',
        component: () => import('./components/Activities.vue'),
        meta: { inMap: true },
      },
      {
        path: '/maps/:mapId(\\d+)/team',
        name: 'team',
        component: () => import('./components/Team.vue'),
        meta: { inMap: true },
      },
      {
        path: '/maps/:mapId(\\d+)/coordination',
        name: 'coordination',
        component: () => import('./components/Coordination.vue'),
        meta: { inMap: true },
      },
    ],
  },
]

const router = createRouter({
  routes,
  history: createWebHistory('app'),
})

router.beforeEach((to, from, next) => {
  if (to.params.mapId && to.meta.mustBeOwner) {
    const map = Store.getters.map(parseInt(to.params.mapId, 10))
    const user = Store.getters.account
    if (map.userId !== user.id) {
      next(false)
      return
    }
  }

  next()
})

export default router
