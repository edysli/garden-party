/**
 * @jest-environment  jsdom
 */
import { mount } from '@vue/test-utils'
import { systemNotificationFactory } from '../../../../../specs/factories/notifications'
import Notification from '../../../../classes/models/Notification'
import NotificationComponent from './_Notification'

/**
 * @typedef {import('vue')}                                     Vue
 * @typedef {import('@vue/test-utils').VueWrapper}              Wrapper
 * @typedef {import('../../../../classes/models/Notification')} Notification
 */

/**
 * Wrapper to mount component with correct data
 *
 * @param   {object}       props              - Element props
 * @param   {Notification} props.notification - Notification instance
 * @returns {Wrapper<Vue>}                    Mounted wrapper for the test
 */
function makeWrapper (props) {
  return mount(NotificationComponent, {
    props,
    global: {
      mocks: {
        $t: string => string,
        $d: date => date ? date.toUTCString() : null,
      },
      stubs: {
        GpMarkdownRenderer: true,
      },
    },
  })
}

jest.mock('../../../../app_helpers/i18n', () => ({}))

let wrapper
let notificationData

describe('system notification', () => {
  beforeEach(() => {
    notificationData = systemNotificationFactory.build({
      created_at: 'Fri, 01 Jan 2021 00:00:00 GMT',
      content: {
        title: 'Welcome',
        message: 'This is a wonderful notification',
      },
    })
  })

  describe('when message is not shown', () => {
    beforeEach(() => {
      wrapper = makeWrapper({
        notification: new Notification(notificationData),
        showMessage: false,
      })
    })

    it('matches the snapshot', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('when message is shown', () => {
    beforeEach(() => {
      wrapper = makeWrapper({
        notification: new Notification(notificationData),
        showMessage: true,
      })
    })

    it('matches the snapshot', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})
