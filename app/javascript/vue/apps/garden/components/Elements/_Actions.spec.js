/**
 * @jest-environment  jsdom
 */
import { mount } from '@vue/test-utils'
import Element from '../../../../classes/models/Element'

import ActionButtonsComponent from './_Actions'
import { elementFactory } from '../../../../../specs/factories/elements'

/**
 * @typedef {import('vue')}                        Vue
 * @typedef {import('@vue/test-utils').VueWrapper} Wrapper
 */

const now = new Date()
const later = new Date()
later.setDate(later.getDate() + 2)
const before = new Date()
before.setDate(before.getDate() - 2)

/**
 * Builds an Element
 *
 * @param   {object}  config                        - Element configuration
 * @param   {Date}    config.implantedAt            - Implantation date
 * @param   {Date}    config.removedAt              - Removal date
 * @param   {Date}    config.implantationPlannedFor - Planned date for implantation
 * @param   {Date}    config.removalPlannedFor      - Planned date for removal
 * @returns {Element}                               Element to use in tests
 */
function makeElement ({
  implantedAt = null,
  removedAt = null,
  implantationPlannedFor = null,
  removalPlannedFor = null,
}) {
  let status = 'planned'
  if (implantedAt && !removedAt) status = 'implanted'
  else if (removedAt) status = 'removed'
  return new Element(elementFactory.build({
    implanted_at: implantedAt || null,
    removed_at: removedAt || null,
    implantation_planned_for: implantationPlannedFor || null,
    removal_planned_for: removalPlannedFor || null,
    status,
  }))
}

/**
 * Wrapper to mount component with correct data
 *
 * @param   {Element}      element             - Element to use
 * @param   {boolean}      showGeometryActions - Whether or not to show the geometry-related buttons
 * @returns {Wrapper<Vue>}                     Mounted wrapper for the test
 */
function makeWrapper (element, showGeometryActions = false) {
  return mount(ActionButtonsComponent, {
    props: {
      element,
      showGeometryActions,
    },
    global: {
      mocks: {
        $t: string => string,
        $d: date => date.toString(),
        $store: {
          actions: {},
          getters: {},
          dispatch: () => Promise.resolve(),
        },
      },
      stubs: ['popper'],
    },
  })
}

jest.mock('../../../../app_helpers/i18n', () => ({}))

let wrapper

describe('interface', () => {
  describe('with an element still in place', () => {
    beforeEach(() => {
      wrapper = makeWrapper(makeElement({}))
    })

    it('has no button to move the element', () => {
      expect(wrapper.find('button[title="js.generic.move"]').exists()).toBeFalsy()
    })

    it('has a button to show observations', () => {
      expect(wrapper.find('button[title="js.elements.action_buttons.show_observations"]').exists()).toBeTruthy()
    })

    it('has a button to edit the element', () => {
      expect(wrapper.find('button[title="js.elements.action_buttons.edit_element"]').exists()).toBeTruthy()
    })

    it('has a button to destroy the element', () => {
      expect(wrapper.find('button[title="js.elements.action_buttons.destroy_element"]').exists()).toBeTruthy()
    })

    describe('with a non-implanted element', () => {
      beforeEach(() => {
        wrapper = makeWrapper(makeElement({}))
      })

      it('has a button to implant the element', () => {
        expect(wrapper.find('button[title="js.elements.action_buttons.implant"]').exists()).toBeTruthy()
      })
      it('has no button to remove the element', () => {
        expect(wrapper.find('button[title="js.elements.action_buttons.remove"]').exists()).toBeFalsy()
      })
    })

    describe('with an implanted element', () => {
      beforeEach(() => {
        wrapper = makeWrapper(makeElement({ implantedAt: before }))
      })

      it('has no button to implant the element', () => {
        expect(wrapper.find('button[title="js.elements.action_buttons.implant"]').exists()).toBeFalsy()
      })
      it('has a button to remove the element', () => {
        expect(wrapper.find('button[title="js.elements.action_buttons.remove"]').exists()).toBeTruthy()
      })
    })

    describe('when geometry actions should be visible', () => {
      beforeEach(() => {
        wrapper = makeWrapper(makeElement({}), true)
      })
      it('has a button to move the element', () => {
        expect(wrapper.find('button[title="js.generic.move"]').exists()).toBeTruthy()
      })
    })
  })

  describe('with removed element', () => {
    beforeEach(() => {
      wrapper = makeWrapper(makeElement({
        implantedAt: before,
        removedAt: now,
      }))
    })

    it('has a button to show observations', () => {
      expect(wrapper.find('button[title="js.elements.action_buttons.show_observations"]').exists()).toBeTruthy()
    })

    it('has a button to edit the element', () => {
      expect(wrapper.find('button[title="js.elements.action_buttons.edit_element"]').exists()).toBeTruthy()
    })

    it('has a button to destroy the element', () => {
      expect(wrapper.find('button[title="js.elements.action_buttons.destroy_element"]').exists()).toBeTruthy()
    })

    it('has a button to duplicate the element', () => {
      expect(wrapper.find('button[title="js.elements.action_buttons.duplicate"]').exists()).toBeTruthy()
    })

    it('don\'t have other buttons', () => {
      // show observations, dropdown trigger, duplicate, edit and delete
      expect(wrapper.findAll('button')).toHaveLength(5)
    })

    describe('when geometry actions should be visible', () => {
      beforeEach(() => {
        wrapper = makeWrapper(makeElement({
          implantedAt: before,
          removedAt: now,
        }), true)
      })
      it('has no button to move the element', () => {
        expect(wrapper.find('button[title="js.generic.move"]').exists()).toBeFalsy()
      })
    })
  })
})

// TODO: Test interactions
