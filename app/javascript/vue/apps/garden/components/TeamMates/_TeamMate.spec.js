/**
 * @jest-environment  jsdom
 */
import { mount } from '@vue/test-utils'
// Models
import TeamMate from '../../../../classes/models/TeamMate'
import Account from '../../../../classes/models/Account'
import Map from '../../../../classes/models/Map'
// Factories
import { teamMateFactory, acceptedTeamMateFactory } from '../../../../../specs/factories/team_mates'
import { accountFactory } from '../../../../../specs/factories/accounts'
import { mapFactory } from '../../../../../specs/factories/maps'
// Components
import TeamMateComponent from './_TeamMate'

/**
 * @typedef {import('vue')}                          Vue
 * @typedef {import('@vue/test-utils').VueWrapper  } Wrapper
 */

/**
 * Wrapper to mount component with correct data
 *
 * @param   {object}       parameters                          - Wrapper parameters
 * @param   {boolean}      parameters.acceptedInvitation       - Whether or not the current teamMate has accepted the invitation
 * @param   {boolean}      parameters.teamMateIsTheMapOwner    - Whether or not the current teamMate is the map owner
 * @param   {boolean}      parameters.currentUserIsMapOwner    - Whether or not the account owner is the map owner
 * @param   {boolean}      parameters.currentUserIsTheTeamMate - Whether or not the account owner is the current teamMate
 * @returns {Wrapper<Vue>}                                     Mounted wrapper for the test
 */
function makeWrapper ({
  currentUserIsMapOwner = false,
  currentUserIsTheTeamMate = false,
  teamMateIsTheMapOwner = false,
  acceptedInvitation = true,
}) {
  const teamMate = acceptedInvitation ? acceptedTeamMateFactory.build({}, { transient: { username: 'Bob' } }) : teamMateFactory.build({}, { transient: { username: 'Bob' } })
  const accountAttributes = accountFactory.build()
  const mapAttributes = mapFactory.build()
  if (currentUserIsTheTeamMate) accountAttributes.id = teamMate.user_id
  if (currentUserIsMapOwner) mapAttributes.user_id = accountAttributes.id
  if (teamMateIsTheMapOwner) mapAttributes.user_id = teamMate.user_id

  return mount(TeamMateComponent, {
    computed: {
      ...TeamMateComponent.computed,
      // Mock data from VueX store
      account: () => new Account(accountAttributes),
      map: () => new Map(mapAttributes),
    },
    props: { teamMate: new TeamMate(teamMate) },
    global: {
      mocks: {
        $t: string => string,
        $d: date => date.toString(),
      },
    },
  })
}

jest.mock('../../../../app_helpers/i18n', () => ({}))

let wrapper

describe('_TeamMate.vue', () => {
  describe.each`
    currentUserIsMapOwner| currentUserIsTheTeamMate | teamMateIsTheMapOwner | acceptedInvitation
    ${true}              | ${true}                  | ${true}               | ${true}
    ${true}              | ${false}                 | ${false}              | ${false}
    ${true}              | ${false}                 | ${false}              | ${true}
    ${false}             | ${true}                  | ${false}              | ${true}
    ${false}             | ${false}                 | ${true}               | ${true}
    ${false}             | ${false}                 | ${false}              | ${true}
    ${false}             | ${false}                 | ${false}              | ${false}
  `('test matrix for content', ({ currentUserIsMapOwner, currentUserIsTheTeamMate, teamMateIsTheMapOwner, acceptedInvitation }) => {
    describe(`when the account owner ${currentUserIsMapOwner ? 'is' : 'is not'} the map owner`, () => {
      describe(`when the account owner ${currentUserIsTheTeamMate ? 'is' : 'is not'} the displayed team mate`, () => {
        describe(`when the displayed team mate ${teamMateIsTheMapOwner ? 'is' : 'is not'} the map owner`, () => {
          describe(`when the displayed team mate ${acceptedInvitation ? 'accepted' : 'has not accepted'} the invitation`, () => {
            it('matches the snapshot', () => {
              wrapper = makeWrapper({ acceptedInvitation, teamMateIsTheMapOwner, currentUserIsMapOwner, currentUserIsTheTeamMate })

              const snapshotDescription = `
                currentUserIsMapOwner: ${currentUserIsMapOwner}
                currentUserIsTheTeamMate: ${currentUserIsTheTeamMate}
                teamMateIsTheMapOwner: ${teamMateIsTheMapOwner}
                acceptedInvitation: ${acceptedInvitation}`
              expect(wrapper.element).toMatchSnapshot(snapshotDescription)
            })
          })
        })
      })
    })
  })
})
