import { createStore } from 'vuex'

import AccountModule from '../../stores/modules/AccountModule'
import ActivityModule from '../../stores/modules/ActivityModule'
import ElementModule from '../../stores/modules/ElementModule'
import FamilyModule from '../../stores/modules/FamilyModule'
import GenusModule from '../../stores/modules/GenusModule'
import HarvestModule from '../../stores/modules/HarvestModule'
import LayerModule from '../../stores/modules/LayerModule'
import LoaderModule from '../../stores/modules/LoaderModule'
import MapModule from '../../stores/modules/MapModule'
import NotificationModule from '../../stores/modules/NotificationModule'
import ObservationModule from '../../stores/modules/ObservationModule'
import PatchModule from '../../stores/modules/PatchModule'
import PathModule from '../../stores/modules/PathModule'
import ResourceInteractionModule from '../../stores/modules/ResourceInteractionModule'
import ResourceInteractionsGroupModule from '../../stores/modules/ResourceInteractionsGroupModule'
import ResourceModule from '../../stores/modules/ResourceModule'
import ResourceNoteModule from '../../stores/modules/ResourceNoteModule'
import TagModule from '../../stores/modules/TagModule'
import TaskModule from '../../stores/modules/TaskModule'
import TeamMateModule from '../../stores/modules/TeamMateModule'
import UserModule from '../../stores/modules/UserModule'

import Model from '../../classes/models/Model'

const Store = createStore({
  state: {
  },
  mutations: {
    resetMapData (state) {
      state.ActivityModule.activities = []
      state.ElementModule.elements = []
      state.LayerModule.layers = []
      state.ObservationModule.observations = []
      state.PatchModule.patches = []
      state.PathModule.paths = []
      state.TaskModule.tasks = []
      state.TaskModule.taskNames = []
      state.TeamMateModule.teamMates = []
    },
  },
  actions: {
    resetMapData ({ commit }) { commit('resetMapData') },
  },
  modules: {
    AccountModule,
    ActivityModule,
    ElementModule,
    FamilyModule,
    GenusModule,
    HarvestModule,
    LayerModule,
    LoaderModule,
    MapModule,
    NotificationModule,
    ObservationModule,
    PatchModule,
    PathModule,
    ResourceInteractionModule,
    ResourceInteractionsGroupModule,
    ResourceModule,
    ResourceNoteModule,
    TagModule,
    TaskModule,
    TeamMateModule,
    UserModule,
  },
})

Model.store = Store

export default Store
