import { createI18n } from 'vue-i18n'

const datetimeFormats = {}
// Extract time formats as VueI18n needs a separate entry
datetimeFormats[window.I18n.locale] = window.I18n.translations[window.I18n.locale].js_dates

// Only the current locale is loaded in window, so we don't care about fallbacks or other things.
export default createI18n({
  locale: window.I18n.locale,
  messages: window.I18n.translations,
  datetimeFormats,
  // Have $t, $n, etc... available in components
  globalInjection: true,
  legacy: false,
})
