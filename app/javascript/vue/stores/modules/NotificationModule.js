import Base from './Base'
import Notification from '../../classes/models/Notification'

export default {
  state: {
    /** @type {Notification[]} */
    notifications: [],
  },
  mutations: {
    resetNotifications: Base.mutations.resetState('notifications'),
    setNotification: Base.mutations.setEntity('notifications'),
    deleteNotification: Base.mutations.deleteEntity('notifications'),
  },
  actions: {
    loadNotifications: Base.actions.loadEntities(Notification, 'setNotification'),
    loadNotification: Base.actions.loadEntity(Notification, 'setNotification'),
    destroyNotification: Base.actions.destroyEntity(Notification, 'deleteNotification'),
    archiveNotification: ({ commit }, id) => Notification.archive(id).then(notification => commit('setNotification', notification)),
    archiveAllNotifications: ({ commit }) => Notification.archiveAll()
      .then((notifications) => {
        notifications.forEach((notification) => { commit('setNotification', notification) })
      }),
    destroyAllNotifications: ({ commit }) => Notification.destroyAllArchived()
      .then((notifications) => {
        commit('resetNotifications')
        notifications.forEach((notification) => { commit('setNotification', notification) })
      }),
  },
  getters: {
    notifications: state => state.notifications.sort((a, b) => b.createdAt - a.createdAt),
    unreadNotification: (state, getters) => getters.notifications.filter(n => !n.archivedAt),
    notification: Base.getters.entity('notifications'),
    searchNotifications: Base.getters.search('notifications', 'action'),
  },
}
