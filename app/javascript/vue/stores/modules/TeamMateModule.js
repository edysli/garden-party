import Base from './Base'
import TeamMate from '../../classes/models/TeamMate'

export default {
  state: {
    /** @type {TeamMate[]} */
    teamMates: [],
  },
  mutations: {
    resetTeamMates: Base.mutations.resetState('teamMates'),
    setTeamMate: Base.mutations.setEntity('teamMates'),
    deleteTeamMate: Base.mutations.deleteEntity('teamMates'),
  },
  actions: {
    loadTeamMates: Base.actions.loadEntities(TeamMate, 'setTeamMate'),
    loadTeamMate: Base.actions.loadEntity(TeamMate, 'setTeamMate'),
    createTeamMate: Base.actions.createEntity(TeamMate, 'setTeamMate', 'teamMate'),
    acceptTeamMateInvitation: ({ dispatch, commit }, { id, notificationId = null }) => TeamMate.acceptInvitation(id)
      .then((entity) => {
        if (notificationId) commit('deleteNotification', notificationId)
        // Reload available maps
        dispatch('loadMaps')
        // Still return the new TeamMate
        return entity
      }),
    refuseTeamMateInvitation: ({ commit }, id) => TeamMate.acceptInvitation(id)
      .then(() => { commit('deleteTeamMate', id) }),
    leaveTeamMate: ({ commit }, id) => TeamMate.leave(id)
      .then(() => { commit('deleteTeamMate', id) }),
    removeTeamMate: ({ commit }, id) => TeamMate.remove(id)
      .then(() => { commit('deleteTeamMate', id) }),
  },
  getters: {
    teamMates: state => state.teamMates.sort((a, b) => a.createdAt - b.createdAt),
    teamMate: Base.getters.entity('teamMates'),
    teamMateByUserId: state => id => state.teamMates.find(t => t.user.id === id),
    searchTeamMates: Base.getters.search('teamMates', 'id'),
  },
}
