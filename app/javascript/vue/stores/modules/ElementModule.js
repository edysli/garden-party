import Base from './Base'
import Element from '../../classes/models/Element'
import EventBus from '../../tools/EventBus'

export default {
  state: {
    /** @type {Element[]} */
    elements: [],
  },
  mutations: {
    resetElements: Base.mutations.resetState('elements'),
    setElement: Base.mutations.setEntity('elements'),
    deleteElement: Base.mutations.deleteEntity('elements'),
  },
  actions: {
    loadElements: Base.actions.loadEntities(Element, 'setElement'),
    loadElement: Base.actions.loadEntity(Element, 'setElement'),
    createElement ({ commit, getters }, { payload, feature }) {
      return Element.create({ element: payload })
        .then(item => {
          commit('setElement', item)
          // Return new instance
          const element = getters.element(item.id)

          // Don't add feature when not given (i.e.: elements in a patch)
          if (feature) element.setFeature(feature)
          if (!element.isPoint) EventBus.emit('updated-patch', element.patch)
          return Promise.resolve(element)
        })
    },
    updateElement: ({ commit, getters }, payload) => Element.update(payload)
      .then(element => {
        const oldLayer = getters.element(element.id).layerId

        commit('setElement', element)
        const updatedElement = getters.element(element.id)

        if (oldLayer !== updatedElement.layerId) {
          EventBus.emit('change-element-layer', { path: updatedElement, fromPathId: oldLayer })
        }
        if (updatedElement.isPoint) {
          EventBus.emit('updated-element', updatedElement)
        } else {
          EventBus.emit('updated-patch', updatedElement.patch)
        }

        // Return updated instance
        return Promise.resolve(updatedElement)
      }),
    saveElement: Base.actions.saveEntity('createElement', 'updateElement'),
    destroyElement: ({ commit, getters }, id) => Element.destroy(id)
      .then(() => {
        const element = getters.element(id)

        commit('deleteElement', id)
        if (element.isPoint) EventBus.emit('destroyed-element', element)
        else EventBus.emit('updated-patch', element.patch)

        return Promise.resolve()
      }),
    loadPatchElements ({ commit }, patchId) {
      return Element.getPatchIndex(patchId)
        .then(items => { items.forEach(item => { commit('setElement', item) }) })
    },
    implantElement ({ dispatch }, { id, date, implantationMode }) {
      const now = new Date()
      const dateObj = new Date(date)
      const payload = { id, implantation_mode: implantationMode }
      if (dateObj > now) payload.implantation_planned_for = date
      else payload.implanted_at = date
      return dispatch('updateElement', payload)
    },
    removeElement ({ dispatch }, { id, date }) {
      const now = new Date()
      const dateObj = new Date(date)
      const payload = { id }
      if (dateObj > now) payload.removal_planned_for = date
      else payload.removed_at = date
      return dispatch('updateElement', payload)
    },
    duplicateElement: ({ commit, getters }, id) => Element.duplicate(id)
      .then((item) => {
        commit('setElement', item)
        const element = getters.element(item.id)

        EventBus.emit('created-element', element)
        // Return new instance
        return Promise.resolve(element)
      }),
  },
  getters: {
    elements: state => state.elements,
    element: Base.getters.entity('elements'),
    activePointElements: state => state.elements.filter(e => !e.isRemoved && e.isPoint),
    pointElements: state => state.elements.filter(e => e.isPoint)
      .sort((a, b) => {
        // Sort by name when state is the same
        if (a.isRemoved === b.isRemoved) return a.displayName.localeCompare(b.displayName)
        // Move removed patches after
        return a.isRemoved ? 1 : -1
      }),
    elementsByPatchId: state => patchId => state.elements
      .filter(c => c.patchId === patchId)
      .sort((a, b) => {
        if (!a.isImplanted && !a.isRemoved && !b.isImplanted && !b.isRemoved) {
          return b.createdAt.valueOf() - a.createdAt.valueOf()
        }
        if (!a.isImplanted && !a.isRemoved) return -1
        if (!b.isImplanted && !b.isRemoved) return 1
        // Move implanted above removed
        if (a.isImplanted && b.isRemoved) return -1
        if (a.isRemoved && b.isImplanted) return 1

        if (a.isImplanted && b.isImplanted) return b.implantedAt.valueOf() - a.implantedAt.valueOf()
        if (a.isRemoved && b.isRemoved) return b.removedAt.valueOf() - a.removedAt.valueOf()

        return a.displayName.localeCompare(b.displayName)
      }),
  },
}
