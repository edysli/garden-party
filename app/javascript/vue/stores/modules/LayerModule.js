import Base from './Base'
import Layer from '../../classes/models/Layer'
import EventBus from '../../tools/EventBus'

export default {
  state: {
    /** @type {Layer[]} */
    layers: [],
  },
  mutations: {
    resetLayers: Base.mutations.resetState('layers'),
    setLayer: Base.mutations.setEntity('layers'),
    deleteLayer: Base.mutations.deleteEntity('layers'),
  },
  actions: {
    loadLayers: Base.actions.loadEntities(Layer, 'setLayer'),
    loadLayer: Base.actions.loadEntity(Layer, 'setLayer'),
    createLayer ({ commit, getters }, payload) {
      return Layer.create(payload)
        .then(layer => {
          commit('setLayer', layer)
          EventBus.emit('layer-created', layer)

          // Return new instance
          return Promise.resolve(getters.layer(layer.id))
        })
    },
    updateLayer ({ commit, getters }, payload) {
      return Layer.update(payload)
        .then(layer => {
          commit('setLayer', layer)

          // Return updated instance
          return Promise.resolve(getters.layer(layer.id))
        })
    },
    saveLayer: Base.actions.saveEntity('createLayer', 'updateLayer'),
    destroyLayer: Base.actions.destroyEntity(Layer, 'deleteLayer'),
  },
  getters: {
    layer: Base.getters.entity('layers'),
    layerSearch: Base.getters.search('layers', 'name'),
    layers: state => state.layers
      .sort((a, b) => b.position - a.position),
  },
}
