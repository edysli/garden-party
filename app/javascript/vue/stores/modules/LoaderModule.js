import randomString from '../../../helpers/RandomString'

export default {
  state: {
    loaders: [],
  },
  mutations: {
    addLoader (state, { key, group, message }) {
      state.loaders.push({ key, group, message })
    },
    removeLoader (state, key) {
      const index = state.loaders.findIndex(l => l.key === key)
      if (index > -1) state.loaders.splice(index, 1)
    },
  },
  actions: {
    load ({ commit }, { promise, message, group = null }) {
      group = group || randomString()
      const key = randomString(8)

      commit('addLoader', { key, group, message })
      return promise
        .then((result) => {
          return Promise.resolve(result)
        })
        .catch((err) => Promise.reject(err))
        .then(() => { commit('removeLoader', key) })
    },
    loadGroup ({ dispatch }, { name, loaders }) {
      return Promise.all(loaders.map((l) => {
        dispatch('load', {
          promise: l.promise,
          message: l.message,
          group: name,
        })
        return l.promise
      }))
    },
  },
  getters: {
    loader: state => group => state.loaders.filter(l => l.group === group),
  },
}
