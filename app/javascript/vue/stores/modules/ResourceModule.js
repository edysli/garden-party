import Base from './Base'
import Resource from '../../classes/models/Resource'

/**
 * Filters a list of resources given on a "month key" given a months list
 *
 * @param   {Resource[]} resources - Resources to filter
 * @param   {string}     key       - Month key in the resource ("shelteredSowingMonths", ...)
 * @param   {number[]}   months    - Array of months like "[1,2,3]"
 * @returns {Resource[]}           - Filtered list
 */
function filterMonth (resources, key, months) {
  const list = []

  // Using "for" for performance
  for (let i = 0, amount = resources.length; i < amount; i++) {
    for (const month of months) {
      if (resources[i][key][month - 1]) list.push(resources[i])
    }
  }

  return list
}

/**
 * Sort resources by name
 *
 * @param   {Resource[]} resources - List to sort
 * @returns {Resource[]}           Sorted list
 */
function sortResources (resources) {
  return resources.sort((a, b) => a.name.localeCompare(b.name))
}

/**
 * Searches for a string in resources attributes
 *
 * @param   {Resource[]} resources - Resource list
 * @param   {string}     term      - Term to search
 * @returns {Resource[]}           - Filtered results
 */
function searchResources (resources, term) {
  if (term.length === 0) return sortResources(resources)

  term = term.toLowerCase()

  const results = []
  // Use "for" for performance
  for (let i = 0, amount = resources.length; i < amount; i++) {
    // Order matters as we want to return as soon as possible
    if (resources[i].name.toLowerCase().includes(term)
      || resources[i].latinName?.toLowerCase().includes(term)
      || resources[i].commonNames.join('').includes(term)) {
      results.push(resources[i])
    }
  }

  return sortResources(results)
}

export default {
  state: {
    /** @type {Resource[]} */
    resources: [],
  },
  mutations: {
    resetResources: Base.mutations.resetState('resources'),
    setResource: Base.mutations.setEntity('resources'),
    deleteResource: Base.mutations.deleteEntity('resources'),
  },
  actions: {
    loadResources: ({ commit }) => Resource.getIndex()
      .then(items => {
        items.forEach(item => {
          commit('setResource', item)
          item.tagList.forEach(t => commit('setTagString', t))
        })

        return Promise.resolve(items)
      }),
    loadResource: ({ commit, getters }, id) => Resource.get(id)
      .then((item) => {
        commit('setResource', item)
        item.tagList.forEach(t => commit('setTagString', t))

        return Promise.resolve(getters.resource(item.id))
      }),
    createResource: ({ commit, getters }, payload) => Resource.create(payload)
      .then((item) => {
        commit('setResource', item)
        item.tagList.forEach(t => commit('setTagString', t))

        return Promise.resolve(getters.resource(item.id))
      }),
    updateResource: ({ commit, getters }, payload) => Resource.update(payload)
      .then((item) => {
        commit('setResource', item)
        item.tagList.forEach(t => commit('setTagString', t))

        return Promise.resolve(getters.resource(item.id))
      }),
    saveResource: Base.actions.saveEntity('createResource', 'updateResource'),
    destroyResource: Base.actions.destroyEntity(Resource, 'deleteResource'),
  },
  getters: {
    resources: Base.getters.entities('resources', 'name'),
    resource: Base.getters.entity('resources'),
    resourceSearch: Base.getters.search('resources', 'name'),
    parentableResources: state => state.resources
      .filter(r => r.parentId === null)
      .sort((a, b) => a.name.localeCompare(b.name)),
    resourcesByResourceInteractionsGroup: (state) => (groupId) => state.resources.filter(r => r.resourceInteractionsGroupId === groupId)
      .sort((a, b) => a.name.localeCompare(b.name)),
    resourcesGroupedByParent: (state) => (searchTerm = '') => {
      const hash = {}
      const resources = searchResources(state.resources.slice(), searchTerm)
      const parentIds = []

      // Grouping by parent using "for" for performance
      for (let i = 0, amount = resources.length; i < amount; i++) {
        if (resources[i].parentId && searchTerm.length === 0) {
          parentIds.push(resources[i].parentId)
          if (!hash[resources[i].parentId]) hash[resources[i].parentId] = { resource: null, children: [resources[i]] }
          else hash[resources[i].parentId].children.push(resources[i])
        } else {
          if (!hash[resources[i].id]) hash[resources[i].id] = { resource: resources[i], children: [] }
          else hash[resources[i].id].resource = resources[i]

          hash[resources[i].id].id = resources[i].id
        }
      }

      // Sort child resources
      for (let i = 0, amount = parentIds.length; i < amount; i++) {
        hash[parentIds[i]].children.sort((a, b) => a.name.localeCompare(b.name))
      }

      const list = []
      const keys = Object.keys(hash)

      // Make an array so it's iterable by virtual-list component
      for (let i = 0, amount = keys.length; i < amount; i++) {
        list.push(hash[keys[i]])
      }

      return list.sort((a, b) => a.resource.name.localeCompare(b.resource.name))
    },
    searchedResources: (state) => (term) => sortResources(searchResources(state.resources.slice(), term)),
    searchedAndFilteredResources: (state) => ({ generic, search, tags, shelteredSowingMonths, soilSowingMonths, harvestingMonths }) => {
      let resources = generic ? state.resources.filter(r => r.generic) : state.resources.slice()

      // Filter by search term
      resources = searchResources(resources, search)

      // For the month filter, select any resource that match one month in any category
      if (shelteredSowingMonths.length > 0) {
        resources = filterMonth(resources, 'shelteredSowingMonths', shelteredSowingMonths)
      }
      if (soilSowingMonths.length > 0) {
        resources = filterMonth(resources, 'soilSowingMonths', soilSowingMonths)
      }
      if (harvestingMonths.length > 0) {
        resources = filterMonth(resources, 'harvestingMonths', harvestingMonths)
      }

      // Select resources matching all of the tags
      if (tags.length === 0) {
        return resources
      } else {
        const matches = []
        // Using "for" for performance
        for (let i = 0, amount = resources.length; i < amount; i++) {
          let tagsCount = 0
          for (const tag of tags) {
            if (resources[i].tagList.includes(tag)) tagsCount++
          }
          if (tagsCount === tags.length) matches.push(resources[i])
        }

        return sortResources(matches)
      }
    },
    childResources: (state) => (parentId) => {
      const list = []

      // Using "for" instead of ".filter" for performance
      for (let i = 0, total = state.resources.length; i < total; i++) {
        if (state.resources[i].parentId === parentId) list.push(state.resources[i])
      }

      return sortResources(list)
    },
    resourcesByGenus: (state) => (genusId) => {
      const list = []

      // Using "for" instead of ".filter" for performance
      for (let i = 0, total = state.resources.length; i < total; i++) {
        if (state.resources[i].genusId === genusId) list.push(state.resources[i])
      }

      return sortResources(list)
    },
    resourcesByFamily: (state, rootGetters, rootState) => (familyId) => {
      const resources = []
      const genera = rootState.GenusModule.genera.filter(g => g.familyId === familyId)

      // Using "for" instead of ".forEach" for performance
      for (let i = 0, totalGenera = genera.length; i < totalGenera; i++) {
        resources.push(...rootGetters.resourcesByGenus(genera[i].id))
      }

      return resources
    },
  },
}
