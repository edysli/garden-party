import Base from './Base'
import Activity from '../../classes/models/Activity'

export default {
  state: {
    /** @type {Activity[]} */
    activities: [],
  },
  mutations: {
    resetActivities: Base.mutations.resetState('activities'),
    setActivity: Base.mutations.setEntity('activities'),
    deleteActivity: Base.mutations.deleteEntity('activities'),
  },
  actions: {
    loadActivities ({ commit }, mapId) {
      Activity.getIndex(mapId)
        .then((items) => {
          items.forEach(item => { commit('setActivity', item) })

          return items
        })
    },
    loadActivity: Base.actions.loadEntity(Activity, 'setActivity'),
  },
  getters: {
    activities: state => state.activities
      .sort((a, b) => { return b.happenedAt - a.happenedAt }),
    activity: Base.getters.entity('activities'),
  },
}
