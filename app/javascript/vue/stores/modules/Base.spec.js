import Base from './Base'
import { testModelFactory, TestModel } from '../../../specs/fakes/TestModel'

// Ignore all possible API calls
jest.mock('../../tools/api', () => {})

const STORE_KEY = 'tests'

// VueX methods mocks
let commitMock
let dispatchMock
let getterMocks
// VueX State
let stateMock
// Data mocks
let oneEntityMock
let manyEntitiesMock

describe('Base module', () => {
  beforeEach(() => {
    commitMock = jest.fn()
    dispatchMock = jest.fn()
    getterMocks = {
      entities: jest.fn(() => testModelFactory.buildList(2)),
      entity: jest.fn(id => testModelFactory.build({ id })),
      search: jest.fn(term => testModelFactory.buildList(2, { name: term })),
    }
    // Generating state
    stateMock = {}
    stateMock[STORE_KEY] = [
      new TestModel(testModelFactory.build({ name: 'something here' })),
      new TestModel(testModelFactory.build({ name: 'here we are too' })),
      new TestModel(testModelFactory.build({ name: 'another one' })),
    ]
  })

  describe('mutations', () => {
    let mutation

    describe('setEntity', () => {
      beforeEach(() => {
        mutation = Base.mutations.setEntity(STORE_KEY)
      })

      describe('when entity exists', () => {
        beforeEach(() => {
          oneEntityMock = new TestModel({
            ...stateMock[STORE_KEY][0],
            name: 'new name',
          })
        })

        test('does not create a new entry', () => {
          const currentLength = stateMock[STORE_KEY].length
          mutation(stateMock, oneEntityMock)
          expect(stateMock[STORE_KEY]).toHaveLength(currentLength)
        })

        test('updates the existing one', () => {
          mutation(stateMock, oneEntityMock)
          const updatedEntity = stateMock[STORE_KEY].find(e => e.id === oneEntityMock.id)
          expect(updatedEntity).toStrictEqual(oneEntityMock)
        })

        test('calls model.updateAttributes', async () => {
          const initialEntity = stateMock[STORE_KEY].find(e => e.id === oneEntityMock.id)
          const spy = jest.spyOn(initialEntity, 'updateAttributes')
          await mutation(stateMock, oneEntityMock)
          expect(spy).toHaveBeenCalled()
        })
      })

      describe('when entity does not exists', () => {
        beforeEach(() => {
          oneEntityMock = new TestModel(testModelFactory.build())
        })

        test('creates a new entry', () => {
          const currentLength = stateMock[STORE_KEY].length
          mutation(stateMock, oneEntityMock)
          expect(stateMock[STORE_KEY]).toHaveLength(currentLength + 1)
        })
      })
    })

    describe('deleteEntity', () => {
      beforeEach(() => {
        mutation = Base.mutations.deleteEntity(STORE_KEY)
      })
      describe('when entity exists', () => {
        test('removes the entity', () => {
          const currentLength = stateMock[STORE_KEY].length
          mutation(stateMock, stateMock[STORE_KEY][0].id)
          expect(stateMock[STORE_KEY]).toHaveLength(currentLength - 1)
        })
      })
      describe('when entity does not exists', () => {
        test('throws an error', () => {
          const executor = () => new Promise(() => mutation(stateMock, 0))
          return expect(executor()).rejects.toThrow('can\'t remove entity with id')
        })
      })
    })
  })

  describe('actions', () => {
    let action

    describe('loadEntities', () => {
      beforeEach(() => {
        manyEntitiesMock = testModelFactory.buildList(2)
        TestModel.getIndex = jest.fn(() => new Promise(resolve => resolve(manyEntitiesMock)))
        action = Base.actions.loadEntities(TestModel, 'setEntity')
      })
      afterEach(() => { TestModel.getIndex.mockReset() })

      test('calls model.getIndex', async () => {
        await action({ commit: commitMock })
        expect(TestModel.getIndex).toHaveBeenCalledTimes(1)
      })

      test('mutates state for each entry', async () => {
        await action({ commit: commitMock })
        expect(commitMock).toHaveBeenCalledTimes(2)
      })
    })

    describe('loadEntity', () => {
      beforeEach(() => {
        oneEntityMock = testModelFactory.build()
        TestModel.get = jest.fn(() => new Promise(resolve => resolve(oneEntityMock)))
        action = Base.actions.loadEntity(TestModel, 'setEntity', oneEntityMock)
      })
      afterEach(() => { TestModel.get.mockReset() })

      test('calls model.get', async () => {
        await action({ commit: commitMock }, 1)
        expect(TestModel.get).toHaveBeenCalledTimes(1)
      })

      test('mutates state with "setEntity"', async () => {
        await action({ commit: commitMock }, 1)
        expect(commitMock).toHaveBeenCalledWith('setEntity', oneEntityMock)
      })
    })

    describe('createEntity', () => {
      beforeEach(() => {
        const oneEntityPayloadMock = testModelFactory.build()
        delete oneEntityPayloadMock.id
        oneEntityMock = { ...oneEntityPayloadMock, id: 1 }
        TestModel.create = jest.fn(() => new Promise(resolve => resolve(oneEntityMock)))
        action = Base.actions.createEntity(TestModel, 'setEntity', 'entity')
      })
      afterEach(() => { TestModel.create.mockReset() })

      test('calls model.create', async () => {
        await action({ commit: commitMock, getters: getterMocks }, { name: 'test' })
        expect(TestModel.create).toHaveBeenCalledTimes(1)
      })

      test('mutates state with "setEntity"', async () => {
        await action({ commit: commitMock, getters: getterMocks }, { name: 'test' })
        expect(commitMock).toHaveBeenCalledWith('setEntity', oneEntityMock)
      })
    })

    describe('updateEntity', () => {
      beforeEach(() => {
        oneEntityMock = testModelFactory.build()
        TestModel.update = jest.fn(() => new Promise(resolve => resolve(oneEntityMock)))
        action = Base.actions.updateEntity(TestModel, 'setEntity', 'entity')
      })
      afterEach(() => { TestModel.update.mockReset() })

      test('calls model.update', async () => {
        await action({
          commit: commitMock,
          getters: getterMocks,
        }, { name: 'test' })
        expect(TestModel.update).toHaveBeenCalledTimes(1)
      })

      test('mutates state with "setEntity"', async () => {
        await action({
          commit: commitMock,
          getters: getterMocks,
        }, { name: 'test' })
        expect(commitMock).toHaveBeenCalledWith('setEntity', oneEntityMock)
      })
    })

    describe('saveEntity', () => {
      beforeEach(() => {
        action = Base.actions.saveEntity('createEntity', 'updateEntity')
      })

      describe('when entity exists', () => {
        test('updates the entity', async () => {
          await action({ dispatch: dispatchMock }, testModelFactory.build({ id: 1 }))
          expect(dispatchMock).toHaveBeenCalledTimes(1)
        })

        test('calls the "updateEntity" action', async () => {
          const entity = testModelFactory.build({ id: 1 })
          await action({ dispatch: dispatchMock }, entity)
          expect(dispatchMock).toHaveBeenCalledWith('updateEntity', entity)
        })
      })

      describe('when entity does not exist', () => {
        test('creates the entity', async () => {
          await action({ dispatch: dispatchMock }, testModelFactory.build({ id: 1 }))
          expect(dispatchMock).toHaveBeenCalledTimes(1)
        })

        test('calls the "createEntity" action', async () => {
          const entity = testModelFactory.build({ id: null })
          await action({ dispatch: dispatchMock }, entity)
          expect(dispatchMock).toHaveBeenCalledWith('createEntity', entity)
        })
      })
    })

    describe('destroyEntity', () => {
      beforeEach(() => {
        TestModel.destroy = jest.fn(() => new Promise(resolve => resolve()))
        action = Base.actions.destroyEntity(TestModel, 'deleteEntity')
      })

      afterEach(() => { TestModel.destroy.mockReset() })

      test('calls model.destroy', async () => {
        await action({ commit: commitMock }, 1)
        expect(TestModel.destroy).toHaveBeenCalledTimes(1)
      })

      test('mutates state with "deleteEntity"', async () => {
        await action({ commit: commitMock, getters: getterMocks }, 1)
        expect(commitMock).toHaveBeenCalledWith('deleteEntity', 1)
      })
    })
  })

  describe('getters', () => {
    let getter

    describe('entities', () => {
      beforeEach(() => {
        getter = Base.getters.entities(STORE_KEY, 'name')
      })
      test('returns a list', () => {
        expect(getter(stateMock)).toHaveLength(3)
      })
      test('list is ordered by name', () => {
        const names = getter(stateMock).map(v => v.name)
        const expected = [
          'another one',
          'here we are too',
          'something here',
        ]
        expect(names).toStrictEqual(expected)
      })
    })

    describe('entity', () => {
      beforeEach(() => {
        getter = Base.getters.entity(STORE_KEY)
      })

      describe('when entity exists', () => {
        test('returns the entity', () => {
          const entity = stateMock[STORE_KEY][0]
          expect(getter(stateMock)(entity.id)).toStrictEqual(entity)
        })
      })

      describe('when entity does not exists', () => {
        test('returns an undefined value', () => {
          expect(getter(stateMock)(0)).toBeUndefined()
        })
      })
    })

    describe('search', () => {
      beforeEach(() => {
        getter = Base.getters.search(STORE_KEY, 'name')
      })

      test('returns a list', () => {
        expect(getter(stateMock)('here')).toHaveLength(2)
      })

      test('list is ordered by name', () => {
        const names = getter(stateMock)('here').map(v => v.name)
        const expected = [
          'here we are too',
          'something here',
        ]
        expect(names).toStrictEqual(expected)
      })
    })
  })
})
