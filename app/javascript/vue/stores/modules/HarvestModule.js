import Base from './Base'
import Harvest from '../../classes/models/Harvest'

export default {
  state: {
    /** @type {Harvest[]} */
    harvests: [],
  },
  mutations: {
    resetHarvests: Base.mutations.resetState('harvests'),
    setHarvest: Base.mutations.setEntity('harvests'),
    deleteHarvest: Base.mutations.deleteEntity('harvests'),
  },
  actions: {
    loadMapHarvests ({ commit }, mapId) {
      return Harvest.indexForMap(mapId)
        .then(items => { items.forEach(item => { commit('setHarvest', item) }) })
    },
    loadElementHarvests ({ commit }, elementId) {
      return Harvest.indexForElement(elementId)
        .then(items => { items.forEach(item => { commit('setHarvest', item) }) })
    },
    loadHarvest: Base.actions.loadEntity(Harvest, 'setHarvest'),
    createHarvest: Base.actions.createEntity(Harvest, 'setHarvest', 'harvest'),
    updateHarvest: Base.actions.updateEntity(Harvest, 'setHarvest', 'harvest'),
    saveHarvest: Base.actions.saveEntity('createHarvest', 'updateHarvest'),
    destroyHarvest: Base.actions.destroyEntity(Harvest, 'deleteHarvest'),
  },
  getters: {
    harvests: Base.getters.entities('harvests', 'quantity'),
    harvestsForElement: state => elementId => state.filter(e => e.elementId === elementId)
      .sort((a, b) => a.quantity - b.quantity),
    harvest: Base.getters.entity('harvests'),
  },
}
