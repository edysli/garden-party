import Base from './Base'
import Observation from '../../classes/models/Observation'

export default {
  state: {
    /** @type {Observation[]} */
    observations: [],
  },
  mutations: {
    resetObservations: Base.mutations.resetState('observations'),
    setObservation: Base.mutations.setEntity('observations'),
    deleteObservation: Base.mutations.deleteEntity('observations'),
  },
  actions: {
    loadObservations: Base.actions.loadEntities(Observation, 'setObservation'),
    loadObservationsBySubject ({ commit }, subject) {
      return Observation.getObservationsForSubject(subject.className, subject.id)
        .then((observations) => {
          observations.forEach(observation => { commit('setObservation', observation) })
        })
    },
    loadObservation: Base.actions.loadEntity(Observation, 'setObservation'),
    createObservation: Base.actions.createEntity(Observation, 'setObservation', 'observation'),
    updateObservation: Base.actions.updateEntity(Observation, 'setObservation', 'observation'),
    saveObservation: Base.actions.saveEntity('createObservation', 'updateObservation'),
    destroyObservation: Base.actions.destroyEntity(Observation, 'deleteObservation'),
  },
  getters: {
    observation: Base.getters.entity('observations'),
    observations: Base.getters.entities('observations', 'madeAt'),
    observationsBySubject: state => ({ subjectId, subjectType }) => state.observations.filter(a => a.subjectId === subjectId && a.subjectType === subjectType)
      .sort((a, b) => b.madeAt - a.madeAt),
    searchObservations: Base.getters.search('observations', 'title'),
    observationSubject: (state, getters) => id => {
      const observation = getters.observation(id)
      if (!observation) return null

      switch (observation.subjectType) {
        case 'Element':
          return getters.element(observation.subjectId)
        case 'Map':
          return getters.map(observation.subjectId)
        case 'Patch':
          return getters.patch(observation.subjectId)
        case 'Path':
          return getters.path(observation.subjectId)
        default:
          return null
      }
    },
  },
}
