import Base from './Base'
import User from '../../classes/models/User'

export default {
  state: {
    /** @type {User[]} */
    users: [],
  },
  mutations: {
    setUser: Base.mutations.setEntity('users'),
    deleteUser: Base.mutations.deleteEntity('users'),
  },
  actions: {
    loadUser: Base.actions.loadEntity(User, 'setUser'),
    findUsers: (_context, payload) => User.search(payload),
  },
  getters: {
    users: Base.getters.entities('users', 'username'),
    user: Base.getters.entity('users'),
  },
}
