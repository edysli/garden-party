import Base from './Base'
import Genus from '../../classes/models/Genus'

export default {
  state: {
    /** @type {Genus[]} */
    genera: [],
  },
  mutations: {
    resetGenera: Base.mutations.resetState('genera'),
    setGenus: Base.mutations.setEntity('genera'),
    deleteGenus: Base.mutations.deleteEntity('genera'),
  },
  actions: {
    loadGenera: Base.actions.loadEntities(Genus, 'setGenus'),
    loadGenus: Base.actions.loadEntity(Genus, 'setGenus'),
    createGenus: Base.actions.createEntity(Genus, 'setGenus', 'genus'),
    updateGenus: Base.actions.updateEntity(Genus, 'setGenus', 'genus'),
    saveGenus: Base.actions.saveEntity('createGenus', 'updateGenus'),
    destroyGenus: Base.actions.destroyEntity(Genus, 'deleteGenus'),
  },
  getters: {
    genera: Base.getters.entities('genera', 'name'),
    genus: Base.getters.entity('genera'),
    genusSearch: Base.getters.search('genera', 'name'),
    generaByFamily: (state) => (familyId) => {
      const list = []

      // Using "for" instead of ".filter" for performance
      for (let i = 0, total = state.genera.length; i < total; i++) {
        if (state.genera[i].familyId === familyId) list.push(state.genera[i])
      }

      return list
        .sort((a, b) => a.name.localeCompare(b.name))
    },
  },
}
