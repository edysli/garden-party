import Base from './Base'
import Family from '../../classes/models/Family'

export default {
  state: {
    /** @type {Family[]} */
    families: [],
  },
  mutations: {
    resetFamilies: Base.mutations.resetState('families'),
    setFamily: Base.mutations.setEntity('families'),
    deleteFamily: Base.mutations.deleteEntity('families'),
  },
  actions: {
    loadFamilies: Base.actions.loadEntities(Family, 'setFamily'),
    loadFamily: Base.actions.loadEntity(Family, 'setFamily'),
    createFamily: Base.actions.createEntity(Family, 'setFamily', 'family'),
    updateFamily: Base.actions.updateEntity(Family, 'setFamily', 'family'),
    saveFamily: Base.actions.saveEntity('createFamily', 'updateFamily'),
    destroyFamily: Base.actions.destroyEntity(Family, 'deleteFamily'),
  },
  getters: {
    families: Base.getters.entities('families', 'name'),
    family: Base.getters.entity('families'),
    familySearch: Base.getters.search('families', 'name'),
  },
}
