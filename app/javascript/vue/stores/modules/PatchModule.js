import Base from './Base'
import Patch from '../../classes/models/Patch'
import EventBus from '../../tools/EventBus'

export default {
  state: {
    /** @type {Patch[]} */
    patches: [],
  },
  mutations: {
    resetPatches: Base.mutations.resetState('patches'),
    setPatch: Base.mutations.setEntity('patches'),
    deletePatch: Base.mutations.deleteEntity('patches'),
  },
  actions: {
    loadPatches: Base.actions.loadEntities(Patch, 'setPatch'),
    loadPatch: Base.actions.loadEntity(Patch, 'setPatch'),
    updatePatch: ({ commit, getters }, payload) => Patch.update(payload)
      .then(patch => {
        const oldLayer = getters.patch(patch.id).layerId

        commit('setPatch', patch)
        const updatedPatch = getters.patch(patch.id)

        if (oldLayer !== updatedPatch.layerId) {
          EventBus.emit('change-patch-layer', { path: updatedPatch, fromPathId: oldLayer })
        }

        // Return updated instance
        return Promise.resolve(updatedPatch)
      }),
    savePatch: Base.actions.saveEntity('createPatch', 'updatePatch'),
    destroyPatch: ({ commit, getters }, id) => Patch.destroy(id)
      .then(() => {
        const patch = getters.patch(id)

        commit('deletePatch', id)
        EventBus.emit('destroyed-patch', patch)

        return Promise.resolve()
      }),
    createPatch ({ commit, dispatch, getters }, { payload, feature }) {
      return Patch.create(payload)
        .then(item => {
          return dispatch('loadPatchElements', item.id)
            // Add and return the new patch once its elements are loaded
            .then(() => {
              commit('setPatch', item)
              // Return new instance
              const patch = getters.patch(item.id)
              patch.setFeature(feature)
              return Promise.resolve(patch)
            })
        })
    },
  },
  getters: {
    patch: Base.getters.entity('patches'),
    patches: state => state.patches.sort((a, b) => {
      if (a.name && b.name) return a.name.localeCompare(b.name)
      if (a.name && !b.name) return -1
      if (!a.name && b.name) return 1

      return a.createdAt.valueOf() - b.createdAt.valueOf()
    }),
    patchesByIds: state => ids => state.patches.filter(p => ids.indexOf(p.id) > -1),
    patchesInLayer: state => layerId => state.patches.filter(p => p.layerId === layerId),
  },
}
