import Base from './Base'
import ResourceInteractionsGroup from '../../classes/models/ResourceInteractionsGroup'

export default {
  state: {
    /** @type {ResourceInteractionsGroup[]} */
    resourceInteractionsGroups: [],
  },
  mutations: {
    resetResourceInteractionsGroups: Base.mutations.resetState('resourceInteractionsGroups'),
    setResourceInteractionsGroup: Base.mutations.setEntity('resourceInteractionsGroups'),
    deleteResourceInteractionsGroup: Base.mutations.deleteEntity('resourceInteractionsGroups'),
  },
  actions: {
    loadResourceInteractionsGroups: Base.actions.loadEntities(ResourceInteractionsGroup, 'setResourceInteractionsGroup', 'resetResourceInteractionsGroups'),
    loadResourceInteractionsGroup: Base.actions.loadEntity(ResourceInteractionsGroup, 'setResourceInteractionsGroup'),
    createResourceInteractionsGroup: Base.actions.createEntity(ResourceInteractionsGroup, 'setResourceInteractionsGroup', 'resourceInteractionsGroup'),
    updateResourceInteractionsGroup: Base.actions.updateEntity(ResourceInteractionsGroup, 'setResourceInteractionsGroup', 'resourceInteractionsGroup'),
    saveResourceInteractionsGroup: Base.actions.saveEntity('createResourceInteractionsGroup', 'updateResourceInteractionsGroup'),
    destroyResourceInteractionsGroup: Base.actions.destroyEntity(ResourceInteractionsGroup, 'deleteResourceInteractionsGroup'),
  },
  getters: {
    resourceInteractionsGroups: Base.getters.entities('resourceInteractionsGroups', 'name'),
    resourceInteractionsGroup: Base.getters.entity('resourceInteractionsGroups'),
    searchResourceInteractionsGroups: Base.getters.search('resourceInteractionsGroups', 'name'),
  },
}
