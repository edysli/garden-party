import i18n from '../app_helpers/i18n'
import Toaster from './Toaster'

/**
 * @typedef          ErrorObject
 * @param {string} error - The error message
 */

const ApiErrors = {
  // i18n-tasks-use t('js.api.unauthorized')
  401: 'js.api.unauthorized',
  // i18n-tasks-use t('js.api.forbidden')
  403: 'js.api.forbidden',
  // i18n-tasks-use t('js.api.page_not_found')
  404: 'js.api.page_not_found',
  // i18n-tasks-use t('js.api.unprocessable_entity')
  422: 'js.api.unprocessable_entity',
  // i18n-tasks-use t('js.api.internal_server_error')
  500: 'js.api.internal_server_error',
}

/**
 * @param   {XMLHttpRequest}       request - The request
 * @param   {object}               context - Context data for errors translations
 * @returns {(object|ErrorObject)}         - Response or error
 */
function handleAPIError (request, context) {
  const status = request.status.toString()
  if (Object.keys(ApiErrors).lastIndexOf(status) > -1) Toaster.error(i18n.global.t(ApiErrors[status], context))
  // i18n-tasks-use t('js.api.unknown_error')
  else Toaster.error(i18n.global.t('js.api.unknown_error', { code: status }))

  try {
    return JSON.parse(request.response)
  } catch (SyntaxError) {
    return { error: 'Unprocessable response (not JSON)' }
  }
}

/**
 * Gets CSRF token from page "meta" headers
 *
 * @returns {string|null} The token or null
 */
function extractCSRF () {
  const token = document.getElementsByName('csrf-token')
  if (token.length > 0) return token[0].content

  return null
}

/**
 * Makes an Api call and returns a promise with data
 *
 * @param   {string}          method - Request verb
 * @param   {string}          url    - Target URL
 * @param   {object}          [data] - Payload
 * @returns {Promise<object>}        Response content, or an error
 */
export default function (method, url, data = {}) {
  if (!data) data = {} // When a null value is passed
  const request = new window.XMLHttpRequest()

  request.open(method.toUpperCase(), url, true)
  request.setRequestHeader('Accept', 'application/json')
  if ((typeof data.append) !== 'function') {
    request.setRequestHeader('Content-Type', 'application/json')
    data = JSON.stringify(data)
  }

  const token = extractCSRF()
  if (token) {
    request.setRequestHeader('X-CSRF-TOKEN', token)
  }

  return new Promise((resolve, reject) => {
    request.onload = function () {
      if (request.status >= 200 && request.status < 400) {
        resolve(request.response ? JSON.parse(request.response) : null)
      } else {
        return reject(handleAPIError(request, { url }))
      }
    }
    // FIXME: Handle connectivity issues
    request.onerror = function () {
      // i18n-tasks-use t('js.api.unreachable_server')
      Toaster.error(i18n.global.t('js.api.unreachable_server'))
      return reject(request)
    }

    request.send(data)
  })
}
