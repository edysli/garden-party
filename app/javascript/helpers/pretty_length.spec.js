import prettyLength from './pretty_length'

describe('prettyLength', () => {
  test('adds the appropriate unit', () => {
    expect(prettyLength(1)).toBe('1m')
    expect(prettyLength(0.1)).toBe('10cm')
    expect(prettyLength(0.01)).toBe('1cm')
    expect(prettyLength(0.001)).toBe('1mm')
  })
})
