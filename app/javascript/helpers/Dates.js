/**
 * Checks if a date is the same day as another
 *
 * @param   {Date}    firstDate  - First date
 * @param   {Date}    secondDate - Second date
 * @returns {boolean}            True if dates are in the same day, false otherwise
 */
export function isSameDay (firstDate, secondDate = new Date()) {
  return (firstDate.getFullYear() === secondDate.getFullYear())
    && (firstDate.getMonth() === secondDate.getMonth())
    && (firstDate.getDate() === secondDate.getDate())
}

/**
 * Checks if a date is the same month as another
 *
 * @param   {Date}    firstDate  - First date
 * @param   {Date}    secondDate - Second date
 * @returns {boolean}            True if dates are in the same month, false otherwise
 */
export function isSameMonth (firstDate, secondDate = new Date()) {
  return (firstDate.getFullYear() === secondDate.getFullYear())
    && (firstDate.getMonth() === secondDate.getMonth())
}

/**
 * Checks if a date is after another (day-based comparison)
 *
 * @param   {Date}    date      - Date to check
 * @param   {Date}    reference - Reference date
 * @returns {boolean}           True if date is after reference, false otherwise
 */
export function isOver (date, reference = new Date()) {
  // not today and date before reference
  return !isSameDay(reference, date) && date < reference
}

/** @see isOver */
export const isAfter = isOver

/**
 * Checks if a date is before another (day-based comparison)
 *
 * @param   {Date}    date      - Date to check
 * @param   {Date}    reference - Reference date
 * @returns {boolean}           True if date is before reference, false otherwise
 */
export function isBefore (date, reference = new Date()) {
  // not today and date before reference
  return !isSameDay(reference, date) && date > reference
}

/**
 * Transforms a Date to a string usable as input field value
 *
 * @param   {Date}    date - Date to transform
 * @param   {boolean} time - Whether to include time
 * @returns {string}       Representation to use with input fields
 */
export function dateToInputString (date, time = false) {
  if (!date) return ''

  const day = `0${date.getDate()}`.slice(-2)
  const month = `0${date.getMonth() + 1}`.slice(-2)
  let string = `${date.getFullYear()}-${month}-${day}`
  if (time) {
    const hours = `0${date.getHours()}`.slice(-2)
    const minutes = `0${date.getMinutes()}`.slice(-2)
    string += `T${hours}:${minutes}`
  }
  return string
}

/**
 * @param   {Date} date - Input date
 * @returns {Date}      - Cloned date, beginning of day
 */
export function beginningOfDay (date) {
  const newDate = new Date(date)
  newDate.setHours(0)
  newDate.setMinutes(0)
  newDate.setSeconds(0)
  newDate.setMilliseconds(0)

  return newDate
}
