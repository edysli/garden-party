import * as Units from './Units'

describe('Units', () => {
  describe('converters', () => {
    describe('"ton" converter', () => {
      it('returns the right value in grams', () => {
        expect(Units.converters.ton.convert(1)).toBe(1000000)
      })
    })
    describe('"kilogram" converter', () => {
      it('returns the right value in grams', () => {
        expect(Units.converters.kilogram.convert(1)).toBe(1000)
      })
    })
    describe('"gram" converter', () => {
      it('returns the right value in grams', () => {
        expect(Units.converters.gram.convert(1)).toBe(1)
      })
    })
    describe('"piece" converter', () => {
      it('returns the right value in pieces', () => {
        expect(Units.converters.piece.convert(1)).toBe(1)
      })
    })
    describe('"centilitre" converter', () => {
      it('returns the right value in litres', () => {
        expect(Units.converters.centilitre.convert(1)).toBe(0.01)
      })
    })
    describe('"millilitre" converter', () => {
      it('returns the right value in litres', () => {
        expect(Units.converters.millilitre.convert(1)).toBe(0.001)
      })
    })
    describe('"litre" converter', () => {
      it('returns the right value in litres', () => {
        expect(Units.converters.litre.convert(1)).toBe(1)
      })
    })
    describe('"cubic_meter" converter', () => {
      it('returns the right value in litres', () => {
        expect(Units.converters.cubic_meter.convert(1)).toBe(1000)
      })
    })
  })

  describe('convertLiquid()', () => {
    it('converts to cubic meters', () => {
      expect(Units.convertVolume(1200)).toStrictEqual({ value: 1.2, unit: 'cubic_meter' })
    })
    it('converts to centilitres', () => {
      expect(Units.convertVolume(0.9)).toStrictEqual({ value: 90, unit: 'centilitre' })
      expect(Units.convertVolume(0.09)).toStrictEqual({ value: 9, unit: 'centilitre' })
    })
    it('converts to millilitres', () => {
      expect(Units.convertVolume(0.009)).toStrictEqual({ value: 9, unit: 'millilitre' })
    })
    it('converts to litres', () => {
      expect(Units.convertVolume(900)).toStrictEqual({ value: 900, unit: 'litre' })
    })
  })

  describe('convertWeight()', () => {
    it('converts to ton', () => {
      expect(Units.convertWeight(90000000)).toStrictEqual({ value: 90, unit: 'ton' })
      expect(Units.convertWeight(1000000)).toStrictEqual({ value: 1, unit: 'ton' })
      expect(Units.convertWeight(-1000000)).toStrictEqual({ value: -1, unit: 'ton' })
    })
    it('converts to kilogram', () => {
      expect(Units.convertWeight(9000)).toStrictEqual({ value: 9, unit: 'kilogram' })
      expect(Units.convertWeight(1000)).toStrictEqual({ value: 1, unit: 'kilogram' })
      expect(Units.convertWeight(-1000)).toStrictEqual({ value: -1, unit: 'kilogram' })
    })
    it('converts to gram', () => {
      expect(Units.convertWeight(900)).toStrictEqual({ value: 900, unit: 'gram' })
      expect(Units.convertWeight(0.9)).toStrictEqual({ value: 0.9, unit: 'gram' })
      expect(Units.convertWeight(-900)).toStrictEqual({ value: -900, unit: 'gram' })
    })
  })

  describe('convertUnit()', () => {
    it('returns the right unit with new value', () => {
      expect(Units.convertUnit(9000, 'litre')).toStrictEqual({ value: 9, unit: 'cubic_meter' })
      expect(Units.convertUnit(9000, 'gram')).toStrictEqual({ value: 9, unit: 'kilogram' })
      expect(Units.convertUnit(3, 'piece')).toStrictEqual({ value: 3, unit: 'piece' })
    })
  })
})
