import * as Dates from './Dates'

const today = new Date()
today.setHours(12, 0, 0, 0)
const someTimeToday = new Date(today)
someTimeToday.setHours(10)

const yesterday = new Date(today)
yesterday.setDate(today.getDate() - 1)
yesterday.setHours(12, 0, 0, 0)
const someTimeYesterday = new Date(yesterday)
someTimeYesterday.setHours(10)

const tomorrow = new Date(today)
tomorrow.setDate(today.getDate() + 1)
tomorrow.setHours(12, 0, 0, 0)
const someTimeTomorrow = new Date(tomorrow)
someTimeTomorrow.setHours(10)

describe('Dates', () => {
  describe('dateToInputString()', () => {
    let date
    beforeEach(() => {
      // As returned date are local dates, we need a little hack to do as if we are
      // in UTC time for input date
      date = new Date('2021-10-28 15:41:08 UTC"')
      date.setMinutes(date.getMinutes() + date.getTimezoneOffset())
    })

    it('returns a well formatted date', () => {
      expect(Dates.dateToInputString(date)).toBe('2021-10-28')
    })

    it('handle date and time output', () => {
      expect(Dates.dateToInputString(date, true)).toBe('2021-10-28T15:41')
    })
  })

  /**
   * Runs the tests for isSameDay, as they are the same in every context
   *
   * @param {Array[]} table - List of Dates tuples and expected result
   */
  function isSameDayTest (table) {
    test.each(table)('isSameDay(%s, %s) is %s', (firstDate, secondDate, expected) => {
      expect(Dates.isSameDay(firstDate, secondDate)).toBe(expected)
    })
  }

  describe('isSameDay()', () => {
    describe('when date is in the past', () => {
      isSameDayTest([
        [yesterday, undefined, false],
        [yesterday, today, false],
        [yesterday, someTimeYesterday, true],
      ])
    })

    describe('when date is same day', () => {
      isSameDayTest([
        [today, undefined, true],
        [today, someTimeToday, true],
      ])
    })

    describe('when date is in the future', () => {
      isSameDayTest([
        [tomorrow, undefined, false],
        [tomorrow, today, false],
        [tomorrow, someTimeTomorrow, true],
      ])
    })
  })

  /**
   * Runs the tests for isSameDay, as they are the same in every context
   *
   * @param {Array[]} table - List of Dates tuples and expected result
   */
  function isSameMonthTest (table) {
    test.each(table)('isSameMonth(%s, %s) is %s', (firstDate, secondDate, expected) => {
      expect(Dates.isSameMonth(firstDate, secondDate)).toBe(expected)
    })
  }

  describe('isSameMonth()', () => {
    const today = new Date()
    const someTimeThisMonth = new Date(today)
    // Select another day in month
    someTimeThisMonth.setDate(someTimeThisMonth.getDate() === 1 ? 12 : 1)

    const previousMonth = new Date(today)
    // Previous month is 2 month back to avoid issues on march 29, 30, 31:
    // these days minus one month ends in march.
    previousMonth.setMonth(today.getMonth() - 2)
    const someTimePreviousMonth = new Date(previousMonth)
    // Select another day in month
    someTimePreviousMonth.setDate(someTimePreviousMonth.getDate() === 1 ? 12 : 1)

    const nextMonth = new Date(today)
    nextMonth.setMonth(today.getMonth() + 1)
    const someTimeNextMonth = new Date(nextMonth)
    // Select another day in month
    someTimeNextMonth.setDate(someTimeNextMonth.getDate() === 1 ? 12 : 1)

    describe('when month is in the past', () => {
      isSameMonthTest([
        [previousMonth, undefined, false],
        [previousMonth, today, false],
        [previousMonth, someTimePreviousMonth, true],
      ])
    })

    describe('when month is the same', () => {
      isSameMonthTest([
        [today, undefined, true],
        [today, someTimeThisMonth, true],
      ])
    })

    describe('when month is in the future', () => {
      isSameMonthTest([
        [nextMonth, undefined, false],
        [nextMonth, today, false],
        [nextMonth, someTimeNextMonth, true],
      ])
    })
  })

  /**
   * Checks if one date is before another
   *
   * @param {Array[]} table - List of Dates tuples and expected result
   */
  function isOverTest (table) {
    test.each(table)('isOver(%s, %s) is %s', (date, reference, expected) => {
      expect(Dates.isOver(date, reference)).toBe(expected)
    })
  }

  describe('isOver()', () => {
    isOverTest([
      [yesterday, undefined, true],
      [yesterday, today, true],
      [tomorrow, undefined, false],
      [tomorrow, today, false],
      [today, tomorrow, true],
    ])
  })

  /**
   * Checks if one date is after another
   *
   * @param {Array[]} table - List of Dates tuples and expected result
   */
  function isBeforeTest (table) {
    test.each(table)('isBefore(%s, %s) is %s', (date, reference, expected) => {
      expect(Dates.isBefore(date, reference)).toBe(expected)
    })
  }

  describe('isBefore()', () => {
    isBeforeTest([
      [yesterday, undefined, false],
      [yesterday, today, false],
      [tomorrow, undefined, true],
      [tomorrow, today, true],
      [today, tomorrow, false],
    ])
  })
})
