/**
 * Makes a camelCased string an_underscored_one
 *
 * @param   {string} string      String in camelcase
 * @param   {string} [separator] Separator for the new string.
 * @returns {string}             The underscored string
 */
export function underscore (string, separator = '_') {
  return string
    .replace(/([a-z\d])([A-Z])/g, '$1' + separator + '$2')
    .replace(/([A-Z]+)([A-Z][a-z\d]+)/g, '$1' + separator + '$2')
    .toLowerCase()
}

/**
 * Upcases the first letter of a string
 *
 * @param   {string} string - The string to upcase
 * @param   {string} locale - Optional target locale
 * @returns {string}        - String with first letter upcased
 */
export function upCaseFirst (string, locale = I18n.locale) {
  const nibbles = string.split('')
  const first = nibbles.shift()
  return [first.toLocaleUpperCase(locale), ...nibbles].join('')
}
