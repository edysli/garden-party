const units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

/**
 * Returns a human readable size value
 *
 * @param   {number}  bytes       - The amount of bytes
 * @param   {number}  [precision] - Desired precision
 * @param   {boolean} [addSpace]  - Adds a space between number and units when true
 * @returns {string}              Something like "4.31 MB"
 */
export default function (bytes, precision = 2, addSpace = true) {
  if (Math.abs(bytes) < 1024) return bytes + (addSpace ? ' ' : '') + units[0]

  const exponent = Math.min(Math.floor(Math.log2(bytes < 0 ? -bytes : bytes) / 10), units.length - 1)
  const n = ((bytes < 0 ? -bytes : bytes) / 2 ** (exponent * 10)).toFixed(precision)
  return (bytes < 0 ? '-' : '') + n + (addSpace ? ' ' : '') + units[exponent]
}
