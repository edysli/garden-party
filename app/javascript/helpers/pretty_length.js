/**
 * Returns a human readable length value
 *
 * @param   {number} length - The length in meters
 * @returns {string}        Something like "4.31m"
 */
export default function (length) {
  if (length >= 1) return `${length}m`
  if (length * 100 >= 1) return `${length * 100}cm`

  // Fallback
  return `${length * 1000}mm`
}
