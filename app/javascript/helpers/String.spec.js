import { underscore, upCaseFirst } from './String'

describe('string methods', () => {
  describe('.underscore', () => {
    it('converts CamelCased strings to underscored_ones', () => {
      expect(underscore('CamelCased')).toBe('camel_cased')
    })
  })
  describe('.upCaseFirst', () => {
    it('upcases string with locale support', () => {
      expect(upCaseFirst('italya', 'tr')).toBe('İtalya')
    })
  })
})
