/**
 * Creates an array of given length filled with given value
 *
 * @param   {number} length - Array length
 * @param   {any}    value  - Default value
 * @returns {any[]}         The new array
 */
export function filledArray (length, value) {
  return Array(length).fill(value, 0, length)
}

/**
 * Picks a random entry from given array
 *
 * @param   {Array} array - Source list
 * @returns {*}           - Random element
 */
export function randomEntry (array) {
  return array[Math.floor(Math.random() * array.length)]
}
