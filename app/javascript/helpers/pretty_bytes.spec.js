import prettyBytes from './pretty_bytes'

describe('prettyBytes', () => {
  test('adds units to the value', () => {
    expect(prettyBytes(1)).toBe('1 B')
    expect(prettyBytes(2 ** 10)).toBe('1.00 KB')
    expect(prettyBytes(1.5 * 2 ** 10)).toBe('1.50 KB')
    expect(prettyBytes(2 ** 20)).toBe('1.00 MB')
    expect(prettyBytes(2 ** 30)).toBe('1.00 GB')
    expect(prettyBytes(2 ** 40)).toBe('1.00 TB')
    expect(prettyBytes(2 ** 50)).toBe('1.00 PB')
  })
  test('rounds at given precision', () => {
    expect(prettyBytes(1, 2)).toBe('1 B')
    expect(prettyBytes(1.567 * 1024, 2)).toBe('1.57 KB')
    expect(prettyBytes(12.567 * 1024, 2)).toBe('12.57 KB')
    expect(prettyBytes(12.567 * 1024, 3)).toBe('12.567 KB')
    expect(prettyBytes(12.567 * 1024, 4)).toBe('12.5670 KB')
  })
  test('can have units glued to value', () => {
    expect(prettyBytes(1, 2, false)).toBe('1B')
    expect(prettyBytes(2 ** 10, 2, false)).toBe('1.00KB')
  })
})
