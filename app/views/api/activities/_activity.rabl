attributes :id,
           :action,
           :happened_at,
           :subject_type,
           :subject_id,
           :subject_name,
           :map_id,
           :user_id,
           :username,
           :created_at, :updated_at

node :data do |activity|
  next activity.data if activity.data.is_a? Hash

  JSON.parse activity.data
end
