attributes :id,
           :user_id,
           :map_id,
           :accepted_at,
           :created_at,
           :updated_at

code :map do |team_mate|
  { name: team_mate.map.name }
end

code :user do |team_mate|
  partial 'api/users/_user', object: team_mate.user
end
