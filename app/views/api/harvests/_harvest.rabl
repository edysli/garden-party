attributes :id,
           :quantity,
           :unit,
           :element_id,
           :user_id,
           :harvested_at,
           :created_at,
           :updated_at
