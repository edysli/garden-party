object @task

attributes :subject_type, :subject_id

node(:subject) do |task|
  next nil unless task.action && task.subject_type

  case task.subject_type
  when 'Element'
    partial 'api/elements/_element', object: task.subject
  when 'Map'
    partial 'api/maps/_map', object: task.subject
  when 'Patch'
    partial 'api/patches/_patch', object: task.subject
  when 'Path'
    partial 'api/paths/_path', object: task.subject
  end
end
