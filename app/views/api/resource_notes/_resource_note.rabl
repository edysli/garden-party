attributes :id,
           :content,
           :visible,
           :resource_id,
           :created_at,
           :updated_at

node(:username) do |r|
  r.user.username
end

node(:user_id) { |r| r.user_id if current_user.id == r.user_id }
