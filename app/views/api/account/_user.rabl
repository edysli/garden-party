attributes :id,
           :role,
           :email,
           :username,
           :preferences,
           :created_at,
           :updated_at
