attributes :id,
           :content,
           :type,
           :action,
           :content,
           :subject_type,
           :subject_id,
           :recipient_id,
           :sender_id,
           :archived_at,
           :created_at,
           :updated_at

code :subject do |notification|
  subject = notification.subject

  case notification.subject_type
  when 'Map'
    {
      name:               subject.name,
      publicly_available: subject.publicly_available,
    }
  when 'TeamMate'
    partial 'api/team_mates/_team_mate', object: subject
  end
end

code :sender do |notification|
  partial 'api/users/_user', object: notification.sender if notification.sender_id
end
