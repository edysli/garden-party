attributes :id,
           :title,
           :content,
           :made_at,
           :user_id,
           :subject_id,
           :subject_type,
           :created_at,
           :updated_at

node(:pictures) do |observation|
  list = []
  observation.pictures.each do |picture|
    list << {
      source:    picture_api_observation_url(observation, picture),
      thumbnail: picture_thumbnail_api_observation_url(observation, picture),
    }
  end

  list
end
