class PreferencesValidator < ActiveModel::EachValidator
  THEMES = [
    nil,
    'dark',
    'light',
  ].freeze

  def validate_each(record, attribute, value)
    return true if value.is_a?(Hash) && value.empty?

    validate_theme(record, attribute, value)
    validate_map(record, attribute, value)
  end

  private

  def validate_theme(record, _attribute, value)
    record.errors.add :theme, I18n.t('activerecord.errors.preferences.is_invalid_theme') unless THEMES.include? value['theme']
  end

  def validate_map(record, _attribute, value)
    map_id = value['map']
    return if map_id.blank?

    record.errors.add :map, I18n.t('activerecord.errors.preferences.is_invalid_map') unless Map.find_by(id: map_id.to_i)
  end
end
