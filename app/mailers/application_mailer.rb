class ApplicationMailer < ActionMailer::Base
  default from: Rails.configuration.garden_party.default_email_options[:from]
  layout 'mailer'

  private

  def subject(scope, string)
    subject_prefix = "[#{Rails.configuration.garden_party.instance[:name]}][#{scope}]"
    "#{subject_prefix} #{string}"
  end
end
