module VersionsHelper
  def version_committer(version)
    return '-' unless version.whodunnit

    user = User.find(version.whodunnit)
    return '?' unless user

    user.username
  end

  def version_diff(version)
    output = ''
    # FIXME: Change PaperTrail serializer to use JSON as we don't really care of the types
    #        of saved values.
    changes = YAML.load version.object_changes # rubocop:disable Security/YAMLLoad

    changes.each_pair do |key, values|
      diff = Diffy::Diff.new(*values, include_plus_and_minus_in_html: true, context: 2, ignore_crlf: true).to_s(:html)
      output += tag.h2 key
      output += diff.html_safe # rubocop:disable Rails/OutputSafety
    end

    output.html_safe # rubocop:disable Rails/OutputSafety
  end
end
