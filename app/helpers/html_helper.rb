module HtmlHelper
  def missing_data_element(html_tag, value, url: nil, replacement: nil, &block)
    content = if value.present? && block
                yield
              elsif value.present?
                value
              else
                link = t('html_helper.missing_data_element.complete_it')
                link = link_to link, url, target: '_blank', rel: 'noopener' if url
                replacement || t('html_helper.missing_data_element.missing_data_html', link: link)
              end

    css_class = value.present? ? '' : '-text--warning'

    content_tag html_tag, content, class: css_class
  end

  def order_by_link(title, field, keep_filters: [])
    direction = params[:order_dir].presence == 'asc' ? 'desc' : 'asc'

    icon = if params[:order_by] == field
             direction == 'asc' ? ' ↑' : ' ↓'
           else
             ''
           end

    parameters = keep_and_merge_params keep_filters, order_by: field, order_dir: direction

    link_to "#{title}#{icon}",
            controller: params[:controller],
            action:     params[:action],
            params:     parameters
  end

  def filter_by_link(title, html_options = {}, filters: {}, keep: [], icon: true)
    keep += [:order_by, :order_dir]
    parameters = keep_and_merge_params keep, filters
    checked = true
    filters.each do |key, value|
      next if params[key] == value

      checked = false
      break
    end

    link_to({ controller: params[:controller], action: params[:action], params: parameters }, html_options) do
      next title unless icon

      tag.input(type: :radio, checked: checked) { title }
    end
  end

  def icon(name, size: nil, spin: false, title: nil)
    title = title ? "<title>#{title}</title>" : ''
    classes = 'gp-icon'
    classes += ' gp-icon--spin' if spin
    classes += " gp-icon--#{size}x" if size

    <<~HTML.html_safe # rubocop:disable Rails/OutputSafety
      <svg class="#{classes}" viewBox="0 0 24 24">
        #{title}
        <use xlink:href="#{asset_url 'icons.svg'}#icon-#{name}" />
      </svg>
    HTML
  end

  private

  def keep_and_merge_params(keep, filters)
    parameters = {}
    keep.each { |f| parameters[f] = params[f] if params[f] }

    parameters.merge filters
  end
end
