module TagsHelper
  # rubocop:disable Rails/OutputSafety
  # user data is escaped in "span" tag
  def compound_tag(name)
    out    = ''
    chunks = name.split('::')
    chunks.each do |chunk|
      out += tag.span chunk, class: 'gp-tag__segment'
    end

    tag.span out.html_safe, class: 'gp-tag'
  end
  # rubocop:enable Rails/OutputSafety

  # rubocop:disable Rails/OutputSafety
  # "compound_tag" is already safe
  def tags_list(taggable)
    out = ''
    taggable.tag_list.each do |tag|
      out += compound_tag tag
    end

    out.html_safe
  end
  # rubocop:enable Rails/OutputSafety

  def tags_for_select
    ActsAsTaggableOn::Tag.all.order(:name).pluck :name
  end
end
