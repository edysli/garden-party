module ChangelogHelper
  # Determines if there are unreleased changes by checking if the partial size is positive
  def unreleased_changes?
    size = Rails.root.join('app', 'views', 'app', "_unreleased_changes.#{I18n.locale}.html.haml").size?
    size && size > 1
  end

  def issue_entry(title, number: nil)
    link_entry :issue, title, number: number
  end

  def merge_request_entry(title, number: nil)
    link_entry :merge_request, title, number: number
  end

  # rubocop:disable Rails/OutputSafety
  def link_entry(type, title, number: nil)
    return title.html_safe unless number

    url_chunk = type == :issue ? '-/issues' : '-/merge_requests'

    link_to(title, "#{Rails.application.config.garden_party[:project][:codebase_url]}/#{url_chunk}/#{number}").html_safe
  end
  # rubocop:enable Rails/OutputSafety
end
