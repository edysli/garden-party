require 'garden_party/activities/task'

class Task < ApplicationRecord
  validates :name, presence: true
  validates :planned_for, presence: true, unless: :done_at
  validates :done_at, presence: true, unless: :planned_for
  validates :subject_type, inclusion: { in: %w[Element Patch Path Map] }
  validate :assignee_validity

  belongs_to :subject, polymorphic: true
  belongs_to :assignee, class_name: 'User', optional: true
  has_many :activities, as: :subject, dependent: :destroy

  scope :all_for_map, lambda { |map_id|
    where(subject_id: map_id, subject_type: 'Map')
      .or(where(subject_id: Element.kept_for_map(map_id).select(:id), subject_type: 'Element'))
      .or(where(subject_id: Patch.kept_for_map(map_id).select(:id), subject_type: 'Patch'))
      .or(where(subject_id: Path.kept_for_map(map_id).select(:id), subject_type: 'Path'))
  }
  scope :for_element, ->(subject_id) { where(subject_id: subject_id, subject_type: 'Element') }
  scope :for_map, ->(subject_id) { where(subject_id: subject_id, subject_type: 'Map') }
  scope :for_patch, ->(subject_id) { where(subject_id: subject_id, subject_type: 'Patch') }
  scope :for_path, ->(subject_id) { where(subject_id: subject_id, subject_type: 'Path') }
  scope :pending, -> { where(done_at: nil) }
  scope :overdue, -> { where('planned_for < ?', Time.zone.today) }
  scope :today, -> { where(planned_for: Time.zone.today) }
  scope :next_week, -> { where(planned_for: (Time.zone.tomorrow...Time.zone.tomorrow + 7.days)) }

  before_validation :set_assignee
  after_create { |task| GardenParty::Activities::Task.create_create_activity task }
  after_update :perform_action, unless: :skip_subject_hooks?
  after_update { |task| GardenParty::Activities::Task.create_update_activity task }
  after_destroy :perform_action, unless: :skip_subject_hooks?
  after_destroy { |task| GardenParty::Activities::Task.create_destroy_activity task }

  # Updates the task without executing subject-related hooks and avoid
  # updates loops.
  def update_without_subject_hooks(values)
    @skip_subject_hooks = true
    update! values
    @skip_subject_hooks = false
  end

  # Destroys the task without executing subject-related hooks and avoid
  # updates loops.
  def destroy_without_subject_hooks
    @skip_subject_hooks = true
    destroy
    @skip_subject_hooks = false
  end

  # Returns the last action performed on a pending task or a task that was
  # finished now
  #
  # @return [Symbol] last task-related change from model fields
  def last_significant_change
    return if done_at_previously_was.present?

    if !persisted? && id.present?
      :destroy
    elsif done_at_previously_changed? && done_at_previously_was.blank?
      :finish
    elsif planned_for_previously_changed?
      :update
    end
  end

  # Returns true when the last update included task finish
  def just_done?
    done_at_previously_was.blank? && done_at.present?
  end

  private

  def skip_subject_hooks?
    action.blank? || @skip_subject_hooks
  end

  def assignee_validity
    return unless assignee_id

    map_id = if subject_type == 'Map'
               subject.id
             else
               subject&.map&.id
             end
    return if TeamMate.find_by(user_id: assignee_id, map_id: map_id)

    errors.add(:assignee, I18n.t('activerecord.errors.not_in_team'))
  end

  # Defines assignee as current user or map owner if unset
  def set_assignee
    self.assignee_id ||= Current.user&.id
    return if assignee_id.present?

    # Fallback to map owner (subject must respond to "map")
    self.assignee_id = if subject_type == 'Map'
                         subject.user_id
                       else
                         subject.map.user_id
                       end
  end

  # Calls "task_callback" on the subject unless the task is already done
  def perform_action
    return if action.blank? || last_significant_change.blank?

    raise "#{subject_type} does not support task callbacks" unless subject.respond_to? :task_callback

    subject.task_callback self
  end
end
