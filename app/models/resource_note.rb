class ResourceNote < ApplicationRecord
  validates :content, presence: true

  belongs_to :user
  belongs_to :resource

  before_validation { |resource_description| resource_description.user ||= Current.user }

  scope :visible, -> { where(visible: true) }
end
