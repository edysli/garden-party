require 'garden_party/activities/observation'

class Observation < ApplicationRecord
  include Discard::Model

  validates :title, presence: true
  validates :made_at, presence: true
  validates :subject_type, inclusion: %w[Element Map Patch Path]
  validate :pictures_format

  belongs_to :user
  belongs_to :subject, polymorphic: true
  has_many_attached :pictures

  # All observation on a map, concerning a non discarded element, patch or path.
  scope :all_for_map, lambda { |map_id|
    undiscarded
      .where(subject_id: map_id, subject_type: 'Map')
      .or(where(subject_id: Element.kept_for_map(map_id).select(:id), subject_type: 'Element'))
      .or(where(subject_id: Patch.kept_for_map(map_id).select(:id), subject_type: 'Patch'))
      .or(where(subject_id: Path.kept_for_map(map_id).select(:id), subject_type: 'Path'))
  }
  scope :for_element, ->(subject_id) { where(subject_id: subject_id, subject_type: 'Element') }
  scope :for_map, ->(subject_id) { where(subject_id: subject_id, subject_type: 'Map') }
  scope :for_patch, ->(subject_id) { where(subject_id: subject_id, subject_type: 'Patch') }
  scope :for_path, ->(subject_id) { where(subject_id: subject_id, subject_type: 'Path') }

  before_validation { |observation| observation.user ||= Current.user }
  before_validation :set_made_at
  before_save :resize_pictures

  after_create { |observation| GardenParty::Activities::Observation.create_create_activity observation }
  after_update { |observation| GardenParty::Activities::Observation.create_update_activity observation }
  after_discard { |observation| GardenParty::Activities::Observation.create_destroy_activity observation }

  private

  def pictures_format
    mimes_types = Rails.configuration.garden_party.observations[:allowed_pictures_mime_types]
    pictures.each do |picture|
      next if picture.persisted?

      errors.add(:picture, I18n.t('activerecord.errors.bad_image_format', formats: mimes_types.join(', '))) unless mimes_types.include? picture.blob.content_type
    end
  end

  def resize_pictures # rubocop:disable Metrics/AbcSize
    return unless pictures.count.positive?

    largest_side = Rails.configuration.garden_party.observations[:largest_pictures_size]
    pictures.each_index do |index|
      next if pictures[index].persisted?

      file      = attachment_changes['pictures'].attachables[index]
      processed = ImageProcessing::MiniMagick.source(file.path)
                                             .resize_to_limit!(largest_side, largest_side)
      FileUtils.mv processed, file.path

      pictures[index].unfurl processed
    end
  end

  def set_made_at
    self.made_at ||= Time.current
  end
end
