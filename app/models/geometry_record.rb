class GeometryRecord < ApplicationRecord
  self.abstract_class = true

  def point?
    return false unless geometry.is_a? Hash

    geometry.dig('geometry', 'type') == 'Point' && !geometry['properties']&.key?('radius')
  end

  def circle?
    return false unless geometry.is_a? Hash

    geometry.dig('geometry', 'type') == 'Point' && geometry['properties']&.key?('radius')
  end

  def polygon?
    return false unless geometry.is_a? Hash

    geometry.dig('geometry', 'type') == 'Polygon'
  end

  def line_string?
    return false unless geometry.is_a? Hash

    geometry.dig('geometry', 'type') == 'LineString'
  end

  def geometry_type
    return 'Point' if point?
    return 'Circle' if circle?

    geometry.dig('geometry', 'type')
  end
end
