require 'garden_party/activity_helper'
require 'garden_party/subject_helper'

class Activity < ApplicationRecord
  validates :action, presence: true

  belongs_to :user
  belongs_to :map
  belongs_to :subject, polymorphic: true, optional: true

  before_validation do |activity|
    map = GardenParty::SubjectHelper.find_map activity
    # Fallback to map owner when no user is specified (system changes)
    activity.user_id ||= Current.user&.id || map.user_id
    # Keep username for cases when an user left the team
    activity.username = user.username
    activity.happened_at ||= Time.current
    activity.map_id = map.id
    activity.subject_name = GardenParty::ActivityHelper.subject_name activity
  end
end
