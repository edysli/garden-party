class Notification < ApplicationRecord
  # Define Pundit policy class for all notification variants so we don't have
  # to create a handful of policies with the same boring content.
  def self.policy_class
    Api::NotificationPolicy
  end

  validate :validate_entity

  belongs_to :subject, polymorphic: true, optional: true
  belongs_to :sender, class_name: 'User', optional: true
  belongs_to :recipient, class_name: 'User'

  after_create :notify_recipient

  scope :archived, -> { where.not(archived_at: nil) }
  scope :unread, -> { where(archived_at: nil) }

  def archive!
    update! archived_at: Time.current
  end

  private

  def notify_recipient
    throw NotImplementedError
  end

  def validate_entity
    throw NotImplementedError
  end
end
