require 'garden_party/activities/patch'

class Patch < GeometryRecord
  include Discard::Model
  has_paper_trail

  validates :geometry, feature: { types: %w[Circle Polygon] }

  belongs_to :map
  belongs_to :layer
  has_many :activities, as: :subject, dependent: :destroy
  has_many :elements, dependent: :destroy, inverse_of: :patch
  has_many :observations, as: :subject, dependent: :destroy
  has_many :tasks, as: :subject, dependent: :destroy

  # Ignore records from discarded layers
  scope :kept, -> { undiscarded.joins(:layer).merge(Layer.kept) }
  # All kept records for a given map
  scope :kept_for_map, ->(map_id) { undiscarded.joins(:layer).merge(Layer.kept_for_map(map_id)) }

  after_create { |patch| GardenParty::Activities::Patch.create_create_activity(patch) }
  after_update { |patch| GardenParty::Activities::Patch.create_update_activity(patch) }
  after_discard { |patch| GardenParty::Activities::Patch.create_destroy_activity(patch) }

  def map_owner
    map.user
  end
end
