require 'garden_party/activities/path'

class Path < GeometryRecord
  include Discard::Model
  has_paper_trail

  validates :geometry, feature: { types: %w[Circle LineString Polygon] }
  validates :stroke_width, presence: true
  validates :stroke_color, presence: true, alpha_color_string: true
  validates :background_color, presence: true, alpha_color_string: true
  validates :stroke_style_preset, presence: false, inclusion: { in: [nil, 'default', 'dotted', 'dashed', 'long_dashes', 'long_dashes2', 'long_dashes3', 'dashed2', 'dashed3'] }

  belongs_to :map
  belongs_to :layer
  has_many :activities, as: :subject, dependent: :destroy
  has_many :observations, as: :subject, dependent: :destroy
  has_many :tasks, as: :subject, dependent: :destroy

  # Ignore records from discarded layers
  scope :kept, -> { undiscarded.joins(:layer).merge(Layer.kept) }
  # All kept records for a given map
  scope :kept_for_map, ->(map_id) { undiscarded.joins(:layer).merge(Layer.kept_for_map(map_id)) }

  after_create { |path| GardenParty::Activities::Path.create_create_activity(path) }
  after_update { |path| GardenParty::Activities::Path.create_update_activity(path) }
  after_discard { |path| GardenParty::Activities::Path.create_destroy_activity(path) }

  def map_owner
    map.user
  end
end
