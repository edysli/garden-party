class TeamMateLeaveNotification < Notification
  class << self
    def create_for!(team_mate)
      create! subject: team_mate.map, sender_id: team_mate.user_id, recipient_id: team_mate.map.user_id
    end
  end

  private

  def notify_recipient
    NotificationMailer.with(notification: self).new_team_mate_leave_notification_email.deliver_later
  end

  def validate_entity
    validate_subject && validate_recipient && validate_sender
  end

  # Map
  def validate_subject
    return true unless subject.blank? || subject_type != 'Map'

    errors.add :subject, I18n.t('activerecord.errors.models.team_mate_leave_notification.subject.bad_subject')
    false
  end

  # Map owner
  def validate_recipient
    return true unless recipient_id.blank? || recipient_id != subject&.user_id

    errors.add :recipient, I18n.t('activerecord.errors.models.team_mate_leave_notification.recipient.bad_recipient')
    false
  end

  # Invitee
  # There is no way to be sure it was invited, so only check for an user.
  #
  # Leave this here to have an unity between all Notification validations instead
  # of using Rails "validates" methods.
  def validate_sender
    return true if sender_id.present?

    errors.add :sender, I18n.t('activerecord.errors.models.team_mate_leave_notification.sender.bad_sender')
    false
  end
end
