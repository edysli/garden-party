require 'garden_party/tasks/element'
require 'garden_party/activities/element'

class Element < GeometryRecord
  include Discard::Model
  has_paper_trail

  enum implantation_mode: { soil_sowing: 0, potted_sowing: 1, potted_shoot: 2, shoot_in_soil: 3 }

  validates :implanted_at, presence: true, if: :removed_at
  validates :implanted_at, presence: true, if: :removal_planned_for
  validates :diameter, numericality: { greater_than: 0.0, allow_nil: true }
  # With this validation set, it's possible to create elements without an
  # associated patch or geometry: this is due to the need to create patches and
  # their elements in one go, leading in validation errors on patch creation when
  # the patch is not yet created but the elements are validated.
  # To assign this issue, it would be great to validate and save elements once
  # the patch is created.
  validates :geometry, feature: { types: %w[Circle Point Polygon], allow_nil: true }
  validates :layer_id, :map_id, presence: true, if: :geometry
  validates :layer_id, :map_id, :geometry, absence: true, if: :patch_id
  validate :valid_removal_plan_date?
  validate :valid_removal_date?

  belongs_to :resource
  belongs_to :patch, optional: true, inverse_of: :elements
  belongs_to :map, optional: true
  belongs_to :layer, optional: true
  has_many :activities, as: :subject, dependent: :destroy
  has_many :observations, as: :subject, dependent: :destroy
  has_many :tasks, as: :subject, dependent: :destroy
  has_many :harvests, dependent: :destroy

  scope :still_implanted, -> { where(removed_at: nil).where.not(implanted_at: nil) }
  scope :removed, -> { where.not(removed_at: nil) }
  scope :planned, -> { where(implanted_at: nil) }
  # Override of the Discard's kept method
  # Selects undiscarded records from undiscarded layers and patches
  scope :kept, lambda {
    elements_on_patches_and_layers = where(patch_id: nil, layer_id: Layer.kept.select(:id)).or(where(patch_id: Patch.kept.select(:id)))
    undiscarded.merge elements_on_patches_and_layers
  }
  # All undiscarded elements, not in a discarded patch or discarded layer, related to a map
  # @param
  scope :kept_for_map, lambda { |map_id|
    elements_on_patches_and_layers = where(patch_id: nil, layer_id: Layer.kept_for_map(map_id).select(:id))
                                     .or(where(patch_id: Patch.kept_for_map(map_id).select(:id)))
    undiscarded.merge elements_on_patches_and_layers
  }

  after_create { |element| GardenParty::Activities::Element.create_create_activity element }
  after_update { |element| GardenParty::Activities::Element.create_update_activity element }
  after_discard { |element| GardenParty::Activities::Element.create_destroy_activity element }
  after_save :update_tasks, unless: :skip_task_hooks?

  def map_owner
    map.user
  end

  def map
    Map.find(map_id || patch.map_id)
  end

  def status
    return :removed if removed_at.present?
    return :implanted if implanted_at.present?

    :planned
  end

  def valid_removal_plan_date?
    return if removal_planned_for.blank? || implanted_at.blank?

    errors.add :removal_planned_for, I18n.t('activerecord.errors.element.date_before_implantation') if removal_planned_for < implanted_at
  end

  def valid_removal_date?
    return if removed_at.blank? || implanted_at.blank?

    errors.add :removed_at, I18n.t('activerecord.errors.element.date_before_implantation') if removed_at < implanted_at
  end

  # Called from a Task when it changes
  #
  # @param task [Task]
  def task_callback(task)
    @skip_task_hooks = true
    update! GardenParty::Tasks::Element.implied_task_changes task
    @skip_task_hooks = false
  end

  def display_name
    resource_name = resource.name

    if point?
      return resource_name if attributes['name'].blank?

      I18n.t('models.element.name.point_element', resource_name: resource_name, element_name: attributes['name'])
    else
      patch_name = patch.name || I18n.t('generic.patch.name')
      I18n.t('models.element.name.element_in_patch', resource_name: resource_name, patch_name: patch_name)
    end
  end

  def duplicate
    new_version = dup
    base_name = new_version.name || new_version.resource.name
    new_version.name = I18n.t('models.element.duplicate_name_suffix', name: base_name)

    new_version
  end

  private

  def skip_task_hooks?
    @skip_task_hooks || discarded?
  end

  def update_tasks
    GardenParty::Tasks::Element.update_tasks self
  end
end
