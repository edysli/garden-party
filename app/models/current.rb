# Thread-safe singleton to store request variables and access it from models.
class Current < ActiveSupport::CurrentAttributes
  attribute :user
end
