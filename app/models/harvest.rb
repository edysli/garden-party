require 'garden_party/activities/harvest'

class Harvest < ApplicationRecord
  include Discard::Model

  enum unit: {
    gram:  0,
    piece: 1,
    litre: 2,
  }

  validates :harvested_at, presence: true
  validates :quantity, presence: true, numericality: { greater_than: 0 }
  validates :unit, presence: true

  belongs_to :element
  belongs_to :user

  # All harvests on a map, concerning a non discarded element
  scope :all_for_map, ->(map_id) { undiscarded.where(element_id: Element.kept_for_map(map_id).select(:id)) }
  scope :for_element, ->(element_id) { where(element_id: element_id) }

  before_validation { |harvest| harvest.user ||= Current.user }
  before_validation :set_harvested_at

  after_create { |harvest| GardenParty::Activities::Harvest.create_create_activity harvest }
  after_update { |harvest| GardenParty::Activities::Harvest.create_update_activity harvest }
  after_discard { |harvest| GardenParty::Activities::Harvest.create_destroy_activity harvest }

  def set_harvested_at
    self.harvested_at ||= Time.current
  end
end
