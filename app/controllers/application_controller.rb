class ApplicationController < ActionController::Base
  class FailedDependencyError < StandardError; end

  include Pundit::Authorization
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_paper_trail_whodunnit
  before_action { Current.user = current_user }

  rescue_from ActiveRecord::RecordNotFound, with: :error_not_found
  rescue_from ActionController::InvalidAuthenticityToken, with: :error_csrf
  rescue_from ActionController::ParameterMissing, with: :error_unprocessable
  rescue_from FailedDependencyError, with: :error_failed_dependency
  rescue_from Pundit::NotAuthorizedError, with: :error_not_authorized

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
    devise_parameter_sanitizer.permit(:account_update, keys: [:username, { preferences: [:map, :theme] }])
    devise_parameter_sanitizer.permit(:accept_invitation, keys: [:username])
  end

  def error_fallback(exception, fallback_message, status)
    message = exception&.message || fallback_message
    respond_to do |format|
      format.json { render json: { error: message }, status: status }
      format.html { raise exception }
    end
  end

  def error_not_found(exception = nil)
    error_fallback(exception, I18n.t('application_controller.errors.resource_not_found'), :not_found)
  end

  def error_unprocessable(exception = nil)
    error_fallback(exception, I18n.t('application_controller.errors.unprocessable_entity'), :unprocessable_entity)
  end

  def error_csrf(exception = nil)
    error_fallback(exception, I18n.t('application_controller.errors.invalid_csrf'), :unprocessable_entity)
  end

  def error_failed_dependency(exception = nil)
    error_fallback(exception, I18n.t('application_controller.errors.failed_dependency'), :failed_dependency)
  end

  def error_not_authorized(_exception = nil)
    message = I18n.t('application_controller.errors.not_authorized')
    respond_to do |format|
      format.json { render json: { error: message }, status: :unauthorized }
      format.html { redirect_to new_user_session_path }
    end
  end
end
