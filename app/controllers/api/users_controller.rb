module Api
  class UsersController < ApiApplicationController
    before_action :set_user, only: [:show, :resend_invitation]

    # GET /api/users/1
    def show
      render 'api/users/show'
    end

    def search
      @users = if params[:username]
                 User.search(params[:username])
               elsif params[:email]
                 user = User.find_by(email: params[:email])
                 user ? [user] : User.none
               else
                 User.none
               end

      render 'api/users/index'
    end

    # POST /api/users/invite
    def invite
      @user = User.invite!(user_params, current_user)
      authorize @user

      if @user.valid?
        render 'api/users/show', status: :created, location: api_user_url(@user)
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end

    # POST /api/users/1/resend_invitation
    def resend_invitation
      @user.invite!(current_user) unless @user.confirmed?

      render 'api/users/show'
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])

      authorize @user
    end

    # Only pick allowed parameters from request
    def user_params
      params.require(:user).permit(:username, :email)
    end
  end
end
