module Api
  class ResourceNotesController < ApiApplicationController
    before_action :set_resource_note, only: [:show, :update, :destroy]

    # GET /api/resource_notes
    def index
      @resource_notes = policy_scope ResourceNote

      render 'api/resource_notes/index'
    end

    # GET /api/resource_notes/1
    def show
      render 'api/resource_notes/show'
    end

    # POST /api/resource_notes
    def create
      authorize ResourceNote
      @resource_note = ResourceNote.new(resource_note_create_params)

      if @resource_note.save
        render 'api/resource_notes/show', status: :created, location: api_resource_note_url(@resource_note)
      else
        render json: @resource_note.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/resource_notes/1
    def update
      if @resource_note.update(resource_note_update_params)
        render 'api/resource_notes/show', status: :ok, location: api_resource_note_url(@resource_note)
      else
        render json: @resource_note.errors, status: :unprocessable_entity
      end
    end

    # DELETE /api/resource_notes/1
    def destroy
      @resource_note.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_resource_note
      @resource_note = ResourceNote.find(params[:id])

      authorize @resource_note
    end

    # Only pick allowed parameters from request
    def resource_note_create_params
      params.require(:resource_note).permit(:resource_id, :content, :visible)
    end

    def resource_note_update_params
      params.require(:resource_note).permit(:content, :visible)
    end
  end
end
