module Api
  class NotificationsController < ApiApplicationController
    before_action :set_notification, only: [:show, :archive, :destroy]

    # GET /api/notifications
    def index
      @notifications = policy_scope(Notification)

      render 'api/notifications/index'
    end

    # GET /api/notifications/1
    def show
      render 'api/notifications/show'
    end

    # PATCH/PUT /api/notifications/1/archive
    def archive
      if @notification.archive!
        render 'api/notifications/show', status: :ok, location: api_notification_url(@notification)
      else
        render json: @notification.errors, status: :unprocessable_entity
      end
    end

    # DELETE /api/notifications/1
    def destroy
      @notification.destroy
    end

    # PATCH /api/notifications/archive_all
    def archive_all
      @notifications = policy_scope(Notification).unread
      @notifications.each(&:archive!)
      render 'api/notifications/index'
    end

    # PATCH /api/notifications/destroy_all_archived
    def destroy_all_archived
      policy_scope(Notification).archived.find_each(&:destroy!)
      @notifications = policy_scope(Notification).all
      render 'api/notifications/index'
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_notification
      @notification = Notification.find(params[:id])

      authorize @notification
    end
  end
end
