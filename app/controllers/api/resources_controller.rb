module Api
  class ResourcesController < ApiApplicationController
    before_action :set_resource, only: [:show, :update, :destroy]

    # GET /resources
    def index
      @resources = policy_scope(Resource).includes(:tags)
    end

    # GET /resources/1
    def show
      render 'api/resources/show'
    end

    # POST /resources
    def create
      authorize Resource
      attributes = Resource.convert_bitfields_arrays(resource_params)
      @resource = Resource.new(attributes)

      if @resource.save
        render 'api/resources/show', status: :created, location: api_resource_url(@resource)
      else
        render json: @resource.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /resources/1
    def update
      new_attributes = Resource.convert_bitfields_arrays(resource_params)
      if @resource.update(new_attributes)
        render 'api/resources/show', location: api_resource_url(@resource)
      else
        render json: @resource.errors, status: :unprocessable_entity
      end
    end

    # DELETE /resources/1
    def destroy
      @resource.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_resource
      @resource = Resource.find(params[:id])

      authorize @resource
    end

    # Only pick allowed parameters from request
    def resource_params
      base_params = :name, :latin_name, :description, :color, :parent_id, :diameter, :genus_id, :resource_interactions_group_id, { tag_list: [], sources: [], common_names: [], sheltered_sowing_months: [], soil_sowing_months: [], harvesting_months: [] }
      base_params.push :sync_id if current_user&.admin?

      params.require(:resource).permit(base_params)
    end
  end
end
