module Api
  class TeamMatesController < ApiApplicationController
    before_action :set_team_mate, only: [:show, :accept, :refuse, :remove, :leave]

    # GET /api/team_mates
    def index
      @team_mates = policy_scope(TeamMate).where map_id: params[:map_id]

      render 'api/team_mates/index'
    end

    # GET /api/team_mates/1
    def show
      render 'api/team_mates/show'
    end

    # POST /api/team_mates
    def create
      @team_mate = TeamMate.new(team_mate_params)
      authorize @team_mate

      if @team_mate.save
        render 'api/team_mates/show', status: :created, location: api_team_mate_url(@team_mate)
      else
        render json: @team_mate.errors, status: :unprocessable_entity
      end
    end

    def accept
      if @team_mate.accept!
        render 'api/team_mates/show', status: :ok, location: api_team_mate_url(@team_mate)
      else
        render json: @team_mate.errors, status: :unprocessable_entity
      end
    end

    def refuse
      @team_mate.refuse!
    end

    def remove
      @team_mate.remove!
    end

    def leave
      @team_mate.leave!
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_team_mate
      @team_mate = TeamMate.find(params[:id])

      authorize @team_mate
    end

    # Only pick allowed parameters from request
    def team_mate_params
      params.require(:team_mate).permit(:user_id, :map_id)
    end
  end
end
