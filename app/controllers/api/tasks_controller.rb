module Api
  class TasksController < ApiApplicationController
    before_action :set_task, only: [:show, :update, :destroy]

    # GET /api/tasks
    def index
      authorize_map

      @tasks = policy_scope(Task)
      filter_by_subject

      @tasks.where(done_at: nil) if params[:pending]

      render 'api/tasks/index'
    end

    def all_for_map
      authorize_map

      @tasks = policy_scope(Task).all_for_map(params[:map_id])

      @tasks = @tasks.pending if params[:pending] == 'true'

      render 'api/tasks/index'
    end

    # GET /api/maps/:map_id/tasks/names
    def names
      names = policy_scope(Task).all_for_map(params[:map_id]).select(:name).distinct(:name).pluck :name
      render json: names
    end

    # GET /api/tasks/1
    def show
      render 'api/tasks/show'
    end

    # POST /api/tasks
    def create
      @task = Task.new(create_task_params)
      authorize @task

      if @task.save
        @render_subject = true
        render 'api/tasks/show', status: :created, location: api_task_url(@task)
      else
        render json: @task.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/tasks/1
    def update
      @task.assign_attributes update_task_params
      authorize @task

      if @task.save
        @render_subject = true
        render 'api/tasks/show', status: :ok, location: api_task_url(@task)
      else
        render json: @task.errors, status: :unprocessable_entity
      end
    end

    # DELETE /api/tasks/1
    def destroy
      @task.destroy

      # Renders an empty task with subject_type, subject_id and the full subject
      render 'api/tasks/show_subject', status: :ok
    end

    private

    def filter_by_subject # rubocop:disable Metrics/AbcSize
      @tasks = if params[:element_id]
                 @tasks.for_element params[:element_id]
               elsif params[:patch_id]
                 @tasks.for_patch params[:patch_id]
               elsif params[:path_id]
                 @tasks.for_path params[:path_id]
               elsif params[:map_id]
                 @tasks.for_map params[:map_id]
               else
                 Task.none
               end
    end

    # Use callbacks to share common setup or constraints between tasks.
    def set_task
      @task = Task.find(params[:id])

      authorize @task
    end

    # Only pick allowed parameters from request
    def create_task_params
      params.require(:task).permit(:name, :notes, :assignee_id, :planned_for, :done_at, :subject_id, :subject_type)
    end

    def update_task_params
      params.require(:task).permit(:notes, :assignee_id, :planned_for, :done_at)
    end

    def authorize_map # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
      map = if params[:element_id]
              Element.find(params[:element_id])&.patch&.map
            elsif params[:map_id]
              Map.find params[:map_id]
            elsif params[:patch_id]
              Patch.find(params[:patch_id])&.map
            elsif params[:path_id]
              Path.find(params[:path_id])&.map
            end

      authorize map, :owned_or_in_team_or_shared?
    end
  end
end
