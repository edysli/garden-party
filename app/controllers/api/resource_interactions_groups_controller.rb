module Api
  class ResourceInteractionsGroupsController < ApiApplicationController
    before_action :set_resource_interactions_group, only: [:show, :update, :destroy]

    # GET /api/resource_interactions_groups
    def index
      @resource_interactions_groups = policy_scope ResourceInteractionsGroup

      render 'api/resource_interactions_groups/index'
    end

    # GET /api/resource_interactions_groups/1
    def show
      render 'api/resource_interactions_groups/show'
    end

    # POST /api/resource_interactions_groups
    def create
      @resource_interactions_group = ResourceInteractionsGroup.new(resource_interactions_group_params)
      authorize @resource_interactions_group

      if @resource_interactions_group.save
        render 'api/resource_interactions_groups/show', status: :created, location: api_resource_interactions_group_url(@resource_interactions_group)
      else
        render json: @resource_interactions_group.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/resource_interactions_groups/1
    def update
      if @resource_interactions_group.update(resource_interactions_group_params)
        render 'api/resource_interactions_groups/show', status: :ok, location: api_resource_interactions_group_url(@resource_interactions_group)
      else
        render json: @resource_interactions_group.errors, status: :unprocessable_entity
      end
    end

    # DELETE /api/resource_interactions_groups/1
    def destroy
      @resource_interactions_group.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_resource_interactions_group
      @resource_interactions_group = ResourceInteractionsGroup.find(params[:id])

      authorize @resource_interactions_group
    end

    # Only pick allowed parameters from request
    def resource_interactions_group_params
      base_params = :name, :description
      base_params.push :sync_id if current_user&.admin?

      params.require(:resource_interactions_group).permit(base_params)
    end
  end
end
