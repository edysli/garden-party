module Api
  class ActivitiesController < ApiApplicationController
    before_action :set_activity, only: [:show]

    # GET /api/activities
    def index
      @activities = policy_scope Activity
      @activities = @activities.where(map_id: params['map_id'])

      render 'api/activities/index'
    end

    # GET /api/activities/1
    def show
      render 'api/activities/show'
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_activity
      @activity = Activity.find(params[:id])

      authorize @activity
    end

    # Only pick allowed parameters from request
    def activity_params
      params.fetch(:activity, {})
    end
  end
end
