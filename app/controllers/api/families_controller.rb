module Api
  class FamiliesController < ApiApplicationController
    before_action :set_family, only: [:show, :update, :destroy]

    # GET /api/families
    def index
      @families = policy_scope Family

      render 'api/families/index'
    end

    # GET /api/families/1
    def show
      render 'api/families/show'
    end

    # POST /api/families
    def create
      @family = Family.new(family_params)
      authorize @family

      if @family.save
        render 'api/families/show', status: :created, location: api_family_url(@family)
      else
        render json: @family.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/families/1
    def update
      if @family.update(family_params)
        render 'api/families/show', status: :ok, location: api_family_url(@family)
      else
        render json: @family.errors, status: :unprocessable_entity
      end
    end

    # DELETE /api/families/1
    def destroy
      @family.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_family
      @family = Family.find(params[:id])

      authorize @family
    end

    # Only pick allowed parameters from request
    def family_params
      base_params = :name, :kingdom, :source
      base_params.push :sync_id if current_user&.admin?

      params.fetch(:family, {}).permit(base_params)
    end
  end
end
