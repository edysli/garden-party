module Api
  class ResourceInteractionsController < ApiApplicationController
    before_action :set_resource_interaction, only: [:show, :update, :destroy]

    # GET /resource_interactions
    def index
      @resource_interactions = policy_scope ResourceInteraction

      render 'api/resource_interactions/index'
    end

    # GET /resource_interactions/1
    def show
      render 'api/resource_interactions/show'
    end

    # POST /resource_interactions
    def create
      authorize ResourceInteraction
      @resource_interaction = ResourceInteraction.new(resource_interaction_params)

      if @resource_interaction.save
        render 'api/resource_interactions/show', status: :created, location: api_resource_interaction_url(@resource_interaction)
      else
        render json: @resource_interaction.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /resource_interactions/1
    def update
      if @resource_interaction.update(resource_interaction_params)
        render 'api/resource_interactions/show', status: :ok, location: api_resource_interaction_url(@resource_interaction)
      else
        render json: @resource_interaction.errors, status: :unprocessable_entity
      end
    end

    # DELETE /resource_interactions/1
    def destroy
      @resource_interaction.destroy
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_resource_interaction
      @resource_interaction = ResourceInteraction.find(params[:id])

      authorize @resource_interaction
    end

    # Only pick allowed parameters from request
    def resource_interaction_params
      base_params = :subject_id, :target_id, :nature, :notes, :sources
      base_params.push :sync_id if current_user&.admin?

      params.require(:resource_interaction).permit(base_params)
    end
  end
end
