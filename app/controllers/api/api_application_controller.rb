module Api
  # FIXME: Find a way to DRY with ApplicationController
  class ApiApplicationController < ActionController::API
    include Pundit::Authorization

    before_action :set_paper_trail_whodunnit
    before_action { Current.user = current_user }

    rescue_from ActiveRecord::RecordNotFound, with: :error_not_found
    rescue_from ActionController::InvalidAuthenticityToken, with: :error_csrf
    rescue_from ActionController::ParameterMissing, with: :error_unprocessable
    rescue_from Pundit::NotAuthorizedError, with: :error_not_authorized

    private

    def error_fallback(exception, fallback_message, status)
      message = exception&.message || fallback_message
      render json: { error: message }, status: status
    end

    def error_not_found(exception = nil)
      error_fallback(exception, I18n.t('application_controller.errors.resource_not_found'), :not_found)
    end

    def error_unprocessable(exception = nil)
      error_fallback(exception, I18n.t('application_controller.errors.unprocessable_entity'), :unprocessable_entity)
    end

    def error_csrf(exception = nil)
      error_fallback(exception, I18n.t('application_controller.errors.invalid_csrf'), :unprocessable_entity)
    end

    def error_not_authorized(_exception = nil)
      message = I18n.t('application_controller.errors.not_authorized')
      render json: { error: message }, status: :unauthorized
    end

    def policy_scope(scope)
      # All notification variants use the same scope, defined in model
      return super(scope) if scope == Notification

      # Other models use an Api::ModelNamePolicy so we prefix it
      super([:api, scope])
    end

    def authorize(record, query = nil)
      # All notification variants use the same scope, defined in model
      return super(record, query) if record.is_a? Notification

      # Other models use an Api::ModelNamePolicy so we prefix it
      super([:api, record], query)
    end

    # Authorises a map for current user, searching it from "something" in params
    def authorize_map # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
      map = if params[:element_id]
              Element.find(params[:element_id])&.map
            elsif params[:map_id]
              Map.find params[:map_id]
            elsif params[:patch_id]
              Patch.find(params[:patch_id])&.map
            elsif params[:path_id]
              Path.find(params[:path_id])&.map
            end

      authorize map, :owned_or_in_team_or_shared?
    end
  end
end
