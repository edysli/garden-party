module Api
  class HarvestsController < ApiApplicationController
    before_action :set_harvest, only: [:show, :update, :destroy]

    # GET /api/harvests
    def index
      authorize_map

      @harvests = policy_scope(Harvest).for_element(params[:element_id])

      render 'api/harvests/index'
    end

    # GET /api/maps/:map_id/all_harvests
    def all_for_map
      authorize_map

      @harvests = policy_scope(Harvest).all_for_map(params[:map_id])

      render 'api/harvests/index'
    end

    # GET /api/harvests/1
    def show
      render 'api/harvests/show'
    end

    # POST /api/harvests
    def create
      @harvest = Harvest.new(create_harvest_params)
      authorize @harvest

      if @harvest.save
        render 'api/harvests/show', status: :created, location: api_harvest_url(@harvest)
      else
        render json: @harvest.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/harvests/1
    def update
      @harvest.assign_attributes update_harvest_params
      authorize @harvest

      if @harvest.save
        render 'api/harvests/show', status: :ok, location: api_harvest_url(@harvest)
      else
        render json: @harvest.errors, status: :unprocessable_entity
      end
    end

    # DELETE /api/harvests/1
    def destroy
      @harvest.discard!
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_harvest
      @harvest = Harvest.find(params[:id])

      authorize @harvest
    end

    # Only pick allowed parameters from request
    def create_harvest_params
      params.require(:harvest).permit :quantity, :unit, :element_id, :harvested_at
    end

    def update_harvest_params
      params.require(:harvest).permit :quantity, :unit, :harvested_at
    end
  end
end
