module Api
  class ObservationsController < ApiApplicationController
    before_action :set_observation, only: [:show, :update, :picture, :picture_thumbnail, :destroy]

    # GET /api/observations
    def index
      authorize_map

      @observations = policy_scope(Observation)
      filter_by_subject

      @observations.where(done_at: nil) if params[:pending]

      render 'api/observations/index'
    end

    # GET /api/maps/:map_id/all_observations
    def all_for_map
      authorize_map

      @observations = policy_scope(Observation).all_for_map(params[:map_id])

      @observations = @observations.pending if params[:pending] == 'true'

      render 'api/observations/index'
    end

    # GET /api/maps/:map_id/observations/names
    def names
      names = policy_scope(Observation).all_for_map(params[:map_id]).select(:name).distinct(:name).pluck :name
      render json: names
    end

    # GET /api/observations/1
    def show
      render 'api/observations/show'
    end

    # POST /api/observations
    def create
      @observation = Observation.new(create_observation_params)
      authorize @observation

      if @observation.save
        render 'api/observations/show', status: :created, location: api_observation_url(@observation)
      else
        render json: @observation.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /api/observations/1
    def update
      @observation.assign_attributes update_observation_params
      authorize @observation

      if @observation.save
        render 'api/observations/show', status: :ok, location: api_observation_url(@observation)
      else
        render json: @observation.errors, status: :unprocessable_entity
      end
    end

    # GET /api/observations/1/pictures/20
    def picture
      blob = @observation.pictures.find(params[:picture_id])

      redirect_to rails_blob_url(blob)
    end

    # GET /api/observations/1/pictures/20/thumbnail
    def picture_thumbnail
      blob = @observation.pictures.find(params[:picture_id])

      redirect_to blob.variant(resize_to_limit: [150, 85]).processed
    end

    # DELETE /api/observations/1
    def destroy
      @observation.discard!
    end

    private

    def filter_by_subject # rubocop:disable Metrics/AbcSize
      @observations = if params[:element_id]
                        @observations.for_element params[:element_id]
                      elsif params[:patch_id]
                        @observations.for_patch params[:patch_id]
                      elsif params[:path_id]
                        @observations.for_path params[:path_id]
                      elsif params[:map_id]
                        @observations.for_map params[:map_id]
                      else
                        Observation.none
                      end
    end

    # Use callbacks to share common setup or constraints between observations.
    def set_observation
      @observation = Observation.find(params[:id])

      authorize @observation
    end

    # Only pick allowed parameters from request
    def create_observation_params
      params.require(:observation).permit(:title, :content, :subject_id, :subject_type, :made_at, pictures: [])
    end

    def update_observation_params
      params.require(:observation).permit(:title, :content, :made_at)
    end
  end
end
