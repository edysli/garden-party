require 'garden_party/bitfield_model_helper'

module Admin
  class ResourcesController < AdminApplicationController
    permitted_params = :name, :latin_name, :common_names, :description, :color, :sources, :diameter, :tag_list, :parent_id, :genus_id, :sync_id, :resource_interactions_group_id
    permitted_params += GardenParty::BitfieldModelHelper.prefix_months :sow_sheltered_in
    permitted_params += GardenParty::BitfieldModelHelper.prefix_months :sow_in_soil_in
    permitted_params += GardenParty::BitfieldModelHelper.prefix_months :harvest_in
    PERMITTED_PARAMS = permitted_params.freeze

    before_action :set_resource, only: [:show, :edit, :update, :destroy]

    # GET /admin/resources
    def index
      @resources = policy_scope(Resource)

      @resources = filter @resources

      @resources = search_order_and_paginate @resources,
                                             allowed_sort_fields: %w[resources.name resources.latin_name],
                                             default_sort_field:  'resources.name',
                                             default_sort_dir:    'asc'
    end

    # GET /admin/resources/1
    def show; end

    # GET /admin/resources/new
    def new
      authorize Resource

      @resource = Resource.new
    end

    # GET /admin/resources/1/edit
    def edit; end

    # POST /admin/resources
    def create
      authorize Resource

      @resource = Resource.new(resource_params)

      if @resource.save
        redirect_to admin_resource_path(@resource), notice: I18n.t('admin.resources.create.success')
      else
        render :new
      end
    end

    # PATCH/PUT /admin/resources/1
    def update
      if @resource.update(resource_params)
        redirect_to admin_resource_path(@resource), notice: I18n.t('admin.resources.update.success')
      else
        render :edit
      end
    end

    # DELETE /admin/resources/1
    def destroy
      @resource.destroy
      redirect_to admin_resources_url, notice: I18n.t('admin.resources.destroy.success')
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_resource
      @resource = Resource.find(params[:id])

      authorize @resource
    end

    # Only pick allowed parameters from request
    def resource_params
      params.require(:resource).permit(PERMITTED_PARAMS)
    end

    def filter(query)
      case params[:elements]
      when 'without'
        query = query.where.missing(:elements)
      end

      query
    end
  end
end
