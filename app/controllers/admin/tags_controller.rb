require 'garden_party/tags'

module Admin
  class TagsController < AdminApplicationController
    before_action :set_tag, only: [:show, :edit, :update, :destroy]

    # GET /admin/tags
    def index
      @tags = policy_scope(::ActsAsTaggableOn::Tag)

      @tags = @tags.where(taggings_count: 0) if params[:unused] == 'true'

      @tags = search_order_and_paginate @tags,
                                        search_fields:       %w[name],
                                        allowed_sort_fields: %w[name taggings_count],
                                        default_sort_field:  'name',
                                        default_sort_dir:    'asc'
    end

    # GET /admin/tags/1
    def show; end

    def batch_process # rubocop:disable Metrics/AbcSize
      authorize ::ActsAsTaggableOn::Tag
      parameters = params.permit(:batch_action, tag_ids: [])

      case parameters[:batch_action]
      when 'merge'
        tag = GardenParty::Tags.merge!(parameters[:tag_ids])

        redirect_to admin_tag_path(tag), notice: I18n.t('admin.tags.merge.success')
      when 'destroy'
        ::ActsAsTaggableOn::Tag.where(id: parameters[:tag_ids]).destroy_all

        redirect_to admin_tags_path, notice: I18n.t('admin.tags.destroy.batch_success')
      else
        redirect_to admin_tags_path, alert: I18n.t('admin.tags.unknown_batch_action')
      end
    end

    # GET /admin/tags/1/edit
    def edit; end

    # POST /admin/tags
    def create
      authorize ::ActsAsTaggableOn::Tag

      @tag = ::ActsAsTaggableOn::Tag.new(tag_params)

      if @tag.save
        redirect_to admin_tag_path(@tag), notice: I18n.t('admin.tags.create.success')
      else
        render :new
      end
    end

    # PATCH/PUT /admin/tags/1
    def update
      if @tag.update(tag_params)
        redirect_to admin_tag_path(@tag), notice: I18n.t('admin.tags.update.success')
      else
        render :edit
      end
    end

    # DELETE /admin/tags/1
    def destroy
      @tag.destroy
      redirect_to admin_tags_url, notice: I18n.t('admin.tags.destroy.success')
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tag
      @tag = ::ActsAsTaggableOn::Tag.find(params[:id])

      authorize @tag
    end

    # Only pick allowed parameters from request
    def tag_params
      params.require(:acts_as_taggable_on_tag).permit(:name)
    end
  end
end
