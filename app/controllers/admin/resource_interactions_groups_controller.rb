module Admin
  class ResourceInteractionsGroupsController < AdminApplicationController
    before_action :set_resource_interactions_group, only: [:show, :edit, :update, :destroy]

    # GET /admin/resource_interactions_groups
    def index
      @resource_interactions_groups = policy_scope ResourceInteractionsGroup.all
    end

    # GET /admin/resource_interactions_groups/1
    def show; end

    # GET /admin/resource_interactions_groups/new
    def new
      authorize ResourceInteractionsGroup

      @resource_interactions_group = ResourceInteractionsGroup.new
    end

    # GET /admin/resource_interactions_groups/1/edit
    def edit; end

    # POST /admin/resource_interactions_groups
    def create
      authorize ResourceInteractionsGroup

      @resource_interactions_group = ResourceInteractionsGroup.new(resource_interactions_group_params)

      if @resource_interactions_group.save
        redirect_to admin_resource_interactions_group_path(@resource_interactions_group), notice: I18n.t('admin.resource_interactions_groups.create.success')
      else
        render :new
      end
    end

    # PATCH/PUT /admin/resource_interactions_groups/1
    def update
      if @resource_interactions_group.update(resource_interactions_group_params)
        redirect_to admin_resource_interactions_group_path(@resource_interactions_group), notice: I18n.t('admin.resource_interactions_groups.update.success')
      else
        render :edit
      end
    end

    # DELETE /admin/resource_interactions_groups/1
    def destroy
      @resource_interactions_group.destroy
      redirect_to admin_resource_interactions_groups_url, notice: I18n.t('admin.resource_interactions_groups.destroy.success')
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_resource_interactions_group
      @resource_interactions_group = ResourceInteractionsGroup.find(params[:id])

      authorize @resource_interactions_group
    end

    # Only pick allowed parameters from request
    def resource_interactions_group_params
      params.require(:resource_interactions_group).permit(:name, :description, :sync_id)
    end
  end
end
